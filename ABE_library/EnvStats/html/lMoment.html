<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate L-Moments</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for lMoment {EnvStats}"><tr><td>lMoment {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate <i>L</i>-Moments
</h2>

<h3>Description</h3>

<p>Estimate the <i>r</i>'th <i>L</i>-moment from a random sample.
</p>


<h3>Usage</h3>

<pre>
  lMoment(x, r = 1, method = "unbiased", 
    plot.pos.cons = c(a = 0.35, b = 0), na.rm = FALSE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations. 
</p>
</td></tr>
<tr valign="top"><td><code>r</code></td>
<td>

<p>positive integer specifying the order of the moment.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying what method to use to compute the 
<i>L</i>-moment.  The possible values are <code>"unbiased"</code> 
(method based on the U-statistic; the default), or <code>"plotting.position"</code> 
(method based on the plotting position formula).  See the DETAILS section for 
more information.
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.cons</code></td>
<td>

<p>numeric vector of length 2 specifying the constants used in the formula for the 
plotting positions when <code>method="plotting.position"</code>.  The default value is 
<code>plot.pos.cons=c(a=0.35, b=0)</code>.  If this vector has a names attribute with 
the value <code>c("a","b")</code> or <code>c("b","a")</code>, then the elements will be 
matched by name in the formula for computing the plotting positions.  Otherwise, 
the first element is mapped to the name <code>"a"</code> and the second element to the 
name <code>"b"</code>.  See the DETAILS section for more information.  This argument is 
ignored if <code>method="ubiased"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>na.rm</code></td>
<td>

<p>logical scalar indicating whether to remove missing values from <code>x</code>.  
If <code>na.rm=FALSE</code> (the default) and <code>x</code> contains missing values, 
then a missing value (<code>NA</code>) is returned.  If <code>na.rm=TRUE</code>, missing 
values are removed from <code>x</code> prior to computing the <i>L</i>-moment.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p><b>Definitions: <i>L</i>-Moments and <i>L</i>-Moment Ratios</b> <br />
The definition of an <i>L</i>-moment given by Hosking (1990) is as follows.  
Let <i>X</i> denote a random variable with cdf <i>F</i>, and let <i>x(p)</i> 
denote the <i>p</i>'th quantile of the distribution.  Furthermore, let
</p>
<p style="text-align: center;"><i>x_{1:n} &le; x_{2:n} &le; &hellip; &le; x_{n:n}</i></p>

<p>denote the order statistics of a random sample of size <i>n</i> drawn from the 
distribution of <i>X</i>.  Then the <i>r</i>'th <i>L</i>-moment is given by:
</p>
<p style="text-align: center;"><i>&lambda;_r = \frac{1}{r} &sum;^{r-1}_{k=0} (-1)^k {r-1 \choose k} E[X_{r-k:r}]</i></p>

<p>for <i>r = 1, 2, &hellip;</i>.
</p>
<p>Hosking (1990) shows that the above equation can be rewritten as:
</p>
<p style="text-align: center;"><i>&lambda;_r = \int^1_0 x(u) P^*_{r-1}(u) du</i></p>

<p>where
</p>
<p style="text-align: center;"><i>P^*_r(u) = &sum;^r_{k=0} p^*_{r,k} u^k</i></p>

<p style="text-align: center;"><i>p^*_{r,k} = (-1)^{r-k} {r \choose k} {r+k \choose k} = \frac{(-1)^{r-k} (r+k)!}{(k!)^2 (r-k)!}</i></p>

<p>The first four <i>L</i>-moments are given by: 
</p>
<p style="text-align: center;"><i>&lambda;_1 = E[X]</i></p>

<p style="text-align: center;"><i>&lambda;_2 = \frac{1}{2} E[X_{2:2} - X_{1:2}]</i></p>

<p style="text-align: center;"><i>&lambda;_3 = \frac{1}{3} E[X_{3:3} - 2X_{2:3} + X_{1:3}]</i></p>

<p style="text-align: center;"><i>&lambda;_4 = \frac{1}{4} E[X_{4:4} - 3X_{3:4} + 3X_{2:4} - X_{1:4}]</i></p>

<p>Thus, the first <i>L</i>-moment is a measure of location, and the second 
<i>L</i>-moment is a measure of scale. 
</p>
<p>Hosking (1990) defines the <i>L</i>-moment ratios of <i>X</i> to be:
</p>
<p style="text-align: center;"><i>&tau;_r = \frac{&lambda;_r}{&lambda;_2}</i></p>

<p>for <i>r = 2, 3, &hellip;</i>.  He shows that for a non-degenerate random variable 
with a finite mean, these quantities lie in the interval <i>(-1, 1)</i>.  
The quantity
</p>
<p style="text-align: center;"><i>&tau;_3 = \frac{&lambda;_3}{&lambda;_2}</i></p>

<p>is the <i>L</i>-moment analog of the coefficient of skewness, and the quantity
</p>
<p style="text-align: center;"><i>&tau;_4 = \frac{&lambda;_4}{&lambda;_2}</i></p>

<p>is the <i>L</i>-moment analog of the coefficient of kurtosis.  Hosking (1990) also 
defines an <i>L</i>-moment analog of the coefficient of variation (denoted the 
<i>L</i>-CV) as:
</p>
<p style="text-align: center;"><i>&lambda; = \frac{&lambda;_2}{&lambda;_1}</i></p>

<p>He shows that for a positive-valued random variable, the <i>L</i>-CV lies 
in the interval <i>(0, 1)</i>.
</p>
<p><b>Relationship Between <i>L</i>-Moments and Probability-Weighted Moments</b> <br />
Hosking (1990) and Hosking and Wallis (1995) show that <i>L</i>-moments can be 
written as linear combinations of probability-weighted moments:
</p>
<p style="text-align: center;"><i>&lambda;_r = (-1)^{r-1} &sum;^{r-1}_{k=0} p^*_{r-1,k} &alpha;_k = &sum;^{r-1}_{j=0} p^*_{r-1,j} &beta;_j</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>&alpha;_k = M(1, 0, k) = \frac{1}{k+1} E[X_{1:k+1}]</i></p>

<p style="text-align: center;"><i>&beta;_j = M(1, j, 0) = \frac{1}{j+1} E[X_{j+1:j+1}]</i></p>

<p>See the help file for <code><a href="../../EnvStats/help/pwMoment.html">pwMoment</a></code> for more information on 
probability-weighted moments.
</p>
<p><b>Estimating L-Moments</b> <br />
The two commonly used methods for estimating <i>L</i>-moments are the 
&ldquo;unbiased&rdquo; method based on U-statistics (Hoeffding, 1948; 
Lehmann, 1975, pp. 362-371), and the &ldquo;plotting-position&rdquo; method.  
Hosking and Wallis (1995) recommend using the unbiased method for almost all 
applications.
</p>
<p><em>Unbiased Estimators</em> (<code>method="unbiased"</code>) <br />
Using the relationship between <i>L</i>-moments and probability-weighted moments 
explained above, the unbiased estimator of the <i>r</i>'th <i>L</i>-moment is based on 
unbiased estimators of probability-weighted moments and is given by:
</p>
<p style="text-align: center;"><i>l_r = (-1)^{r-1} &sum;^{r-1}_{k=0} p^*_{r-1,k} a_k = &sum;^{r-1}_{j=0} p^*_{r-1,j} b_j</i></p>

<p>where
</p>
<p style="text-align: center;"><i>a_k = \frac{1}{n} &sum;^{n-k}_{i=1} x_{i:n} \frac{{n-i \choose k}}{{n-1 \choose k}}</i></p>

<p style="text-align: center;"><i>b_j = \frac{1}{n} &sum;^{n}_{i=j+1} x_{i:n} \frac{{i-1 \choose j}}{{n-1 \choose j}}</i></p>

<p><em>Plotting-Position Estimators</em> (<code>method="plotting.position"</code>) <br />
Using the relationship between <i>L</i>-moments and probability-weighted moments 
explained above, the plotting-position estimator of the <i>r</i>'th <i>L</i>-moment 
is based on the plotting-position estimators of probability-weighted moments and 
is given by:
</p>
<p style="text-align: center;"><i>\tilde{&lambda;}_r = (-1)^{r-1} &sum;^{r-1}_{k=0} p^*_{r-1,k} \tilde{&alpha;}_k = &sum;^{r-1}_{j=0} p^*_{r-1,j} \tilde{&beta;}_j</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\tilde{&alpha;}_k = \frac{1}{n} &sum;^n_{i=1} (1 - p_{i:n})^k x_{i:n}</i></p>

<p style="text-align: center;"><i>\tilde{&beta;}_j =  \frac{1}{n} &sum;^{n}_{i=1} p^j_{i:n} x_{i:n}</i></p>

<p>and
</p>
<p style="text-align: center;"><i>p_{i:n} = \hat{F}(x_{i:n})</i></p>

<p>denotes the plotting position of the <i>i</i>'th order statistic in the random 
sample of size <i>n</i>, that is, a distribution-free estimate of the cdf of 
<i>X</i> evaluated at the <i>i</i>'th order statistic.  Typically, plotting 
positions have the form:
</p>
<p style="text-align: center;"><i>p_{i:n} = \frac{i-a}{n+b}</i></p>

<p>where <i>b &gt; -a &gt; -1</i>.  For this form of plotting position, the 
plotting-position estimators are asymptotically equivalent to their 
unbiased estimator counterparts. 
</p>
<p><b>Estimating <i>L</i>-Moment Ratios</b> <br />
<i>L</i>-moment ratios are estimated by simply replacing the population 
<i>L</i>-moments with the estimated <i>L</i>-moments.  The estimated ratios 
based on the unbiased estimators are given by:
</p>
<p style="text-align: center;"><i>t_r = \frac{l_r}{l_2}</i></p>

<p>and the estimated ratios based on the plotting-position estimators are given by:
</p>
<p style="text-align: center;"><i>\tilde{&tau;}_r = \frac{\tilde{&lambda;}_r}{\tilde{&lambda;}_2}</i></p>

<p>In particular, the <i>L</i>-moment skew is estimated by:
</p>
<p style="text-align: center;"><i>t_3 = \frac{l_3}{l_2}</i></p>

<p>or 
</p>
<p style="text-align: center;"><i>\tilde{&tau;}_3 = \frac{\tilde{&lambda;}_3}{\tilde{&lambda;}_2}</i></p>

<p>and the <i>L</i>-moment kurtosis is estimated by:
</p>
<p style="text-align: center;"><i>t_4 = \frac{l_4}{l_2}</i></p>

<p>or 
</p>
<p style="text-align: center;"><i>\tilde{&tau;}_4 = \frac{\tilde{&lambda;}_4}{\tilde{&lambda;}_2}</i></p>

<p>Similarly, the <i>L</i>-moment coefficient of variation can be estimated using 
the unbiased <i>L</i>-moment estimators:
</p>
<p style="text-align: center;"><i>l = \frac{l_2}{l_1}</i></p>

<p>or using the plotting-position L-moment estimators:
</p>
<p style="text-align: center;"><i>\tilde{&lambda;} = \frac{\tilde{&lambda;}_2}{\tilde{&lambda;}_1}</i></p>



<h3>Value</h3>

<p>A numeric scalar&ndash;the value of the <i>r</i>'th <i>L</i>-moment as defined by Hosking (1990).
</p>


<h3>Note</h3>

<p>Hosking (1990) introduced the idea of <i>L</i>-moments, which are expectations 
of certain linear combinations of order statistics, as the basis of a general 
theory of describing theoretical probability distributions, computing summary 
statistics from observed data, estimating distribution parameters and quantiles, 
and performing hypothesis tests.  The theory of <i>L</i>-moments parallels the 
theory of conventional moments.  <i>L</i>-moments have several advantages over 
conventional moments, including:
</p>

<ul>
<li> <p><i>L</i>-moments can characterize a wider range of distributions because 
they always exist as long as the distribution has a finite mean.
</p>
</li>
<li> <p><i>L</i>-moments are estimated by linear combinations of order statistics, 
so estimators based on <i>L</i>-moments are more robust to the presence of 
outliers than estimators based on conventional moments.
</p>
</li>
<li><p> Based on the author's and others' experience, <i>L</i>-moment estimators 
are less biased and approximate their asymptotic distribution more closely in 
finite samples than estimators based on conventional moments.
</p>
</li>
<li> <p><i>L</i>-moment estimators are sometimes more efficient (smaller RMSE) than 
even maximum likelihood estimators for small samples.
</p>
</li></ul>

<p>Hosking (1990) presents a table with formulas for the <i>L</i>-moments of common 
probability distributions.  Articles that illustrate the use of <i>L</i>-moments 
include Fill and Stedinger (1995), Hosking and Wallis (1995), and 
Vogel and Fennessey (1993).
</p>
<p>Hosking (1990) and Hosking and Wallis (1995) show the relationship between 
probabiity-weighted moments and <i>L</i>-moments.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Fill, H.D., and J.R. Stedinger. (1995).  <i>L</i> Moment and Probability Plot 
Correlation Coefficient Goodness-of-Fit Tests for the Gumbel Distribution and 
Impact of Autocorrelation.  <em>Water Resources Research</em> <b>31</b>(1), 225&ndash;229.
</p>
<p>Hosking, J.R.M. (1990).  L-Moments: Analysis and Estimation of Distributions 
Using Linear Combinations of Order Statistics.  <em>Journal of the Royal 
Statistical Society, Series B</em> <b>52</b>(1), 105&ndash;124.
</p>
<p>Hosking, J.R.M., and J.R. Wallis (1995).  A Comparison of Unbiased and 
Plotting-Position Estimators of <i>L</i> Moments.  <em>Water Resources Research</em> 
<b>31</b>(8), 2019&ndash;2025.
</p>
<p>Vogel, R.M., and N.M. Fennessey. (1993).  <i>L</i> Moment Diagrams Should 
Replace Product Moment Diagrams.  <em>Water Resources Research</em> <b>29</b>(6), 
1745&ndash;1752.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/cv.html">cv</a></code>, <code><a href="../../EnvStats/help/skewness.html">skewness</a></code>, <code><a href="../../EnvStats/help/kurtosis.html">kurtosis</a></code>, 
<code><a href="../../EnvStats/help/pwMoment.html">pwMoment</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a generalized extreme value distribution 
  # with parameters location=10, scale=2, and shape=.25, then compute the 
  # first four L-moments. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rgevd(20, location = 10, scale = 2, shape = 0.25) 

  lMoment(dat) 
  #[1] 10.59556 

  lMoment(dat, 2) 
  #[1] 1.0014 

  lMoment(dat, 3) 
  #[1] 0.1681165
 
  lMoment(dat, 4) 
  #[1] 0.08732692

  #----------

  # Now compute some L-moments based on the plotting-position estimators:

  lMoment(dat, method = "plotting.position") 
  #[1] 10.59556

  lMoment(dat, 2, method = "plotting.position") 
  #[1] 1.110264 

  lMoment(dat, 3, method="plotting.position", plot.pos.cons = c(.325,1)) 
  #[1] -0.4430792
 
  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
