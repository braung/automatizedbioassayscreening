<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: The Generalized Extreme Value Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for GEVD {EnvStats}"><tr><td>GEVD {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
The Generalized Extreme Value Distribution
</h2>

<h3>Description</h3>

<p>Density, distribution function, quantile function, and random generation 
for the generalized extreme value distribution.
</p>


<h3>Usage</h3>

<pre>
  dgevd(x, location = 0, scale = 1, shape = 0)
  pgevd(q, location = 0, scale = 1, shape = 0)
  qgevd(p, location = 0, scale = 1, shape = 0)
  rgevd(n, location = 0, scale = 1, shape = 0)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>q</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>vector of probabilities between 0 and 1.
</p>
</td></tr>
<tr valign="top"><td><code>n</code></td>
<td>

<p>sample size.  If <code>length(n)</code> is larger than 1, then <code>length(n)</code> 
random values are returned.
</p>
</td></tr>
<tr valign="top"><td><code>location</code></td>
<td>

<p>vector of location parameters.
</p>
</td></tr>
<tr valign="top"><td><code>scale</code></td>
<td>

<p>vector of positive scale parameters.
</p>
</td></tr>
<tr valign="top"><td><code>shape</code></td>
<td>

<p>vector of shape parameters.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>X</i> be a generalized extreme value random variable with parameters 
<code>location=</code><i>&eta;</i>, <code>scale=</code><i>&theta;</i>, and <code>shape=</code><i>&kappa;</i>.  
When the shape parameter <i>&kappa; = 0</i>, the generalized extreme value distribution 
reduces to the <a href="../../EnvStats/help/EVD.html">extreme value distribution</a>.  When the shape parameter 
<i>&kappa; \ne 0</i>, the cumulative distribution function of <i>X</i> is given by:
</p>
<p style="text-align: center;"><i>F(x; &eta;, &theta;, &kappa;) = exp\{-[1 - &kappa;(x-&eta;)/&theta;]^{1/&kappa;}\}</i></p>

<p>where <i>-&infin; &lt; &eta;, &kappa; &lt; &infin;</i> and <i>&theta; &gt; 0</i>.  
When <i>&kappa; &gt; 0</i>, the range of <i>x</i> is:
</p>
<p style="text-align: center;"><i>-&infin; &lt; x &le; &eta; + &theta;/&kappa;</i></p>

<p>and when <i>&kappa; &lt; 0</i> the range of <i>x</i> is:
</p>
<p style="text-align: center;"><i>&eta; + &theta;/&kappa; &le; x &lt; &infin;</i></p>

<p>The <i>p^th</i> quantile of <i>X</i> is given by:
</p>
<p style="text-align: center;"><i>x_{p} = &eta; + \frac{&theta; \{1 - [-log(p)]^{&kappa;}\}}{&kappa;}</i></p>



<h3>Value</h3>

<p>density (<code>devd</code>), probability (<code>pevd</code>), quantile (<code>qevd</code>), or 
random sample (<code>revd</code>) for the generalized extreme value distribution with 
location parameter(s) determined by <code>location</code>, scale parameter(s) 
determined by <code>scale</code>, and shape parameter(s) determined by <code>shape</code>.
</p>


<h3>Note</h3>

<p>Two-parameter <a href="../../EnvStats/help/EVD.html">extreme value distributions (EVD)</a> have been applied 
extensively since the 1930's to several fields of study, including the distributions 
of hydrological and meteorological variables, human lifetimes, and strength of 
materials.  The three-parameter generalized extreme value distribution (GEVD) was 
introduced by Jenkinson (1955) to model annual maximum and minimum values of 
meteorological events.  Since then, it has been used extensively in the hydological 
and meteorological fields.
</p>
<p>The three families of EVDs are all special kinds of GEVDs.  When the shape 
parameter <i>&kappa; = 0</i>, the GEVD reduces to the 
<a href="../../EnvStats/help/EVD.html">Type I extreme value (Gumbel) distribution</a>.  (The function 
<code><a href="../../EnvStats/help/zTestGevdShape.html">zTestGevdShape</a></code> allows you to test the null hypothesis that the shape 
parameter is equal to 0.)  When <i>&kappa; &gt; 0</i>, the GEVD is the same as the Type II 
extreme value distribution, and when <i>&kappa; &lt; 0</i> it is the same as the 
Type III extreme value distribution.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Jenkinson, A.F. (1955).  The Frequency Distribution of the Annual Maximum 
(or Minimum) of Meteorological Events.  <em>Quarterly Journal of the Royal 
Meteorological Society</em>, <b>81</b>, 158&ndash;171.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/egevd.html">egevd</a></code>, <code><a href="../../EnvStats/help/zTestGevdShape.html">zTestGevdShape</a></code>, <code><a href="../../EnvStats/help/EVD.html">EVD</a></code>, 
<a href="../../EnvStats/help/Probability+20Distributions+20and+20Random+20Numbers.html">Probability Distributions and Random Numbers</a>.
</p>


<h3>Examples</h3>

<pre>
  # Density of a generalized extreme value distribution with 
  # location=0, scale=1, and shape=0, evaluated at 0.5: 

  dgevd(.5) 
  #[1] 0.3307043

  #----------

  # The cdf of a generalized extreme value distribution with 
  # location=1, scale=2, and shape=0.25, evaluated at 0.5: 

  pgevd(.5, 1, 2, 0.25) 
  #[1] 0.2795905

  #----------

  # The 90'th percentile of a generalized extreme value distribution with 
  # location=-2, scale=0.5, and shape=-0.25: 

  qgevd(.9, -2, 0.5, -0.25) 
  #[1] -0.4895683

  #----------

  # Random sample of 4 observations from a generalized extreme value 
  # distribution with location=5, scale=2, and shape=1. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(20) 
  rgevd(4, 5, 2, 1) 
  #[1] 6.738692 6.473457 4.446649 5.727085
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
