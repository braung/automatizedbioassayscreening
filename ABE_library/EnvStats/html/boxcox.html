<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Boxcox Power Transformation</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for boxcox {EnvStats}"><tr><td>boxcox {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Boxcox Power Transformation
</h2>

<h3>Description</h3>

<p><code>boxcox</code> is a generic function used to compute the value(s) of an objective 
for one or more Box-Cox power transformations, or to compute an optimal power 
transformation based on a specified objective.  The function invokes particular 
<code><a href="../../utils/html/methods.html">methods</a></code> which depend on the <code><a href="../../base/html/class.html">class</a></code> of the first 
argument. 
</p>
<p>Currently, there is a default method and a method for objects of class <code>"lm"</code>.
</p>


<h3>Usage</h3>

<pre>
boxcox(x, ...)

## Default S3 method:
boxcox(x, 
    lambda = {if (optimize) c(-2, 2) else seq(-2, 2, by = 0.5)}, 
    optimize = FALSE, objective.name = "PPCC", 
    eps = .Machine$double.eps, include.x = TRUE, ...)

## S3 method for class 'lm'
boxcox(x, 
    lambda = {if (optimize) c(-2, 2) else seq(-2, 2, by = 0.5)}, 
    optimize = FALSE, objective.name = "PPCC", 
    eps = .Machine$double.eps, include.x = TRUE, ...) 
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>an object of class <code>"lm"</code> for which the response variable is all positive 
numbers, or else a numeric vector of positive numbers.  When <code>x</code> is an 
object of class <code>"lm"</code>, the object must have been created with a 
call to the function <code><a href="../../stats/html/lm.html">lm</a></code> that includes the <code>data</code> argument.  
When <code>x</code> is a numeric vector of positive observations, missing (<code>NA</code>), 
undefined (<code>NaN</code>), and infinite (<code>-Inf, Inf</code>) values are allowed but 
will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>lambda</code></td>
<td>

<p>numeric vector of finite values indicating what powers to use for the 
Box-Cox transformation.  When <code>optimize=FALSE</code>, the default value is <br />
<code>lambda=seq(-2, 2, by=0.5)</code>.  When <code>optimize=TRUE</code>, <code>lambda</code> 
must be a vector with two values indicating the range over which the 
optimization will occur and the range of these two values must include 1.  
In this case, the default value is <code>lambda=c(-2, 2)</code>.
</p>
</td></tr>
<tr valign="top"><td><code>optimize</code></td>
<td>

<p>logical scalar indicating whether to simply evalute the objective function at the 
given values of <code>lambda</code> (<code>optimize=FALSE</code>; the default), or to compute 
the optimal power transformation within the bounds specified by 
<code>lambda</code> (<code>optimize=TRUE</code>).
</p>
</td></tr>
<tr valign="top"><td><code>objective.name</code></td>
<td>

<p>character string indicating what objective to use. The possible values are
<code>"PPCC"</code> (probability plot correlation coefficient; the default), 
<code>"Shapiro-Wilk"</code> (the Shapiro-Wilk goodness-of-fit statistic), and 
<code>"Log-Likelihood"</code> (the log-likelihood function). 
</p>
</td></tr>
<tr valign="top"><td><code>eps</code></td>
<td>

<p>finite, positive numeric scalar.  When the absolute value of <code>lambda</code> is less 
than <code>eps</code>, lambda is assumed to be 0 for the Box-Cox transformation.  
The default value is <code>eps=.Machine$double.eps</code>.
</p>
</td></tr>
<tr valign="top"><td><code>include.x</code></td>
<td>

<p>logical scalar indicating whether to include the finite, non-missing values of 
the argument <code>x</code> with the returned object. The default value is 
<code>include.x=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>...</code></td>
<td>

<p>optional arguments for possible future methods.  Currently not used.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Two common assumptions for several standard parametric hypothesis tests are:
</p>

<ol>
<li><p> The observations all come from a normal distribution.
</p>
</li>
<li><p> The observations all come from distributions with the same variance.
</p>
</li></ol>

<p>For example, the standard one-sample t-test assumes all the observations 
come from the same normal distribution, and the standard two-sample t-test 
assumes that all the observations come from a normal distribution with the same 
variance, although the mean may differ between the two groups.  
</p>
<p>When the original data do not satisfy the above assumptions, data transformations 
are often used to attempt to satisfy these assumptions.  The rest of this section 
is divided into two parts:  one that discusses Box-Cox transformations in the 
context of the original observations, and one that  discusses Box-Cox 
transformations in the context of linear models.
<br />
</p>
<p><b>Box-Cox Transformations Based on the Original Observations</b> <br />
Box and Cox (1964) presented a formalized method for deciding on a data 
transformation.  Given a random variable <i>X</i> from some distribution with 
only positive values, the Box-Cox family of power transformations is defined as:
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
  <i>Y</i> </td><td style="text-align: left;"> = </td><td style="text-align: left;"> <i>\frac{X^&lambda; - 1}{&lambda;}</i> </td><td style="text-align: left;"> <i>&lambda; \ne 0</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
          </td><td style="text-align: left;">   </td><td style="text-align: left;">                                     </td><td style="text-align: left;">                     </td>
</tr>
<tr>
 <td style="text-align: left;">
          </td><td style="text-align: left;">   </td><td style="text-align: left;"> <i>log(X)</i>                        </td><td style="text-align: left;"> <i>&lambda; = 0 \;\;\;\;\;\; (1)</i>
  </td>
</tr>

</table>

<p>where <i>Y</i> is assumed to come from a normal distribution.  This transformation is 
continuous in <i>&lambda;</i>.  Note that this transformation also preserves ordering.  
See the help file for <code><a href="../../EnvStats/help/boxcoxTransform.html">boxcoxTransform</a></code> for more information on data 
transformations.
</p>
<p>Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote a random sample of 
<i>n</i> observations from some distribution and assume that there exists some 
value of <i>&lambda;</i> such that the transformed observations
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
  <i>y_i</i> </td><td style="text-align: left;"> = </td><td style="text-align: left;"> <i>\frac{x_i^&lambda; - 1}{&lambda;}</i> </td><td style="text-align: left;"> <i>&lambda; \ne 0</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
            </td><td style="text-align: left;">   </td><td style="text-align: left;">                                       </td><td style="text-align: left;">                     </td>
</tr>
<tr>
 <td style="text-align: left;">
            </td><td style="text-align: left;">   </td><td style="text-align: left;"> <i>log(x_i)</i>                        </td><td style="text-align: left;"> <i>&lambda; = 0 \;\;\;\;\;\; (2)</i>
  </td>
</tr>

</table>

<p>(<i>i = 1, 2, &hellip;, n</i>) form a random sample from a normal distribution.
</p>
<p>Box and Cox (1964) proposed choosing the appropriate value of <i>&lambda;</i> based on 
maximizing the likelihood function.  Alternatively, an appropriate value of 
<i>&lambda;</i> can be chosen based on another objective, such as maximizing the 
probability plot correlation coefficient or the Shapiro-Wilk goodness-of-fit 
statistic.
</p>
<p>In the case when <code>optimize=TRUE</code>, the function <code>boxcox</code> calls the 
<span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/html/nlminb.html">nlminb</a></code> to minimize the negative value of the 
objective (i.e., maximize the objective) over the range of possible values of 
<i>&lambda;</i> specified in the argument <code>lambda</code>.  The starting value for 
the optimization is always <i>&lambda;=1</i> (i.e., no transformation).
</p>
<p>The rest of this sub-section explains how the objective is computed for the 
various options for <code>objective.name</code>. 
<br />
</p>
<p><em>Objective Based on Probability Plot Correlation Coefficient</em> (<code>objective.name="PPCC"</code>) <br />
When <code>objective.name="PPCC"</code>, the objective is computed as the value of the 
normal probability plot correlation coefficient based on the transformed data 
(see the description of the Probability Plot Correlation Coefficient (PPCC) 
goodness-of-fit test in the help file for <code><a href="../../EnvStats/help/gofTest.html">gofTest</a></code>).  That is, 
the objective is the correlation coefficient for the normal 
<a href="../../EnvStats/help/qqPlot.html">quantile-quantile plot</a> for the transformed data.  
Large values of the PPCC tend to indicate a good fit to a normal distribution.
<br />
</p>
<p><em>Objective Based on Shapiro-Wilk Goodness-of-Fit Statistic</em> (<code>objective.name="Shapiro-Wilk"</code>) <br />
When <code>objective.name="Shapiro-Wilk"</code>, the objective is computed as the value of 
the Shapiro-Wilk goodness-of-fit statistic based on the transformed data 
(see the description of the Shapiro-Wilk test in the help file for 
<code><a href="../../EnvStats/help/gofTest.html">gofTest</a></code>).  Large values of the Shapiro-Wilk statistic tend to 
indicate a good fit to a normal distribution.
<br />
</p>
<p><em>Objective Based on Log-Likelihood Function</em> (<code>objective.name="Log-Likelihood"</code>) <br />
When <code>objective.name="Log-Likelihood"</code>, the objective is computed as the value 
of the log-likelihood function.  Assuming the transformed observations in 
Equation (2) above come from a normal distribution with mean <i>&mu;</i> and 
standard deviation <i>&sigma;</i>, we can use the change of variable formula to 
write the log-likelihood function as:
</p>
<p style="text-align: center;"><i>log[L(&lambda;, &mu;, &sigma;)] = \frac{-n}{2}log(2&pi;) - \frac{n}{2}log(&sigma;^2) - \frac{1}{2&sigma;^2} &sum;_{i=1}^n (y_i - &mu;)^2 + (&lambda; - 1) &sum;_{i=1}^n log(x_i) \;\;\;\;\;\; (3)</i></p>

<p>where <i>y_i</i> is defined in Equation (2) above (Box and Cox, 1964). 
For a fixed value of <i>&lambda;</i>, the log-likelihood function 
is maximized by replacing <i>&mu;</i> and <i>&sigma;</i> with their maximum likelihood 
estimators:
</p>
<p style="text-align: center;"><i>\hat{&mu;} = \frac{1}{n} &sum;_{i=1}^n y_i \;\;\;\;\;\; (4)</i></p>

<p style="text-align: center;"><i>\hat{&sigma;} = [\frac{1}{n} &sum;_{i=1}^n (y_i - \bar{y})^2]^{1/2} \;\;\;\;\;\; (5)</i></p>

<p>Thus, when <code>optimize=TRUE</code>, Equation (3) is maximized by iteratively solving for 
<i>&lambda;</i> using the values for <i>&mu;</i> and <i>&sigma;</i> given in 
Equations (4) and (5).  When <code>optimize=FALSE</code>, the value of the objective is 
computed by using Equation (3), using the values of <i>&lambda;</i> specified in the 
argument <code>lambda</code>, and using the values for <i>&mu;</i> and <i>&sigma;</i> given 
in Equations (4) and (5).
<br />
</p>
<p><b>Box-Cox Transformation for Linear Models</b> <br />
In the case of a standard linear regression model with <i>n</i> observations and 
<i>p</i> predictors:
</p>
<p style="text-align: center;"><i>Y_i = &beta;_0 + &beta;_1 X_{i1} + &hellip; + &beta;_p X_{ip} + &epsilon;_i, \; i=1,2,&hellip;,n \;\;\;\;\;\; (6)</i></p>

<p>the standard assumptions are:
</p>

<ol>
<li><p> The error terms <i>&epsilon;_i</i> come from a normal distribution with mean 0.
</p>
</li>
<li><p> The variance is the same for all of the error terms and does not depend on 
the predictor variables.
</p>
</li></ol>

<p>Assuming <i>Y</i> is a random variable from some distribution that may depend on 
the predictor variables and <i>Y</i> takes on only positive values, the Box-Cox 
family of power transformations is defined as:
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
  <i>Y^*</i> </td><td style="text-align: left;"> = </td><td style="text-align: left;"> <i>\frac{Y^&lambda; - 1}{&lambda;}</i> </td><td style="text-align: left;"> <i>&lambda; \ne 0</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
            </td><td style="text-align: left;">   </td><td style="text-align: left;">                                     </td><td style="text-align: left;">                     </td>
</tr>
<tr>
 <td style="text-align: left;">
            </td><td style="text-align: left;">   </td><td style="text-align: left;"> <i>log(Y)</i>                        </td><td style="text-align: left;"> <i>&lambda; = 0 \;\;\;\;\;\; (7)</i>
  </td>
</tr>

</table>

<p>where <i>Y^*</i> becomes the new response variable and the errors are now 
assumed to come from a normal distribution with a mean of 0 and a constant variance.
</p>
<p>In this case, the objective is computed as described above, but it is based on the 
residuals from the fitted linear model in which the response variable is now 
<i>Y^*</i> instead of <i>Y</i>.
</p>


<h3>Value</h3>

<p>When <code>x</code> is an object of class <code>"lm"</code>, <code>boxcox</code> returns 
a list of class <code>"boxcoxLm"</code> containing the results.  See 
the help file for <code><a href="../../EnvStats/help/boxcoxLm.object.html">boxcoxLm.object</a></code> for details.
</p>
<p>When <code>x</code> is simply a numeric vector of positive numbers, 
<code>boxcox</code> returns a list of class <code>"boxcox"</code> containing the 
results.  See the help file for <code><a href="../../EnvStats/help/boxcox.object.html">boxcox.object</a></code> for details.
</p>


<h3>Note</h3>

<p>Data transformations are often used to induce normality, homoscedasticity, 
and/or linearity, common assumptions of parametric statistical tests and 
estimation procedures.  Transformations are not &ldquo;tricks&rdquo; used by the 
data analyst to hide what is going on, but rather useful tools for 
understanding and dealing with data (Berthouex and Brown, 2002, p.61).  
Hoaglin (1988) discusses &ldquo;hidden&rdquo; transformations that are used everyday, 
such as the pH scale for measuring acidity.  Johnson and Wichern (2007, p.192) 
note that &quot;Transformations are nothing more than a reexpression of the data 
in different units.&quot;
</p>
<p>In the case of a linear model, there are at least two approaches to improving 
a model fit:  transform the <i>Y</i> and/or <i>X</i> variable(s), and/or use 
more predictor variables.  Often in environmental data analysis, we assume the 
observations come from a lognormal distribution and automatically take 
logarithms of the data.  For a simple linear regression 
(i.e., one predictor variable), if regression diagnostic plots indicate that a 
straight line fit is not adequate, but that the variance of the errors 
appears to be fairly constant, you may only need to transform the predictor 
variable <i>X</i> or perhaps use a quadratic or cubic model in <i>X</i>.  
On the other hand, if the diagnostic plots indicate that the constant 
variance and/or normality assumptions are suspect, you probably need to consider 
transforming the response variable <i>Y</i>.  Data transformations for 
linear regression models are discussed in Draper and Smith (1998, Chapter 13) 
and Helsel and Hirsch (1992, pp. 228-229).
</p>
<p>One problem with data transformations is that translating results on the 
transformed scale back to the original scale is not always straightforward.  
Estimating quantities such as means, variances, and confidence limits in the 
transformed scale and then transforming them back to the original scale 
usually leads to biased and inconsistent estimates (Gilbert, 1987, p.149; 
van Belle et al., 2004, p.400).  For example, exponentiating the confidence 
limits for a mean based on log-transformed data does not yield a 
confidence interval for the mean on the original scale.  Instead, this yields 
a confidence interval for the median (see the help file for <code><a href="../../EnvStats/help/elnormAlt.html">elnormAlt</a></code>).  
It should be noted, however, that quantiles (percentiles) and rank-based 
procedures are invariant to monotonic transformations 
(Helsel and Hirsch, 1992, p.12).
</p>
<p>Finally, there is no guarantee that a Box-Cox tranformation based on the 
&ldquo;optimal&rdquo; value of <i>&lambda;</i> will provide an adequate transformation 
to allow the assumption of approximate normality and constant variance.  Any 
set of transformed data should be inspected relative to the assumptions you 
want to make about it (Johnson and Wichern, 2007, p.194).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). 
<em>Statistics for Environmental Engineers, Second Edition</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>Box, G.E.P., and D.R. Cox. (1964).  An Analysis of Transformations 
(with Discussion).  <em>Journal of the Royal Statistical Society, Series B</em> 
<b>26</b>(2), 211&ndash;252.
</p>
<p>Draper, N., and H. Smith. (1998). <em>Applied Regression Analysis</em>. Third Edition. 
John Wiley and Sons, New York, pp.47-53.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution 
Monitoring</em>. Van Nostrand Reinhold, NY.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992).  
<em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY.
</p>
<p>Hinkley, D.V., and G. Runger. (1984).  The Analysis of Transformed Data 
(with Discussion).  <em>Journal of the American Statistical Association</em> 
<b>79</b>, 302&ndash;320.
</p>
<p>Hoaglin, D.C., F.M. Mosteller, and J.W. Tukey, eds. (1983).  
<em>Understanding Robust and Exploratory Data Analysis</em>.  
John Wiley and Sons, New York, Chapter 4.
</p>
<p>Hoaglin, D.C. (1988).  Transformations in Everyday Experience. 
<em>Chance</em> <b>1</b>, 40&ndash;45.
</p>
<p>Johnson, N. L., S. Kotz, and A.W. Kemp. (1992).  <em>Univariate 
Discrete Distributions, Second Edition</em>.  John Wiley and Sons, New York, 
p.163.
</p>
<p>Johnson, R.A., and D.W. Wichern. (2007).  
<em>Applied Multivariate Statistical Analysis, Sixth Edition</em>.  
Pearson Prentice Hall, Upper Saddle River, NJ, pp.192&ndash;195.
</p>
<p>Shumway, R.H., A.S. Azari, and P. Johnson. (1989).  
Estimating Mean Concentrations Under Transformations for Environmental 
Data With Detection Limits.  <em>Technometrics</em> <b>31</b>(3), 347&ndash;356.
</p>
<p>Stoline, M.R. (1991).  An Examination of the Lognormal and Box and Cox 
Family of Transformations in Fitting Environmental Data.  
<em>Environmetrics</em> <b>2</b>(1), 85&ndash;106.
</p>
<p>van Belle, G., L.D. Fisher, Heagerty, P.J., and Lumley, T. (2004). 
<em>Biostatistics: A Methodology for the Health Sciences, 2nd Edition</em>. 
John Wiley &amp; Sons, New York.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. 
Fifth Edition. Prentice-Hall, Upper Saddle River, NJ, 
Chapter 13.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/boxcox.object.html">boxcox.object</a></code>, <code><a href="../../EnvStats/help/plot.boxcox.html">plot.boxcox</a></code>, <code><a href="../../EnvStats/help/print.boxcox.html">print.boxcox</a></code>, 
<code><a href="../../EnvStats/help/boxcoxLm.object.html">boxcoxLm.object</a></code>, <code><a href="../../EnvStats/help/plot.boxcoxLm.html">plot.boxcoxLm</a></code>, <code><a href="../../EnvStats/help/print.boxcoxLm.html">print.boxcoxLm</a></code>, 
<code><a href="../../EnvStats/help/boxcoxTransform.html">boxcoxTransform</a></code>, <a href="../../EnvStats/help/Data+20Transformations.html">Data Transformations</a>, 
<a href="../../EnvStats/help/Goodness-of-Fit+20Tests.html">Goodness-of-Fit Tests</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 30 observations from a lognormal distribution with 
  # mean=10 and cv=2.  Look at some values of various objectives 
  # for various transformations.  Note that for both the PPCC and 
  # the Log-Likelihood objective, the optimal value of lambda is 
  # about 0, indicating that a log transformation is appropriate.  
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  x &lt;- rlnormAlt(30, mean = 10, cv = 2) 

  dev.new()
  hist(x, col = "cyan")

  # Using the PPCC objective:
  #--------------------------

  boxcox(x) 
  #Results of Box-Cox Transformation
  #---------------------------------
  #
  #Objective Name:                  PPCC
  #
  #Data:                            x
  #
  #Sample Size:                     30
  #
  # lambda      PPCC
  #   -2.0 0.5423739
  #   -1.5 0.6402782
  #   -1.0 0.7818160
  #   -0.5 0.9272219
  #    0.0 0.9921702
  #    0.5 0.9581178
  #    1.0 0.8749611
  #    1.5 0.7827009
  #    2.0 0.7004547

  boxcox(x, optimize = TRUE)
  #Results of Box-Cox Transformation
  #---------------------------------
  #
  #Objective Name:                  PPCC
  #
  #Data:                            x
  #
  #Sample Size:                     30
  #
  #Bounds for Optimization:         lower = -2
  #                                 upper =  2
  #
  #Optimal Value:                   lambda = 0.04530789
  #
  #Value of Objective:              PPCC = 0.9925919


  # Using the Log-Likelihodd objective
  #-----------------------------------

  boxcox(x, objective.name = "Log-Likelihood") 
  #Results of Box-Cox Transformation
  #---------------------------------
  #
  #Objective Name:                  Log-Likelihood
  #
  #Data:                            x
  #
  #Sample Size:                     30
  #
  # lambda Log-Likelihood
  #   -2.0     -154.94255
  #   -1.5     -128.59988
  #   -1.0     -106.23882
  #   -0.5      -90.84800
  #    0.0      -85.10204
  #    0.5      -88.69825
  #    1.0      -99.42630
  #    1.5     -115.23701
  #    2.0     -134.54125

  boxcox(x, objective.name = "Log-Likelihood", optimize = TRUE) 
  #Results of Box-Cox Transformation
  #---------------------------------
  #
  #Objective Name:                  Log-Likelihood
  #
  #Data:                            x
  #
  #Sample Size:                     30
  #
  #Bounds for Optimization:         lower = -2
  #                                 upper =  2
  #
  #Optimal Value:                   lambda = 0.0405156
  #
  #Value of Objective:              Log-Likelihood = -85.07123

  #----------

  # Plot the results based on the PPCC objective
  #---------------------------------------------
  boxcox.list &lt;- boxcox(x)
  dev.new()
  plot(boxcox.list)

  #Look at QQ-Plots for the candidate values of lambda
  #---------------------------------------------------
  plot(boxcox.list, plot.type = "Q-Q Plots", same.window = FALSE) 

  #==========

  # The data frame Environmental.df contains daily measurements of 
  # ozone concentration, wind speed, temperature, and solar radiation
  # in New York City for 153 consecutive days between May 1 and 
  # September 30, 1973.  In this example, we'll plot ozone vs. 
  # temperature and look at the Q-Q plot of the residuals.  Then 
  # we'll look at possible Box-Cox transformations.  The "optimal" one 
  # based on the PPCC looks close to a log-transformation 
  # (i.e., lambda=0).  The power that produces the largest PPCC is 
  # about 0.2, so a cube root (lambda=1/3) transformation might work too.

  head(Environmental.df)
  #           ozone radiation temperature wind
  #05/01/1973    41       190          67  7.4
  #05/02/1973    36       118          72  8.0
  #05/03/1973    12       149          74 12.6
  #05/04/1973    18       313          62 11.5
  #05/05/1973    NA        NA          56 14.3
  #05/06/1973    28        NA          66 14.9

  tail(Environmental.df)
  #           ozone radiation temperature wind
  #09/25/1973    14        20          63 16.6
  #09/26/1973    30       193          70  6.9
  #09/27/1973    NA       145          77 13.2
  #09/28/1973    14       191          75 14.3
  #09/29/1973    18       131          76  8.0
  #09/30/1973    20       223          68 11.5


  # Fit the model with the raw Ozone data
  #--------------------------------------
  ozone.fit &lt;- lm(ozone ~ temperature, data = Environmental.df) 

  # Plot Ozone vs. Temperature, with fitted line 
  #---------------------------------------------
  dev.new()
  with(Environmental.df, 
    plot(temperature, ozone, xlab = "Temperature (degrees F)",
      ylab = "Ozone (ppb)", main = "Ozone vs. Temperature"))
  abline(ozone.fit) 

  # Look at the Q-Q Plot for the residuals 
  #---------------------------------------
  dev.new()
  qqPlot(ozone.fit$residuals, add.line = TRUE) 

  # Look at Box-Cox transformations of Ozone 
  #-----------------------------------------
  boxcox.list &lt;- boxcox(ozone.fit) 
  boxcox.list 
  #Results of Box-Cox Transformation
  #---------------------------------
  #
  #Objective Name:                  PPCC
  #
  #Linear Model:                    ozone.fit
  #
  #Sample Size:                     116
  #
  # lambda      PPCC
  #   -2.0 0.4286781
  #   -1.5 0.4673544
  #   -1.0 0.5896132
  #   -0.5 0.8301458
  #    0.0 0.9871519
  #    0.5 0.9819825
  #    1.0 0.9408694
  #    1.5 0.8840770
  #    2.0 0.8213675

  # Plot PPCC vs. lambda based on Q-Q plots of residuals 
  #-----------------------------------------------------
  dev.new()
  plot(boxcox.list) 

  # Look at Q-Q plots of residuals for the various transformation 
  #--------------------------------------------------------------
  plot(boxcox.list, plot.type = "Q-Q Plots", same.window = FALSE)

  # Compute the "optimal" transformation
  #-------------------------------------
  boxcox(ozone.fit, optimize = TRUE)
  #Results of Box-Cox Transformation
  #---------------------------------
  #
  #Objective Name:                  PPCC
  #
  #Linear Model:                    ozone.fit
  #
  #Sample Size:                     116
  #
  #Bounds for Optimization:         lower = -2
  #                                 upper =  2
  #
  #Optimal Value:                   lambda = 0.2004305
  #
  #Value of Objective:              PPCC = 0.9940222

  #==========

  # Clean up
  #---------
  rm(x, boxcox.list, ozone.fit)
  graphics.off()
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
