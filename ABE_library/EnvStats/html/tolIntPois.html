<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Tolerance Interval for a Poisson Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tolIntPois {EnvStats}"><tr><td>tolIntPois {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Tolerance Interval for a Poisson Distribution
</h2>

<h3>Description</h3>

<p>Construct a <i>&beta;</i>-content or <i>&beta;</i>-expectation tolerance 
interval for a <a href="../../stats/help/Poisson.html">Poisson distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  tolIntPois(x, coverage = 0.95, cov.type = "content", ti.type = "two-sided", 
    conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations, or an object resulting from a call to an 
estimating function that assumes a Poisson distribution 
(i.e., <code><a href="../../EnvStats/help/epois.html">epois</a></code> or <code><a href="../../EnvStats/help/epoisCensored.html">epoisCensored</a></code>).  
If <code>cov.type="content"</code> then <code>x</code> must be a numeric vector.  
If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>a scalar between 0 and 1 indicating the desired coverage of the tolerance interval.  
The default value is <code>coverage=0.95</code>.  If <code>cov.type="expectation"</code>, 
this argument is ignored.
</p>
</td></tr>
<tr valign="top"><td><code>cov.type</code></td>
<td>

<p>character string specifying the coverage type for the tolerance interval.  
The possible values are <code>"content"</code> (<i>&beta;</i>-content; the default), and 
<code>"expectation"</code> (<i>&beta;</i>-expectation).  See the DETAILS section for more 
information.
</p>
</td></tr>
<tr valign="top"><td><code>ti.type</code></td>
<td>

<p>character string indicating what kind of tolerance interval to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level associated with the tolerance 
interval.  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>A tolerance interval for some population is an interval on the real line constructed so as to 
contain <i>100 &beta; \%</i> of the population (i.e., <i>100 &beta; \%</i> of all 
future observations), where <i>0 &lt; &beta; &lt; 1</i>.  The quantity <i>100 &beta; \%</i> is called 
the <em>coverage</em>.
</p>
<p>There are two kinds of tolerance intervals (Guttman, 1970):
</p>
 
<ul>
<li><p> A <i>&beta;</i>-content tolerance interval with confidence level <i>100(1-&alpha;)\%</i> is 
constructed so that it contains at least <i>100 &beta; \%</i> of the population (i.e., the 
coverage is at least <i>100 &beta; \%</i>) with probability <i>100(1-&alpha;)\%</i>, where 
<i>0 &lt; &alpha; &lt; 1</i>. The quantity <i>100(1-&alpha;)\%</i> is called the confidence level or 
confidence coefficient associated with the tolerance interval.
</p>
</li>
<li><p> A <i>&beta;</i>-expectation tolerance interval is constructed so that the <em>average</em> coverage of 
the interval is <i>100 &beta; \%</i>.
</p>
</li></ul>
 
<p><b>Note:</b> A <i>&beta;</i>-expectation tolerance interval with coverage <i>100 &beta; \%</i> is 
equivalent to a prediction interval for one future observation with associated confidence level 
<i>100 &beta; \%</i>.  Note that there is no explicit confidence level associated with a 
<i>&beta;</i>-expectation tolerance interval.  If a <i>&beta;</i>-expectation tolerance interval is 
treated as a <i>&beta;</i>-content tolerance interval, the confidence level associated with this 
tolerance interval is usually around 50% (e.g., Guttman, 1970, Table 4.2, p.76).  
</p>
<p>Because of the discrete nature of the <a href="../../stats/help/Poisson.html">Poisson distribution</a>, 
even true tolerance intervals (tolerance intervals based on the true value of 
<i>&lambda;</i>) will usually not contain exactly <i>&beta;\%</i> of the population.  
For example, for the Poisson distribution with parameter <code>lambda=2</code>, the 
interval [0, 4] contains 94.7% of this distribution and the interval [0, 5] 
contains 98.3% of this distribution.  Thus, no interval can contain exactly 95% 
of this distribution.
</p>
<p><em><i>&beta;</i>-Content Tolerance Intervals for a Poisson Distribution</em> <br />
Zacks (1970) showed that for monotone likelihood ratio (MLR) families of discrete 
distributions, a uniformly most accurate upper <i>&beta;100\%</i> <i>&beta;</i>-content 
tolerance interval with associated confidence level <i>(1-&alpha;)100\%</i> is 
constructed by finding the upper <i>(1-&alpha;)100\%</i> confidence limit for the 
parameter associated with the distribution, and then computing the <i>&beta;</i>'th 
quantile of the distribution assuming the true value of the parameter is equal to 
the upper confidence limit.  This idea can be extended to one-sided lower and 
two-sided tolerance limits.
</p>
<p>It can be shown that all distributions that are one parameter exponential families 
have the MLR property, and the Poisson distribution is a one-parameter exponential 
family, so the method of Zacks (1970) can be applied to a Poisson distribution.
</p>
<p>Let <i>X</i> denote a <a href="../../stats/help/Poisson.html">Poisson random variable</a> with parameter 
<code>lambda=</code><i>&lambda;</i>.  Let <i>x_{p|&lambda;}</i> denote the <i>p</i>'th quantile 
of this distribution. That is,
</p>
<p style="text-align: center;"><i>Pr(X &lt; x_{p|&lambda;}) &le; p &le; Pr(X &le; x_{p|&lambda;}) \;\;\;\;\;\; (1)</i></p>

<p>Note that due to the discrete nature of the Poisson distribution, there will be 
several values of <i>p</i> associated with one value of <i>X</i>.  For example, for 
<i>&lambda;=2</i>, the value 1 is the <i>p</i>'th quantile for any value of <i>p</i> 
between 0.140 and 0.406.
</p>
<p>Let <i>\underline{x}</i> denote a vector of <i>n</i> observations from a 
<a href="../../stats/help/Poisson.html">Poisson distribution</a> with parameter <code>lambda=</code><i>&lambda;</i>.  
When <code>ti.type="upper"</code>, the first step is to compute the one-sided upper 
<i>(1-&alpha;)100\%</i> confidence limit for <i>&lambda;</i> based on the observations 
<i>\underline{x}</i> (see the help file for <code><a href="../../EnvStats/help/epois.html">epois</a></code>).  Denote this upper 
confidence limit by <i>UCL</i>.  The one-sided upper <i>&beta;100\%</i> tolerance limit 
is then given by:
</p>
<p style="text-align: center;"><i>[0, x_{&beta; | &lambda; = UCL}] \;\;\;\;\;\; (2)</i></p>

<p>Similarly, when <code>ti.type="lower"</code>, the first step is to compute the one-sided 
lower <i>(1-&alpha;)100\%</i> confidence limit for <i>&lambda;</i> based on the 
observations <i>\underline{x}</i>.  Denote this lower confidence limit by <i>LCL</i>.  
The one-sided lower <i>&beta;100\%</i> tolerance limit is then given by:
</p>
<p style="text-align: center;"><i>[x_{1-&beta; | &lambda; = LCL}, &infin;] \;\;\;\;\;\; (3)</i></p>

<p>Finally, when <code>ti.type="two-sided"</code>, the first step is to compute the two-sided 
<i>(1-&alpha;)100\%</i> confidence limits for <i>&lambda;</i> based on the 
observations <i>\underline{x}</i>.  Denote these confidence limits by <i>LCL</i> and 
<i>UCL</i>. The two-sided <i>&beta;100\%</i> tolerance limit is then given by:
</p>
<p style="text-align: center;"><i>[x_{\frac{1-&beta;}{2} | &lambda; = LCL}, x_{\frac{1+&beta;}{2} | &lambda; = UCL}] \;\;\;\;\;\; (4)</i></p>

<p>Note that the function <code>tolIntPois</code> uses the exact confidence limits for 
<i>&lambda;</i> when computing <i>&beta;</i>-content tolerance limits (see 
<code><a href="../../EnvStats/help/epois.html">epois</a></code>).
<br />
</p>
<p><em><i>&beta;</i>-Expectation Tolerance Intervals for a Poisson Distribution</em> <br />
As stated above, a <i>&beta;</i>-expectation tolerance interval with coverage 
<i>&beta;100\%</i> is equivalent to a prediction interval for one future observation 
with associated confidence level <i>&beta;100\%</i>.  This is because the probability 
that any single future observation will fall into this interval is <i>&beta;100\%</i>, 
so the distribution of the number of <i>N</i> future observations that will fall into 
this interval is <a href="../../stats/help/Binomial.html">binomial</a> with parameters 
<code>size=</code><i>N</i> and <code>prob=</code><i>&beta;</i>.  Hence the expected proportion of 
future observations that fall into this interval is <i>&beta;100\%</i> and is 
independent of the value of <i>N</i>.  See the help file for <code><a href="../../EnvStats/help/predIntPois.html">predIntPois</a></code> 
for information on how these intervals are constructed.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>tolIntPois</code> returns a list of class 
<code>"estimate"</code> containing the estimated parameters, a component called 
<code>interval</code> containing the tolerance interval information, and other 
information.  See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>tolIntPois</code> 
returns a list whose class is the same as <code>x</code>.  The list contains the same 
components as <code>x</code>.  If <code>x</code> already has a component called 
<code>interval</code>, this component is replaced with the tolerance interval 
information.
</p>


<h3>Note</h3>

<p>Tolerance intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Meeker, 1991; Krishnamoorthy and 
Mathew, 2009).  References that discuss tolerance intervals in the context of 
environmental monitoring include:  Berthouex and Brown (2002, Chapter 21), 
Gibbons et al. (2009), Millard and Neerchal (2001, Chapter 6), Singh et al. (2010b), 
and USEPA (2009).
</p>
<p>Gibbons (1987b) used the Poisson distribution to model the number of detected 
compounds per scan of the 32 volatile organic priority pollutants (VOC), and 
also to model the distribution of chemical concentration (in ppb).  He explained 
the derivation of a one-sided upper <i>&beta;</i>-content tolerance limit for a 
Poisson distribution based on the work of Zacks (1970) using the Pearson-Hartley 
approximation to the confidence limits for the mean parameter <i>&lambda;</i> 
(see the help file for <code><a href="../../EnvStats/help/epois.html">epois</a></code>).  Note that there are several 
typographical errors in the derivation and examples on page 575 of Gibbons (1987b) 
because there is confusion between where the value of <i>&beta;</i> (the coverage) 
should be and where the value of <i>1-&alpha;</i> (the confidence level) should be.  
Gibbons et al. (2009, pp.103-104) gives correct formulas.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Gibbons, R.D. (1987b).  Statistical Models for the Analysis of Volatile Organic 
Compounds in Waste Disposal Sites.  <em>Ground Water</em> <b>25</b>, 572&ndash;580.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Guttman, I. (1970). <em>Statistical Tolerance Regions: Classical and Bayesian</em>. 
Hafner Publishing Co., Darien, CT.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991). <em>Statistical Intervals: A Guide for Practitioners</em>. 
John Wiley and Sons, New York.
</p>
<p>Johnson, N. L., S. Kotz, and A. Kemp. (1992).  <em>Univariate Discrete 
Distributions</em>.  Second Edition.  John Wiley and Sons, New York, Chapter 4.
</p>
<p>Krishnamoorthy K., and T. Mathew. (2009). 
<em>Statistical Tolerance Regions: Theory, Applications, and Computation</em>. 
John Wiley and Sons, Hoboken.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton.
</p>
<p>Zacks, S. (1970).  Uniformly Most Accurate Upper Tolerance Limits for Monotone 
Likelihood Ratio Families of Discrete Distributions.  <em>Journal of the 
American Statistical Association</em> <b>65</b>, 307&ndash;316.
</p>


<h3>See Also</h3>

<p><code><a href="../../stats/html/Poisson.html">Poisson</a></code>, <code><a href="../../EnvStats/help/epois.html">epois</a></code>, <code><a href="../../EnvStats/help/eqpois.html">eqpois</a></code>, 
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, <a href="../../EnvStats/help/Tolerance+20Intervals.html">Tolerance Intervals</a>, 
<a href="../../EnvStats/help/Estimating+20Distribution+20Parameters.html">Estimating Distribution Parameters</a>, <a href="../../EnvStats/help/Estimating+20Distribution+20Quantiles.html">Estimating Distribution Quantiles</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a Poisson distribution with parameter 
  # lambda=2. The interval [0, 4] contains 94.7% of this distribution and 
  # the interval [0,5] contains 98.3% of this distribution.  Thus, because 
  # of the discrete nature of the Poisson distribution, no interval contains 
  # exactly 95% of this distribution.  Use tolIntPois to estimate the mean 
  # parameter of the true distribution, and construct a one-sided upper 95% 
  # beta-content tolerance interval with associated confidence level 90%. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rpois(20, 2) 
  tolIntPois(dat, conf.level = 0.9)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Poisson
  #
  #Estimated Parameter(s):          lambda = 1.8
  #
  #Estimation Method:               mle/mme/mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Tolerance Interval Coverage:     95%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Zacks
  #
  #Tolerance Interval Type:         two-sided
  #
  #Confidence Level:                90%
  #
  #Tolerance Interval:              LTL = 0
  #                                 UTL = 6

  #------

  # Clean up
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
