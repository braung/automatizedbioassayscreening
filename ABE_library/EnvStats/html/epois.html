<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameter of a Poisson Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for epois {EnvStats}"><tr><td>epois {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameter of a Poisson Distribution
</h2>

<h3>Description</h3>

<p>Estimate the mean of a <a href="../../stats/help/Poisson.html">Poisson distribution</a>, and 
optionally construct a confidence interval for the mean.
</p>


<h3>Usage</h3>

<pre>
  epois(x, method = "mle/mme/mvue", ci = FALSE, ci.type = "two-sided", 
    ci.method = "exact", conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Currently the only possible 
value is <code>"mle/mme/mvue"</code> (maximum likelihood/method of moments/minimum variance unbiased; 
the default).  See the DETAILS section for more information. 
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the 
location or scale parameter.  The default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval 
for the location or scale parameter.  Possible values are <code>"exact"</code> 
(the default), <code>"pearson.hartley.approx"</code> (Pearson-Hartley approximation), and 
<code>"normal.approx"</code> (normal approximation).  See the DETAILS section for more 
information.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>. This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from a <a href="../../stats/help/Poisson.html">Poisson distribution</a> with 
parameter <code>lambda=</code><i>&lambda;</i>.  It can be shown (e.g., Forbes et al., 2009) 
that if <i>y</i> is defined as:
</p>
<p style="text-align: center;"><i>y = &sum;_{i=1}^n x_i \;\;\;\; (1)</i></p>

<p>then <i>y</i> is an observation from a Poisson distribution with parameter 
<code>lambda=</code><i>n &lambda;</i>.
</p>
<p><b>Estimation</b> <br />
The maximum likelihood, method of moments, and minimum variance unbiased estimator 
(mle/mme/mvue) of <i>&lambda;</i> is given by:
</p>
<p style="text-align: center;"><i>\hat{&lambda;} = \bar{x} \;\;\;\; (2)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i = \frac{y}{n} \;\;\;\; (3)</i></p>

<p><br />
</p>
<p><b>Confidence Intervals</b> <br />
There are three possible ways to construct a confidence interval for 
<i>&lambda;</i>:  based on the exact distribution of the estimator of 
<i>&lambda;</i> (<code>ci.type="exact"</code>), based on an approximation of 
Pearson and Hartley (<code>ci.type="pearson.hartley.approx"</code>), or based on the 
normal approximation <br />
(<code>ci.type="normal.approx"</code>).
</p>
<p><em>Exact Confidence Interval</em> (<code>ci.method="exact"</code>) <br />
If <code>ci.type="two-sided"</code>, an exact <i>(1-&alpha;)100\%</i> confidence interval 
for <i>&lambda;</i> can be constructed as <i>[LCL, UCL]</i>, where the confidence 
limits are computed such that:
</p>
<p style="text-align: center;"><i>Pr[Y &ge; y \| &lambda; = LCL] = \frac{&alpha;}{2} \;\;\;\; (4)</i></p>

<p style="text-align: center;"><i>Pr[Y &le; y \| &lambda; = UCL] = \frac{&alpha;}{2} \;\;\;\; (5)</i></p>

<p>where <i>y</i> is defined in equation (1) and <i>Y</i> denotes a Poisson random 
variable with parameter <code>lambda=</code><i>n &lambda;</i>.
</p>
<p>If <code>ci.type="lower"</code>, <i>&alpha;/2</i> is replaced with <i>&alpha;</i> in 
equation (4) and <i>UCL</i> is set to <i>&infin;</i>.
</p>
<p>If <code>ci.type="upper"</code>, <i>&alpha;/2</i> is replaced with <i>&alpha;</i> in 
equation (5) and <i>LCL</i> is set to 0.
</p>
<p>Note that an exact upper confidence bound can be computed even when all 
observations are 0.
<br />
</p>
<p><em>Pearson-Hartley Approximation</em> (<code>ci.method="pearson.hartley.approx"</code>) <br />
For a two-sided <i>(1-&alpha;)100\%</i> confidence interval for <i>&lambda;</i>, the 
Pearson and Hartley approximation (Zar, 2010, p.587; Pearson and Hartley, 1970, p.81) 
is given by:
</p>
<p style="text-align: center;"><i>[\frac{&chi;^2_{2n\bar{x}, &alpha;/2}}{2n}, \frac{&chi;^2_{2n\bar{x} + 2, 1 - &alpha;/2}}{2n}] \;\;\;\; (6)</i></p>

<p>where <i>&chi;^2_{&nu;, p}</i> denotes the <i>p</i>'th quantile of the 
<a href="../../stats/help/Chisquare.html">chi-square distribution</a> with <i>&nu;</i> degrees of freedom.
One-sided confidence intervals are computed in a similar fashion.
<br />
</p>
<p><em>Normal Approximation</em> (<code>ci.method="normal.approx"</code>)
An approximate <i>(1-&alpha;)100\%</i> confidence interval for <i>&lambda;</i> can be 
constructed assuming the distribution of the estimator of <i>&lambda;</i> is 
approximately normally distributed.  A two-sided confidence interval is constructed 
as:
</p>
<p style="text-align: center;"><i>[\hat{&lambda;} - z_{1-&alpha;/2} \hat{&sigma;}_{\hat{&lambda;}}, \hat{&lambda;} + z_{1-&alpha;/2} \hat{&sigma;}_{\hat{&lambda;}}] \;\;\;\; (7)</i></p>

<p>where <i>z_p</i> is the <i>p</i>'th quantile of the standard normal distribution, and 
the quantity
</p>
<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&lambda;}} = &radic;{\hat{&lambda;} / n} \;\;\;\; (8)</i></p>

<p>denotes the estimated asymptotic standard deviation of the estimator of 
<i>&lambda;</i>.
</p>
<p>One-sided confidence intervals are constructed in a similar manner.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information. <br /> 
See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Poisson.html">Poisson distribution</a> is named after Poisson, who 
derived this distribution as the limiting distribution of the 
<a href="../../stats/help/Binomial.html">binomial distribution</a> with parameters <code>size=</code><i>N</i> 
and <code>prob=</code><i>p</i>, where <i>N</i> tends to infinity, <i>p</i> tends to 0, and 
<i>Np</i> stays constant.
</p>
<p>In this context, the Poisson distribution was used by Bortkiewicz (1898) to model 
the number of deaths (per annum) from kicks by horses in Prussian Army Corps.  In 
this case, <i>p</i>, the probability of death from this cause, was small, but the 
number of soldiers exposed to this risk, <i>N</i>, was large.
</p>
<p>The Poisson distribution has been applied in a variety of fields, including quality 
control (modeling number of defects produced in a process), ecology (number of 
organisms per unit area), and queueing theory.  Gibbons (1987b) used the Poisson 
distribution to model the number of detected compounds per scan of the 32 volatile 
organic priority pollutants (VOC), and also to model the distribution of chemical 
concentration (in ppb).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Gibbons, R.D. (1987b).  Statistical Models for the Analysis of Volatile Organic 
Compounds in Waste Disposal Sites.  <em>Ground Water</em> <b>25</b>, 572-580.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Johnson, N. L., S. Kotz, and A. Kemp. (1992).  <em>Univariate Discrete 
Distributions</em>.  Second Edition.  John Wiley and Sons, New York, Chapter 4.
</p>
<p>Pearson, E.S., and H.O. Hartley, eds. (1970).  <em>Biometrika Tables for 
Statisticians, Volume 1</em>.  Cambridge Universtiy Press, New York, p.81.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. 
Fifth Edition. Prentice-Hall, Upper Saddle River, NJ, pp. 585&ndash;586.  
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Poisson.html">Poisson</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a Poisson distribution with parameter 
  # lambda=2, then estimate the parameter and construct a 90% confidence 
  # interval. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rpois(20, lambda = 2) 
  epois(dat, ci = TRUE, conf.level = 0.9) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Poisson
  #
  #Estimated Parameter(s):          lambda = 1.8
  #
  #Estimation Method:               mle/mme/mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         lambda
  #
  #Confidence Interval Method:      exact
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                90%
  #
  #Confidence Interval:             LCL = 1.336558
  #                                 UCL = 2.377037

  #----------

  # Compare the different ways of constructing confidence intervals for 
  # lambda using the same data as in the previous example:

  epois(dat, ci = TRUE, ci.method = "pearson", 
    conf.level = 0.9)$interval$limits 
  #     LCL      UCL 
  #1.336558 2.377037

  epois(dat, ci = TRUE, ci.method = "normal.approx",  
    conf.level = 0.9)$interval$limits 
  #     LCL      UCL 
  #1.306544 2.293456 

  #----------

  # Clean up
  #---------

  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
