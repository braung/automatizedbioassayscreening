<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Quantiles of a Hypergeometric Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eqhyper {EnvStats}"><tr><td>eqhyper {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Quantiles of a Hypergeometric Distribution
</h2>

<h3>Description</h3>

<p>Estimate quantiles of a <a href="../../stats/help/Hypergeometric.html">hypergeometric distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  eqhyper(x, m = NULL, total = NULL, k = NULL, p = 0.5, method = "mle", digits = 0)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>non-negative integer indicating the number of white balls out of a sample of 
size <code>k</code> drawn without replacement from the urn, or an object resulting 
from a call to an estimating function that assumes a hypergeometric distribution 
(e.g., <code><a href="../../EnvStats/help/ehyper.html">ehyper</a></code>).  Missing (<code>NA</code>), 
undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are not 
allowed.
</p>
</td></tr>
<tr valign="top"><td><code>m</code></td>
<td>

<p>non-negative integer indicating the number of white balls in the urn.  
You must supply <code>m</code> or <code>total</code>, but not both.  
Missing values (<code>NA</code>s) are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>total</code></td>
<td>

<p>positive integer indicating the total number of balls in the urn (i.e., 
<code>m+n</code>).  You must supply <code>m</code> or <code>total</code>, but not both.
Missing values (<code>NA</code>s) are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>positive integer indicating the number of balls drawn without replacement from the 
urn.  Missing values (<code>NA</code>s) are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities for which quantiles will be estimated.  
All values of <code>p</code> must be between 0 and 1.  The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimating the parameters of the 
hypergeometric distribution.  Possible values are 
<code>"mle"</code> (maximum likelihood; the default) and <code>"mvue"</code> 
(minimum variance unbiased).  The mvue method is only available when you 
are estimating <i>m</i> (i.e., when you supply the argument <code>total</code>).
See the DETAILS section of the help file for <code><a href="../../EnvStats/help/ehyper.html">ehyper</a></code> for more 
information on these estimation methods. 
</p>
</td></tr>
<tr valign="top"><td><code>digits</code></td>
<td>

<p>an integer indicating the number of decimal places to round to when printing out 
the value of <code>100*p</code>. The default value is <code>digits=0</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>eqhyper</code> returns estimated quantiles as well as 
estimates of the hypergeometric distribution parameters.  
</p>
<p>Quantiles are estimated by 1) estimating the distribution parameters by 
calling <code><a href="../../EnvStats/help/ehyper.html">ehyper</a></code>, and then 2) calling the function 
<code><a href="../../stats/help/Hypergeometric.html">qhyper</a></code> and using the estimated values for 
the distribution parameters.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>eqhyper</code> returns a 
list of class <code>"estimate"</code> containing the estimated quantile(s) and other 
information. See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>eqhyper</code> 
returns a list whose class is the same as <code>x</code>.  The list 
contains the same components as <code>x</code>, as well as components called 
<code>quantiles</code> and <code>quantile.method</code>.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Hypergeometric.html">hypergeometric distribution</a> can be described by 
an urn model with <i>M</i> white balls and <i>N</i> black balls.  If <i>K</i> balls 
are drawn <em>with</em> replacement, then the number of white balls in the sample 
of size <i>K</i> follows a <a href="../../stats/help/Binomial.html">binomial distribution</a> with 
parameters <code>size=</code><i>K</i> and <code>prob=</code><i>M/(M+N)</i>.  If <i>K</i> balls are 
drawn <em>without</em> replacement, then the number of white balls in the sample of 
size <i>K</i> follows a <a href="../../stats/help/Hypergeometric.html">hypergeometric distribution</a> 
with parameters <code>m=</code><i>M</i>, <code>n=</code><i>N</i>, and <code>k=</code><i>K</i>.
</p>
<p>The name &ldquo;hypergeometric&rdquo; comes from the fact that the probabilities 
associated with this distribution can be written as successive terms in the 
expansion of a function of a Gaussian hypergeometric series.
</p>
<p>The hypergeometric distribution is applied in a variety of fields, including 
quality control and estimation of animal population size.  It is also the 
distribution used to compute probabilities for 
<a href="../../stats/help/fisher.test.html">Fishers's exact test</a> for a 2x2 contingency table.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and A. Kemp. (1992).  
<em>Univariate Discrete Distributions</em>.  Second Edition. John Wiley and Sons, 
New York, Chapter 6.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/ehyper.html">ehyper</a></code>, <a href="../../stats/help/Hypergeometric.html">Hypergeometric</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate an observation from a hypergeometric distribution with 
  # parameters m=10, n=30, and k=5, then estimate the parameter m, and
  # the 80'th percentile. 
  # Note: the call to set.seed simply allows you to reproduce this example. 
  # Also, the only parameter actually estimated is m; once m is estimated, 
  # n is computed by subtracting the estimated value of m (8 in this example) 
  # from the given of value of m+n (40 in this example).  The parameters 
  # n and k are shown in the output in order to provide information on 
  # all of the parameters associated with the hypergeometric distribution.

  set.seed(250) 
  dat &lt;- rhyper(nn = 1, m = 10, n = 30, k = 5) 
  dat 
  #[1] 1   

  eqhyper(dat, total = 40, k = 5, p = 0.8) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Hypergeometric
  #
  #Estimated Parameter(s):          m =  8
  #                                 n = 32
  #                                 k =  5
  #
  #Estimation Method:               mle for 'm'
  #
  #Estimated Quantile(s):           80'th %ile = 2
  #
  #Quantile Estimation Method:      Quantile(s) Based on
  #                                 mle for 'm' Estimators
  #
  #Data:                            dat
  #
  #Sample Size:                     1

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
