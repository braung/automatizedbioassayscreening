<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Determine Detection Limit</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for detectionLimitCalibrate {EnvStats}"><tr><td>detectionLimitCalibrate {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Determine Detection Limit
</h2>

<h3>Description</h3>

<p>Determine the detection limit based on using a calibration line (or curve) and
inverse regression.
</p>


<h3>Usage</h3>

<pre>
  detectionLimitCalibrate(object, coverage = 0.99, simultaneous = TRUE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>object</code></td>
<td>

<p>an object of class <code>"calibrate"</code> that is the result of calling the function
<code><a href="../../EnvStats/help/calibrate.html">calibrate</a></code>.
</p>
</td></tr>
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>optional numeric scalar between 0 and 1 indicating the confidence level associated with
the prediction intervals used in determining the detection limit.
The default value is <code>coverage=0.99</code>.
</p>
</td></tr>
<tr valign="top"><td><code>simultaneous</code></td>
<td>

<p>optional logical scalar indicating whether to base the prediction intervals on
simultaneous or non-simultaneous prediction limits.  The default value is <br />
<code>simultaneous=TRUE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The idea of a decision limit and detection limit is directly related to calibration and
can be framed in terms of a hypothesis test, as shown in the table below.
The null hypothesis is that the chemical is not present in the physical sample, i.e.,
<i>H_0: C = 0</i>, where C denotes the concentration.
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
  <b>Your Decision</b>         </td><td style="text-align: center;"> <b><i>H_0</i> True (<i>C = 0</i>)</b> </td><td style="text-align: center;"> <b><i>H_0</i> False (<i>C &gt; 0</i>)</b> </td>
</tr>
<tr>
 <td style="text-align: left;">
                               </td><td style="text-align: center;">                                     </td><td style="text-align: center;">                                      </td>
</tr>
<tr>
 <td style="text-align: left;">
  Reject <i>H_0</i>             </td><td style="text-align: center;"> Type I Error                        </td><td style="text-align: center;">                                      </td>
</tr>
<tr>
 <td style="text-align: left;">
    (Declare Chemical Present) </td><td style="text-align: center;"> (Probability = <i>&alpha;</i>)        </td><td style="text-align: center;">                                      </td>
</tr>
<tr>
 <td style="text-align: left;">
                               </td><td style="text-align: center;">                                     </td><td style="text-align: center;">                                      </td>
</tr>
<tr>
 <td style="text-align: left;">
  Do Not Reject <i>H_0</i>      </td><td style="text-align: center;">                                     </td><td style="text-align: center;"> Type II Error                        </td>
</tr>
<tr>
 <td style="text-align: left;">
    (Declare Chemical Absent)  </td><td style="text-align: center;">                                     </td><td style="text-align: center;"> (Probability = <i>&beta;</i>)          </td>
</tr>
<tr>
 <td style="text-align: left;">
  </td>
</tr>

</table>

<p>Ideally, you would like to minimize both the Type I and Type II error rates.
Just as we use critical values to compare against the test statistic for a hypothesis test,
we need to use a critical signal level <i>S_D</i> called the <strong>decision limit</strong> to decide
whether the chemical is present or absent.  If the signal is less than or equal to <i>S_D</i>
we will declare the chemical is absent, and if the signal is greater than <i>S_D</i> we will
declare the chemical is present.
</p>
<p>First, suppose no chemical is present (i.e., the null hypothesis is true).
If we want to guard against the mistake of declaring that the chemical is present when in fact it is
absent (Type I error), then we should choose <i>S_D</i> so that the probability of this happening is
some small value <i>&alpha;</i>.  Thus, the value of <i>S_D</i> depends on what we want to use for
<i>&alpha;</i> (the Type I error rate), and the true (but unknown) value of <i>&sigma;</i>
(the standard deviation of the errors assuming a constant standard deviation)
(Massart et al., 1988, p. 111).
</p>
<p>When the true concentration is 0, the decision limit is the (1-<i>&alpha;</i>)100th percentile of the
distribution of the signal <em>S</em>.  Note that the decision limit is on the scale of and in units
of the signal <em>S</em>.
</p>
<p>Now suppose that in fact the chemical is present in some concentration <em>C</em>
(i.e., the null hypothesis is false).  If we want to guard against the mistake of
declaring that the chemical is absent when in fact it is present (Type II error),
then we need to determine a minimal concentration <i>C_DL</i> called the <strong>detection limit (DL)</strong>
that we know will yield a signal less than the decision limit <i>S_D</i> only a small fraction of the
time (<i>&beta;</i>).
</p>
<p>In practice we do not know the true value of the standard deviation of the errors (<i>&sigma;</i>),
so we cannot compute the true decision limit.  Also, we do not know the true values of the
intercept and slope of the calibration line, so we cannot compute the true detection limit.
Instead, we usually set <i>&alpha; = &beta;</i> and estimate the decision and detection limits
by computing prediction limits for the calibration line and using inverse regression.
</p>
<p>The estimated detection limit corresponds to the upper confidence bound on concentration given that the
signal is equal to the estimated decision limit.  Currie (1997) discusses other ways to define the
detection limit, and Glaser et al. (1981) define a quantity called the method detection limit.
</p>


<h3>Value</h3>

<p>A numeric vector of length 2 indicating the signal detection limit and the concentration
detection limit. This vector has two attributes called <code>coverage</code>
and <code>simultaneous</code> indicating the values of these arguments that were used in the
call to <code>detectionLimitCalibrate</code>.
</p>


<h3>Note</h3>

<p>Perhaps no other topic in environmental statistics has generated as much confusion or controversy as
the topic of detection limits.  After decades of disparate terminology, ISO and IUPAC provided harmonized
guidance on the topic in 1995 (Currie, 1997).  Intuitively, the idea of a detection limit is simple to
grasp:  the <strong>detection limit</strong> is &ldquo;the smallest amount or concentration of a particular
substance that can be reliably detected in a given type of sample or medium by a specific measurement
process&rdquo; (Currie, 1997, p. 152).  Unfortunately, because of the exceedingly complex nature of measuring
chemical concentrations, this simple idea is difficult to apply in practice.
</p>
<p>Detection and quantification capabilities are fundamental performance characteristics of the
<strong>Chemical Measurement Process (CMP)</strong> (Currie, 1996, 1997).  In this help file we discuss
some currently accepted definitions of the terms decision, detection, and quantification limits.
For more details, the reader should consult the references listed in this help file.
</p>
<p>The <strong>quantification limit</strong> is defined as the concentration <em>C</em> at which the
coefficient of variation (also called relative standard deviation or RSD) for the
distribution of the signal <em>S</em> is some small value, usually taken to be 10%
(Currie, 1968, 1997). In practice the quantification limit is difficult to estimate
because we have to estimate both the mean and the standard deviation of the signal <em>S</em>
for any particular concentration, and usually the standard deviation varies with concentration.
Variations of the quantification limit include the quantitation limit (Keith, 1991, p. 109),
minimum level (USEPA, 1993), and alternative minimum level (Gibbons et al., 1997a).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Clark, M.J.R., and P.H. Whitfield. (1994). Conflicting Perspectives About Detection Limits and About the Censoring of Environmental Data.
<em>Water Resources Bulletin</em> <b>30</b>(6), 1063&ndash;1079.
</p>
<p>Clayton, C.A., J.W. Hines, and P.D. Elkins. (1987). Detection Limits with Specified Assurance Probabilities.
<em>Analytical Chemistry</em> <b>59</b>, 2506&ndash;2514.
</p>
<p>Code of Federal Regulations. (1996). Definition and Procedure for the Determination of the Method Detection Limit&ndash;Revision 1.11.
Title 40, Part 136, Appendix B, 7-1-96 Edition, pp.265&ndash;267.
</p>
<p>Currie, L.A. (1968). Limits for Qualitative Detection and Quantitative Determination: Application to Radiochemistry.
<em>Annals of Chemistry</em> <b>40</b>, 586&ndash;593.
</p>
<p>Currie, L.A. (1988). <em>Detection in Analytical Chemistry: Importance, Theory, and Practice</em>.
American Chemical Society, Washington, D.C.
</p>
<p>Currie, L.A. (1995). Nomenclature in Evaluation of Analytical Methods Including Detection and Quantification Capabilities.
<em>Pure &amp; Applied Chemistry</em> <b>67</b>(10), 1699-1723.
</p>
<p>Currie, L.A. (1996). Foundations and Future of Detection and Quantification Limits.
<em>Proceedings of the Section on Statistics and the Environment</em>, American Statistical Association, Alexandria, VA.
</p>
<p>Currie, L.A. (1997). Detection: International Update, and Some Emerging Di-Lemmas Involving Calibration, the Blank, and Multiple Detection Decisions.
<em>Chemometrics and Intelligent Laboratory Systems</em> <b>37</b>, 151-181.
</p>
<p>Davis, C.B. (1994). Environmental Regulatory Statistics. In Patil, G.P., and C.R. Rao, eds.,
<em>Handbook of Statistics, Vol. 12: Environmental Statistics</em>.
North-Holland, Amsterdam, a division of Elsevier, New York, NY,
Chapter 26, 817&ndash;865.
</p>
<p>Davis, C.B. (1997). Challenges in Regulatory Environmetrics.
<em>Chemometrics and Intelligent Laboratory Systems</em> <b>37</b>, 43&ndash;53.
</p>
<p>Gibbons, R.D. (1995). Some Statistical and Conceptual Issues in the Detection of Low-Level Environmental Pollutants
(with Discussion). <em>Environmetrics</em> <b>2</b>, 125-167.
</p>
<p>Gibbons, R.D., D.E. Coleman, and R.F. Maddalone. (1997a). An Alternative Minimum Level Definition for Analytical Quantification.
<em>Environmental Science &amp; Technology</em> <b>31</b>(7), 2071&ndash;2077.
Comments and Discussion in Volume <b>31</b>(12), 3727&ndash;3731, and Volume <b>32</b>(15), 2346&ndash;2353.
</p>
<p>Gibbons, R.D., D.E. Coleman, and R.F. Maddalone. (1997b). Response to Comment on
&ldquo;An Alternative Minimum Level Definition for Analytical Quantification&rdquo;.
<em>Environmental Science and Technology</em> <b>31</b>(12), 3729&ndash;3731.
</p>
<p>Gibbons, R.D., D.E. Coleman, and R.F. Maddalone. (1998). Response to Comment on
&ldquo;An Alternative Minimum Level Definition for Analytical Quantification&rdquo;.
<em>Environmental Science and Technology</em> <b>32</b>(15), 2349&ndash;2353.
</p>
<p>Gibbons, R.D., N.E. Grams, F.H. Jarke, and K.P. Stoub. (1992). Practical Quantitation Limits.
<em>Chemometrics Intelligent Laboratory Systems</em> <b>12</b>, 225&ndash;235.
</p>
<p>Gibbons, R.D., F.H. Jarke, and K.P. Stoub. (1991). Detection Limits: For Linear Calibration Curves with Increasing Variance and Multiple Future Detection Decisions.
In Tatsch, D.E., editor. <em>Waste Testing and Quality Assurance: Volume 3</em>.
American Society for Testing and Materials, Philadelphi, PA.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009).
<em>Statistical Methods for Groundwater Monitoring</em>. Second Edition.
John Wiley &amp; Sons, Hoboken.  Chapter 6, p. 111.
</p>
<p>Helsel, D.R. (2012). <em>Statistics for Censored Environmental Data Using Minitab and R,
Second Edition</em>.  John Wiley &amp; Sons, Hoboken, New Jersey.  Chapter 3, p. 22.
</p>
<p>Glasser, J.A., D.L. Foerst, G.D. McKee, S.A. Quave, and W.L. Budde. (1981). Trace Analyses for Wastewaters.
<em>Environmental Science and Technology</em> <b>15</b>, 1426&ndash;1435.
</p>
<p>Hubaux, A., and G. Vos. (1970). Decision and Detection Limits for Linear Calibration Curves.
<em>Annals of Chemistry</em> <b>42</b>, 849&ndash;855.
</p>
<p>Kahn, H.D., C.E. White, K. Stralka, and R. Kuznetsovski. (1997). Alternative Estimates of Detection.
<em>Proceedings of the Twentieth Annual EPA Conference on Analysis of Pollutants in the Environment, May 7-8, Norfolk, VA</em>.
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Kahn, H.D., W.A. Telliard, and C.E. White. (1998). Comment on &ldquo;An Alternative
Minimum Level Definition for Analytical Quantification&rdquo; (with Response).
<em>Environmental Science &amp; Technology</em> <b>32</b>(5), 2346&ndash;2353.
</p>
<p>Kaiser, H. (1965). Zum Problem der Nachweisgrenze. Fresenius' Z. <em>Anal. Chem.</em> <b>209</b>, 1.
</p>
<p>Keith, L.H. (1991). <em>Environmental Sampling and Analysis: A Practical Guide</em>.
Lewis Publishers, Boca Raton, FL, Chapter 10.
</p>
<p>Kimbrough, D.E. (1997). Comment on &ldquo;An Alternative Minimum Level Definition for Analytical Quantification&rdquo; (with Response).
<em>Environmental Science &amp; Technology</em> <b>31</b>(12), 3727&ndash;3731.
</p>
<p>Lambert, D., B. Peterson, and I. Terpenning. (1991). Nondetects, Detection Limits, and the Probability of Detection.
<em>Journal of the American Statistical Association</em> <b>86</b>(414), 266&ndash;277.
</p>
<p>Massart, D.L., B.G.M. Vandeginste, S.N. Deming, Y. Michotte, and L. Kaufman. (1988).
<em>Chemometrics: A Textbook</em>. Elsevier, New York, Chapter 7.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>.
CRC Press, Boca Raton, Florida.
</p>
<p>Porter, P.S., R.C. Ward, and H.F. Bell. (1988). The Detection Limit.
<em>Environmental Science &amp; Technology</em> <b>22</b>(8), 856&ndash;861.
</p>
<p>Rocke, D.M., and S. Lorenzato. (1995). A Two-Component Model for Measurement Error in Analytical Chemistry.
<em>Technometrics</em> <b>37</b>(2), 176&ndash;184.
</p>
<p>Singh, A. (1993). Multivariate Decision and Detection Limits.
<em>Analytica Chimica Acta</em> <b>277</b>, 205-214.
</p>
<p>Spiegelman, C.H. (1997). A Discussion of Issues Raised by Lloyd Currie and a Cross Disciplinary View of Detection Limits and Estimating Parameters That Are Often At or Near Zero.
<em>Chemometrics and Intelligent Laboratory Systems</em> <b>37</b>, 183&ndash;188.
</p>
<p>USEPA. (1987c). List (Phase 1) of Hazardous Constituents for Ground-Water Monitoring; Final Rule.
<em>Federal Register</em> <b>52</b>(131), 25942&ndash;25953 (July 9, 1987).
</p>
<p>Zorn, M.E., R.D. Gibbons, and W.C. Sonzogni. (1997). Weighted Least-Squares Approach to Calculating Limits of Detection and Quantification by Modeling Variability as a Function of Concentration.
<em>Analytical Chemistry</em> <b>69</b>, 3069&ndash;3075.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/calibrate.html">calibrate</a></code>, <code><a href="../../EnvStats/help/inversePredictCalibrate.html">inversePredictCalibrate</a></code>, <code><a href="../../EnvStats/help/pointwise.html">pointwise</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # The data frame EPA.97.cadmium.111.df contains calibration
  # data for cadmium at mass 111 (ng/L) that appeared in
  # Gibbons et al. (1997b) and were provided to them by the U.S. EPA.
  #
  # The Example section in the help file for calibrate shows how to
  # plot these data along with the fitted calibration line and 99%
  # non-simultaneous prediction limits.
  #
  # For the current example, we will compute the decision limit (7.68)
  # and detection limit (12.36 ng/L) based on using alpha = beta = 0.01
  # and a linear calibration line with constant variance. See
  # Millard and Neerchal (2001, pp.566-575) for more details on this
  # example.

  calibrate.list &lt;- calibrate(Cadmium ~ Spike, data = EPA.97.cadmium.111.df)

  detectionLimitCalibrate(calibrate.list, simultaneous = FALSE)
  #        Decision Limit (Signal) Detection Limit (Concentration)
  #                       7.677842                       12.364670
  #attr(,"coverage")
  #[1] 0.99
  #attr(,"simultaneous")
  #[1] FALSE

  #----------

  # Clean up
  #---------
  rm(calibrate.list)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
