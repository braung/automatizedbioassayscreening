<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Two-Sample Rank Test to Detect a Shift in a Proportion of the...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for quantileTest {EnvStats}"><tr><td>quantileTest {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Two-Sample Rank Test to Detect a Shift in a Proportion of the &quot;Treated&quot; Population
</h2>

<h3>Description</h3>

<p>Two-sample rank test to detect a positive shift in a proportion of one population 
(here called the &ldquo;treated&rdquo; population) compared to another (here called the 
&ldquo;reference&rdquo; population).  This test is usually called the quantile test 
(Johnson et al., 1987).
</p>


<h3>Usage</h3>

<pre>
  quantileTest(x, y, alternative = "greater", target.quantile = 0.5, 
    target.r = NULL, exact.p = TRUE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations from the &ldquo;treatment&rdquo; group.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr> 
<tr valign="top"><td><code>y</code></td>
<td>

<p>numeric vector of observations from the &ldquo;reference&rdquo; group.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values 
are <code>"greater"</code> (right tail of treatment group shifted to the right of the 
right tail of the reference group) and <code>"less"</code> (left tail of treatment group 
shifted to the left of the left tail of the reference group).  The default value is <br />
<code>alternative="greater"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>target.quantile</code></td>
<td>

<p>numeric scalar between 0 and 1 indicating the desired quantile to use as the 
lower cut off point for the test.  Because of the discrete nature of empirical 
quantiles, the upper bound for the possible empirical quantiles will often differ 
from the value of <code>target.quantile</code>.  The default value is 
<code>target.quantile=0.5</code> (i.e., the median).  This argument is ignored if the 
argument <code>target.r</code> is supplied.
</p>
</td></tr>
<tr valign="top"><td><code>target.r</code></td>
<td>

<p>integer indicating the rank of the observation to use as the lower cut off point 
for the test.  The value of <code>target.r</code> must be greater than or equal to 
2 and less than or equal to <i>N</i> (the total number of valid observations 
contained in the arguments <code>x</code> and <code>y</code>).  The actual rank of the cut off 
point may differ from <code>target.r</code> in the case of tied observations in 
<code>x</code> and/or <code>y</code>.  The default value of this argument is <code>NULL</code>, in 
which case the argument <code>target.quantile</code> is used to determine the lower cut 
off for the test.
</p>
</td></tr>
<tr valign="top"><td><code>exact.p</code></td>
<td>

<p>logical scalar indicating whether to compute the p-value based on the exact 
distribution of the test statistic (<code>exact.p=TRUE</code>; the default) or based 
on the normal approximation (<code>exact.p=FALSE</code>).
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>X</i> denote a random variable representing measurements from a 
&ldquo;treatment&rdquo; group with cumulative distribution function (cdf)
</p>
<p style="text-align: center;"><i>F_X(t) = Pr(X &le; t) \;\;\;\;\;\; (1)</i></p>

<p>and let <i>x_1, x_2, &hellip;, x_m</i> denote <i>m</i> observations from this 
treatment group.  Let <i>Y</i> denote a random variable from a &ldquo;reference&rdquo; 
group with cdf
</p>
<p style="text-align: center;"><i>F_Y(t) = Pr(Y &le; t) \;\;\;\;\;\; (2)</i></p>

<p>and let <i>y_1, y_2, &hellip;, y_n</i> denote <i>n</i> observations from this 
reference group.  Consider the null hypothesis:
</p>
<p style="text-align: center;"><i>H_0: F_X(t) = F_Y(t), \;\; -&infin; &lt; t &lt; &infin; \;\;\;\;\;\; (3)</i></p>

<p>versus the alternative hypothesis
</p>
<p style="text-align: center;"><i>H_a: F_X(t) = (1 - &epsilon;) F_Y(t) + &epsilon; F_Z(t) \;\;\;\;\;\; (4)</i></p>

<p>where <i>Z</i> denotes some random variable with cdf
</p>
<p style="text-align: center;"><i>F_Z(t) = Pr(Z &le; t) \;\;\;\;\;\; (5)</i></p>

<p>and <i>0 &lt; &epsilon; &le; 1</i>, <i>F_Z(t) &le; F_Y(t)</i> for all values of <i>t</i>, 
and <i>F_Z(t) \ne F_Y(t)</i> for at least one value of <i>t</i>.
</p>
<p>In English, the alternative hypothesis (4) says that a portion <i>&epsilon;</i> of the 
distribution for the treatment group (the distribution of <i>X</i>) is shifted to the 
right of the distribution for the reference group (the distribution of <i>Y</i>).  
The alternative hypothesis (4) with <i>&epsilon; = 1</i> is the alternative hypothesis 
associated with testing a location shift, for which the the 
<a href="../../stats/help/wilcox.test.html">Wilcoxon rank sum test</a> can be used. 
</p>
<p>Johnson et al. (1987) investigated locally most powerful rank tests for the test of 
the null hypothesis (3) against the alternative hypothesis (4).  They considered the 
case when <i>Y</i> and <i>Z</i> were normal random variables and the case when the 
densities of <i>Y</i> and <i>Z</i> assumed only two positive values.  For the latter 
case, the locally most powerful rank test reduces to the following procedure, which 
Johnson et al. (1987) call the quantile test.
</p>
 
<ol>
<li><p> Combine the <i>n</i> observations from the reference group and the <i>m</i> 
observations from the treatment group and rank them from smallest to largest.  
Tied observations receive the average rank of all observations tied at that value.
</p>
</li>
<li><p> Choose a quantile <i>q</i> and determine the smallest rank <i>r</i> such that
</p>
<p style="text-align: center;"><i>\frac{r}{m+n+1} &gt; q \;\;\;\;\;\; (6)</i></p>

<p>Note that because of the discrete nature of ranks, any quantile <i>q'</i> such 
that
</p>
<p style="text-align: center;"><i>\frac{r}{m+n+1} &gt; q' &ge; \frac{r-1}{m+n+1} \;\;\;\;\;\; (7)</i></p>

<p>will yield the same value for <i>r</i> as the quantile <i>q</i> does.  
Alternatively, choose a value of <i>r</i>.  The bounds on an associated quantile 
are then given in Equation (7).  Note: the component called <code>parameters</code> in 
the list returned by <code>quantileTest</code> contains an element named 
<code>quantile.ub</code>.  The value of this element is the left-hand side of Equation (7).
</p>
</li>
<li><p> Set <i>k</i> equal to the number of observations from the treatment group 
(the number of <i>X</i> observations) with ranks bigger than or equal to <i>r</i>.
</p>
</li>
<li><p> Under the null hypothesis (3), the probability that at least <i>k</i> out of 
the <i>r</i> largest observations come from the treatment group is given by:
</p>
<p style="text-align: center;"><i>p = &sum;_{i=k}^r \frac{{m+n-r \choose m-i} {r \choose i}}{{m+n \choose n}} \;\;\;\;\;\; (8)</i></p>

<p>This probability may be approximated by:
</p>
<p style="text-align: center;"><i>p = 1 - &Phi;(\frac{k - &mu;_k - 1/2}{&sigma;_k}) \;\;\;\;\;\; (9)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>&mu;_k = \frac{mr}{m+n} \;\;\;\;\;\; (10)</i></p>

<p style="text-align: center;"><i>&sigma;_k^2 = \frac{mnr(m+n-r)}{(m+n)^2 (m+n-1)} \;\;\;\;\;\; (11)</i></p>

<p>and <i>&Phi;</i> denotes the cumulative distribution function of the standard 
normal distribution (USEPA, 1994, pp.7.16-7.17).  
(See <code><a href="../../EnvStats/help/quantileTestPValue.html">quantileTestPValue</a></code>.)
</p>
</li>
<li><p> Reject the null hypothesis (3) in favor of the alternative hypothesis (4) at 
significance level <i>&alpha;</i> if <i>p &le; &alpha;</i>.
</p>
</li></ol>

<p>Johnson et al. (1987) note that their quantile test is asymptotically equivalent 
to one proposed by Carrano and Moore (1982) in the context of a two-sided test.  
Also, when <i>q=0.5</i>, the quantile test reduces to Mood's median test for two 
groups (see Zar, 2010, p.172; Conover, 1980, pp.171-178).
</p>
<p>The optimal choice of <i>q</i> or <i>r</i> in Step 2 above (i.e., the choice that 
yields the largest power) depends on the true underlying distributions of 
<i>Y</i> and <i>Z</i> and the mixing proportion <i>&epsilon;</i>.  
Johnson et al. (1987) performed a simulation study and showed that the quantile 
test performs better than the Wilcoxon rank sum test and the normal scores test 
under the alternative of a mixed normal distribution with a shift of at least 
2 standard deviations in the <i>Z</i> distribution.  USEPA (1994, pp.7.17-7.21) 
shows that when the mixing proportion <i>&epsilon;</i> is small and the shift is 
large, the quantile test is more powerful than the Wilcoxon rank sum test, and 
when <i>&epsilon;</i> is large and the shift is small the Wilcoxon rank sum test 
is more powerful than the quantile test.
</p>


<h3>Value</h3>

<p>A list of class <code>"htest"</code> containing the results of the hypothesis test.  
See the help file for <br />
<code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The EPA guidance document <em>Statistical Methods for Evaluating the Attainment of 
Cleanup Standards, Volume 3: Reference-Based Standards for Soils and Solid Media</em> 
(USEPA, 1994, pp.4.7-4.9) recommends three different statistical tests for 
determining whether a remediated Superfund site has attained compliance:  
the Wilcoxon rank sum test, the quantile test, and the &ldquo;hot measurement&rdquo; 
comparison test.  The Wilcoxon rank sum test and quantile test are nonparametric 
tests that compare chemical concentrations in the cleanup area with those in the 
reference area.  The hot-measurement comparison test compares concentrations in the 
cleanup area with a pre-specified upper limit value <em>Hm</em> (the value of 
<em>Hm</em> must be negotiated between the EPA and the Superfund-site owner or 
operator).  The Wilcoxon rank sum test is appropriate for detecting uniform failure 
of remedial action throughout the cleanup area.  The quantile test is appropriate 
for detecting failure in only a few areas within the cleanup area.  The 
hot-measurement comparison test is appropriate for detecting hot spots that need to 
be remediated regardless of the outcomes of the other two tests.
</p>
<p>USEPA (1994, pp.4.7-4.9) recommends applying all three tests to all cleanup units 
within a cleanup area.  This leads to the usual multiple comparisons problem:  the 
probability of at least one of the tests indicating non-compliance, when in fact the 
cleanup area is in compliance, is greater than the pre-set Type I error level for 
any of the individual tests.  USEPA (1994, p.3.3) recommends against using 
multiple comparison procedures to control the overall Type I error and suggests 
instead a re-sampling scheme where additional samples are taken in cases where 
non-compliance is indicated.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Carrano, A., and D. Moore. (1982).  The Rationale and Methodology for Quantifying 
Sister Chromatid Exchange in Humans.  In Heddle, J.A., ed., 
<em>Mutagenicity: New Horizons in Genetic Toxocology</em>.  Academic Press, New York, 
pp.268-304.
</p>
<p>Conover, W.J. (1980). <em>Practical Nonparametric Statistics</em>.  Second Edition. 
John Wiley and Sons, New York, Chapter 4.
</p>
<p>Johnson, R.A., S. Verrill, and D.H. Moore. (1987).  Two-Sample Rank Tests for 
Detecting Changes That Occur in a Small Proportion of the Treated Population.  
<em>Biometrics</em> <b>43</b>, 641-655.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001).  <em>Environmental Statistics with 
S-PLUS</em>. CRC Press, Boca Raton, FL, pp.435-439.
</p>
<p>USEPA. (1994).  <em>Statistical Methods for Evaluating the Attainment of Cleanup 
Standards, Volume 3: Reference-Based Standards for Soils and Solid Media</em>.  
EPA/230-R-94-004.  Office of Policy, Planning, and Evaluation, 
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Zar, J.H. (2010).  <em>Biostatistical Analysis</em>.  Fifth Edition.  
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/quantileTestPValue.html">quantileTestPValue</a></code>, <code><a href="../../stats/html/wilcox.test.html">wilcox.test</a></code>, 
<code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code>, <a href="../../EnvStats/help/Hypothesis+20Tests.html">Hypothesis Tests</a>.
</p>


<h3>Examples</h3>

<pre>
  # Following Example 7.5 on pages 7.23-7.24 of USEPA (1994b), perform the 
  # quantile test for the TcCB data (the data are stored in EPA.94b.tccb.df).  
  # There are n=47 observations from the reference area and m=77 observations 
  # from the cleanup unit.  The target rank is set to 9, resulting in a value 
  # of quantile.ub=0.928.  Note that the p-value is 0.0114, not 0.0117.

  EPA.94b.tccb.df
  #    TcCB.orig   TcCB Censored      Area
  #1        0.22   0.22    FALSE Reference
  #2        0.23   0.23    FALSE Reference
  #...
  #46       1.20   1.20    FALSE Reference
  #47       1.33   1.33    FALSE Reference
  #48      &lt;0.09   0.09     TRUE   Cleanup
  #49       0.09   0.09    FALSE   Cleanup
  #...
  #123     51.97  51.97    FALSE   Cleanup
  #124    168.64 168.64    FALSE   Cleanup

  # Determine the values to use for r and k for 
  # a desired significance level of 0.01 
  #--------------------------------------------

  p.vals &lt;- quantileTestPValue(m = 77, n = 47, 
    r = c(rep(8, 3), rep(9, 3), rep(10, 3)), 
    k = c(6, 7, 8, 7, 8, 9, 8, 9, 10)) 

  round(p.vals, 3) 
  #[1] 0.355 0.122 0.019 0.264 0.081 0.011 0.193 0.053 0.007 

  # Choose r=9, k=9 to get a significance level of 0.011
  #-----------------------------------------------------

  with(EPA.94b.tccb.df, 
    quantileTest(TcCB[Area=="Cleanup"], TcCB[Area=="Reference"], 
    target.r = 9)) 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 e = 0
  #
  #Alternative Hypothesis:          Tail of Fx Shifted to Right of
  #                                 Tail of Fy.
  #                                 0 &lt; e &lt;= 1, where
  #                                 Fx(t) = (1-e)*Fy(t) + e*Fz(t),
  #                                 Fz(t) &lt;= Fy(t) for all t,
  #                                 and Fy != Fz
  #
  #Test Name:                       Quantile Test
  #
  #Data:                            x = TcCB[Area == "Cleanup"]  
  #                                 y = TcCB[Area == "Reference"]
  #
  #Sample Sizes:                    nx = 77
  #                                 ny = 47
  #
  #Test Statistics:                 k (# x obs of r largest) = 9
  #                                 r                        = 9
  #
  #Test Statistic Parameters:       m           = 77.000
  #                                 n           = 47.000
  #                                 quantile.ub =  0.928
  #
  #P-value:                         0.01136926

  #==========

  # Clean up
  #---------

  rm(p.vals)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
