<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Quantiles of a Geometric Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eqgeom {EnvStats}"><tr><td>eqgeom {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Quantiles of a Geometric Distribution
</h2>

<h3>Description</h3>

<p>Estimate quantiles of a <a href="../../stats/help/Geometric.html">geometric distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  eqgeom(x, p = 0.5, method = "mle/mme", digits = 0)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>a numeric vector of observations, or an object resulting from a call to an 
estimating function that assumes a geometric distribution 
(e.g., <code><a href="../../EnvStats/help/egeom.html">egeom</a></code>). If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities for which quantiles will be estimated.  
All values of <code>p</code> must be between 0 and 1.  The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method to use to estimate the probability parameter.  
Possible values are <code>"mle/mme"</code> (maximum likelihood and method of moments; 
the default) and <code>"mvue"</code> (minimum variance unbiased).  You cannot use 
<code>method="mvue"</code> if <code>length(x)=1</code>.  See the DETAILS section of the help file 
for <code><a href="../../EnvStats/help/egeom.html">egeom</a></code> for more information on these estimation methods.
</p>
</td></tr>
<tr valign="top"><td><code>digits</code></td>
<td>

<p>an integer indicating the number of decimal places to round to when printing out 
the value of <code>100*p</code>. The default value is <code>digits=0</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>eqgeom</code> returns estimated quantiles as well as 
the estimate of the rate parameter.  
</p>
<p>Quantiles are estimated by 1) estimating the probability parameter by 
calling <code><a href="../../EnvStats/help/egeom.html">egeom</a></code>, and then 2) calling the function 
<code><a href="../../stats/help/Geometric.html">qgeom</a></code> and using the estimated value for 
the probability parameter.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>eqgeom</code> returns a 
list of class <code>"estimate"</code> containing the estimated quantile(s) and other 
information. See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>eqgeom</code> 
returns a list whose class is the same as <code>x</code>.  The list 
contains the same components as <code>x</code>, as well as components called 
<code>quantiles</code> and <code>quantile.method</code>.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Geometric.html">geometric distribution</a> with parameter 
<code>prob=</code><i>p</i> is a special case of the 
<a href="../../stats/help/NegBinomial.html">negative binomial distribution</a> with parameters 
<code>size=1</code> and <code>prob=p</code>.
</p>
<p>The negative binomial distribution has its roots in a gambling game where 
participants would bet on the number of tosses of a coin necessary to achieve 
a fixed number of heads.  The negative binomial distribution has been applied 
in a wide variety of fields, including accident statistics, birth-and-death 
processes, and modeling spatial distributions of biological organisms.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and A. Kemp. (1992).  
<em>Univariate Discrete Distributions</em>.  Second Edition. John Wiley and Sons, 
New York, Chapter 5.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/egeom.html">egeom</a></code>, <a href="../../stats/help/Geometric.html">Geometric</a>, <code><a href="../../EnvStats/help/enbinom.html">enbinom</a></code>, 
<a href="../../stats/help/NegBinomial.html">NegBinomial</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate an observation from a geometric distribution with parameter 
  # prob=0.2, then estimate the parameter prob and the 90'th percentile. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rgeom(1, prob = 0.2) 
  dat 
  #[1] 4 

  eqgeom(dat, p = 0.9)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Geometric
  #
  #Estimated Parameter(s):          prob = 0.2
  #
  #Estimation Method:               mle/mme
  #
  #Estimated Quantile(s):           90'th %ile = 10
  #
  #Quantile Estimation Method:      Quantile(s) Based on
  #                                 mle/mme Estimators
  #
  #Data:                            dat
  #
  #Sample Size:                     1

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
