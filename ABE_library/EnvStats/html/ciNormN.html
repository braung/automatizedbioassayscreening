<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Sample Size for Specified Half-Width of Confidence Interval...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for ciNormN {EnvStats}"><tr><td>ciNormN {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Sample Size for Specified Half-Width of Confidence Interval for Normal Distribution Mean or Difference Between Two Means
</h2>

<h3>Description</h3>

<p>Compute the sample size necessary to achieve a specified half-width of a 
confidence interval for the mean of a normal distribution or the difference 
between two means, given the estimated standard deviation and confidence level.
</p>


<h3>Usage</h3>

<pre>
  ciNormN(half.width, sigma.hat = 1, conf.level = 0.95, 
    sample.type = ifelse(is.null(n2), "one.sample", "two.sample"), 
    n2 = NULL, round.up = TRUE, n.max = 5000, tol = 1e-07, maxiter = 1000)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>half.width</code></td>
<td>

<p>numeric vector of (positive) half-widths.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>sigma.hat</code></td>
<td>

<p>numeric vector specifying the value(s) of the estimated standard deviation(s).
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric vector of numbers between 0 and 1 indicating the confidence level 
associated with the confidence interval(s).  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>sample.type</code></td>
<td>

<p>character string indicating whether this is a one-sample <br />
(<code>sample.type="one.sample"</code>) or two-sample <br /> 
(<code>sample.type="two.sample"</code>) confidence interval.  <br />
When <code>sample.type="one.sample"</code>, the computed sample size is based on 
a confidence interval for a single mean.  <br />
When <code>sample.type="two.sample"</code>, the computed sample size is based on 
a confidence interval for the difference between two means.  <br />
The default value is <code>sample.type="one.sample"</code> unless the argument 
<code>n2</code> is supplied.
</p>
</td></tr>
<tr valign="top"><td><code>n2</code></td>
<td>

<p>numeric vector of sample sizes for group 2.  The default value is <code>NULL</code>, 
in which case it is assumed that the sample sizes for groups 1 and 2 are equal.  
This argument is ignored when <code>sample.type="one.sample"</code>.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are not allowed. 
</p>
</td></tr>
<tr valign="top"><td><code>round.up</code></td>
<td>

<p>logical scalar indicating whether to round up the values of the computed sample size(s) 
to the next smallest integer.  The default value is <code>round.up=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.max</code></td>
<td>

<p>positive integer greater than 1 specifying the maximum sample size for the single 
group when <code>sample.type="one.sample"</code> or for group 1 when <br />
<code>sample.type="two.sample"</code>.  The default value is <code>n.max=5000</code>.
</p>
</td></tr>
<tr valign="top"><td><code>tol</code></td>
<td>

<p>numeric scalar indicating the tolerance to use in the <code><a href="../../stats/html/uniroot.html">uniroot</a></code> 
search algorithm.  The default value is <code>tol=1e-7</code>.
</p>
</td></tr>
<tr valign="top"><td><code>maxiter</code></td>
<td>

<p>positive integer indicating the maximum number of iterations to use in the 
<code><a href="../../stats/html/uniroot.html">uniroot</a></code> search algorithm.  The default value is 
<code>maxiter=1000</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If the arguments <code>half.width</code>, <code>n2</code>, <code>sigma.hat</code>, and 
<code>conf.level</code> are not all the same length, they are replicated to be the same length as 
the length of the longest argument.
</p>
<p>The function <code>ciNormN</code> uses the formulas given in the help file for 
<code><a href="../../EnvStats/help/ciNormHalfWidth.html">ciNormHalfWidth</a></code> for the half-width of the confidence interval 
to iteratively solve for the sample size.  For the two-sample case, the default 
is to assume equal sample sizes for each group unless the argument <code>n2</code> 
is supplied.
</p>


<h3>Value</h3>

<p>When <code>sample.type="one.sample"</code>, or <code>sample.type="two.sample"</code> and <code>n2</code> 
is not supplied (so equal sample sizes for each group is assumed), 
the function <code>ciNormN</code> returns a numeric vector of sample sizes.  
When <code>sample.type="two.sample"</code> and <code>n2</code> is supplied, 
the function <code>ciNormN</code> returns a list with two components called <code>n1</code> and <code>n2</code>, 
specifying the sample sizes for each group.
</p>


<h3>Note</h3>

<p>The normal distribution and lognormal distribution are probably the two most frequently used 
distributions to model environmental data.  In order to make any kind of probability 
statement about a normally-distributed population (of chemical concentrations for example), 
you have to first estimate the mean and standard deviation (the population parameters) of the 
distribution.  Once you estimate these parameters, it is often useful to characterize the 
uncertainty in the estimate of the mean.  This is done with confidence intervals.
</p>
<p>In the course of designing a sampling program, an environmental scientist may wish to determine 
the relationship between sample size, confidence level, and half-width if one of the objectives 
of the sampling program is to produce confidence intervals.  The functions 
<code><a href="../../EnvStats/help/ciNormHalfWidth.html">ciNormHalfWidth</a></code>, <code><a href="../../EnvStats/help/ciNormN.html">ciNormN</a></code>, and <code><a href="../../EnvStats/help/plotCiNormDesign.html">plotCiNormDesign</a></code> 
can be used to investigate these relationships for the case of normally-distributed observations.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). 
<em>Statistics for Environmental Engineers</em>.  Second Edition.   
Lewis Publishers, Boca Raton, FL.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution Monitoring</em>. 
Van Nostrand Reinhold, New York, NY.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). 
<em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, Chapter 7.
</p>
<p>Millard, S.P., and N. Neerchal. (2001).  <em>Environmental Statistics with S-PLUS</em>.  
CRC Press, Boca Raton, FL.
</p>
<p>Ott, W.R. (1995). <em>Environmental Statistics and Data Analysis</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C. p.21-3.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. 
Fifth Edition. Prentice-Hall, Upper Saddle River, NJ, 
Chapters 7 and 8.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/ciNormHalfWidth.html">ciNormHalfWidth</a></code>, <code><a href="../../EnvStats/help/plotCiNormDesign.html">plotCiNormDesign</a></code>, <code><a href="../../stats/html/Normal.html">Normal</a></code>, 
<code><a href="../../EnvStats/help/enorm.html">enorm</a></code>, <code><a href="../../stats/html/t.test.html">t.test</a></code>, <br />
<a href="../../EnvStats/help/Estimating+20Distribution+20Parameters.html">Estimating Distribution Parameters</a>.
</p>


<h3>Examples</h3>

<pre>
  # Look at how the required sample size for a one-sample 
  # confidence interval decreases with increasing half-width:

  seq(0.25, 1, by = 0.25) 
  #[1] 0.25 0.50 0.75 1.00 

  ciNormN(half.width = seq(0.25, 1, by = 0.25)) 
  #[1] 64 18 10 7 

  ciNormN(seq(0.25, 1, by=0.25), round = FALSE) 
  #[1] 63.897899 17.832337  9.325967  6.352717

  #----------------------------------------------------------------

  # Look at how the required sample size for a one-sample 
  # confidence interval increases with increasing estimated 
  # standard deviation for a fixed half-width:

  seq(0.5, 2, by = 0.5) 
  #[1] 0.5 1.0 1.5 2.0 

  ciNormN(half.width = 0.5, sigma.hat = seq(0.5, 2, by = 0.5)) 
  #[1] 7 18 38 64

  #----------------------------------------------------------------

  # Look at how the required sample size for a one-sample 
  # confidence interval increases with increasing confidence 
  # level for a fixed half-width:

  seq(0.5, 0.9, by = 0.1) 
  #[1] 0.5 0.6 0.7 0.8 0.9 

  ciNormN(half.width = 0.25, conf.level = seq(0.5, 0.9, by = 0.1)) 
  #[1] 9 13 19 28 46

  #----------------------------------------------------------------

  # Modifying the example on pages 21-4 to 21-5 of USEPA (2009), 
  # determine the required sample size in order to achieve a 
  # half-width that is 10% of the observed mean (based on the first 
  # four months of observations) for the Aldicarb level at the first 
  # compliance well.  Assume a 95% confidence level and use the 
  # estimated standard deviation from the first four months of data. 
  # (The data are stored in EPA.09.Ex.21.1.aldicarb.df.) 
  #
  # The required sample size is 20, so almost two years of data are 
  # required assuming observations are taken once per month.

  EPA.09.Ex.21.1.aldicarb.df
  #   Month   Well Aldicarb.ppb
  #1      1 Well.1         19.9
  #2      2 Well.1         29.6
  #3      3 Well.1         18.7
  #4      4 Well.1         24.2
  #...

  mu.hat &lt;- with(EPA.09.Ex.21.1.aldicarb.df, 
    mean(Aldicarb.ppb[Well=="Well.1"]))

  mu.hat 
  #[1] 23.1 

  sigma.hat &lt;- with(EPA.09.Ex.21.1.aldicarb.df, 
    sd(Aldicarb.ppb[Well=="Well.1"]))

  sigma.hat 
  #[1] 4.93491 

  ciNormN(half.width = 0.1 * mu.hat, sigma.hat = sigma.hat) 
  #[1] 20

  #----------
  # Clean up
  rm(mu.hat, sigma.hat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
