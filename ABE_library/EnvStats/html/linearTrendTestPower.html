<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Power of a t-Test for Linear Trend</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for linearTrendTestPower {EnvStats}"><tr><td>linearTrendTestPower {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Power of a t-Test for Linear Trend
</h2>

<h3>Description</h3>

<p>Compute the power of a parametric test for linear trend, given the sample size or 
predictor variable values, scaled slope, and significance level.
</p>


<h3>Usage</h3>

<pre>
  linearTrendTestPower(n, x = lapply(n, seq), slope.over.sigma = 0, alpha = 0.05, 
    alternative = "two.sided", approx = FALSE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>numeric vector of sample sizes.  All values of <code>n</code> must be positive integers 
larger than 2.  This argument is ignored when <code>x</code> is supplied.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of predictor variable values, or a list in which each component is 
a numeric vector of predictor variable values.  Usually, the predictor variable is 
time (e.g., days, months, quarters, etc.).  The default value is 
<code>x=lapply(n,seq)</code>, which yields a list in which the i'th component is the 
seqence of integers from 1 to the i'th value of the vector <code>n</code>.  If <code>x</code> 
is a numeric vector, it must contain at least three elements, two of which must be 
unique.  If <code>x</code> is a list of numeric vectors, each component of <code>x</code> 
must contain at least three elements, two of which must be unique.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>slope.over.sigma</code></td>
<td>

<p>numeric vector specifying the ratio of the true slope to the standard deviation of 
the error terms (<i>&sigma;</i>).  This is also called the &quot;scaled slope&quot;.  The 
default value is <code>slope.over.sigma=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alpha</code></td>
<td>

<p>numeric vector of numbers between 0 and 1 indicating the Type I error level 
associated with the hypothesis test.  The default value is <code>alpha=0.05</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values 
are <code>"two.sided"</code> (the default), <code>"greater"</code>, and <code>"less"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>approx</code></td>
<td>

<p>logical scalar indicating whether to compute the power based on an approximation to 
the non-central t-distribution.  The default value is <code>FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If the argument <code>x</code> is a vector, it is converted into a list with one 
component.  If the arguments <code>n</code>, <code>x</code>, <code>slope.over.sigma</code>, and 
<code>alpha</code> are not all the same length, they are replicated to be the same 
length as the length of the longest argument.
</p>
<p><b>Basic Model</b> <br />
Consider the simple linear regression model
</p>
<p style="text-align: center;"><i>Y = &beta;_0 + &beta;_1 X + &epsilon; \;\;\;\;\;\; (1)</i></p>

<p>where <i>X</i> denotes the predictor variable (observed without error), 
<i>&beta;_0</i> denotes the intercept, <i>&beta;_1</i> denotes the slope, and the 
error term <i>&epsilon;</i> is assumed to be a random variable from a normal 
distribution with mean 0 and standard deviation <i>&sigma;</i>.  Let
</p>
<p style="text-align: center;"><i>(\underline{x}, \underline{y}) = (x_1, y_1), (x_2, y_2), &hellip;, (x_n, y_n) \;\;\;\;\;\; (2)</i></p>

<p>denote <i>n</i> independent observed <i>(X,Y)</i> pairs from the model (1).
</p>
<p>Often in environmental data analysis, we are interested in determining whether there 
is a trend in some indicator variable over time.  In this case, the predictor 
variable <i>X</i> is time (e.g., day, month, quarter, year, etc.), and the <i>n</i> 
values of the response variable <i>Y</i> represent measurements taken over time.  
The slope then represents the change in the average of the response variable per 
one unit of time.
</p>
<p>When the argument <code>x</code> is a numeric vector, it represents the 
<i>n</i> values of the predictor variable.  When the argument <code>x</code> is a 
list, each component of <code>x</code> is a numeric vector that represents a set values 
of the predictor variable (and the number of elements may vary by component).  
By default, the argument <code>x</code> is a list for which the i'th component is simply 
the integers from 1 to the value of the i'th element of the argument <code>n</code>, 
representing, for example, Day 1, Day2, ..., Day <code>n[i]</code>.
</p>
<p>In the discussion that follows, be sure not to confuse the intercept and slope 
coefficients <i>&beta;_0</i> and <i>&beta;_1</i> with the Type II error of the 
hypothesis test, which is denoted by <i>&beta;</i>.
<br />
</p>
<p><b>Estimation of Coefficients and Confidence Interval for Slope</b> <br />
The standard least-squares estimators of the slope and intercept are given by:
</p>
<p style="text-align: center;"><i>\hat{&beta;}_1 = \frac{S_{xy}}{S_{xx}} \;\;\;\;\;\; (3)</i></p>

<p style="text-align: center;"><i>\hat{&beta;}_0 = \bar{y} - \hat{&beta;}_1 \bar{x} \;\;\;\;\;\; (4)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>S_{xy} = &sum;_{i=1}^n (x_i - \bar{x})(y_i - \bar{y}) \;\;\;\;\;\; (5)</i></p>

<p style="text-align: center;"><i>S_{xx} = &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (6)</i></p>

<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (7)</i></p>

<p style="text-align: center;"><i>\bar{y} = \frac{1}{n} &sum;_{i=1}^n y_i \;\;\;\;\;\; (8)</i></p>

<p>(Draper and Smith, 1998, p.25; Zar, 2010, p.332-334; Berthoux and Brown, 2002, p.297; 
Helsel and Hirsch, p.226).  The estimator of slope in Equation (3) has a normal 
distribution with mean equal to the true slope, and variance given by:
</p>
<p style="text-align: center;"><i>Var(\hat{&beta;}_1) = &sigma;_{\hat{&beta;}_1}^2 = \frac{&sigma;^2}{S_{xx}} \;\;\;\;\;\; (9)</i></p>

<p>(Draper and Smith, 1998, p.35; Zar, 2010, p.341; Berthoux and Brown, 2002, p.299; 
Helsel and Hirsch, 1992, p.227). Thus, a <i>(1-&alpha;)100\%</i> two-sided confidence 
interval for the slope is given by:
</p>
<p style="text-align: center;"><i>[ \hat{&beta;}_1 - t_{n-2}(1-&alpha;/2) \hat{&sigma;}_{\hat{&beta;}_1}, \;\; \hat{&beta;}_1 + t_{n-2}(1-&alpha;/2) \hat{&sigma;}_{\hat{&beta;}_1} ] \;\;\;\;\;\; (10)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&beta;}_1} = \frac{\hat{&sigma;}}{&radic;{S_{xx}}} \;\;\;\;\;\; (11)</i></p>

<p style="text-align: center;"><i>\hat{&sigma;}^2 = s^2 = \frac{1}{n-2} &sum;_{i=1}^n (y_i - \hat{y}_i)^2 \;\;\;\;\;\; (12)</i></p>

<p style="text-align: center;"><i>\hat{y}_i = \hat{&beta;}_0 + \hat{&beta;}_1 x_i \;\;\;\;\;\; (13)</i></p>

<p>and <i>t_{&nu;}(p)</i> denotes the <i>p</i>'th quantile of 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>&nu;</i> degrees of freedom 
(Draper and Smith, 1998, p.36; Zar, 2010, p.343; Berthoux and Brown, 2002, p.300; 
Helsel and Hirsch, 1992, p.240).
<br />
</p>
<p><b>Testing for a Non-Zero Slope</b> <br />
Consider the null hypothesis of a zero slope coefficient:
</p>
<p style="text-align: center;"><i>H_0: &beta;_1 = 0 \;\;\;\;\;\; (14)</i></p>

<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>):
</p>
<p style="text-align: center;"><i>H_a: &beta;_1 &gt; 0 \;\;\;\;\;\; (15)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: &beta;_1 &lt; 0 \;\;\;\;\;\; (16)</i></p>

<p>and the two-sided alternative (<code>alternative="two.sided"</code>)
</p>
<p style="text-align: center;"><i>H_a: &beta;_1 \ne 0 \;\;\;\;\;\; (17)</i></p>

<p>The test of the null hypothesis (14) versus any of the three alternatives (15)-(17) is 
based on the Student t-statistic:
</p>
<p style="text-align: center;"><i>t = \frac{\hat{&beta;}_1}{\hat{&sigma;}_{\hat{&beta;}_1}} =  \frac{\hat{&beta;}_1}{s/&radic;{S_{xx}}} \;\;\;\;\;\; (18)</i></p>

<p>Under the null hypothesis (14), the t-statistic in (18) follows a 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>n-2</i> degrees of freedom 
(Draper and Smith, 1998, p.36; Zar, 2010, p.341; 
Helsel and Hirsch, 1992, pp.238-239).
</p>
<p>The formula for the power of the test of a zero slope depends on which alternative 
is being tested.  
The two subsections below describe exact and approximate formulas for the power of 
the test.  Note that none of the equations for the power of the t-test 
requires knowledge of the values <i>&beta;_1</i> or <i>&sigma;</i> 
(the population standard deviation of the error terms), only the ratio 
<i>&beta;_1/&sigma;</i>.  The argument <code>slope.over.sigma</code> is this ratio, and it is 
referred to as the &ldquo;scaled slope&rdquo;.
<br />
</p>
<p><b><em>Exact Power Calculations</em></b> (<code>approx=FALSE</code>) <br />
This subsection describes the exact formulas for the power of the t-test for a 
zero slope.
<br />
</p>
<p><em>Upper one-sided alternative</em> (<code>alternative="greater"</code>) <br />
The standard Student's t-test rejects the null hypothesis (1) in favor of the 
upper alternative hypothesis (2) at level-<i>&alpha;</i> if
</p>
<p style="text-align: center;"><i>t &ge; t_{&nu;}(1 - &alpha;) \;\;\;\;\;\; (19)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>&nu; = n - 2 \;\;\;\;\;\; (20)</i></p>

<p>and, as noted previously, <i>t_{&nu;}(p)</i> denotes the <i>p</i>'th quantile of 
Student's t-distribution with <i>&nu;</i> degrees of freedom.    
The power of this test, denoted by <i>1-&beta;</i>, where <i>&beta;</i> denotes the 
probability of a Type II error, is given by:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t_{&nu;, &Delta;} &ge; t_{&nu;}(1 - &alpha;)] = 1 - G[t_{&nu;}(1 - &alpha;), &nu;, &Delta;] \;\;\;\;\;\; (21)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>&Delta; = &radic;{S_{xx}} \frac{&beta;_1}{&sigma;} \;\;\;\;\;\; (22)</i></p>

<p>and <i>t_{&nu;, &Delta;}</i> denotes a 
<a href="../../stats/help/TDist.html">non-central Student's t-random variable</a> with 
<i>&nu;</i> degrees of freedom and non-centrality parameter <i>&Delta;</i>, and 
<i>G(x, &nu;, &Delta;)</i> denotes the cumulative distribution function of this 
random variable evaluated at <i>x</i> (Johnson et al., 1995, pp.508-510).  
Note that when the predictor variable <i>X</i> represents equally-spaced measures 
of time (e.g., days, months, quarters, etc.) and 
</p>
<p style="text-align: center;"><i>x_i = i, \;\; i = 1, 2, &hellip;, n \;\;\;\;\;\; (23)</i></p>

<p>then the non-centrality parameter in Equation (22) becomes:
</p>
<p style="text-align: center;"><i>&Delta; = &radic;{\frac{(n-1)n(n+1)}{12}} \frac{&beta;_1}{&sigma;} \;\;\;\;\;\; (24)</i></p>

<p><br />
</p>
<p><em>Lower one-sided alternative</em> (<code>alternative="less"</code>) <br />
The standard Student's t-test rejects the null hypothesis (1) in favor of the 
lower alternative hypothesis (3) at level-<i>&alpha;</i> if
</p>
<p style="text-align: center;"><i>t &le; t_{&nu;}(&alpha;) \;\;\;\;\;\; (25)</i></p>

<p>and the power of this test is given by:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t_{&nu;, &Delta;} &le; t_{&nu;}(&alpha;)] = G[t_{&nu;}(&alpha;), &nu;, &Delta;] \;\;\;\;\;\; (26)</i></p>

<p><br />
</p>
<p><em>Two-sided alternative</em> (<code>alternative="two.sided"</code>) <br />
The standard Student's t-test rejects the null hypothesis (14) in favor of the 
two-sided alternative hypothesis (17) at level-<i>&alpha;</i> if
</p>
<p style="text-align: center;"><i>|t| &ge; t_{&nu;}(1 - &alpha;/2) \;\;\;\;\;\; (27)</i></p>

<p>and the power of this test is given by:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t_{&nu;, &Delta;} &le; t_{&nu;}(&alpha;/2)] + Pr[t_{&nu;, &Delta;} &ge; t_{&nu;}(1 - &alpha;/2)]</i></p>

<p style="text-align: center;"><i>= G[t_{&nu;}(&alpha;/2), &nu;, &Delta;] + 1 - G[t_{&nu;}(1 - &alpha;/2), &nu;, &Delta;] \;\;\;\;\;\; (28)</i></p>

<p>The power of the t-test given in Equation (28) can also be expressed in terms of the 
cumulative distribution function of the <a href="../../stats/help/FDist.html">non-central F-distribution</a> 
as follows. Let <i>F_{&nu;_1, &nu;_2, &Delta;}</i> denote a 
<a href="../../stats/help/FDist.html">non-central F random variable</a> with <i>&nu;_1</i> and 
<i>&nu;_2</i> degrees of freedom and non-centrality parameter <i>&Delta;</i>, and let 
<i>H(x, &nu;_1, &nu;_2, &Delta;)</i> denote the cumulative distribution function of this 
random variable evaluated at <i>x</i>. Also, let <i>F_{&nu;_1, &nu;_2}(p)</i> denote 
the <i>p</i>'th quantile of the central F-distribution with <i>&nu;_1</i> and 
<i>&nu;_2</i> degrees of freedom.  It can be shown that
</p>
<p style="text-align: center;"><i>(t_{&nu;, &Delta;})^2 \cong F_{1, &nu;, &Delta;^2} \;\;\;\;\;\; (29)</i></p>

<p>where <i>\cong</i> denotes &ldquo;equal in distribution&rdquo;.  Thus, it follows that
</p>
<p style="text-align: center;"><i>[t_{&nu;}(1 - &alpha;/2)]^2 = F_{1, &nu;}(1 - &alpha;) \;\;\;\;\;\; (30)</i></p>

<p>so the formula for the power of the t-test given in Equation (28) can also be 
written as:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr\{(t_{&nu;, &Delta;})^2  &ge; [t_{&nu;}(1 - &alpha;/2)]^2\}</i></p>

<p style="text-align: center;"><i>= Pr[F_{1, &nu;, &Delta;^2} &ge; F_{1, &nu;}(1 - &alpha;)] = 1 - H[F_{1, &nu;}(1-&alpha;), 1, &nu;, &Delta;^2] \;\;\;\;\;\; (31)</i></p>

<p><br />
</p>
<p><b><em>Approximate Power Calculations</em></b> (<code>approx=TRUE</code>) <br />
Zar (2010, pp.115&ndash;118) presents an approximation to the power for the t-test 
given in Equations (21), (26), and (28) above.  His approximation to the power 
can be derived by using the approximation
</p>
<p style="text-align: center;"><i>&radic;{S_{xx}} \frac{&beta;_1}{s} \approx &radic;{SS_{xx}} \frac{&beta;_1}{&sigma;} = &Delta; \;\;\;\;\;\; (32)</i></p>

<p>where <i>\approx</i> denotes &ldquo;approximately equal to&rdquo;.  Zar's approximation 
can be summarized in terms of the cumulative distribution function of the 
non-central t-distribution as follows:
</p>
<p style="text-align: center;"><i>G(x, &nu;, &Delta;) \approx G(x - &Delta;, &nu;, 0) = G(x - &Delta;, &nu;) \;\;\;\;\;\; (33)</i></p>

<p>where <i>G(x, &nu;)</i> denotes the cumulative distribution function of the 
central Student's t-distribution with <i>&nu;</i> degrees of freedom evaluated at 
<i>x</i>.
</p>
<p>The following three subsections explicitly derive the approximation to the power of 
the t-test for each of the three alternative hypotheses.
<br />
</p>
<p><em>Upper one-sided alternative</em> (<code>alternative="greater"</code>) <br />
The power for the upper one-sided alternative (15) given in Equation (21) can be 
approximated as:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t &ge; t_{&nu;}(1 - &alpha;)]</i></p>

<p style="text-align: center;"><i>= Pr[\frac{\hat{&beta;}_1}{s/&radic;{S_{xx}}} &ge; t_{&nu;}(1 - &alpha;) - &radic;{S_{xx}}\frac{&beta;_1}{s}]</i></p>

<p style="text-align: center;"><i>\approx Pr[t_{&nu;} &ge; t_{&nu;}(1 - &alpha;) - &Delta;]</i></p>

<p style="text-align: center;"><i>= 1 - Pr[t_{&nu;} &le; t_{&nu;}(1 - &alpha;) - &Delta;]</i></p>

<p style="text-align: center;"><i> = 1 - G[t_{&nu;}(1-&alpha;) - &Delta;, &nu;] \;\;\;\;\;\; (34)</i></p>

<p>where <i>t_{&nu;}</i> denotes a central Student's t-random variable with <i>&nu;</i> 
degrees of freedom.
<br />
</p>
<p><em>Lower one-sided alternative</em> (<code>alternative="less"</code>) <br />
The power for the lower one-sided alternative (16) given in Equation (26) can be 
approximated as:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t &le; t_{&nu;}(&alpha;)]</i></p>

<p style="text-align: center;"><i>= Pr[\frac{\hat{&beta;}_1}{s/&radic;{S_{xx}}} &le; t_{&nu;}(&alpha;) - &radic;{S_{xx}}\frac{&beta;_1}{s}]</i></p>

<p style="text-align: center;"><i>\approx Pr[t_{&nu;} &le; t_{&nu;}(&alpha;) - &Delta;]</i></p>

<p style="text-align: center;"><i> = G[t_{&nu;}(&alpha;) - &Delta;, &nu;] \;\;\;\;\;\; (35)</i></p>

<p><br />
</p>
<p><em>Two-sided alternative</em> (<code>alternative="two.sided"</code>) <br />
The power for the two-sided alternative (17) given in Equation (28) can be 
approximated as:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t &le; t_{&nu;}(&alpha;/2)] + Pr[t &ge; t_{&nu;}(1 - &alpha;/2)]</i></p>

<p style="text-align: center;"><i>= Pr[\frac{\hat{&beta;}_1}{s/&radic;{S_{xx}}} &le; t_{&nu;}(&alpha;/2) - &radic;{SS_{xx}}\frac{&beta;_1}{s}] + Pr[\frac{\hat{&beta;}_1}{s/&radic;{S_{xx}}} &ge; t_{&nu;}(1 - &alpha;) - &radic;{SS_{xx}}\frac{&beta;_1}{s}]</i></p>

<p style="text-align: center;"><i>\approx Pr[t_{&nu;} &le; t_{&nu;}(&alpha;/2) - &Delta;] + Pr[t_{&nu;} &ge; t_{&nu;}(1 - &alpha;/2) - &Delta;]</i></p>

<p style="text-align: center;"><i>= G[t_{&nu;}(&alpha;/2) - &Delta;, &nu;] + 1 - G[t_{&nu;}(1-&alpha;/2) - &Delta;, &nu;] \;\;\;\;\;\; (36)</i></p>



<h3>Value</h3>

<p>a numeric vector powers.
</p>


<h3>Note</h3>

<p>Often in environmental data analysis, we are interested in determining whether 
there is a trend in some indicator variable over time.  In this case, the predictor 
variable <i>X</i> is time (e.g., day, month, quarter, year, etc.), and the <i>n</i> 
values of the response variable represent measurements taken over time.  The slope 
then represents the change in the average of the response variable per one unit of 
time.
</p>
<p>You can use the parametric model (1) to model your data, then use the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function 
<code><a href="../../stats/html/lm.html">lm</a></code> to fit the regression coefficients and the <code><a href="../../stats/html/summary.lm.html">summary.lm</a></code> 
function to perform a test for the significance of the slope coefficient.  The 
function <code>linearTrendTestPower</code> computes the power of this t-test, given a 
fixed value of the sample size, scaled slope, and significance level.
</p>
<p>You can also use <a href="../../EnvStats/help/kendallTrendTest.html">Kendall's nonparametric test for trend</a> 
if you don't want to assume the error terms are normally distributed.  When the 
errors are truly normally distributed, the asymptotic relative efficiency of 
Kendall's test for trend versus the parametric t-test for a zero slope is 0.98, 
and Kendall's test can be more powerful than the parametric t-test when the errors 
are not normally distributed.  Thus the function <code>linearTrendTestPower</code> can 
also be used to estimate the power of Kendall's test for trend.
</p>
<p>In the course of designing a sampling program, an environmental scientist may wish 
to determine the relationship between sample size, significance level, power, and 
scaled slope if one of the objectives of the sampling program is to determine 
whether a trend is occurring.  The functions <code>linearTrendTestPower</code>, 
<code><a href="../../EnvStats/help/linearTrendTestN.html">linearTrendTestN</a></code>, <code><a href="../../EnvStats/help/linearTrendTestScaledMds.html">linearTrendTestScaledMds</a></code>, and <br />
<code><a href="../../EnvStats/help/plotLinearTrendTestDesign.html">plotLinearTrendTestDesign</a></code> can be used to investigate these 
relationships.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). 
<em>Statistics for Environmental Engineers</em>.  Second Edition.   
Lewis Publishers, Boca Raton, FL.
</p>
<p>Draper, N., and H. Smith. (1998).  <em>Applied Regression Analysis</em>.  
Third Edition.  John Wiley and Sons, New York, Chapter 1.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). 
<em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, Chapter 9.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995).  <em>Continuous Univariate 
Distributions, Volume 2</em>.  Second Edition.  John Wiley and Sons, New York, 
Chapters 28, 31
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, FL.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/linearTrendTestN.html">linearTrendTestN</a></code>, <code><a href="../../EnvStats/help/linearTrendTestScaledMds.html">linearTrendTestScaledMds</a></code>, 
<code><a href="../../EnvStats/help/plotLinearTrendTestDesign.html">plotLinearTrendTestDesign</a></code>, <code><a href="../../stats/html/lm.html">lm</a></code>, <br />
<code><a href="../../stats/html/summary.lm.html">summary.lm</a></code>, <code><a href="../../EnvStats/help/kendallTrendTest.html">kendallTrendTest</a></code>, 
<a href="../../EnvStats/help/Power+20and+20Sample+20Size.html">Power and Sample Size</a>, <a href="../../stats/html/Normal.html">Normal</a>, <code><a href="../../stats/html/t.test.html">t.test</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Look at how the power of the t-test for zero slope increases with increasing 
  # sample size:

  seq(5, 30, by = 5) 
  #[1] 5 10 15 20 25 30 

  power &lt;- linearTrendTestPower(n = seq(5, 30, by = 5), slope.over.sigma = 0.1) 

  round(power, 2) 
  #[1] 0.06 0.13 0.34 0.68 0.93 1.00

  #----------

  # Repeat the last example, but compute the approximate power instead of the 
  # exact:

  power &lt;- linearTrendTestPower(n = seq(5, 30, by = 5), slope.over.sigma = 0.1, 
    approx = TRUE) 

  round(power, 2) 
  #[1] 0.05 0.11 0.32 0.68 0.93 0.99

  #----------

  # Look at how the power of the t-test for zero slope increases with increasing 
  # scaled slope:

  seq(0.05, 0.2, by = 0.05) 
  #[1] 0.05 0.10 0.15 0.20 

  power &lt;- linearTrendTestPower(15, slope.over.sigma = seq(0.05, 0.2, by = 0.05)) 

  round(power, 2) 
  #[1] 0.12 0.34 0.64 0.87

  #----------

  # Look at how the power of the t-test for zero slope increases with increasing 
  # values of Type I error:

  power &lt;- linearTrendTestPower(20, slope.over.sigma = 0.1, 
    alpha = c(0.001, 0.01, 0.05, 0.1)) 

  round(power, 2) 
  #[1] 0.14 0.41 0.68 0.80

  #----------

  # Show that for a simple regression model, you get a greater power of detecting 
  # a non-zero slope if you take all the observations at two endpoints, rather than 
  # spreading the observations evenly between two endpoints. 
  # (Note: This design usually cannot work with environmental monitoring data taken 
  # over time since usually observations taken close together in time are not 
  # independent.)

  linearTrendTestPower(x = 1:10, slope.over.sigma = 0.1) 
  #[1] 0.1265976


  linearTrendTestPower(x = c(rep(1, 5), rep(10, 5)), slope.over.sigma = 0.1) 
  #[1] 0.2413823

  #==========

  # Clean up
  #---------
  rm(power)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
