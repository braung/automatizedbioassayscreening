<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Zero-Modified Normal Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for ezmnorm {EnvStats}"><tr><td>ezmnorm {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Zero-Modified Normal Distribution
</h2>

<h3>Description</h3>

<p>Estimate the mean and standard deviation of a 
<a href="../../EnvStats/help/ZeroModifiedNormal.html">zero-modified normal distribution</a>, and 
optionally construct a confidence interval for the mean.
</p>


<h3>Usage</h3>

<pre>
  ezmnorm(x, method = "mvue", ci = FALSE, ci.type = "two-sided", 
    ci.method = "normal.approx", conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Currently, the only possible 
value is <code>"mvue"</code> (minimum variance unbiased; the default).  See the DETAILS 
section for more information.
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the 
mean.  The default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval 
for the mean.  Currently the only possible value is <code>"normal.approx"</code> 
(the default).  See the DETAILS section for more information.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>. This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from a 
<a href="../../EnvStats/help/ZeroModifiedNormal.html">zero-modified normal distribution</a> with 
parameters <code>mean=</code><i>&mu;</i>, <code>sd=</code><i>&sigma;</i>, and <code>p.zero=</code><i>p</i>.  
Let <i>r</i> denote the number of observations in <i>\underline{x}</i> that are equal 
to 0, and order the observations so that <i>x_1, x_2, &hellip;, x_r</i> denote 
the <i>r</i> zero observations, and <i>x_{r+1}, x_{r+2}, &hellip;, x_n</i> denote the 
<i>n-r</i> non-zero observations.
</p>
<p>Note that <i>&mu;</i> is <em>not</em> the mean of the zero-modified normal distribution; 
it is the mean of the normal part of the distribution.  Similarly, <i>&sigma;</i> is 
<em>not</em> the standard deviation of the zero-modified normal distribution; it is 
the standard deviation of the normal part of the distribution.
</p>
<p>Let <i>&gamma;</i> and <i>&delta;</i> denote the mean and standard deviation of the 
overall zero-modified normal distribution.  Aitchison (1955) shows that:
</p>
<p style="text-align: center;"><i>&gamma; = (1 - p) &mu;  \;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>&delta;^2 = (1 - p) &sigma;^2 + p (1 - p) &mu;^2 \;\;\;\; (2)</i></p>

<p><br />
</p>
<p><b>Estimation</b> <br />
</p>
<p><em>Minimum Variance Unbiased Estimation</em> (<code>method="mvue"</code>) <br />
Aitchison (1955) shows that the minimum variance unbiased estimators (mvue's) of 
<i>&gamma;</i> and <i>&delta;</i> are:
</p>
<p style="text-align: center;"><i>\hat{&gamma;}_{mvue} = \bar{x} \;\;\;\; (3)</i></p>


<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>\hat{&delta;}^2_{mvue} =</i> </td><td style="text-align: left;"> <i>\frac{n-r-1}{n-1} (s^*)^2 + \frac{r}{n} (\frac{n-r}{n-1}) (\bar{x}^*)^2</i> </td><td style="text-align: left;"> if <i>r &lt; n - 1</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                                  </td><td style="text-align: left;"> <i>x_n^2 / n</i> </td><td style="text-align: left;"> if <i>r = n - 1</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                                  </td><td style="text-align: left;"> <i>0</i> </td><td style="text-align: left;"> if <i>r = n \;\;\;\; (4)</i>
  </td>
</tr>

</table>

<p>where 
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\; (5)</i></p>

<p style="text-align: center;"><i>\bar{x}^* = \frac{1}{n-r} &sum;_{i=r+1}^n x_i \;\;\;\; (6)</i></p>

<p style="text-align: center;"><i>(s^*)^2 = \frac{1}{n-r-1} &sum;_{i=r+1}^n (x_i - \bar{x}^*)^2 \;\;\;\; (7)</i></p>

<p>Note that the quantity in equation (5) is the sample mean of all observations 
(including 0 values), the quantity in equation (6) is the sample mean of all non-zero 
observations, and the quantity in equation (7) is the sample variance of all 
non-zero observations.  Also note that for <i>r=n-1</i> or <i>r=n</i>, the estimator 
of <i>&delta;^2</i> is the sample variance for all observations (including 0 values).
<br />
</p>
<p><b>Confidence Intervals</b> <br />
</p>
<p><em>Based on Normal Approximation</em> (<code>ci.method="normal.approx"</code>) <br />
An approximate <i>(1-&alpha;)100\%</i> confidence interval for <i>&gamma;</i> is 
constructed based on the assumption that the estimator of <i>&gamma;</i> is 
approximately normally distributed.  Aitchison (1955) shows that 
</p>
<p style="text-align: center;"><i>Var(\hat{&gamma;}_{mvue}) = Var(\bar{x}) = \frac{&delta;^2}{n} \;\;\;\; (8)</i></p>

<p>Thus, an approximate two-sided <i>(1-&alpha;)100\%</i> confidence interval for 
<i>&gamma;</i> is constructed as:
</p>
<p style="text-align: center;"><i>[ \hat{&gamma;}_{mvue} - t_{n-2, 1-&alpha;/2} \frac{\hat{&delta;}_{mvue}}{&radic;{n}}, \; \hat{&gamma;}_{mvue} + t_{n-2, 1-&alpha;/2} \frac{\hat{&delta;}_{mvue}}{&radic;{n}} ] \;\;\;\; (9)</i></p>

<p>where <i>t_{&nu;, p}</i> is the <i>p</i>'th quantile of 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>&nu;</i> degrees of freedom.
</p>
<p>One-sided confidence intervals are computed in a similar fashion.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information.  
See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>The component called <code>parameters</code> is a numeric vector with the following 
estimated parameters: 
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <b>Parameter Name</b> </td><td style="text-align: left;"> <b>Explanation</b> </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>mean</code> </td><td style="text-align: left;"> mean of the normal (Gaussian) part of the distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>sd</code>   </td><td style="text-align: left;"> standard deviation of the normal (Gaussian) part of the distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>p.zero</code>  </td><td style="text-align: left;"> probability that an observation will be 0. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>mean.zmnorm</code> </td><td style="text-align: left;"> mean of the overall zero-modified normal distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>sd.zmnorm</code> </td><td style="text-align: left;"> standard deviation of the overall normal distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
  </td>
</tr>

</table>



<h3>Note</h3>

<p>The <a href="../../EnvStats/help/ZeroModifiedNormal.html">zero-modified normal distribution</a> is sometimes 
used to model chemical concentrations for which some observations are reported 
as &ldquo;Below Detection Limit&rdquo;.  See, for example USEPA (1992c, pp.27-34).  
In most cases, however, the zero-modified lognormal (delta) distribution will be 
more appropriate, since chemical concentrations are bounded below at 0 
(e.g., Gilliom and Helsel, 1986; Owen and DeRouen, 1980).
</p>
<p>Once you estimate the parameters of the zero-modified normal distribution, it is 
often useful to characterize the uncertainty in the estimate of the mean.  This is 
done with a confidence interval.
</p>
<p>One way to try to assess whether a 
<a href="../../EnvStats/help/ZeroModifiedLognormal.html">zero-modified lognormal (delta)</a>, 
<a href="../../EnvStats/help/ZeroModifiedNormal.html">zero-modified normal</a>, censored normal, or 
censored lognormal is the best model for the data is to construct both 
censored and detects-only probability plots (see <code><a href="../../EnvStats/help/qqPlotCensored.html">qqPlotCensored</a></code>).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Aitchison, J. (1955).  On the Distribution of a Positive Random Variable Having 
a Discrete Probability Mass at the Origin.  <em>Journal of the American 
Statistical Association</em> <b>50</b>, 901&ndash;908.
</p>
<p>Gilliom, R.J., and D.R. Helsel. (1986).  Estimation of Distributional Parameters 
for Censored Trace Level Water Quality Data: 1. Estimation Techniques.  
<em>Water Resources Research</em> <b>22</b>, 135&ndash;146.
</p>
<p>Owen, W., and T. DeRouen. (1980).  Estimation of the Mean for Lognormal Data 
Containing Zeros and Left-Censored Values, with Applications to the Measurement 
of Worker Exposure to Air Contaminants.  <em>Biometrics</em> <b>36</b>, 707&ndash;719.
</p>
<p>USEPA (1992c).  <em>Statistical Analysis of Ground-Water Monitoring Data at 
RCRA Facilities: Addendum to Interim Final Guidance</em>.  Office of Solid Waste, 
Permits and State Programs Division, US Environmental Protection Agency, 
Washington, D.C.
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/ZeroModifiedNormal.html">ZeroModifiedNormal</a>, <a href="../../stats/help/Normal.html">Normal</a>, 
<code><a href="../../EnvStats/help/ezmlnorm.html">ezmlnorm</a></code>, <a href="../../EnvStats/help/ZeroModifiedLognormal.html">ZeroModifiedLognormal</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 100 observations from a zero-modified normal distribution 
  # with mean=4, sd=2, and p.zero=0.5, then estimate the parameters.  
  # According to equations (1) and (2) above, the overall mean is 
  # mean.zmnorm=2 and the overall standard deviation is sd.zmnorm=sqrt(6).  
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rzmnorm(100, mean = 4, sd = 2, p.zero = 0.5) 
  ezmnorm(dat, ci = TRUE) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Zero-Modified Normal
  #
  #Estimated Parameter(s):          mean        = 4.037732
  #                                 sd          = 1.917004
  #                                 p.zero      = 0.450000
  #                                 mean.zmnorm = 2.220753
  #                                 sd.zmnorm   = 2.465829
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     100
  #
  #Confidence Interval for:         mean.zmnorm
  #
  #Confidence Interval Method:      Normal Approximation
  #                                 (t Distribution)
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = 1.731417
  #                                 UCL = 2.710088

  #----------

  # Following Example 9 on page 34 of USEPA (1992c), compute an 
  # estimate of the mean of the zinc data, assuming a 
  # zero-modified normal distribution. The data are stored in 
  # EPA.92c.zinc.df.

  head(EPA.92c.zinc.df) 
  #  Zinc.orig  Zinc Censored Sample Well
  #1        &lt;7  7.00     TRUE      1    1
  #2     11.41 11.41    FALSE      2    1
  #3        &lt;7  7.00     TRUE      3    1
  #4        &lt;7  7.00     TRUE      4    1
  #5        &lt;7  7.00     TRUE      5    1
  #6     10.00 10.00    FALSE      6    1

  New.Zinc &lt;- EPA.92c.zinc.df$Zinc 
  New.Zinc[EPA.92c.zinc.df$Censored] &lt;- 0 
  ezmnorm(New.Zinc, ci = TRUE) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Zero-Modified Normal
  #
  #Estimated Parameter(s):          mean        = 11.891000
  #                                 sd          =  1.594523
  #                                 p.zero      =  0.500000
  #                                 mean.zmnorm =  5.945500
  #                                 sd.zmnorm   =  6.123235
  #
  #Estimation Method:               mvue
  #
  #Data:                            New.Zinc
  #
  #Sample Size:                     40
  #
  #Confidence Interval for:         mean.zmnorm
  #
  #Confidence Interval Method:      Normal Approximation
  #                                 (t Distribution)
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = 3.985545
  #                                 UCL = 7.905455

  #----------

  # Clean up
  rm(dat, New.Zinc)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
