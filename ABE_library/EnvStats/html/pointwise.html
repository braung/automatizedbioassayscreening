<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Pointwise Confidence Limits for Predictions</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for pointwise {EnvStats}"><tr><td>pointwise {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Pointwise Confidence Limits for Predictions
</h2>

<h3>Description</h3>

<p>Computes pointwise confidence limits for predictions computed by the function 
<code><a href="../../EnvStats/help/predict.html">predict</a></code>.
</p>


<h3>Usage</h3>

<pre>
  pointwise(results.predict, coverage = 0.99, 
    simultaneous = FALSE, individual = FALSE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>results.predict</code></td>
<td>

<p>output from a call to <code><a href="../../EnvStats/help/predict.html">predict</a></code> with <code>se.fit=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>optional numeric scalar between 0 and 1 indicating the confidence level associated with the confidence limits. 
The default value is <code>coverage=0.99</code>.
</p>
</td></tr>
<tr valign="top"><td><code>simultaneous</code></td>
<td>

<p>optional logical scalar indicating whether to base the confidence limits for the 
predicted values on simultaneous or non-simultaneous prediction limits. 
The default value is <code>simultaneous=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>individual</code></td>
<td>

<p>optional logical scalar indicating whether to base the confidence intervals for the 
predicted values on prediction limits for the mean (<code>individual=FALSE</code>) or 
prediction limits for an individual observation (<code>individual=TRUE</code>). 
The default value is <code>individual=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>This function computes pointwise confidence limits for predictions computed by the 
function <br />
<code><a href="../../EnvStats/help/predict.html">predict</a></code>. The limits are computed at those points specified by the argument 
<code>newdata</code> of <code><a href="../../EnvStats/help/predict.html">predict</a></code>.
</p>
<p>The <code><a href="../../EnvStats/help/predict.html">predict</a></code> function is a generic function with methods for several 
different classes.  The funciton <code>pointwise</code> was part of the S language.  
The modifications to <code>pointwise</code> in the package <span class="pkg">EnvStats</span> involve confidence 
limits for predictions for a linear model (i.e., an object of class <code>"lm"</code>).
</p>
<p><strong>Confidence Limits for a Predicted Mean Value (<code>individual=FALSE</code>)</strong>.  
Consider a standard linear model with <i>p</i> predictor variables.  
Often, one of the major goals of regression analysis is to predict a future 
value of the response variable given known values of the predictor variables. 
The equations for the predicted mean value of the response given  
fixed values of the predictor variables as well as the equation for a 
two-sided (1-<i>&alpha;</i>)100% confidence interval for the mean value of the 
response can be found in Draper and Smith (1998, p.80) and 
Millard and Neerchal (2001, p.547).
</p>
<p>Technically, this formula is a confidence interval for the mean of 
the response for one set of fixed values of the predictor variables and 
corresponds to the case when <code>simultaneous=FALSE</code>. To create simultaneous 
confidence intervals over the range of of the predictor variables, 
the critical t-value in the equation has to be replaced with a critical 
F-value and the modified formula is given in Draper and Smith (1998, p. 83), 
Miller (1981a, p. 111), and Millard and Neerchal (2001, p. 547).  
This formula is used in the case when <code>simultaneous=TRUE</code>.
</p>
<p><strong>Confidence Limits for a Predicted Individual Value (<code>individual=TRUE</code>)</strong>.  
In the above section we discussed how to create a confidence interval for 
the mean of the response given fixed values for the predictor variables.  
If instead we want to create a prediction interval for a single 
future observation of the response variable, the fomula is given in 
Miller (1981a, p. 115) and Millard and Neerchal (2001, p. 551).
</p>
<p>Technically, this formula is a prediction interval for a single future 
observation for one set of fixed values of the predictor variables and 
corresponds to the case when <code>simultaneous=FALSE</code>.  Miller (1981a, p. 115) 
gives a formula for simultaneous prediction intervals for <i>k</i> future 
observations. If we are interested in creating an interval that will 
encompass <em>all</em> possible future observations over the range of the 
preictor variables with some specified probability however, we need to 
create simultaneous tolerance intervals.  A formula for such an interval 
was developed by Lieberman and Miller (1963) and is given in 
Miller (1981a, p. 124).  This formula is used in the case when 
<code>simultaneous=TRUE</code>.
</p>


<h3>Value</h3>

<p>a list with the following components:
</p>
<table summary="R valueblock">
<tr valign="top"><td><code>upper</code></td>
<td>
<p>upper limits of pointwise confidence intervals.</p>
</td></tr>
<tr valign="top"><td><code>fit</code></td>
<td>
<p>surface values.  This is the same as the component <code>fit</code> of the argument <br /> 
<code>results.predict</code>.</p>
</td></tr>
<tr valign="top"><td><code>lower</code></td>
<td>
<p>lower limits of pointwise confidence intervals.</p>
</td></tr>
</table>


<h3>Note</h3>

<p>The function <code>pointwise</code> is called by the functions 
<code><a href="../../EnvStats/help/detectionLimitCalibrate.html">detectionLimitCalibrate</a></code> and <br />
<code><a href="../../EnvStats/help/inversePredictCalibrate.html">inversePredictCalibrate</a></code>, which are used in <strong>calibration</strong>. 
</p>
<p>Almost always the process of determining the concentration of a chemical in 
a soil, water, or air sample involves using some kind of machine that 
produces a signal, and this signal is related to the concentration of the 
chemical in the physical sample. The process of relating the machine signal 
to the concentration of the chemical is called <strong>calibration</strong> 
(see <code><a href="../../EnvStats/help/calibrate.html">calibrate</a></code>). Once calibration has been performed, 
estimated concentrations in physical samples with unknown concentrations 
are computed using inverse regression.  The uncertainty in the process used 
to estimate the concentration may be quantified with decision, detection, 
and quantitation limits.
</p>
<p>In practice, only the point estimate of concentration is reported (along 
with a possible qualifier), without confidence bounds for the true 
concentration <i>C</i>. This is most unfortunate because it gives the 
impression that there is no error associated with the reported concentration. 
Indeed, both the International Organization for Standardization (ISO) and 
the International Union of Pure and Applied Chemistry (IUPAC) recommend 
always reporting both the estimated concentration and the uncertainty 
associated with this estimate (Currie, 1997).
</p>


<h3>Author(s)</h3>

<p>Authors of S (for code for <code>pointwise</code> in S).
</p>
<p>Steven P. Millard (for modification to allow the arguments <code>simultaneous</code> and <code>individual</code>); <br />
<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Chambers, J.M., and Hastie, T.J., eds. (1992). <em>Statistical Models in S</em>. 
Chapman and Hall/CRC, Boca Raton, FL.
</p>
<p>Draper, N., and H. Smith. (1998). <em>Applied Regression Analysis</em>. 
Third Edition. John Wiley and Sons, New York, Chapter 3.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, FL, pp.546-553.
</p>
<p>Miller, R.G. (1981a). <em>Simultaneous Statistical Inference</em>. 
Springer-Verlag, New York, pp.111, 124.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/predict.html">predict</a></code>, <code><a href="../../EnvStats/help/predict.lm.html">predict.lm</a></code>, 
<code><a href="../../stats/html/lm.html">lm</a></code>, <code><a href="../../EnvStats/help/calibrate.html">calibrate</a></code>, 
<code><a href="../../EnvStats/help/inversePredictCalibrate.html">inversePredictCalibrate</a></code>, <code><a href="../../EnvStats/help/detectionLimitCalibrate.html">detectionLimitCalibrate</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Using the data in the built-in data frame Air.df, 
  # fit the cube root of ozone as a function of temperature. 
  # Then compute predicted values for ozone at 70 and 90 
  # degrees F, and compute 95% confidence intervals for the 
  # mean value of ozone at these temperatures.

  # First create the lm object 
  #---------------------------

  ozone.fit &lt;- lm(ozone ~ temperature, data = Air.df) 


  # Now get predicted values and CIs at 70 and 90 degrees 
  #------------------------------------------------------

  predict.list &lt;- predict(ozone.fit, 
    newdata = data.frame(temperature = c(70, 90)), se.fit = TRUE) 

  pointwise(predict.list, coverage = 0.95) 
  # $upper
  #        1        2 
  # 2.839145 4.278533 

  # $fit
  #        1        2 
  # 2.697810 4.101808 

  # $lower
  #        1        2 
  # 2.556475 3.925082 

  #--------------------------------------------------------------------

  # Continuing with the above example, create a scatterplot of ozone 
  # vs. temperature, and add the fitted line along with simultaneous 
  # 95% confidence bands.

  x &lt;- Air.df$temperature 

  y &lt;- Air.df$ozone 

  dev.new()
  plot(x, y, xlab="Temperature (degrees F)",  
    ylab = expression(sqrt("Ozone (ppb)", 3))) 

  abline(ozone.fit, lwd = 2) 

  new.x &lt;- seq(min(x), max(x), length=100) 

  predict.ozone &lt;- predict(ozone.fit, 
    newdata = data.frame(temperature = new.x), se.fit = TRUE) 

  ci.ozone &lt;- pointwise(predict.ozone, coverage=0.95, 
    simultaneous=TRUE) 

  lines(new.x, ci.ozone$lower, lty=2, lwd = 2, col = 2) 

  lines(new.x, ci.ozone$upper, lty=2, lwd = 2, col = 2) 

  title(main=paste("Cube Root Ozone vs. Temperature with Fitted Line", 
    "and Simultaneous 95% Confidence Bands", 
    sep="\n")) 

  #--------------------------------------------------------------------

  # Redo the last example by creating non-simultaneous 
  # confidence bounds and prediction bounds as well.

  dev.new()
  plot(x, y, xlab = "Temperature (degrees F)", 
    ylab = expression(sqrt("Ozone (ppb)", 3))) 

  abline(ozone.fit, lwd = 2) 

  new.x &lt;- seq(min(x), max(x), length=100) 

  predict.ozone &lt;- predict(ozone.fit, 
    newdata = data.frame(temperature = new.x), se.fit = TRUE) 

  ci.ozone &lt;- pointwise(predict.ozone, coverage=0.95) 

  lines(new.x, ci.ozone$lower, lty=2, col = 2, lwd = 2) 

  lines(new.x, ci.ozone$upper, lty=2, col = 2, lwd = 2) 

  pi.ozone &lt;- pointwise(predict.ozone, coverage = 0.95, 
    individual = TRUE)

  lines(new.x, pi.ozone$lower, lty=4, col = 4, lwd = 2) 

  lines(new.x, pi.ozone$upper, lty=4, col = 4, lwd = 2) 

  title(main=paste("Cube Root Ozone vs. Temperature with Fitted Line", 
    "and 95% Confidence and Prediction Bands", 
    sep="\n")) 

  #--------------------------------------------------------------------

  # Clean up
  rm(predict.list, ozone.fit, x, y, new.x, predict.ozone, ci.ozone, 
    pi.ozone)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
