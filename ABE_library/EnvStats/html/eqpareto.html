<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Quantiles of a Pareto Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eqpareto {EnvStats}"><tr><td>eqpareto {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Quantiles of a Pareto Distribution
</h2>

<h3>Description</h3>

<p>Estimate quantiles of a <a href="../../EnvStats/help/Pareto.html">Pareto distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  eqpareto(x, p = 0.5, method = "mle", plot.pos.con = 0.375, digits = 0)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>a numeric vector of observations, or an object resulting from a call to an 
estimating function that assumes a Pareto distribution 
(e.g., <code><a href="../../EnvStats/help/epareto.html">epareto</a></code>).  If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities for which quantiles will be estimated.  
All values of <code>p</code> must be between 0 and 1.  The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimating the distribution parameters.  
Possible values are 
<code>"mle"</code> (maximum likelihood; the default), and <code>"lse"</code> (least-squares).  
See the DETAILS section of the help file for <code><a href="../../EnvStats/help/epareto.html">epareto</a></code> for more 
information on these estimation methods. 
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.con</code></td>
<td>

<p>numeric scalar between 0 and 1 containing the value of the plotting position 
constant used to construct the values of the empirical cdf.  The default value is 
<code>plot.pos.con=0.375</code>.  This argument is used only when <code>method="lse"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>digits</code></td>
<td>

<p>an integer indicating the number of decimal places to round to when printing out 
the value of <code>100*p</code>. The default value is <code>digits=0</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>eqpareto</code> returns estimated quantiles as well as 
estimates of the location and scale parameters.  
</p>
<p>Quantiles are estimated by 1) estimating the location and scale parameters by 
calling <code><a href="../../EnvStats/help/epareto.html">epareto</a></code>, and then 2) calling the function 
<code><a href="../../EnvStats/help/Pareto.html">qpareto</a></code> and using the estimated values for 
location and scale.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>eqpareto</code> returns a 
list of class <code>"estimate"</code> containing the estimated quantile(s) and other 
information. See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>eqpareto</code> 
returns a list whose class is the same as <code>x</code>.  The list 
contains the same components as <code>x</code>, as well as components called 
<code>quantiles</code> and <code>quantile.method</code>.
</p>


<h3>Note</h3>

<p>The Pareto distribution is named after Vilfredo Pareto (1848-1923), a professor 
of economics.  It is derived from Pareto's law, which states that the number of 
persons <i>N</i> having income <i>&ge; x</i> is given by:
</p>
<p style="text-align: center;"><i>N = A x^{-&theta;}</i></p>

<p>where <i>&theta;</i> denotes Pareto's constant and is the shape parameter for the 
probability distribution.
</p>
<p>The Pareto distribution takes values on the positive real line.  All values must be 
larger than the &ldquo;location&rdquo; parameter <i>&eta;</i>, which is really a threshold 
parameter.  There are three kinds of Pareto distributions.  The one described here 
is the Pareto distribution of the first kind.  Stable Pareto distributions have 
<i>0 &lt; &theta; &lt; 2</i>.  Note that the <i>r</i>'th moment only exists if 
<i>r &lt; &theta;</i>.
</p>
<p>The Pareto distribution is related to the 
<a href="../../stats/help/Exponential.html">exponential distribution</a> and 
<a href="../../stats/help/Logistic.html">logistic distribution</a> as follows.  
Let <i>X</i> denote a Pareto random variable with <code>location=</code><i>&eta;</i> and 
<code>shape=</code><i>&theta;</i>.  Then <i>log(X/&eta;)</i> has an exponential distribution 
with parameter <code>rate=</code><i>&theta;</i>, and <i>-log\{ [(X/&eta;)^&theta;] - 1 \}</i> 
has a logistic distribution with parameters <code>location=</code><i>0</i> and 
<code>scale=</code><i>1</i>.
</p>
<p>The Pareto distribution has a very long right-hand tail.  It is often applied in 
the study of socioeconomic data, including the distribution of income, firm size, 
population, and stock price fluctuations.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/epareto.html">epareto</a></code>, <a href="../../EnvStats/help/Pareto.html">Pareto</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 30 observations from a Pareto distribution with 
  # parameters location=1 and shape=1 then estimate the parameters 
  # and the 90'th percentile.
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rpareto(30, location = 1, shape = 1) 
  eqpareto(dat, p = 0.9) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Pareto
  #
  #Estimated Parameter(s):          location = 1.009046
  #                                 shape    = 1.079850
  #
  #Estimation Method:               mle
  #
  #Estimated Quantile(s):           90'th %ile = 8.510708
  #
  #Quantile Estimation Method:      Quantile(s) Based on
  #                                 mle Estimators
  #
  #Data:                            dat
  #
  #Sample Size:                     30

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
