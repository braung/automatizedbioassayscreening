<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Quantiles of a Binomial Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eqbinom {EnvStats}"><tr><td>eqbinom {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Quantiles of a Binomial Distribution
</h2>

<h3>Description</h3>

<p>Estimate quantiles of a <a href="../../stats/help/Binomial.html">binomial distribution</a>.
</p>


<h3>Usage</h3>

<pre>
 eqbinom(x, size = NULL, p = 0.5, method = "mle/mme/mvue", digits = 0)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric or logical vector of observations, or an object resulting from a call to an 
estimating function that assumes a binomial distribution 
(e.g., <code><a href="../../EnvStats/help/ebinom.html">ebinom</a></code>).  If <code>x</code> is a vector of observations, then when 
<code>size</code> is not supplied, <code>x</code> must be 
a numeric vector of 0s (&ldquo;failures&rdquo;) and 1s (&ldquo;successes&rdquo;), or else a logical vector 
of <code>FALSE</code> values (&ldquo;failures&rdquo;) and <code>TRUE</code> values (&ldquo;successes&rdquo;).  When 
<code>size</code> is supplied, <code>x</code> must be a non-negative integer containing the number of 
&ldquo;successes&rdquo; out of the number of trials indicated by <code>size</code>.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>size</code></td>
<td>

<p>positive integer indicating the of number of trials; <code>size</code> must be at least as 
large as the value of <code>x</code>.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities for which quantiles will be estimated.  
All values of <code>p</code> must be between 0 and 1.  The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  The only possible value is 
<code>"mle/mme/mvue"</code> (maximum likelihood, method of moments, and minimum variance unbiased).  
See the DETAILS section of the help file for <code><a href="../../EnvStats/help/ebinom.html">ebinom</a></code> for more information.
</p>
</td></tr>
<tr valign="top"><td><code>digits</code></td>
<td>

<p>an integer indicating the number of decimal places to round to when printing out 
the value of <code>100*p</code>. The default value is <code>digits=0</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>eqbinom</code> returns estimated quantiles as well as 
estimates of the <code>prob</code> parameter.  
</p>
<p>Quantiles are estimated by 1) estimating the prob parameter by 
calling <code><a href="../../EnvStats/help/ebinom.html">ebinom</a></code>, and then 2) calling the function 
<code><a href="../../stats/help/Binomial.html">qbinom</a></code> and using the estimated value for 
<code>prob</code>.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>eqbinom</code> returns a 
list of class <code>"estimate"</code> containing the estimated quantile(s) and other 
information. See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>eqbinom</code> 
returns a list whose class is the same as <code>x</code>.  The list 
contains the same components as <code>x</code>, as well as components called 
<code>quantiles</code> and <code>quantile.method</code>.
</p>


<h3>Note</h3>

<p>The binomial distribution is used to model processes with binary (Yes-No, Success-Failure, 
Heads-Tails, etc.) outcomes.  It is assumed that the outcome of any one trial is independent 
of any other trial, and that the probability of &ldquo;success&rdquo;, <i>p</i>, is the same on 
each trial.  A binomial discrete random variable <i>X</i> is the number of &ldquo;successes&rdquo; in 
<i>n</i> independent trials.  A special case of the binomial distribution occurs when <i>n=1</i>, 
in which case <i>X</i> is also called a Bernoulli random variable.
</p>
<p>In the context of environmental statistics, the binomial distribution is sometimes used to model 
the proportion of times a chemical concentration exceeds a set standard in a given period of 
time (e.g., Gilbert, 1987, p.143).  The binomial distribution is also used to compute an upper 
bound on the overall Type I error rate for deciding whether a facility or location is in 
compliance with some set standard.  Assume the null hypothesis is that the facility is in compliance.  
If a test of hypothesis is conducted periodically over time to test compliance and/or several tests 
are performed during each time period, and the facility or location is always in compliance, and 
each single test has a Type I error rate of <i>&alpha;</i>, and the result of each test is 
independent of the result of any other test (usually not a reasonable assumption), then the number 
of times the facility is declared out of compliance when in fact it is in compliance is a 
binomial random variable with probability of &ldquo;success&rdquo; <i>p=&alpha;</i> being the 
probability of being declared out of compliance (see USEPA, 2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Agresti, A., and B.A. Coull. (1998). Approximate is Better than &quot;Exact&quot; for Interval Estimation 
of Binomial Proportions. <em>The American Statistician</em>, <b>52</b>(2), 119&ndash;126.
</p>
<p>Agresti, A., and B. Caffo. (2000). Simple and Effective Confidence Intervals for Proportions 
and Differences of Proportions Result from Adding Two Successes and Two Failures. <em>The 
American Statistician</em>, <b>54</b>(4), 280&ndash;288.
</p>
<p>Berthouex, P.M., and L.C. Brown. (1994). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton, FL, Chapters 2 and 15.
</p>
<p>Cochran, W.G. (1977). <em>Sampling Techniques</em>. John Wiley and Sons, New York, Chapter 3.
</p>
<p>Fisher, R.A., and F. Yates. (1963). 
<em>Statistical Tables for Biological, Agricultural, and Medical Research</em>. 6th edition. 
Hafner, New York, 146pp.
</p>
<p>Fleiss, J. L. (1981). <em>Statistical Methods for Rates and Proportions</em>. Second Edition. 
John Wiley and Sons, New York, Chapters 1-2.
</p>
<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution Monitoring</em>. 
Van Nostrand Reinhold, New York, NY, Chapter 11.
</p>
<p>Johnson, N. L., S. Kotz, and A.W. Kemp. (1992).  <em>Univariate 
Discrete Distributions</em>.  Second Edition.  John Wiley and Sons, New York, 
Chapter 3.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, Florida.
</p>
<p>Newcombe, R.G. (1998a). Two-Sided Confidence Intervals for the Single Proportion:  Comparison of 
Seven Methods. <em>Statistics in Medicine</em>, <b>17</b>, 857&ndash;872.
</p>
<p>Ott, W.R. (1995). <em>Environmental Statistics and Data Analysis</em>. 
Lewis Publishers, Boca Raton, FL, Chapter 4.
</p>
<p>USEPA. (1989b). <em>Statistical Analysis of Ground-Water Monitoring Data at RCRA Facilities, Interim Final Guidance</em>. 
EPA/530-SW-89-026. Office of Solid Waste, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C. p.6-38.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, NJ, Chapter 24. 
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/ebinom.html">ebinom</a></code>, <code><a href="../../stats/help/Binomial.html">Binomial</a></code>, 
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a binomial distribution with 
  # parameters size=1 and prob=0.2, then estimate the 'prob' 
  # parameter and the 90'th percentile. 
  # (Note: the call to set.seed simply allows you to reproduce this example. 

  set.seed(251) 
  dat &lt;- rbinom(20, size = 1, prob = 0.2) 
  eqbinom(dat, p = 0.9) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Binomial
  #
  #Estimated Parameter(s):          size = 20.0
  #                                 prob =  0.1
  #
  #Estimation Method:               mle/mme/mvue for 'prob'
  #
  #Estimated Quantile(s):           90'th %ile = 4
  #
  #Quantile Estimation Method:      Quantile(s) Based on
  #                                 mle/mme/mvue for 'prob' Estimators
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #
  #

  #----------
  # Clean up

  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
