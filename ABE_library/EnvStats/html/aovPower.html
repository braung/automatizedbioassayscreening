<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Compute the Power of a One-Way Fixed-Effects Analysis of...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for aovPower {EnvStats}"><tr><td>aovPower {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Compute the Power of a One-Way Fixed-Effects Analysis of Variance 
</h2>

<h3>Description</h3>

<p>Compute the power of a one-way fixed-effects analysis of variance, 
given the sample sizes, population means, population standard deviation, and 
significance level.
</p>


<h3>Usage</h3>

<pre>
  aovPower(n.vec, mu.vec = rep(0, length(n.vec)), sigma = 1, alpha = 0.05)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n.vec</code></td>
<td>

<p>numeric vector of sample sizes for each group.  The <i>i^{th}</i> element of <code>n.vec</code> 
denotes the sample size for group <i>i</i>.  The length of <code>n.vec</code> must be at least 2, 
and all elements of <code>n.vec</code> must be greater than or equal to 2.  Missing (<code>NA</code>), 
undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>mu.vec</code></td>
<td>

<p>numeric vector of population means.  The length of <code>mu.vec</code> must be the 
same as the length of <code>n.vec</code>.  The default value is a vector of zeros.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>sigma</code></td>
<td>

<p>numeric scalar specifying the population standard deviation (<i>&sigma;</i>) 
for each group. The default value is <code>sigma=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alpha</code></td>
<td>

<p>numeric scalar between 0 and 1 indicating the Type I error level associated 
with the hypothesis test.  The default value is <code>alpha=0.05</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Consider <i>k</i> normally distributed populations with common standard deviation 
<i>&sigma;</i>. Let <i>&mu;_i</i> denote the mean of the <i>i</i>'th group 
(<i>i = 1, 2, &hellip;, k</i>), and let 
<i>\underline{x}_i = x_{i1}, x_{i2}, &hellip;, x_{in_i}</i> denote a vector of 
<i>n_i</i> observations from the <i>i</i>'th group.  
The statistical method of analysis of variance (ANOVA) tests the null hypothesis:
</p>
<p style="text-align: center;"><i>H_0: &mu;_1 = &mu;_2 = \cdots = &mu;_k \;\;\;\;\;\; (1)</i></p>

<p>against the alternative hypothesis that at least one of the means is different from 
the rest by using the F-statistic given by:
</p>
<p style="text-align: center;"><i>F = \frac{[&sum;_{i=1}^k n_i(\bar{x}_{i.} - \bar{x}_{..})^2]/(k-1)}{[&sum;_{i=1}^k &sum;_{j=1}^{n_i} (x_{ij} - \bar{x}_{i.})^2]/(N-k)} \;\;\;\;\;\; (2)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\bar{x}_{i.} = \frac{1}{n_i} &sum;_{j=1}^{n_i} x_{ij} \;\;\;\;\;\; (3)</i></p>

<p style="text-align: center;"><i>\bar{x}_{..} = \frac{1}{N} &sum;_{i=1}^k n_i\bar{x}_{i.} = \frac{1}{N} &sum;_{i=1}^k &sum;_{j=1}^{n_i} x_{ij} \;\;\;\;\;\; (4)</i></p>

<p style="text-align: center;"><i>N = &sum;_{i=1}^k n_i \;\;\;\;\;\; (5)</i></p>

<p>Under the null hypothesis (1), the F-statistic in (2) follows an 
<a href="../../stats/help/FDist.html">F-distribution</a> with <i>k-1</i> and <i>N-k</i> degrees of freedom.  
Analysis of variance rejects the null hypothesis (1) at significance level 
<i>&alpha;</i> when
</p>
<p style="text-align: center;"><i>F &gt; F_{k-1, N-k}(1 - &alpha;) \;\;\;\;\;\; (6)</i></p>

<p>where <i>F_{&nu;_1, &nu;_2}(p)</i> denotes the <i>p</i>'th quantile of the 
F-distribution with <i>&nu;_1</i> and <i>&nu;_2</i> degrees of freedom 
(Zar, 2010, Chapter 10; Berthouex and Brown, 2002, Chapter 24; 
Helsel and Hirsh, 1992, pp. 164&ndash;169).
</p>
<p>The power of this test, denoted by <i>1-&beta;</i>, where <i>&beta;</i> denotes the 
probability of a Type II error, is given by:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[F_{k-1, N-k, &Delta;} &gt; F_{k-1, N-k}(1 - &alpha;)] \;\;\;\;\;\; (7)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>&Delta; = \frac{&sum;_{i=1}^k n_i(&mu;_i - \bar{&mu;}_.)^2}{&sigma;^2} \;\;\;\;\;\; (8)</i></p>

<p style="text-align: center;"><i>\bar{&mu;}_. = \frac{1}{k} &sum;_{i=1}^k &mu; _i \;\;\;\;\;\; (9)</i></p>

<p>and <i>F_{&nu;_1, &nu;_2, &Delta;}</i> denotes a 
<a href="../../stats/help/FDist.html">non-central F random variable</a> with <i>&nu;_1</i> and 
<i>&nu;_2</i> degrees of freedom and non-centrality parameter <i>&Delta;</i>.  
Equation (7) can be re-written as:
</p>
<p style="text-align: center;"><i>1 - &beta; = 1 - H[F_{k-1, N-k}(1 - &alpha;), k-1, N-k, &Delta;] \;\;\;\;\;\; (10)</i></p>

<p>where <i>H(x, &nu;_1, &nu;_2, &Delta;)</i> denotes the cumulative distribution function 
of this random variable evaluated at <i>x</i> (Scheffe, 1959, pp.38&ndash;39, 62&ndash;65).
</p>
<p>The power of the one-way fixed-effects ANOVA depends on the 
sample sizes for each of the <i>k</i> groups, the value of the 
population means for each of the <i>k</i> groups, the population 
standard deviation <i>&sigma;</i>, and the significance level
<i>&alpha;</i>.
</p>


<h3>Value</h3>

<p>a numeric scalar indicating the power of the one-way fixed-effects ANOVA for 
the given sample sizes, population means, population standard deviation, and 
significance level.
</p>


<h3>Note</h3>

<p>The normal and lognormal distribution are probably the two most 
frequently used distributions to model environmental data.  
Sometimes it is necessary to compare several means to determine 
whether any are significantly different from each other 
(e.g., USEPA, 2009, p.6-38).  In this case, assuming 
normally distributed data, you perform a one-way parametric 
analysis of variance.
</p>
<p>In the course of designing a sampling program, an environmental 
scientist may wish to determine the relationship between sample 
size, Type I error level, power, and differences in means if 
one of the objectives of the sampling program is to determine 
whether a particular mean differs from a group of means.  The 
functions <code>aovPower</code>, <code><a href="../../EnvStats/help/aovN.html">aovN</a></code>, and 
<code><a href="../../EnvStats/help/plotAovDesign.html">plotAovDesign</a></code> can be used to investigate these 
relationships for the case of normally-distributed observations.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002).  
<em>Statistics for Environmental Engineers</em>.  Second Edition.  
Lewis Publishers, Boca Raton, FL.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). 
<em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, Chapter 7.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York, 
Chapters 27, 29, 30.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, Florida.
</p>
<p>Scheffe, H. (1959). <em>The Analysis of Variance</em>. 
John Wiley and Sons, New York, 477pp.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C. p.6-38.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. 
Fifth Edition. Prentice-Hall, Upper Saddle River, NJ, 
Chapter 10.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/aovN.html">aovN</a></code>, <code><a href="../../EnvStats/help/plotAovDesign.html">plotAovDesign</a></code>, 
<code><a href="../../stats/html/Normal.html">Normal</a></code>, <code><a href="../../stats/html/aov.html">aov</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Look at how the power of a one-way ANOVA increases 
  # with increasing sample size:

  aovPower(n.vec = rep(5, 3), mu.vec = c(10, 15, 20), sigma = 5) 
  #[1] 0.7015083 

  aovPower(n.vec = rep(10, 3), mu.vec = c(10, 15, 20), sigma = 5) 
  #[1] 0.9732551

  #----------------------------------------------------------------

  # Look at how the power of a one-way ANOVA increases 
  # with increasing variability in the population means:

  aovPower(n.vec = rep(5,3), mu.vec = c(10, 10, 11), sigma=5) 
  #[1] 0.05795739 

  aovPower(n.vec = rep(5, 3), mu.vec = c(10, 10, 15), sigma = 5) 
  #[1] 0.2831863 

  aovPower(n.vec = rep(5, 3), mu.vec = c(10, 13, 15), sigma = 5) 
  #[1] 0.2236093 

  aovPower(n.vec = rep(5, 3), mu.vec = c(10, 15, 20), sigma = 5) 
  #[1] 0.7015083

  #----------------------------------------------------------------

  # Look at how the power of a one-way ANOVA increases 
  # with increasing values of Type I error:

  aovPower(n.vec = rep(10,3), mu.vec = c(10, 12, 14), 
    sigma = 5, alpha = 0.001) 
  #[1] 0.02655785 

  aovPower(n.vec = rep(10,3), mu.vec = c(10, 12, 14), 
    sigma = 5, alpha = 0.01) 
  #[1] 0.1223527 

  aovPower(n.vec = rep(10,3), mu.vec = c(10, 12, 14), 
    sigma = 5, alpha = 0.05) 
  #[1] 0.3085313 

  aovPower(n.vec = rep(10,3), mu.vec = c(10, 12, 14), 
    sigma = 5, alpha = 0.1) 
  #[1] 0.4373292

  #==========

  # The example on pages 5-11 to 5-14 of USEPA (1989b) shows 
  # log-transformed concentrations of lead (mg/L) at two 
  # background wells and four compliance wells, where observations 
  # were taken once per month over four months (the data are 
  # stored in EPA.89b.loglead.df.)  Assume the true mean levels 
  # at each well are 3.9, 3.9, 4.5, 4.5, 4.5, and 5, respectively. 
  # Compute the power of a one-way ANOVA to test for mean 
  # differences between wells.  Use alpha=0.05, and assume the 
  # true standard deviation is equal to the one estimated from 
  # the data in this example.

  # First look at the data 
  names(EPA.89b.loglead.df)
  #[1] "LogLead"   "Month"     "Well"      "Well.type"

  dev.new()
  stripChart(LogLead ~ Well, data = EPA.89b.loglead.df,
    show.ci = FALSE, xlab = "Well Number", 
    ylab="Log [ Lead (ug/L) ]", 
    main="Lead Concentrations at Six Wells")

  # Note: The assumption of a constant variance across 
  # all wells is suspect.


  # Now perform the ANOVA and get the estimated sd 
  aov.list &lt;- aov(LogLead ~ Well, data=EPA.89b.loglead.df) 

  summary(aov.list) 
  #            Df Sum Sq Mean Sq F value  Pr(&gt;F)  
  #Well         5 5.7447 1.14895  3.3469 0.02599 *
  #Residuals   18 6.1791 0.34328                  
  #---
  #Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 '' 1 

  # Now call the function aovPower 
  aovPower(n.vec = rep(4, 6), 
    mu.vec = c(3.9,3.9,4.5,4.5,4.5,5), sigma=sqrt(0.34)) 
  #[1] 0.5523148

  # Clean up
  rm(aov.list)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
