<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Two-Sample Linear Rank Test to Detect a Difference Between...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for twoSampleLinearRankTest {EnvStats}"><tr><td>twoSampleLinearRankTest {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Two-Sample Linear Rank Test to Detect a Difference Between Two Distributions
</h2>

<h3>Description</h3>

<p>Two-sample linear rank test to detect a difference (usually a shift) between two
distributions.  The <a href="../../stats/html/wilcox.test.html">Wilcoxon Rank Sum test</a> is a special case of
a linear rank test.  The function <br />
<code>twoSampleLinearRankTest</code> is part of
<span class="pkg">EnvStats</span> mainly because this help file gives the necessary background to
explain two-sample linear rank tests for censored data (see <br />
<code><a href="../../EnvStats/help/twoSampleLinearRankTestCensored.html">twoSampleLinearRankTestCensored</a></code>).
</p>


<h3>Usage</h3>

<pre>
  twoSampleLinearRankTest(x, y, location.shift.null = 0, scale.shift.null = 1,
    alternative = "two.sided", test = "wilcoxon", shift.type = "location")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of values for the first sample.
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>,
<code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>y</code></td>
<td>

<p>numeric vector of values for the second sample.
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>,
<code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>location.shift.null</code></td>
<td>

<p>numeric scalar indicating the hypothesized value of <i>&Delta;</i>, the location
shift between the two distributions, under the null hypothesis.  The default value is
<code>location.shift.null=0</code>.  This argument is ignored if <code>shift.type="scale"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>scale.shift.null</code></td>
<td>

<p>numeric scalar indicating the hypothesized value of <i>&tau;</i>, the scale shift
between the two distributions, under the null hypothesis.  The default value is <br />
<code>scale.shift.null=1</code>.  This argument is ignored if <code>shift.type="location"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values
are <code>"two.sided"</code> (the default), <code>"less"</code>, and <code>"greater"</code>.  See the
DETAILS section below for more information.
</p>
</td></tr>
<tr valign="top"><td><code>test</code></td>
<td>

<p>character string indicating which linear rank test to use.  The possible values are:
<code>"wilcoxon"</code> (the default), <code>"normal.scores"</code>, <code>"moods.median"</code>, and
<code>"savage.scores"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>shift.type</code></td>
<td>

<p>character string indicating which kind of shift is being tested.  The possible values
are <code>"location"</code> (the default) and <code>"scale"</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>twoSampleLinearRankTest</code> allows you to compare two samples using
a locally most powerful rank test (LMPRT) to determine whether the two samples come
from the same distribution.  The sections below explain the concepts of location and
scale shifts, linear rank tests, and LMPRT's.
<br />
</p>
<p><b>Definitions of Location and Scale Shifts</b> <br />
Let <i>X</i> denote a random variable representing measurements from group 1 with
cumulative distribution function (cdf):
</p>
<p style="text-align: center;"><i>F_1(t) = Pr(X &le; t) \;\;\;\;\;\; (1)</i></p>

<p>and let <i>x_1, x_2, &hellip;, x_m</i> denote <i>m</i> independent observations from this
distribution.  Let <i>Y</i> denote a random variable from group 2 with cdf:
</p>
<p style="text-align: center;"><i>F_2(t) = Pr(Y &le; t) \;\;\;\;\;\; (2)</i></p>

<p>and let <i>y_1, y_2, &hellip;, y_n</i> denote <i>n</i> independent observations from this
distribution.  Set <i>N = m + n</i>.
<br />
</p>
<p><em>General Hypotheses to Test Differences Between Two Populations</em> <br />
A very general hypothesis to test whether two distributions are the same is
given by:
</p>
<p style="text-align: center;"><i>H_0: F_1(t) = F_2(t), -&infin; &lt; t &lt; &infin; \;\;\;\;\;\; (3)</i></p>

<p>versus the two-sided alternative hypothesis:
</p>
<p style="text-align: center;"><i>H_a: F_1(t) \ne F_2(t) \;\;\;\;\;\; (4)</i></p>

<p>with strict inequality for at least one value of <i>t</i>.
The two possible one-sided hypotheses would be:
</p>
<p style="text-align: center;"><i>H_0: F_1(t) &ge; F_2(t) \;\;\;\;\;\; (5)</i></p>

<p>versus the alternative hypothesis:
</p>
<p style="text-align: center;"><i>H_a: F_1(t) &lt; F_2(t) \;\;\;\;\;\; (6)</i></p>

<p>and
</p>
<p style="text-align: center;"><i>H_0: F_1(t) &le; F_2(t) \;\;\;\;\;\; (7)</i></p>

<p>versus the alternative hypothesis:
</p>
<p style="text-align: center;"><i>H_a: F_1(t) &gt; F_2(t) \;\;\;\;\;\; (8)</i></p>

<p>A similar set of hypotheses to test whether the two distributions are the same are
given by (Conover, 1980, p. 216):
</p>
<p style="text-align: center;"><i>H_0: Pr(X &lt; Y) = 1/2 \;\;\;\;\;\; (9)</i></p>

<p>versus the two-sided alternative hypothesis:
</p>
<p style="text-align: center;"><i>H_a: Pr(X &lt; Y) \ne 1/2 \;\;\;\;\;\; (10)</i></p>

<p>or
</p>
<p style="text-align: center;"><i>H_0: Pr(X &lt; Y) &ge; 1/2 \;\;\;\;\;\; (11)</i></p>

<p>versus the alternative hypothesis:
</p>
<p style="text-align: center;"><i>H_a: Pr(X &lt; Y) &lt; 1/2 \;\;\;\;\;\; (12)</i></p>

<p>or
</p>
<p style="text-align: center;"><i>H_0: Pr(X &lt; Y) &le; 1/2 \;\;\;\;\;\; (13)</i></p>

<p>versus the alternative hypothesis:
</p>
<p style="text-align: center;"><i>H_a: Pr(X &lt; Y) &gt; 1/2 \;\;\;\;\;\; (14)</i></p>

<p>Note that this second set of hypotheses (9)&ndash;(14) is <b>not</b> equivalent to the
set of hypotheses (3)&ndash;(8).  For example, if <i>X</i> takes on the values 1 and 4
with probability 1/2 for each, and <i>Y</i> only takes on values in the interval
(1, 4) with strict inequality at the enpoints (e.g., <i>Y</i> takes on the values
2 and 3 with probability 1/2 for each), then the null hypothesis (9) is
true but the null hypothesis (3) is not true.  However, the null hypothesis (3)
implies the null hypothesis (9), (5) implies (11), and (7) implies (13).
<br />
</p>
<p><em>Location Shift</em> <br />
A special case of the alternative hypotheses (4), (6), and (8) above is the
<b><em>location shift</em></b> alternative:
</p>
<p style="text-align: center;"><i>H_a: F_1(t) = F_2(t - &Delta;) \;\;\;\;\;\; (15)</i></p>

<p>where <i>&Delta;</i> denotes the shift between the two groups.  (Note: some references
refer to (15) above as a shift in the median, but in fact this kind of shift
represents a shift in every single quantile, not just the median.)
If <i>&Delta;</i> is positive, this means that observations in group 1 tend to be
larger than observations in group 2, and if <i>&Delta;</i> is negative, observations
in group 1 tend to be smaller than observations in group 2.
</p>
<p>The alternative hypothesis (15) is called a <b><em>location shift</em></b>: the only
difference between the two distributions is a difference in location (e.g., the
standard deviation is assumed to be the same for both distributions).  A location
shift is not applicable to distributions that are bounded below or above by some
constant, such as a lognormal distribution.  For lognormal distributions, the
location shift could refer to a shift in location of the distribution of the
log-transformed observations.
</p>
<p>For a location shift, the null hypotheses (3) can be generalized as:
</p>
<p style="text-align: center;"><i>H_0: F_1(t) = F_2(t - &Delta;_0), -&infin; &lt; t &lt; &infin; \;\;\;\;\;\; (16)</i></p>

<p>where <i>&Delta;_0</i> denotes the null shift between the two groups.  Almost always,
however, the null shift is taken to be 0 and we will assume this for the rest of this
help file.
</p>
<p>Alternatively, the null and alternative hypotheses can be written as
</p>
<p style="text-align: center;"><i>H_0: &Delta; = 0 \;\;\;\;\;\; (17)</i></p>

<p>versus the alternative hypothesis
</p>
<p style="text-align: center;"><i>H_a: &Delta; &gt; 0 \;\;\;\;\;\; (18)</i></p>

<p>The other one-sided alternative hypothesis (<i>&Delta; &lt; 0</i>) and two-sided
alternative hypothesis (<i>&Delta; \ne 0</i>) could be considered as well.
</p>
<p>The general hypotheses (3)-(14) are <em>not</em> location shift hypotheses
(e.g., the standard deviation does not have to be the same for both distributions),
but they do allow for distributions that are bounded below or above by a constant
(e.g., lognormal distributions).
<br />
</p>
<p><em>Scale Shift</em> <br />
A special kind of scale shift replaces the alternative hypothesis (15) with the
alternative hypothesis:
</p>
<p style="text-align: center;"><i>H_a: F_1(t) = F_2(t/&tau;) \;\;\;\;\;\; (19)</i></p>

<p>where <i>&tau;</i> denotes the shift in scale between the two groups.  Alternatively,
the null and alternative hypotheses for this scale shift can be written as
</p>
<p style="text-align: center;"><i>H_0: &tau; = 1 \;\;\;\;\;\; (20)</i></p>

<p>versus the alternative hypothesis
</p>
<p style="text-align: center;"><i>H_a: &tau; &gt; 1 \;\;\;\;\;\; (21)</i></p>

<p>The other one-sided alternative hypothesis (<i>t &lt; 1</i>) and two-sided alternative
hypothesis (<i>t \ne 1</i>) could be considered as well.
</p>
<p>This kind of scale shift often involves a shift in both location and scale.  For
example, suppose the underlying distribution for both groups is
<a href="../../stats/html/Exponential.html">exponential</a>, with parameter <code>rate=</code><i>&lambda;</i>.  Then
the mean and standard deviation of the reference group is <i>1/&lambda;</i>, while
the mean and standard deviation of the treatment group is <i>&tau;/&lambda;</i>.  In
this case, the alternative hypothesis (21) implies the more general alternative
hypothesis (8).
<br />
</p>
<p><b>Linear Rank Tests</b> <br />
The usual nonparametric test to test the null hypothesis of the same distribution
for both groups versus the location-shift alternative (18) is the
<a href="../../stats/html/wilcox.test.html">Wilcoxon Rank Sum test</a>
(Gilbert, 1987, pp.247-250; Helsel and Hirsch, 1992, pp.118-123;
Hollander and Wolfe, 1999).  Note that the Mann-Whitney U test is equivalent to the
Wilcoxon Rank Sum test (Hollander and Wolfe, 1999; Conover, 1980, p.215,
Zar, 2010).  Hereafter, this test will be abbreviated as the MWW test.  The MWW test
is performed by combining the <i>m</i> <i>X</i> observations with the <i>n</i> <i>Y</i>
observations and ranking them from smallest to largest, and then computing the
statistic
</p>
<p style="text-align: center;"><i>W = &sum;_{i=1}^m R_i \;\;\;\;\;\; (22)</i></p>

<p>where <i>R_1, R_2, &hellip;, R_m</i> denote the ranks of the <i>X</i> observations when
the <i>X</i> and <i>Y</i> observations are combined ranked.  The null
hypothesis (5), (11), or (17) is rejected in favor of the alternative hypothesis
(6), (12)  or (18) if the value of <i>W</i> is too large.  For small sample sizes,
the exact distribution of <i>W</i> under the null hypothesis is fairly easy to
compute and may be found in tables (e.g., Hollander and Wolfe, 1999;
Conover, 1980, pp.448-452).  For larger sample sizes, a normal approximation is
usually used (Hollander and Wolfe, 1999; Conover, 1980, p.217).  For the
<span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/html/wilcox.test.html">wilcox.test</a></code>, an exact p-value is computed if the
samples contain less than 50 finite values and there are no ties.
</p>
<p><b>It is important to note that the MWW test is actually testing the more general
hypotheses (9)-(14) (Conover, 1980, p.216; Divine et al., 2013), even though it
is often presented as only applying to location shifts.</b>
</p>
<p>The MWW W-statistic in Equation (22) is an example of a
<b><em>linear rank statistic</em></b> (Hettmansperger, 1984, p.147; Prentice, 1985),
which is any statistic that can be written in the form:
</p>
<p style="text-align: center;"><i>L = &sum;_{i=1}^m a(R_i) \;\;\;\;\;\; (23)</i></p>

<p>where <i>a()</i> denotes a score function.  Statistics of this form are also called
<b><em>general scores statistics</em></b> (Hettmansperger, 1984, p.147).  The MWW test
uses the identity score function:
</p>
<p style="text-align: center;"><i>a(R_i) = R_i \;\;\;\;\;\; (24)</i></p>

<p>Any test based on a linear rank statistic is called a <b><em>linear rank test</em></b>.
Under the null hypothesis (3), (9), (17), or (20), the distribution of the linear
rank statistic <i>L</i> does not depend on the form of the underlying distribution of
the <i>X</i> and <i>Y</i> observations.  Hence, tests based on <i>L</i> are
nonparametric (also called distribution-free).  If the null hypothesis is not true,
however, the distribution of <i>L</i> will depend not only on the distributions of the
<i>X</i> and <i>Y</i> observations, but also upon the form the score function
<i>a()</i>.
<br />
</p>
<p><b>Locally Most Powerful Linear Rank Tests</b> <br />
The decision of what scores to use may be based on considering the power of the test.
A locally most powerful rank test (LMPRT) of the null hypothesis (17) versus the
alternative (18) maximizes the slope of the power (as a function of <i>&Delta;</i>) in
the neighborhood where <i>&Delta;=0</i>.  A LMPRT of the null hypothesis (20) versus
the alternative (21) maximizes the slope of the power (as a function of <i>&tau;</i>)
in the neighborhood where <i>&tau;=1</i>.  That is, LMPRT's are the best linear rank
test you can use for detecting small shifts in location or scale.
</p>
<p>Table 1 below shows the score functions associated with the LMPRT's for various
assumed underlying distributions (Hettmansperger, 1984, Chapter 3;
Millard and Deverel, 1988, p.2090).  A test based on the identity score function of
Equation (24) is equivalent to a test based on the score shown in Table 1 associated
with the logistic distribution, thus the MWW test is the LMPRT for detecting a
location shift when the underlying observations follow the logistic distribution.
When the underlying distribution is normal or lognormal, the LMPRT for a location
shift uses the &ldquo;Normal scores&rdquo; shown in Table 1.  When the underlying
distribution is exponential, the LMPRT for detecting a scale shift is based on the
&ldquo;Savage scores&rdquo; shown in Table 1.
</p>
<p><em>Table 1.  Scores of LMPRT's for Various Distributions</em><br />
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
  <b>Distribution</b> </td><td style="text-align: left;"> <b>Score</b> <i>a(R_i)</i>  </td><td style="text-align: left;"> <b>Shift Type</b> </td><td style="text-align: left;"> <b>Test Name</b> </td>
</tr>
<tr>
 <td style="text-align: left;">
  Logistic            </td><td style="text-align: left;"> <i>[2/(N+1)]R_i - 1</i>     </td><td style="text-align: left;"> Location          </td><td style="text-align: left;"> Wilcoxon Rank Sum </td>
</tr>
<tr>
 <td style="text-align: left;">
                      </td><td style="text-align: left;">                            </td><td style="text-align: left;">                   </td><td style="text-align: left;">                   </td>
</tr>
<tr>
 <td style="text-align: left;">
  Normal or           </td><td style="text-align: left;"> <i>&Phi;^{-1}[R_i/(N+1)]</i>* </td><td style="text-align: left;"> Location          </td><td style="text-align: left;"> Van der Waerden or </td>
</tr>
<tr>
 <td style="text-align: left;">
  Lognormal (log-scale) </td><td style="text-align: left;">                          </td><td style="text-align: left;">                   </td><td style="text-align: left;"> Normal scores     </td>
</tr>
<tr>
 <td style="text-align: left;">
                      </td><td style="text-align: left;">                            </td><td style="text-align: left;">                   </td><td style="text-align: left;">                   </td>
</tr>
<tr>
 <td style="text-align: left;">
  Double Exponential  </td><td style="text-align: left;"> <i>sign[R_i - (N+1)/2]</i>  </td><td style="text-align: left;"> Location          </td><td style="text-align: left;"> Mood's Median     </td>
</tr>
<tr>
 <td style="text-align: left;">
                      </td><td style="text-align: left;">                            </td><td style="text-align: left;">                   </td><td style="text-align: left;">                   </td>
</tr>
<tr>
 <td style="text-align: left;">
  Exponential or      </td><td style="text-align: left;"> <i>&sum;_{j=1}^{R_i} (N-j+1)^{-1}</i> </td><td style="text-align: left;"> Scale    </td><td style="text-align: left;"> Savage scores     </td>
</tr>
<tr>
 <td style="text-align: left;">
  Extreme Value       </td><td style="text-align: left;">                            </td><td style="text-align: left;">                   </td><td style="text-align: left;">
  </td>
</tr>

</table>

<p>* Denotes an approximation to the true score.  The symbol <i>&Phi;</i> denotes the
cumulative distribution function of the standard normal distribution, and <i>sign</i>
denotes the <code><a href="../../base/html/sign.html">sign</a></code> function.
</p>
<p>A large sample normal approximation to the distribution of the linear rank statistic
<i>L</i> for arbitrary score functions is given by Hettmansperger (1984, p.148).
Under the null hypothesis (17) or (20), the mean and variance of <i>L</i> are given by:
</p>
<p style="text-align: center;"><i>E(L) = &mu;_L = \frac{m}{N} &sum;_{i=1}^N a_i = m \bar{a} \;\;\;\;\;\; (24)</i></p>

<p style="text-align: center;"><i>Var(L) = &sigma;_L^2 = \frac{mn}{N(N-1)} &sum;_{i=1}^N (a_i - \bar{a})^2 \;\;\;\;\;\; (25)</i></p>

<p>Hettmansperger (1984, Chapter 3) shows that under the null hypothesis of no
difference between the two groups, the statistic
</p>
<p style="text-align: center;"><i>z = \frac{L - &mu;_L}{&sigma;_L} \;\;\;\;\;\; (26)</i></p>

<p>is approximately distributed as a standard normal random variable for
&ldquo;large&rdquo; sample sizes.  This statistic will tend to be large if the
observations in group 1 tend to be larger than the observations in group 2.
</p>


<h3>Value</h3>

<p>a list of class <code>"htest"</code> containing the results of the hypothesis test.
See the help file for <code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/html/wilcox.test.html">Wilcoxon Rank Sum test</a>, also known as the Mann-Whitney U
test, is the standard nonparametric test used to test for differences between two
groups (e.g., Zar, 2010; USEPA, 2009, pp.16-14 to 16-20).  Other possible
nonparametric tests include linear rank tests based on scores other than the ranks,
including the &ldquo;normal scores&rdquo; test and the &ldquo;Savage scores&rdquo; tests.
The normal scores test is actually slightly more powerful than the Wilcoxon Rank Sum
test for detecting small shifts in location if the underlying distribution is normal
or lognormal.  In general, however, there will be little difference between these
two tests.
</p>
<p>The results of calling the function <code>twoSampleLinearRankTest</code> with the
argument <code>test="wilcoxon"</code> will match those of calling the built-in
<span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/html/wilcox.test.html">wilcox.test</a></code> with the arguments <code>exact=FALSE</code> and
<code>correct=FALSE</code>.  In general, it is better to use the built-in function
<code><a href="../../stats/html/wilcox.test.html">wilcox.test</a></code> for performing the Wilcoxon Rank Sum test, since this
function can compute exact (rather than approximate) p-values.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Conover, W.J. (1980).  <em>Practical Nonparametric Statistics</em>.  Second Edition.
John Wiley and Sons, New York, Chapter 4.
</p>
<p>Divine, G., H.J. Norton, R. Hunt, and J. Dinemann. (2013).  A Review of Analysis
and Sample Size Calculation Considerations for Wilcoxon Tests.  <em>Anesthesia
&amp; Analgesia</em> <b>117</b>, 699&ndash;710.
</p>
<p>Hettmansperger, T.P. (1984).  <em>Statistical Inference Based on Ranks</em>.
John Wiley and Sons, New York, 323pp.
</p>
<p>Hollander, M., and D.A. Wolfe. (1999). <em>Nonparametric Statistical Methods,
Second Edition</em>.  John Wiley and Sons, New York.
</p>
<p>Millard, S.P., and S.J. Deverel. (1988).  Nonparametric Statistical Methods for
Comparing Two Sites Based on Data With Multiple Nondetect Limits.
<em>Water Resources Research</em>, <b>24</b>(12), 2087&ndash;2098.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001).  <em>Environmental Statistics with
S-PLUS</em>.  CRC Press, Boca Raton, FL, pp.432&ndash;435.
</p>
<p>Prentice, R.L. (1985).  Linear Rank Tests.  In Kotz, S., and N.L. Johnson, eds.
<em>Encyclopedia of Statistical Science</em>.  John Wiley and Sons, New York.
Volume 5, pp.51&ndash;58.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition.
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><code><a href="../../stats/html/wilcox.test.html">wilcox.test</a></code>, <code><a href="../../EnvStats/help/twoSampleLinearRankTestCensored.html">twoSampleLinearRankTestCensored</a></code>,
<code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 15 observations from a normal distribution with parameters
  # mean=3 and sd=1.  Call these the observations from the reference group.
  # Generate 10 observations from a normal distribution with parameters
  # mean=3.5 and sd=1.  Call these the observations from the treatment group.
  # Compare the results of calling wilcox.test to those of calling
  # twoSampleLinearRankTest with test="normal.scores".
  # (The call to set.seed allows you to reproduce this example.)

  set.seed(346)
  x &lt;- rnorm(15, mean = 3)
  y &lt;- rnorm(10, mean = 3.5)

  wilcox.test(x, y)

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 location shift = 0
  #
  #Alternative Hypothesis:          True location shift is not equal to 0
  #
  #Test Name:                       Wilcoxon rank sum test
  #
  #Data:                            x and y
  #
  #Test Statistic:                  W = 32
  #
  #P-value:                         0.0162759


  twoSampleLinearRankTest(x, y, test = "normal.scores")

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 Fy(t) = Fx(t)
  #
  #Alternative Hypothesis:          Fy(t) != Fx(t) for at least one t
  #
  #Test Name:                       Two-Sample Linear Rank Test:
  #                                 Normal Scores Test
  #                                 Based on Normal Approximation
  #
  #Data:                            x = x
  #                                 y = y
  #
  #Sample Sizes:                    nx = 15
  #                                 ny = 10
  #
  #Test Statistic:                  z = -2.431099
  #
  #P-value:                         0.01505308

  #----------

  # Clean up
  #---------
  rm(x, y)

  #==========

  # Following Example 6.6 on pages 6.22-6.26 of USEPA (1994b), perform the
  # Wilcoxon Rank Sum test for the TcCB data (stored in EPA.94b.tccb.df).
  # There are m=47 observations from the reference area and n=77 observations
  # from the cleanup unit.  Then compare the results using the other available
  # linear rank tests.  Note that Mood's median test yields a p-value less
  # than 0.10, while the other tests yield non-significant p-values.
  # In this case, Mood's median test is picking up the residual contamination
  # in the cleanup unit. (See the example in the help file for quantileTest.)

  names(EPA.94b.tccb.df)
  #[1] "TcCB.orig" "TcCB"      "Censored"  "Area"

  summary(EPA.94b.tccb.df$Area)
  #  Cleanup Reference
  #       77        47

  with(EPA.94b.tccb.df,
    twoSampleLinearRankTest(TcCB[Area=="Cleanup"], TcCB[Area=="Reference"]))

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 Fy(t) = Fx(t)
  #
  #Alternative Hypothesis:          Fy(t) != Fx(t) for at least one t
  #
  #Test Name:                       Two-Sample Linear Rank Test:
  #                                 Wilcoxon Rank Sum Test
  #                                 Based on Normal Approximation
  #
  #Data:                            x = TcCB[Area == "Cleanup"]
  #                                 y = TcCB[Area == "Reference"]
  #
  #Sample Sizes:                    nx = 77
  #                                 ny = 47
  #
  #Test Statistic:                  z = -1.171872
  #
  #P-value:                         0.2412485

  with(EPA.94b.tccb.df,
    twoSampleLinearRankTest(TcCB[Area=="Cleanup"],
      TcCB[Area=="Reference"], test="normal.scores"))$p.value
  #[1] 0.3399484

  with(EPA.94b.tccb.df,
    twoSampleLinearRankTest(TcCB[Area=="Cleanup"],
      TcCB[Area=="Reference"], test="moods.median"))$p.value
  #[1] 0.09707393

  with(EPA.94b.tccb.df,
    twoSampleLinearRankTest(TcCB[Area=="Cleanup"],
      TcCB[Area=="Reference"], test="savage.scores"))$p.value
  #[1] 0.2884351
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
