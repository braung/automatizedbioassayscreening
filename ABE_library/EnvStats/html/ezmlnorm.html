<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Zero-Modified Lognormal (Delta)...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for ezmlnorm {EnvStats}"><tr><td>ezmlnorm {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Zero-Modified Lognormal (Delta) Distribution
</h2>

<h3>Description</h3>

<p>Estimate the parameters of a 
<a href="../../EnvStats/help/ZeroModifiedLognormal.html">zero-modified lognormal distribution</a> or a 
<a href="../../EnvStats/help/ZeroModifiedLognormalAlt.html">zero-modified lognormal distribution (alternative parameterization)</a>, 
and optionally construct a confidence interval for the mean.
</p>


<h3>Usage</h3>

<pre>
  ezmlnorm(x, method = "mvue", ci = FALSE, ci.type = "two-sided", 
    ci.method = "normal.approx", conf.level = 0.95)

  ezmlnormAlt(x, method = "mvue", ci = FALSE, ci.type = "two-sided", 
    ci.method = "normal.approx", conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.  Missing (<code>NA</code>), undefined (<code>NaN</code>), 
and infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  The only possible value is 
<code>"mvue"</code> (minimum variance unbiased; the default).  See the DETAILS section for 
more information on this estimation method. 
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the 
mean.  The default value is <code>FALSE</code>.  If <code>ci=TRUE</code> and there are less 
than three non-missing observations in <code>x</code>, or if all observations are 
zeros, a warning will be issued and no confidence interval will be computed.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence 
interval for the mean.  The only possible value is <code>"normal.approx"</code> 
(the default).  See the DETAILS section for more information. 
This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>. This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from a 
<a href="../../EnvStats/help/ZeroModifiedLognormal.html">zero-modified lognormal distribution</a> with 
parameters <code>meanlog=</code><i>&mu;</i>, <code>sdlog=</code><i>&sigma;</i>, and 
<code>p.zero=</code><i>p</i>.  Alternatively, let 
<i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from a 
<a href="../../EnvStats/help/ZeroModifiedLognormalAlt.html">zero-modified lognormal distribution 
(alternative parameterization)</a> with parameters <code>mean=</code><i>&theta;</i>, 
<code>cv=</code><i>&tau;</i>, and <code>p.zero=</code><i>p</i>.
</p>
<p>Let <i>r</i> denote the number of observations in <i>\underline{x}</i> that are equal 
to 0, and order the observations so that <i>x_1, x_2, &hellip;, x_r</i> denote 
the <i>r</i> zero observations and <i>x_{r+1}, x_{r+2}, &hellip;, x_n</i> denote 
the <i>n-r</i> non-zero observations.
</p>
<p>Note that <i>&theta;</i> is <em>not</em> the mean of the zero-modified lognormal 
distribution; it is the mean of the lognormal part of the distribution.  Similarly, 
<i>&tau;</i> is <em>not</em> the coefficient of variation of the zero-modified 
lognormal distribution; it is the coefficient of variation of the lognormal 
part of the distribution.
</p>
<p>Let <i>&gamma;</i>, <i>&delta;</i>, and <i>&phi;</i> denote the mean, standard deviation, 
and coefficient of variation of the overall zero-modified lognormal (delta) 
distribution.  Let <i>&eta;</i> denote the standard deviation of the lognormal 
part of the distribution, so that <i>&eta; = &theta; &tau;</i>.  Aitchison (1955) 
shows that:
</p>
<p style="text-align: center;"><i>&gamma; = (1 - p) &theta;  \;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>&delta;^2 = (1 - p) &eta;^2 + p (1 - p) &theta;^2 \;\;\;\; (2)</i></p>

<p>so that
</p>
<p style="text-align: center;"><i>&phi; = \frac{&delta;}{&gamma;} = \frac{&radic;{&tau;^2 + p}}{&radic;{1-p}} \;\;\;\; (3)</i></p>

<p><br />
</p>
<p><b>Estimation</b> <br />
</p>
<p><em>Minimum Variance Unbiased Estimation</em> (<code>method="mvue"</code>) <br />
Aitchison (1955) shows that the minimum variance unbiased estimators (mvue's) of 
<i>&gamma;</i> and <i>&delta;</i> are:
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>\hat{&gamma;}_{mvue} =</i> </td><td style="text-align: left;"> <i>(1-\frac{r}{n}) e^{\bar{y}} g_{n-r-1}(\frac{s^2}{2})</i> </td><td style="text-align: left;"> if <i>r &lt; n - 1</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                                </td><td style="text-align: left;"> <i>x_n / n</i> </td><td style="text-align: left;"> if <i>r = n - 1</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                                </td><td style="text-align: left;"> <i>0</i> </td><td style="text-align: left;"> if <i>r = n \;\;\;\; (4)</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
                                </td><td style="text-align: left;">         </td><td style="text-align: left;"> </td>
</tr>
<tr>
 <td style="text-align: left;">
    <i>\hat{&delta;}^2_{mvue} =</i> </td><td style="text-align: left;"> <i>(1-\frac{r}{n}) e^{2\bar{y}} \{g_{n-r-1}(2s^2) - \frac{n-r-1}{n-1} g_{n-r-1}[\frac{(n-r-2)s^2}{n-r-1}] \} </i> </td><td style="text-align: left;"> if <i>r &lt; n - 1</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                                  </td><td style="text-align: left;"> <i>x_n^2 / n</i> </td><td style="text-align: left;"> if <i>r = n - 1</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                                  </td><td style="text-align: left;"> <i>0</i> </td><td style="text-align: left;"> if <i>r = n \;\;\;\; (5)</i>
  </td>
</tr>

</table>

<p>where 
</p>
<p style="text-align: center;"><i>y_i = log(x_i), \; r = r+1, r+2, &hellip;, n \;\;\;\; (6)</i></p>

<p style="text-align: center;"><i>\bar{y} = \frac{1}{n-r} &sum;_{i=r+1}^n y_i \;\;\;\; (7)</i></p>

<p style="text-align: center;"><i>s^2 = \frac{1}{n-r-1} &sum;_{i=r+1}^n (y_i - \bar{y})^2 \;\;\;\; (8)</i></p>

<p style="text-align: center;"><i>g_m(z) = &sum;_{i=0}^&infin; \frac{m^i (m+2i)}{m(m+2) \cdots (m+2i)} (\frac{m}{m+1})^i (\frac{z^i}{i!}) \;\;\;\; (9)</i></p>

<p>Note that when <i>r=n-1</i> or <i>r=n</i>, the estimator of <i>&gamma;</i> is simply the 
sample mean for all observations (including zero values), and the estimator for 
<i>&delta;^2</i> is simply the sample variance for all observations.
</p>
<p>The expected value and asymptotic variance of the mvue of <i>&gamma;</i> are 
(Aitchison and Brown, 1957, p.99; Owen and DeRouen, 1980):
</p>
<p style="text-align: center;"><i>E(\hat{&gamma;}_{mvue}) = &gamma; \;\;\;\; (10)</i></p>

<p style="text-align: center;"><i>AVar(\hat{&gamma;}_{mvue}) = \frac{1}{n} exp(2&mu; + &sigma;^2) (1-p) (p + \frac{2&sigma;^2 + &sigma;^4}{2}) \;\;\;\; (11)</i></p>

<p><br />
</p>
<p><b>Confidence Intervals</b> <br />
</p>
<p><em>Based on Normal Approximation</em> (<code>ci.method="normal.approx"</code>) <br />
An approximate <i>(1-&alpha;)100\%</i> confidence interval for <i>&gamma;</i> is 
constructed based on the assumption that the estimator of <i>&gamma;</i> is 
approximately normally distributed.  Thus, an approximate two-sided 
<i>(1-&alpha;)100\%</i> confidence interval for <i>&gamma;</i> is constructed as:
</p>
<p style="text-align: center;"><i>[ \hat{&gamma;}_{mvue} - t_{n-2, 1-&alpha;/2} \hat{&sigma;}_{\hat{&gamma;}}, \; \hat{&gamma;}_{mvue} + t_{n-2, 1-&alpha;/2} \hat{&sigma;}_{\hat{&gamma;}} ] \;\;\;\; (12)</i></p>

<p>where <i>t_{&nu;, p}</i> is the <i>p</i>'th quantile of 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>&nu;</i> degrees of freedom, and 
the quantity <i>\hat{&sigma;}_{\hat{&gamma;}}</i> is the estimated standard deviation 
of the mvue of <i>&gamma;</i>, and is computed by replacing the values of 
<i>&mu;</i>, <i>&sigma;</i>, and <i>p</i> in equation (11) above with their estimated 
values and taking the square root.
</p>
<p>Note that there must be at least 3 non-missing observations (<i>n &ge; 3</i>) and 
at least one observation must be non-zero (<i>r &le; n-1</i>) in order to construct 
a confidence interval.
</p>
<p>One-sided confidence intervals are computed in a similar fashion.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information.  
See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>For the function <code>ezmlnorm</code>, the component called <code>parameters</code> is a 
numeric vector with the following estimated parameters: 
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <b>Parameter Name</b> </td><td style="text-align: left;"> <b>Explanation</b> </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>meanlog</code> </td><td style="text-align: left;"> mean of the log of the lognormal part of the distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>sdlog</code>   </td><td style="text-align: left;"> standard deviation of the log of the lognormal part of the distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>p.zero</code>  </td><td style="text-align: left;"> probability that an observation will be 0. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>mean.zmlnorm</code> </td><td style="text-align: left;"> mean of the overall zero-modified lognormal (delta) distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>sd.zmlnorm</code> </td><td style="text-align: left;"> standard deviation of the overall zero-modified lognormal (delta) distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
  </td>
</tr>

</table>

<p>For the function <code>ezmlnormAlt</code>, the component called <code>parameters</code> is a 
numeric vector with the following estimated parameters: 
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <b>Parameter Name</b> </td><td style="text-align: left;"> <b>Explanation</b> </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>mean</code> </td><td style="text-align: left;"> mean of the lognormal part of the distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>cv</code>   </td><td style="text-align: left;"> coefficient of variation of the lognormal part of the distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>p.zero</code>  </td><td style="text-align: left;"> probability that an observation will be 0. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>mean.zmlnorm</code> </td><td style="text-align: left;"> mean of the overall zero-modified lognormal (delta) distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
    <code>sd.zmlnorm</code> </td><td style="text-align: left;"> standard deviation of the overall zero-modified lognormal (delta) distribution. </td>
</tr>
<tr>
 <td style="text-align: left;">
  </td>
</tr>

</table>



<h3>Note</h3>

<p>The zero-modified lognormal (delta) distribution is sometimes used to 
model chemical concentrations for which some observations are reported as 
&ldquo;Below Detection Limit&rdquo; (the nondetects are assumed equal to 0).  
See, for example, Gilliom and Helsel (1986), Owen and DeRouen (1980), and 
Gibbons et al. (2009, Chapter 12).  USEPA (2009, Chapter 15) recommends this 
strategy only in specific situations, and Helsel (2012, Chapter 1) strongly 
discourages this approach to dealing with non-detects.
</p>
<p>A variation of the zero-modified lognormal (delta) distribution is the 
<a href="../../EnvStats/help/ZeroModifiedNormal.html">zero-modified normal distribution</a>, in which a 
normal distribution is mixed with a positive probability mass at 0. 
</p>
<p>One way to try to assess whether a zero-modified lognormal (delta), 
zero-modified normal, censored normal, or censored lognormal is the best 
model for the data is to construct both censored and detects-only probability 
plots (see <code><a href="../../EnvStats/help/qqPlotCensored.html">qqPlotCensored</a></code>).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Aitchison, J. (1955).  On the Distribution of a Positive Random Variable Having 
a Discrete Probability Mass at the Origin.  <em>Journal of the American 
Statistical Association</em> <b>50</b>, 901&ndash;908.
</p>
<p>Aitchison, J., and J.A.C. Brown (1957).  <em>The Lognormal Distribution 
(with special reference to its uses in economics)</em>.  Cambridge University Press, 
London. pp.94-99.
</p>
<p>Crow, E.L., and K. Shimizu. (1988).  <em>Lognormal Distributions: 
Theory and Applications</em>.  Marcel Dekker, New York, pp.47&ndash;51.
</p>
<p>Gibbons, RD., D.K. Bhaumik, and S. Aryal. (2009).  <em>Statistical Methods 
for Groundwater Monitoring</em>.  Second Edition.  John Wiley and Sons, Hoboken, NJ.
</p>
<p>Gilliom, R.J., and D.R. Helsel. (1986).  Estimation of Distributional Parameters 
for Censored Trace Level Water Quality Data: 1. Estimation Techniques.  
<em>Water Resources Research</em> <b>22</b>, 135&ndash;146.
</p>
<p>Helsel, D.R. (2012).  <em>Statistics for Censored Environmental Data Using 
Minitab and R</em>.  Second Edition.  John Wiley and Sons, Hoboken, NJ, Chapter 1.
</p>
<p>Johnson, N. L., S. Kotz, and A.W. Kemp. (1992).  <em>Univariate Discrete Distributions</em>. 
Second Edition. John Wiley and Sons, New York, p.312.
</p>
<p>Owen, W., and T. DeRouen. (1980).  Estimation of the Mean for Lognormal Data 
Containing Zeros and Left-Censored Values, with Applications to the Measurement 
of Worker Exposure to Air Contaminants.  <em>Biometrics</em> <b>36</b>, 707&ndash;719.
</p>
<p>USEPA (1992c).  <em>Statistical Analysis of Ground-Water Monitoring Data at 
RCRA Facilities: Addendum to Interim Final Guidance</em>.  Office of Solid Waste, 
Permits and State Programs Division, US Environmental Protection Agency, 
Washington, D.C.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/ZeroModifiedLognormal.html">Zero-Modified Lognormal</a>, 
<a href="../../EnvStats/help/ZeroModifiedNormal.html">Zero-Modified Normal</a>, <a href="../../stats/help/Lognormal.html">Lognormal</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 100 observations from a zero-modified lognormal (delta) 
  # distribution with mean=2, cv=1, and p.zero=0.5, then estimate the 
  # parameters. According to equations (1) and (3) above, the overall mean 
  # is mean.zmlnorm=1 and the overall cv is cv.zmlnorm=sqrt(3). 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rzmlnormAlt(100, mean = 2, cv = 1, p.zero = 0.5) 
  ezmlnormAlt(dat, ci = TRUE) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Zero-Modified Lognormal (Delta)
  #
  #Estimated Parameter(s):          mean         = 1.9604561
  #                                 cv           = 0.9169411
  #                                 p.zero       = 0.4500000
  #                                 mean.zmlnorm = 1.0782508
  #                                 cv.zmlnorm   = 1.5307175
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     100
  #
  #Confidence Interval for:         mean.zmlnorm
  #
  #Confidence Interval Method:      Normal Approximation
  #                                 (t Distribution)
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = 0.748134
  #                                 UCL = 1.408368

  #----------

  # Clean up
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
