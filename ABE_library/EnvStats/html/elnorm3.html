<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Three-Parameter Lognormal...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for elnorm3 {EnvStats}"><tr><td>elnorm3 {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Three-Parameter Lognormal Distribution (Log-Scale)
</h2>

<h3>Description</h3>

<p>Estimate the mean, standard deviation, and threshold parameters for a 
<a href="../../EnvStats/help/Lognormal3.html">three-parameter lognormal distribution</a>, and optionally 
construct a confidence interval for the threshold or the median of the distribution.
</p>


<h3>Usage</h3>

<pre>
  elnorm3(x, method = "lmle", ci = FALSE, ci.parameter = "threshold", 
    ci.method = "avar", ci.type = "two-sided", conf.level = 0.95, 
    threshold.lb.sd = 100, evNormOrdStats.method = "royston")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Possible values are:
</p>
 
<ul>
<li> <p><code>"lmle"</code> (local maximum likelihood; the default)
</p>
</li>
<li> <p><code>"mme"</code> (method of moments)
</p>
</li>
<li> <p><code>"mmue"</code> (method of moments using an unbaised estimate of variance)
</p>
</li>
<li> <p><code>"mmme"</code> (modified method of moments due to Cohen and Whitten (1980))
</p>
</li>
<li> <p><code>"zero.skew"</code> (zero-skewness estimator due to Griffiths (1980))
</p>
</li>
<li> <p><code>"royston.skew"</code> (estimator based on Royston's (1992b) index of skewness).  
</p>
</li></ul>

<p>See the DETAILS section for more information.
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for either 
the threshold or median of the distribution.  The default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.parameter</code></td>
<td>

<p>character string indicating the parameter for which the confidence interval is 
desired.  The possible values are <code>"threshold"</code> (the default) and 
<code>"median"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating the method to use to construct the confidence interval.  
The possible values are <code>"avar"</code> (asymptotic variance; the default), <br />
<code>"likelihood.profile"</code>, and <code>"skewness"</code> (method suggested by Royston 
(1992b) for <code>method="zero.skew"</code>).  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>. This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>threshold.lb.sd</code></td>
<td>

<p>a positive numeric scalar specifying the range over which to look for the 
local maximum likelihood (<code>method="lmle"</code>) or zero-skewness <br />
(<code>method="zero.skewness"</code>) estimator of threshold.  The range is set to <br />
<code>[ mean(x) - threshold.lb.sd * sd(x), min(x) ]</code>.  If you receive a warning 
message that <code>elnorm3</code> is unable to find an acceptable estimate of threshold 
in this range, it may be because of convergence problems specific to the data in 
<code>x</code>.  When this occurs, try changing the value of <code>threshold.lb.sd</code>.  This 
same range is used in constructing confidence intervals for the threshold parameter.  
The default value is <code>threshold.lb.sd=100</code>.  This argument is relevant only if 
<code>method="lmle"</code>, <code>method="zero.skew"</code>, 
<code>ci.method="likelihood.profile"</code>, and/or <code>ci.method="skewness"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>evNormOrdStats.method</code></td>
<td>

<p>character string indicating which method to use in the call to 
<code>link{evNormOrdStatsScalar}</code> when <code>method="mmme"</code>.  See the DETAILS 
section for more information. 
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>X</i> denote a random variable from a 
<a href="../../EnvStats/help/Lognormal3.html">three-parameter lognormal distribution</a> with 
parameters <code>meanlog=</code><i>&mu;</i>, <code>sdlog=</code><i>&sigma;</i>, and 
<code>threshold=</code><i>&gamma;</i>.  Let <i>\underline{x}</i> denote a vector of 
<i>n</i> observations from this distribution.  Furthermore, let <i>x_{(i)}</i> denote 
the <i>i</i>'th order statistic in the sample, so that <i>x_{(1)}</i> denotes the 
smallest value and <i>x_{(n)}</i> denote the largest value in <i>\underline{x}</i>.  
Finally, denote the sample mean and variance by:
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (2)</i></p>

<p>Note that the sample variance is the unbiased version.  Denote the method of 
moments estimator of variance by:
</p>
<p style="text-align: center;"><i>s^2_m = \frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (3)</i></p>

<p><b>Estimation</b> <br />
</p>
<p><em>Local Maximum Likelihood Estimation</em> (<code>method="lmle"</code>) <br />
Hill (1963) showed that the likelihood function approaches infinity as <i>&gamma;</i> 
approaches <i>x_{(1)}</i>, so that the global maximum likelihood estimators of 
<i>(&mu;, &sigma;, &gamma;)</i> are <i>(-&infin;, &infin;, x_{(1)})</i>, which are 
inadmissible, since <i>&gamma;</i> must be smaller than <i>x_{(1)}</i>.  Cohen (1951) 
suggested using local maximum likelihood estimators (lmle's), derived by equating 
partial derivatives of the log-likelihood function to zero.  These estimators were 
studied by Harter and Moore (1966), Calitz (1973), Cohen and Whitten (1980), and 
Griffiths (1980), and appear to possess most of the desirable properties ordinarily 
associated with maximum likelihood estimators.
</p>
<p>Cohen (1951) showed that the lmle of <i>&gamma;</i> is given by the solution to the 
following equation:
</p>
<p style="text-align: center;"><i>[&sum;_{i=1}^n \frac{1}{w_i}] \, \{&sum;_{i=1}^n y_i - &sum;_{i=1}^n y_i^2 + \frac{1}{n}[&sum;_{i=1}^n y_i]^2 \} - n &sum;_{i=1}^n \frac{y_i}{w_i} = 0 \;\;\;\; (4)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>w_i = x_i - \hat{&gamma;} \;\;\;\; (5)</i></p>

<p style="text-align: center;"><i>y_i = log(x_i - \hat{&gamma;}) = log(w_i) \;\;\;\; (6)</i></p>

<p>and that the lmle's of <i>&mu;</i> and <i>&sigma;</i> then follow as:
</p>
<p style="text-align: center;"><i>\hat{&mu;} = \frac{1}{n} &sum;_{i=1}^n y_i = \bar{y} \;\;\;\; (7)</i></p>

<p style="text-align: center;"><i>\hat{&sigma;}^2 = \frac{1}{n} &sum;_{i=1}^n (y_i - \bar{y})^2 \;\;\;\; (8)</i></p>

<p>Unfortunately, while equation (4) simplifies the task of computing the lmle's, 
for certain data sets there still may be convergence problems (Calitz, 1973), and 
occasionally multiple roots of equation (4) may exist.  When multiple roots to 
equation (4) exisit, Cohen and Whitten (1980) recommend using the one that results 
in closest agreement between the mle of <i>&mu;</i> (equation (7)) and the sample 
mean (equation (1)).
</p>
<p>On the other hand, Griffiths (1980) showed that for a given value of the threshold 
parameter <i>&gamma;</i>, the maximized value of the log-likelihood (the 
&ldquo;profile likelihood&rdquo; for <i>&gamma;</i>) is given by:
</p>
<p style="text-align: center;"><i>log[L(&gamma;)] = \frac{-n}{2} [1 + log(2&pi;) + 2\hat{&mu;} + log(\hat{&sigma;}^2) ] \;\;\;\; (9)</i></p>

<p>where the estimates of <i>&mu;</i> and <i>&sigma;</i> are defined in equations (7) 
and (8), so the lmle of <i>&gamma;</i> reduces to an iterative search over the values 
of <i>&gamma;</i>.  Griffiths (1980) noted that the distribution of the lmle of 
<i>&gamma;</i> is far from normal and that <i>log[L(&gamma;)]</i> is not quadratic 
near the lmle of <i>&gamma;</i>.  He suggested a better parameterization based on
</p>
<p style="text-align: center;"><i>&eta; = -log(x_{(1)} - &gamma;) \;\;\;\; (10)</i></p>

<p>Thus, once the lmle of <i>&eta;</i> is found using equations (9) and (10), the lmle of 
<i>&gamma;</i> is given by:
</p>
<p style="text-align: center;"><i>\hat{&gamma;} = x_{(1)} - exp(-\hat{&eta;}) \;\;\;\; (11)</i></p>

<p>When <code>method="lmle"</code>, the function <code>elnorm3</code> uses the function 
<code><a href="../../stats/html/nlminb.html">nlminb</a></code> to search for the minimum of <i>-2log[L(&eta;)]</i>, using the 
modified method of moments estimator (<code>method="mmme"</code>; see below) as the 
starting value for <i>&gamma;</i>.  Equation (11) is then used to solve for the 
lmle of <i>&gamma;</i>, and equation (4) is used to &ldquo;fine tune&rdquo; the estimated 
value of <i>&gamma;</i>.  The lmle's of <i>&mu;</i> and <i>&sigma;</i> are then computed 
using equations (6)-(8).
<br />
</p>
<p><em>Method of Moments Estimation</em> (<code>method="mme"</code>) <br />  
Denote the <i>r</i>'th sample central moment by:
</p>
<p style="text-align: center;"><i>m_r = \frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^r \;\;\;\; (12)</i></p>

<p>and note that
</p>
<p style="text-align: center;"><i>s^2_m = m_2 \;\;\;\; (13)</i></p>

<p>Equating the sample first moment (the sample mean) with its population value 
(the population mean), and equating the second and third sample central moments 
with their population values yields (Johnson et al., 1994, p.228):
</p>
<p style="text-align: center;"><i>\bar{x} = &gamma; + &beta; &radic;{&omega;} \;\;\;\; (14)</i></p>

<p style="text-align: center;"><i>m_2 = s^2_m = &beta;^2 &omega; (&omega; - 1) \;\;\;\; (15)</i></p>

<p style="text-align: center;"><i>m_3 = &beta;^3 &omega;^{3/2} (&omega; - 1)^2 (&omega; + 2) \;\;\;\; (16)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>&beta; = exp(&mu;) \;\;\;\; (17)</i></p>

<p style="text-align: center;"><i>&omega; = exp(&sigma;^2) \;\;\;\; (18)</i></p>

<p>Combining equations (15) and (16) yields:
</p>
<p style="text-align: center;"><i>b_1 = \frac{m_3}{m_2^{3/2}} = (&omega; + 2) &radic;{&omega; - 1} \;\;\;\; (19)</i></p>

<p>The quantity on the left-hand side of equation (19) is the usual estimator of 
skewness.  Solving equation (19) for <i>&omega;</i> yields:
</p>
<p style="text-align: center;"><i>\hat{&omega;} = (d + h)^{1/3} + (d - h)^{1/3} - 1 \;\;\;\; (20)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>d = 1 + \frac{b_1}{2} \;\;\;\; (21)</i></p>

<p style="text-align: center;"><i>h = sqrt{d^2 - 1} \;\;\;\; (22)</i></p>

<p>Using equation (18), the method of moments estimator of <i>&sigma;</i> is then 
computed as:
</p>
<p style="text-align: center;"><i>\hat{&sigma;}^2 = log(\hat{&omega;}) \;\;\;\; (23)</i></p>

<p>Combining equations (15) and (17), the method of moments estimator of <i>&mu;</i> 
is computed as:
</p>
<p style="text-align: center;"><i>\hat{&mu;} = \frac{1}{2} log[\frac{s^2_m}{\hat{omega}(\hat{&omega;} - 1)}] \;\;\;\; (24)</i></p>

<p>Finally, using equations (14), (17), and (18), the method of moments estimator of 
<i>&gamma;</i> is computed as:
</p>
<p style="text-align: center;"><i>\bar{x} - exp(\hat{mu} + \frac{\hat{&sigma;}^2}{2}) \;\;\;\; (25)</i></p>

<p>There are two major problems with using method of moments estimators for the 
three-parameter lognormal distribution.  First, they are subject to very large 
sampling error due to the use of second and third sample moments 
(Cohen, 1988, p.121; Johnson et al., 1994, p.228).  Second, Heyde (1963) showed 
that the lognormal distribution is not uniquely determined by its moments.
<br />
</p>
<p><em>Method of Moments Estimators Using an Unbiased Estimate of Variance</em> (<code>method="mmue"</code>) <br />
This method of estimation is exactly the same as the method of moments 
(<code>method="mme"</code>), except that the unbiased estimator of variance (equation (3)) 
is used in place of the method of moments one (equation (4)).  This modification is 
given in Cohen (1988, pp.119-120).
<br />
</p>
<p><em>Modified Method of Moments Estimation</em> (<code>method="mmme"</code>) <br />
This method of estimation is described by Cohen (1988, pp.125-132).  It was 
introduced by Cohen and Whitten (1980; their MME-II with r=1) and was further 
investigated by Cohen et al. (1985).  It is motivated by the fact that the first 
order statistic in the sample, <i>x_{(1)}</i>, contains more information about 
the threshold parameter <i>&gamma;</i> than any other observation and often more 
information than all of the other observations combined (Cohen, 1988, p.125).
</p>
<p>The first two sets of equations are the same as for the modified method of moments 
estimators (<code>method="mmme"</code>), i.e., equations (14) and (15) with the 
unbiased estimator of variance (equation (3)) used in place of the method of 
moments one (equation (4)).  The third equation replaces equation (16) 
by equating a function of the first order statistic with its expected value:
</p>
<p style="text-align: center;"><i>log(x_{(1)} - &gamma;) = &mu; + &sigma; E[Z_{(1,n)}] \;\;\;\; (26)</i></p>

<p>where <i>E[Z_{(i,n)}]</i> denotes the expected value of the <i>i</i>'th order 
statistic in a random sample of <i>n</i> observations from a standard normal 
distribution.  (See the help file for <code><a href="../../EnvStats/help/evNormOrdStats.html">evNormOrdStats</a></code> for information 
on how <i>E[Z_{(i,n)}]</i> is computed.)  Using equations (17) and (18), 
equation (26) can be rewritten as:
</p>
<p style="text-align: center;"><i>x_{(1)} = &gamma; + &beta; exp\{&radic;{log(&omega;)} \, E[Z_{(i,n)}] \} \;\;\;\; (27)</i></p>

<p>Combining equations (14), (15), (17), (18), and (27) yields the following equation 
for the estimate of <i>&omega;</i>:
</p>
<p style="text-align: center;"><i>\frac{s^2}{[\bar{x} - x_{(1)}]^2} = \frac{\hat{&omega;}(\hat{&omega;} - 1)}{[&radic;{\hat{&omega;}} - exp\{&radic;{log(&omega;)} \, E[Z_{(i,n)}] \} ]^2} \;\;\;\; (28)</i></p>

<p>After equation (28) is solved for <i>\hat{&omega;}</i>, the estimate of <i>&sigma;</i> 
is again computed using equation (23), and the estimate of <i>&mu;</i> is computed 
using equation (24), where the unbiased estimate of variaince is used in place of 
the biased one (just as for <code>method="mmue"</code>).
<br />
</p>
<p><em>Zero-Skewness Estimation</em> (<code>method="zero.skew"</code>) <br />
This method of estimation was introduced by Griffiths (1980), and elaborated upon 
by Royston (1992b).  The idea is that if the threshold parameter <i>&gamma;</i> were 
known, then the distribution of:
</p>
<p style="text-align: center;"><i>Y = log(X - &gamma;) \;\;\;\; (29)</i></p>

<p>is normal, so the skew of <i>Y</i> is 0.  Thus, the threshold parameter <i>&gamma;</i> 
is estimated as that value that forces the sample skew (defined in equation (19)) of 
the observations defined in equation (6) to be 0.  That is, the zero-skewness 
estimator of <i>&gamma;</i> is the value that satisfies the following equation:
</p>
<p style="text-align: center;"><i>0 = \frac{\frac{1}{n} &sum;_{i=1}^n (y_i - \bar{y})^3}{[\frac{1}{n} &sum;_{i=1}^n (y_i - \bar{y})^2]^{3/2}} \;\;\;\; (30)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>y_i = log(x_i - \hat{&gamma;}) \;\;\;\; (31)</i></p>

<p>Note that since the denominator in equation (30) is always positive (assuming 
there are at least two unique values in <i>\underline{x}</i>), only the numerator 
needs to be used to determine the value of <i>\hat{&gamma;}</i>.
</p>
<p>Once the value of <i>\hat{&gamma;}</i> has been determined, <i>&mu;</i> and <i>&sigma;</i> 
are estimated using equations (7) and (8), except the unbiased estimator of variance 
is used in equation (8).
</p>
<p>Royston (1992b) developed a modification of the Shaprio-Wilk goodness-of-fit test 
for normality based on tranforming the data using equation (6) and the zero-skewness 
estimator of <i>&gamma;</i> (see <code><a href="../../EnvStats/help/gofTest.html">gofTest</a></code>).
<br />
</p>
<p><em>Estimators Based on Royston's Index of Skewness</em> (<code>method="royston.skew"</code>) <br />
This method of estimation is discussed by Royston (1992b), and is similar to the 
zero-skewness method discussed above, except a different measure of skewness is used.  
Royston's (1992b) index of skewness is given by:
</p>
<p style="text-align: center;"><i>q = \frac{y_{(n)} - \tilde{y}}{\tilde{y} - y_{(1)}} \;\;\;\; (32)</i></p>

<p>where <i>y_{(i)}</i> denotes the <i>i</i>'th order statistic of <i>y</i> and <i>y</i> 
is defined in equation (31) above, and <i>\tilde{y}</i> denotes the median of <i>y</i>.  
Royston (1992b) shows that the value of <i>&gamma;</i> that yields a value of 
<i>q=0</i> is given by:
</p>
<p style="text-align: center;"><i>\hat{&gamma;} = \frac{y_{(1)}y_{(n)} - \tilde{y}^2}{y_{(1)} + y_{(n)} - 2\tilde{y}} \;\;\;\; (33)</i></p>

<p>Again, as for the zero-skewness method, once the value of <i>\hat{&gamma;}</i> has 
been determined, <i>&mu;</i> and <i>&sigma;</i> are estimated using equations (7) and (8), 
except the unbiased estimator of variance is used in equation (8).
</p>
<p>Royston (1992b) developed this estimator as a quick way to estimate <i>&gamma;</i>.
<br />
</p>
<p><b>Confidence Intervals</b> <br />
This section explains three different methods for constructing confidence intervals 
for the threshold parameter <i>&gamma;</i>, or the median of the three-parameter 
lognormal distribution, which is given by:
</p>
<p style="text-align: center;"><i>Med[X] = &gamma; + exp(&mu;) = &gamma; + &beta; \;\;\;\; (34)</i></p>

<p><br />
</p>
<p><em>Normal Approximation Based on Asymptotic Variances and Covariances</em> (<code>ci.method="avar"</code>) <br />
Formulas for asymptotic variances and covariances for the three-parameter lognormal 
distribution, based on the information matrix, are given in Cohen (1951), Cohen and 
Whitten (1980), Cohen et al., (1985), and Cohen (1988).  The relevant quantities for 
<i>&gamma;</i> and the median are:
</p>
<p style="text-align: center;"><i>Var(\hat{&gamma;}) = &sigma;^2_{\hat{&gamma;}} = \frac{&sigma;^2}{n} \, (\frac{&beta;^2}{&omega;}) H \;\;\;\; (35)</i></p>

<p style="text-align: center;"><i>Var(\hat{&beta;}) = &sigma;^2_{\hat{&beta;}} = \frac{&sigma;^2}{n} \, &beta;^2 (1 + H) \;\;\;\; (36)</i></p>

<p style="text-align: center;"><i>Cov(\hat{&gamma;}, \hat{&beta;}) = &sigma;_{\hat{&gamma;}, \hat{&beta;}} = \frac{-&sigma;^3}{n} \, (\frac{&beta;^2}{&radic;{&omega;}}) H \;\;\;\; (37)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>H = [&omega; (1 + &sigma;^2) - 2&sigma;^2 - 1]^{-1} \;\;\;\; (38)</i></p>

<p>A two-sided <i>(1-&alpha;)100\%</i> confidence interval for <i>&gamma;</i> is computed as:
</p>
<p style="text-align: center;"><i>\hat{&gamma;} - t_{n-2, 1-&alpha;/2} \hat{&sigma;}_{\hat{&gamma;}}, \, \hat{&gamma;} + t_{n-2, 1-&alpha;/2} \hat{&sigma;}_{\hat{&gamma;}} \;\;\;\; (39)</i></p>

<p>where <i>t_{&nu;, p}</i> denotes the <i>p</i>'th quantile of 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>n</i> degrees of freedom, and the 
quantity <i>\hat{&sigma;}_{\hat{&gamma;}}</i> is computed using equations (35) and (38) 
and substituting estimated values of <i>&beta;</i>, <i>&omega;</i>, and <i>&sigma;</i>.  
One-sided confidence intervals are computed in a similar manner.
</p>
<p>A two-sided <i>(1-&alpha;)100\%</i> confidence interval for the median (see equation 
(34) above) is computed as:
</p>
<p style="text-align: center;"><i>\hat{&gamma;} + \hat{&beta;} - t_{n-2, 1-&alpha;/2} \hat{&sigma;}_{\hat{&gamma;} + \hat{&beta;}}, \, \hat{&gamma;} + \hat{&beta;} + t_{n-2, 1-&alpha;/2} \hat{&sigma;}_{\hat{&gamma;} + \hat{&beta;}} \;\;\;\; (40)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>\hat{&sigma;}^2_{\hat{&gamma;} + \hat{&beta;}} = \hat{&sigma;}^2_{\hat{&gamma;}} + \hat{&sigma;}^2_{\hat{&beta;}} + \hat{&sigma;}_{\hat{&gamma;}, \hat{&beta;}} \;\;\;\; (41)</i></p>

<p>is computed using equations (35)-(38) and substituting estimated values of 
<i>&beta;</i>, <i>&omega;</i>, and <i>&sigma;</i>.  One-sided confidence intervals are 
computed in a similar manner.
</p>
<p>This method of constructing confidence intervals is analogous to using the Wald test 
(e.g., Silvey, 1975, pp.115-118) to test hypotheses on the parameters.
</p>
<p>Because of the regularity problems associated with the global maximum likelihood 
estimators, it is questionble whether the asymptotic variances and covariances shown 
above apply to local maximum likelihood estimators.  Simulation studies, however, 
have shown that these estimates of variance and covariance perform reasonably well 
(Harter and Moore, 1966; Cohen and Whitten, 1980).
</p>
<p>Note that this method of constructing confidence intervals can be used with 
estimators other than the lmle's.  Cohen and Whitten (1980) and Cohen et al. (1985) 
found that the asymptotic variances and covariances are reasonably close to 
corresponding simulated variances and covariances for the modified method of moments 
estimators (<code>method="mmme"</code>).
<br />
</p>
<p><em>Likelihood Profile</em> (<code>ci.method="likelihood.profile"</code>) <br />
Griffiths (1980) suggested constructing confidence intervals for the threshold 
parameter <i>&gamma;</i> based on the profile likelihood function given in equations 
(9) and (10).  Royston (1992b) further elaborated upon this procedure.  A 
two-sided <i>(1-&alpha;)100\%</i> confidence interval for <i>&eta;</i> is constructed as:
</p>
<p style="text-align: center;"><i>[&eta;_{LCL}, &eta;_{UCL}] \;\;\;\; (42)</i></p>

<p>by finding the two values of <i>&eta;</i> (one larger than the lmle of <i>&eta;</i> and 
one smaller than the lmle of <i>&eta;</i>) that satisfy:
</p>
<p style="text-align: center;"><i>log[L(&eta;)] = log[L(\hat{&eta;}_{lmle})] - \frac{1}{2} &chi;^2_{1, &alpha;/2} \;\;\;\; (43)</i></p>

<p>where <i>&chi;^2_{&nu;, p}</i> denotes the <i>p</i>'th quantile of the 
<a href="../../stats/help/Chisquare.html">chi-square distribution</a> with <i>&nu;</i> degrees of freedom.  
Once these values are found, the two-sided confidence for <i>&gamma;</i> is computed as:
</p>
<p style="text-align: center;"><i>[&gamma;_{LCL}, &gamma;_{UCL}] \;\;\;\; (44)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>&gamma;_{LCL} = x_{(1)} - exp(-&eta;_{LCL}) \;\;\;\; (45)</i></p>

<p style="text-align: center;"><i>&gamma;_{UCL} = x_{(1)} - exp(-&eta;_{UCL}) \;\;\;\; (46)</i></p>

<p>One-sided intervals are construced in a similar manner.
</p>
<p>This method of constructing confidence intervals is analogous to using the 
likelihood-ratio test (e.g., Silvey, 1975, pp.108-115) to test hypotheses on the 
parameters.
</p>
<p>To construct a two-sided <i>(1-&alpha;)100\%</i> confidence interval for the median 
(see equation (34)), Royston (1992b) suggested the following procedure:
</p>

<ol>
<li><p> Construct a confidence interval for <i>&gamma;</i> using the likelihood 
profile procedure.
</p>
</li>
<li><p> Construct a confidence interval for <i>&beta;</i> as: 
</p>
<p style="text-align: center;"><i>[&beta;_{LCL}, &beta;_{UCL}] = [exp(\hat{&mu;} - t_{n-2, 1-&alpha;/2} \frac{\hat{&sigma;}}{n}), \, exp(\hat{&mu;} + t_{n-2, 1-&alpha;/2} \frac{\hat{&sigma;}}{n})] \;\;\;\; (47)</i></p>

</li>
<li><p> Construct the confidence interval for the median as: 
</p>
<p style="text-align: center;"><i>[&gamma;_{LCL} + &beta;_{LCL}, &gamma;_{UCL} + &beta;_{UCL}] \;\;\;\; (48)</i></p>

</li></ol>
 
<p>Royston (1992b) actually suggested using the quantile from the standard normal 
distribution instead of Student's t-distribution in step 2 above.  The function 
<code>elnorm3</code>, however, uses the Student's t quantile.
</p>
<p>Note that this method of constructing confidence intervals can be used with 
estimators other than the lmle's.
<br />
</p>
<p><em>Royston's Confidence Interval Based on Significant Skewness</em> (<code>ci.method="skewness"</code>) <br />
Royston (1992b) suggested constructing confidence intervals for the threshold 
parameter <i>&gamma;</i> based on the idea behind the zero-skewness estimator 
(<code>method="zero.skew"</code>).  A two-sided <i>(1-&alpha;)100\%</i> confidence interval 
for <i>&gamma;</i> is constructed by finding the two values of <i>&gamma;</i> that yield 
a p-value of <i>&alpha;/2</i> for the test of zero-skewness on the observations 
<i>\underline{y}</i> defined in equation (6) (see <code><a href="../../EnvStats/help/gofTest.html">gofTest</a></code>).  One-sided 
confidence intervals are constructed in a similar manner.
</p>
<p>To construct <i>(1-&alpha;)100\%</i> confidence intervals for the median 
(see equation (34)), the exact same procedure is used as for 
<code>ci.method="likelihood.profile"</code>, except that the confidence interval for 
<i>&gamma;</i> is based on the zero-skewness method just described instead of the 
likelihood profile method.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information.  
See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The problem of estimating the parameters of a three-parameter lognormal distribution 
has been extensively discussed by Aitchison and Brown (1957, Chapter 6), 
Calitz (1973), Cohen (1951), Cohen (1988), Cohen and Whitten (1980), 
Cohen et al. (1985), Griffiths (1980), Harter and Moore (1966), Hill (1963), and 
Royston (1992b).  Stedinger (1980) and Hoshi et al. (1984) discuss fitting the 
three-parameter lognormal distribution to hydrologic data.
</p>
<p>The global maximum likelihood estimates are inadmissible.  In the past, several 
researchers have found that the local maximum likelihood estimates (lmle's) 
occasionally fail because of convergence problems, but they were not using the 
likelihood profile and reparameterization of Griffiths (1980).  Cohen (1988) 
recommends the modified methods of moments estimators over lmle's because they are 
easy to compute, they are unbiased with respect to <i>&mu;</i> and <i>&sigma;^2</i> (the 
mean and standard deviation on the log-scale), their variances are minimal or near 
minimal, and they do not suffer from regularity problems.
</p>
<p>Because the distribution of the lmle of the threshold parameter <i>&gamma;</i> is far 
from normal for moderate sample sizes (Griffiths, 1980), it is questionable whether 
confidence intervals for <i>&gamma;</i> or the median based on asymptotic variances 
and covariances will perform well.  Cohen and Whitten (1980) and Cohen et al. (1985), 
however, found that the asymptotic variances and covariances are reasonably close to 
corresponding simulated variances and covariances for the modified method of moments 
estimators (<code>method="mmme"</code>).  In a simulation study (5000 monte carlo trials), 
Royston (1992b) found that the coverage of confidence intervals for <i>&gamma;</i> 
based on the likelihood profile (<code>ci.method="likelihood.profile"</code>) was very 
close the nominal level (94.1% for a nominal level of 95%), although not 
symmetric.  Royston (1992b) also found that the coverage of confidence intervals 
for <i>&gamma;</i> based on the skewness method (<code>ci.method="skewness"</code>) was also 
very close (95.4%) and symmetric.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Aitchison, J., and J.A.C. Brown (1957).  <em>The Lognormal Distribution 
(with special references to its uses in economics)</em>.  Cambridge University Press, 
London, Chapter 5.
</p>
<p>Calitz, F. (1973).  Maximum Likelihood Estimation of the Parameters of the 
Three-Parameter Lognormal Distribution&ndash;a Reconsideration.  <em>Australian 
Journal of Statistics</em> <b>15</b>(3), 185&ndash;190.
</p>
<p>Cohen, A.C. (1951).  Estimating Parameters of Logarithmic-Normal Distributions by 
Maximum Likelihood.  <em>Journal of the American Statistical Association</em> 
<b>46</b>, 206&ndash;212.
</p>
<p>Cohen, A.C. (1988).  Three-Parameter Estimation.  In Crow, E.L., and K. Shimizu, eds. 
<em>Lognormal Distributions: Theory and Applications</em>.  Marcel Dekker, New York, 
Chapter 4.
</p>
<p>Cohen, A.C., and B.J. Whitten. (1980).  Estimation in the Three-Parameter Lognormal 
Distribution.  <em>Journal of the American Statistical Association</em> <b>75</b>, 
399&ndash;404.
</p>
<p>Cohen, A.C., B.J. Whitten, and Y. Ding. (1985).  Modified Moment Estimation for the 
Three-Parameter Lognormal Distribution.  <em>Journal of Quality Technology</em> 
<b>17</b>, 92&ndash;99.
</p>
<p>Crow, E.L., and K. Shimizu. (1988).  <em>Lognormal Distributions: Theory and 
Applications</em>.  Marcel Dekker, New York, Chapter 2.
</p>
<p>Griffiths, D.A. (1980).  Interval Estimation for the Three-Parameter Lognormal 
Distribution via the Likelihood Function.  <em>Applied Statistics</em> <b>29</b>, 
58&ndash;68.
</p>
<p>Harter, H.L., and A.H. Moore. (1966).  Local-Maximum-Likelihood Estimation of the 
Parameters of Three-Parameter Lognormal Populations from Complete and Censored 
Samples.  <em>Journal of the American Statistical Association</em> <b>61</b>, 842&ndash;851.
</p>
<p>Heyde, C.C. (1963).  On a Property of the Lognormal Distribution.  <em>Journal of 
the Royal Statistical Society, Series B</em> <b>25</b>, 392&ndash;393.
</p>
<p>Hill, .B.M. (1963).  The Three-Parameter Lognormal Distribution and Bayesian 
Analysis of a Point-Source Epidemic.  <em>Journal of the American Statistical 
Association</em> <b>58</b>, 72&ndash;84.
</p>
<p>Hoshi, K., J.R. Stedinger, and J. Burges. (1984).  Estimation of Log-Normal 
Quantiles: Monte Carlo Results and First-Order Approximations.  <em>Journal of 
Hydrology</em> <b>71</b>, 1&ndash;30.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York.
</p>
<p>Royston, J.P. (1992b).  Estimation, Reference Ranges and Goodness of Fit for the 
Three-Parameter Log-Normal Distribution.  <em>Statistics in Medicine</em> <b>11</b>, 
897&ndash;912.
</p>
<p>Stedinger, J.R. (1980).  Fitting Lognormal Distributions to Hydrologic Data. 
<em>Water Resources Research</em> <b>16</b>(3), 481&ndash;490.
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/Lognormal3.html">Lognormal3</a>, <a href="../../stats/help/Lognormal.html">Lognormal</a>, <a href="../../EnvStats/help/LognormalAlt.html">LognormalAlt</a>, 
<a href="../../stats/help/Normal.html">Normal</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a 3-parameter lognormal distribution 
  # with parameters meanlog=1.5, sdlog=1, and threshold=10, then use 
  # Cohen and Whitten's (1980) modified moments estimators to estimate 
  # the parameters, and construct a confidence interval for the 
  # threshold based on the estimated asymptotic variance. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rlnorm3(20, meanlog = 1.5, sdlog = 1, threshold = 10) 
  elnorm3(dat, method = "mmme", ci = TRUE)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            3-Parameter Lognormal
  #
  #Estimated Parameter(s):          meanlog   = 1.5206664
  #                                 sdlog     = 0.5330974
  #                                 threshold = 9.6620403
  #
  #Estimation Method:               mmme
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         threshold
  #
  #Confidence Interval Method:      Normal Approximation
  #                                 Based on Asymptotic Variance
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL =  6.985258
  #                                 UCL = 12.338823

  #----------

  # Repeat the above example using the other methods of estimation 
  # and compare. 

  round(elnorm3(dat, "lmle")$parameters, 1) 
  #meanlog     sdlog threshold 
  #    1.3       0.7      10.5 
 
  round(elnorm3(dat, "mme")$parameters, 1) 
  #meanlog     sdlog threshold 
  #    2.1       0.3       6.0 
 
  round(elnorm3(dat, "mmue")$parameters, 1) 
  #meanlog     sdlog threshold 
  #    2.2       0.3       5.8 
  
  round(elnorm3(dat, "mmme")$parameters, 1) 
  #meanlog     sdlog threshold 
  #    1.5       0.5       9.7 
  
  round(elnorm3(dat, "zero.skew")$parameters, 1) 
  #meanlog     sdlog threshold 
  #    1.3       0.6      10.3 
 
  round(elnorm3(dat, "royston")$parameters, 1)
  #meanlog     sdlog threshold 
  #    1.4       0.6      10.1 

  #----------

  # Compare methods for computing a two-sided 95% confidence interval 
  # for the threshold: 
  # modified method of moments estimator using asymptotic variance, 
  # lmle using asymptotic variance, 
  # lmle using likelihood profile, and 
  # zero-skewness estimator using the skewness method.

  elnorm3(dat, method = "mmme", ci = TRUE, 
    ci.method = "avar")$interval$limits 
  #      LCL       UCL 
  # 6.985258 12.338823 
 
  elnorm3(dat, method = "lmle", ci = TRUE, 
    ci.method = "avar")$interval$limits 
  #       LCL       UCL 
  #  9.017223 11.980107 
 
  elnorm3(dat, method = "lmle", ci = TRUE, 
    ci.method="likelihood.profile")$interval$limits 
  #      LCL       UCL 
  # 3.699989 11.266029 
 

  elnorm3(dat, method = "zero.skew", ci = TRUE, 
    ci.method = "skewness")$interval$limits 
  #      LCL       UCL 
  #-25.18851  11.18652

  #----------

  # Now construct a confidence interval for the median of the distribution 
  # based on using the modified method of moments estimator for threshold 
  # and the asymptotic variances and covariances.  Note that the true median 
  # is given by threshold + exp(meanlog) = 10 + exp(1.5) = 14.48169.

  elnorm3(dat, method = "mmme", ci = TRUE, ci.parameter = "median") 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            3-Parameter Lognormal
  #
  #Estimated Parameter(s):          meanlog   = 1.5206664
  #                                 sdlog     = 0.5330974
  #                                 threshold = 9.6620403
  #
  #Estimation Method:               mmme
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         median
  #
  #Confidence Interval Method:      Normal Approximation
  #                                 Based on Asymptotic Variance
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = 11.20541
  #                                 UCL = 17.26922

  #----------

  # Compare methods for computing a two-sided 95% confidence interval 
  # for the median: 
  # modified method of moments estimator using asymptotic variance, 
  # lmle using asymptotic variance, 
  # lmle using likelihood profile, and 
  # zero-skewness estimator using the skewness method.

  elnorm3(dat, method = "mmme", ci = TRUE, ci.parameter = "median", 
    ci.method = "avar")$interval$limits 
  #     LCL      UCL 
  #11.20541 17.26922 
 
  elnorm3(dat, method = "lmle", ci = TRUE, ci.parameter = "median", 
    ci.method = "avar")$interval$limits 
  #     LCL      UCL 
  #12.28326 15.87233 

  elnorm3(dat, method = "lmle", ci = TRUE, ci.parameter = "median", 
    ci.method = "likelihood.profile")$interval$limits 
  #      LCL       UCL 
  # 6.314583 16.165525 

  elnorm3(dat, method = "zero.skew", ci = TRUE, ci.parameter = "median", 
    ci.method = "skewness")$interval$limits 
  #      LCL       UCL 
  #-22.38322  16.33569

  #----------

  # Clean up
  #---------

  rm(dat)

</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
