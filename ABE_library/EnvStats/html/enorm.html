<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Normal (Gaussian) Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for enorm {EnvStats}"><tr><td>enorm {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Normal (Gaussian) Distribution
</h2>

<h3>Description</h3>

<p>Estimate the mean and standard deviation parameters of a 
<a href="../../stats/help/Normal.html">normal (Gaussian) distribution</a>, and optionally construct a 
confidence interval for the mean or the variance.
</p>


<h3>Usage</h3>

<pre>
  enorm(x, method = "mvue", ci = FALSE, ci.type = "two-sided", 
    ci.method = "exact", conf.level = 0.95, ci.param = "mean")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Possible values are 
<code>"mvue"</code> (minimum variance unbiased; the default), and <code>"mle/mme"</code> 
(maximum likelihood/method of moments).    
See the DETAILS section for more information on these estimation methods.
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the 
mean or variance.  The default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval 
for the mean or variance.  The only possible value is <code>"exact"</code> (the default).  
See the DETAILS section for more information.  This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>. This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.param</code></td>
<td>

<p>character string indicating which parameter to create a confidence interval for.  
The possible values are <code>ci.param="mean"</code> (the default) and <br />
<code>ci.param="variance"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from an <a href="../../stats/help/Normal.html">normal (Gaussian) distribution</a> with 
parameters <code>mean=</code><i>&mu;</i> and <code>sd=</code><i>&sigma;</i>.
</p>
<p><b>Estimation</b> <br />
</p>
<p><em>Minimum Variance Unbiased Estimation</em> (<code>method="mvue"</code>) <br />
The minimum variance unbiased estimators (mvue's) of the mean and variance are:
</p>
<p style="text-align: center;"><i>\hat{&mu;}_{mvue} = \bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>\hat{&sigma;}^2_{mvue} = s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (2)</i></p>

<p>(Johnson et al., 1994; Forbes et al., 2011).  Note that when <code>method="mvue"</code>, 
the estimated standard deviation is the square root of the mvue of the variance, 
but is not itself an mvue.
</p>
<p><em>Maximum Likelihood/Method of Moments Estimation</em> (<code>method="mle/mme"</code>) <br />
The maximum likelihood estimator (mle) and method of moments estimator (mme) of the 
mean are both the same as the mvue of the mean given in equation (1) above.  The 
mle and mme of the variance is given by:
</p>
<p style="text-align: center;"><i>\hat{&sigma;}^2_{mle} = s^2_m = \frac{n-1}{n} s^2 = \frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (3)</i></p>

<p>When <code>method="mle/mme"</code>, the estimated standard deviation is the square root of 
the mle of the variance, and is itself an mle.
<br />
</p>
<p><b>Confidence Intervals</b> <br />
</p>
<p><em>Confidence Interval for the Mean</em> (<code>ci.param="mean"</code>) <br />
When <code>ci=TRUE</code> and <code>ci.param = "mean"</code>, the usual confidence interval 
for <i>&mu;</i> is constructed as follows.  If <code>ci.type="two-sided"</code>, a 
the <i>(1-&alpha;)</i>100% confidence interval for <i>&mu;</i> is given by:
</p>
<p style="text-align: center;"><i>[\hat{&mu;} - t(n-1, 1-&alpha;/2) \frac{\hat{&sigma;}}{&radic;{n}}, \, \hat{&mu;} + t(n-1, 1-&alpha;/2) \frac{\hat{&sigma;}}{&radic;{n}}] \;\;\;\; (4)</i></p>

<p>where <i>t(&nu;, p)</i> is the <i>p</i>'th quantile of 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>&nu;</i> degrees of freedom 
(Zar, 2010; Gilbert, 1987; Ott, 1995; Helsel and Hirsch, 1992).
</p>
<p>If <code>ci.type="lower"</code>, the <i>(1-&alpha;)</i>100% confidence interval for 
<i>&mu;</i> is given by:
</p>
<p style="text-align: center;"><i>[\hat{&mu;} - t(n-1, 1-&alpha;) \frac{\hat{&sigma;}}{&radic;{n}}, \, &infin;] \;\;\;\; (5)</i></p>

<p>and if <code>ci.type="upper"</code>, the confidence interval is given by:
</p>
<p style="text-align: center;"><i>[-&infin;, \, \hat{&mu;} + t(n-1, 1-&alpha;/2) \frac{\hat{&sigma;}}{&radic;{n}}] \;\;\;\; (6)</i></p>

<p><em>Confidence Interval for the Variance</em> (<code>ci.param="variance"</code>) <br />
When <code>ci=TRUE</code> and <code>ci.param = "variance"</code>, the usual confidence interval 
for <i>&sigma;^2</i> is constructed as follows.  A two-sided 
<i>(1-&alpha;)</i>100% confidence interval for <i>&sigma;^2</i> is given by:
</p>
<p style="text-align: center;"><i>[ \frac{(n-1)s^2}{&chi;^2_{n-1,1-&alpha;/2}}, \, \frac{(n-1)s^2}{&chi;^2_{n-1,&alpha;/2}} ] \;\;\;\; (7)</i></p>

<p>Similarly, a one-sided upper <i>(1-&alpha;)</i>100% confidence interval for the 
population variance is given by:
</p>
<p style="text-align: center;"><i>[ 0, \, \frac{(n-1)s^2}{&chi;^2_{n-1,&alpha;}} ] \;\;\;\; (8)</i></p>

<p>and a one-sided lower <i>(1-&alpha;)</i>100% confidence interval for the population 
variance is given by:
</p>
<p style="text-align: center;"><i>[ \frac{(n-1)s^2}{&chi;^2_{n-1,1-&alpha;}}, \, &infin; ] \;\;\;\; (9)</i></p>

<p>(van Belle et al., 2004; Zar, 2010).
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information.  
See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The normal and lognormal distribution are probably the two most frequently used 
distributions to model environmental data.  In order to make any kind of 
probability statement about a normally-distributed population (of chemical 
concentrations for example), you have to first estimate the mean and standard 
deviation (the population parameters) of the distribution.  Once you estimate 
these parameters, it is often useful to characterize the uncertainty in the 
estimate of the mean or variance.  This is done with confidence intervals.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). 
<em>Statistics for Environmental Engineers</em>.  Second Edition.   
Lewis Publishers, Boca Raton, FL.
</p>
<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution Monitoring</em>. 
Van Nostrand Reinhold, New York, NY.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). 
<em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, Chapter 7.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, FL.
</p>
<p>Ott, W.R. (1995). <em>Environmental Statistics and Data Analysis</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>van Belle, G., L.D. Fisher, Heagerty, P.J., and Lumley, T. (2004). 
<em>Biostatistics: A Methodology for the Health Sciences, 2nd Edition</em>. 
John Wiley &amp; Sons, New York.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Normal.html">Normal</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a N(3, 2) distribution, then estimate 
  # the parameters and create a 95% confidence interval for the mean. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rnorm(20, mean = 3, sd = 2) 
  enorm(dat, ci = TRUE) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Normal
  #
  #Estimated Parameter(s):          mean = 2.861160
  #                                 sd   = 1.180226
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         mean
  #
  #Confidence Interval Method:      Exact
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = 2.308798
  #                                 UCL = 3.413523

  #----------

  # Using the same data, construct an upper 90% confidence interval for
  # the variance.

  enorm(dat, ci = TRUE, ci.type = "upper", ci.param = "variance")$interval

  #Confidence Interval for:         variance
  #
  #Confidence Interval Method:      Exact
  #
  #Confidence Interval Type:        upper
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = 0.000000
  #                                 UCL = 2.615963

  #----------

  # Clean up
  #---------
  rm(dat)

  #----------

  # Using the Reference area TcCB data in the data frame EPA.94b.tccb.df, 
  # estimate the mean and standard deviation of the log-transformed data, 
  # and construct a 95% confidence interval for the mean.

  with(EPA.94b.tccb.df, enorm(log(TcCB[Area == "Reference"]), ci = TRUE))  

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Normal
  #
  #Estimated Parameter(s):          mean = -0.6195712
  #                                 sd   =  0.4679530
  #
  #Estimation Method:               mvue
  #
  #Data:                            log(TcCB[Area == "Reference"])
  #
  #Sample Size:                     47
  #
  #Confidence Interval for:         mean
  #
  #Confidence Interval Method:      Exact
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = -0.7569673
  #                                 UCL = -0.4821751
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
