<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of an Extreme Value (Gumbel) Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eevd {EnvStats}"><tr><td>eevd {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of an Extreme Value (Gumbel) Distribution
</h2>

<h3>Description</h3>

<p>Estimate the location and scale parameters of an 
<a href="../../EnvStats/help/EVD.html">extreme value distribution</a>, and optionally construct a 
confidence interval for one of the parameters.
</p>


<h3>Usage</h3>

<pre>
  eevd(x, method = "mle", pwme.method = "unbiased", 
    plot.pos.cons = c(a = 0.35, b = 0), ci = FALSE, 
    ci.parameter = "location", ci.type = "two-sided", 
    ci.method = "normal.approx", conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Possible values are 
<code>"mle"</code> (maximum likelihood; the default), <code>"mme"</code> (methods of moments), 
<code>"mmue"</code> (method of moments based on the unbiased estimator of variance), and 
<code>"pwme"</code> (probability-weighted moments).  See the DETAILS section for more 
information on these estimation methods. 
</p>
</td></tr>
<tr valign="top"><td><code>pwme.method</code></td>
<td>

<p>character string specifying what method to use to compute the 
probability-weighted moments when <code>method="pwme"</code>.  The possible values are 
<code>"ubiased"</code> (method based on the U-statistic; the default), or 
<code>"plotting.position"</code> (method based on the plotting position formula).  
See the DETAILS section in this help file and the help file for <code><a href="../../EnvStats/help/pwMoment.html">pwMoment</a></code> 
for more information.  This argument is ignored if <code>method</code> is not equal to 
<code>"pwme"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.cons</code></td>
<td>

<p>numeric vector of length 2 specifying the constants used in the formula for the 
plotting positions when <code>method="pwme"</code> and <br />
<code>pwme.method="plotting.position"</code>.  The default value is <br />
<code>plot.pos.cons=c(a=0.35, b=0)</code>.  If this vector has a names attribute with 
the value <code>c("a","b")</code> or <code>c("b","a")</code>, then the elements will be 
matched by name in the formula for computing the plotting positions.  Otherwise, 
the first element is mapped to the name <code>"a"</code> and the second element to the 
name <code>"b"</code>.  See the DETAILS section in this help file and the help file 
for <code><a href="../../EnvStats/help/pwMoment.html">pwMoment</a></code> for more information.  This argument is ignored if 
<code>method</code> is not equal to <code>"pwme"</code> or if <code>pwme.method="ubiased"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the 
location or scale parameter.  The default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.parameter</code></td>
<td>

<p>character string indicating the parameter for which the confidence interval is 
desired.  The possible values are <code>"location"</code> (the default) and <code>"scale"</code>.  
This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval 
for the location or scale parameter.  Currently, the only possible value is 
<code>"normal.approx"</code> (the default).  See the DETAILS section for more information.  
This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>. This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from an <a href="../../EnvStats/help/EVD.html">extreme value distribution</a> with 
parameters <code>location=</code><i>&eta;</i> and <code>scale=</code><i>&theta;</i>.
</p>
<p><b>Estimation</b> <br />
</p>
<p><em>Maximum Likelihood Estimation</em> (<code>method="mle"</code>) <br />
The maximum likelihood estimators (mle's) of <i>&eta;</i> and <i>&theta;</i> are 
the solutions of the simultaneous equations (Forbes et al., 2011):
</p>
<p style="text-align: center;"><i>\hat{&eta;}_mle = \hat{&theta;}_mle \, log[\frac{1}{n} &sum;_{i=1}^{n} exp(\frac{-x_i}{\hat{&theta;}_mle})]</i></p>

<p style="text-align: center;"><i>\hat{&theta;}_mle = \bar{x} - \frac{&sum;_{i=1}^{n} x_i exp(\frac{-x_i}{\hat{&theta;}_mle})}{&sum;_{i=1}^{n} exp(\frac{-x_i}{\hat{&theta;}_mle})}</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i</i></p>
<p>.
</p>
<p><em>Method of Moments Estimation</em> (<code>method="mme"</code>) <br />
The method of moments estimators (mme's) of <i>&eta;</i> and <i>&theta;</i> are 
given by (Johnson et al., 1995, p.27):
</p>
<p style="text-align: center;"><i>\hat{&eta;}_{mme} = \bar{x} - &epsilon; \hat{&theta;}_{mme}</i></p>

<p style="text-align: center;"><i>\hat{&theta;}_{mme} = \frac{&radic;{6}}{&pi;} s_m</i></p>

<p>where <i>&epsilon;</i> denotes <a href="../../EnvStats/help/EulersConstant.html">Euler's constant</a> and 
<i>s_m</i> denotes the square root of the method of moments estimator of variance:
</p>
<p style="text-align: center;"><i>s_m^2 = \frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2</i></p>

<p><em>Method of Moments Estimators Based on the Unbiased Estimator of Variance</em> (<code>method="mmue"</code>) <br />
These estimators are the same as the method of moments estimators except that 
the method of moments estimator of variance is replaced with the unbiased estimator 
of variance:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2</i></p>

<p><em>Probability-Weighted Moments Estimation</em> (<code>method="pwme"</code>)<br />
Greenwood et al. (1979) show that the relationship between the distribution 
parameters <i>&eta;</i> and <i>&theta;</i> and the probability-weighted moments 
is given by:
</p>
<p style="text-align: center;"><i>&eta; = M(1, 0, 0) - &epsilon; &theta;</i></p>

<p style="text-align: center;"><i>&theta; = \frac{M(1, 0, 0) - 2M(1, 0, 1)}{log(2)}</i></p>

<p>where <i>M(i, j, k)</i> denotes the <i>ijk</i>'th probability-weighted moment and 
<i>&epsilon;</i> denotes <a href="../../EnvStats/help/EulersConstant.html">Euler's constant</a>.  
The probability-weighted moment estimators (pwme's) of <i>&eta;</i> and 
<i>&theta;</i> are computed by simply replacing the <i>M(i,j,k)</i>'s in the 
above two equations with estimates of the <i>M(i,j,k)</i>'s (and for the 
estimate of <i>&eta;</i>, replacing <i>&theta;</i> with its estimated value).  
See the help file for <code><a href="../../EnvStats/help/pwMoment.html">pwMoment</a></code> for more information on how to 
estimate the <i>M(i,j,k)</i>'s.  Also, see Landwehr et al. (1979) for an example 
of this method of estimation using the unbiased (U-statistic type) 
probability-weighted moment estimators.  Hosking et al. (1985) note that this 
method of estimation using the U-statistic type probability-weighted moments 
is equivalent to Downton's (1966) linear estimates with linear coefficients. <br />
</p>
<p><b>Confidence Intervals</b> <br />
When <code>ci=TRUE</code>, an approximate <i>(1-&alpha;)</i>100% confidence intervals 
for <i>&eta;</i> can be constructed assuming the distribution of the estimator of 
<i>&eta;</i> is approximately normally distributed.  A two-sided confidence 
interval is constructed as:
</p>
<p style="text-align: center;"><i>[\hat{&eta;} - t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&eta;}}, \, \hat{&eta;} + t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&eta;}}]</i></p>

<p>where <i>t(&nu;, p)</i> is the <i>p</i>'th quantile of 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with 
<i>&nu;</i> degrees of freedom, and the quantity 
</p>
<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&eta;}}</i></p>

<p>denotes the estimated asymptotic standard deviation of the estimator of <i>&eta;</i>.
</p>
<p>Similarly, a two-sided confidence interval for <i>&theta;</i> is constructed as:
</p>
<p style="text-align: center;"><i>[\hat{&theta;} - t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&theta;}}, \, \hat{&theta;} + t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&theta;}}]</i></p>

<p>One-sided confidence intervals for <i>&eta;</i> and <i>&theta;</i> are computed in 
a similar fashion.
</p>
<p><em>Maximum Likelihood</em> (<code>method="mle"</code>) <br />
Downton (1966) shows that the estimated asymptotic variances of the mle's of 
<i>&eta;</i> and <i>&theta;</i> are given by:
</p>
<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&eta;}_mle}^2 = \frac{\hat{&theta;}_mle^2}{n} [1 + \frac{6(1 - &epsilon;)^2}{&pi;^2}] = \frac{1.10867 \hat{&theta;}_mle^2}{n}</i></p>

<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&theta;}_mle}^2 = \frac{6}{&pi;^2} \frac{\hat{&theta;}_mle^2}{n} = \frac{0.60793 \hat{&theta;}_mle^2}{n}</i></p>

<p>where <i>&epsilon;</i> denotes <a href="../../EnvStats/help/EulersConstant.html">Euler's constant</a>.
</p>
<p><em>Method of Moments</em> (<code>method="mme"</code> or <code>method="mmue"</code>) <br />
Tiago de Oliveira (1963) and Johnson et al. (1995, p.27) show that the 
estimated asymptotic variance of the mme's of <i>&eta;</i> and <i>&theta;</i> 
are given by:
</p>
<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&eta;}_mme}^2 = \frac{\hat{&theta;}_mme^2}{n} [\frac{&pi;^2}{6} + \frac{&epsilon;^2}{4}(&beta;_2 - 1) - \frac{&pi; &epsilon; &radic;{&beta;_1}}{&radic;{6}}] = \frac{1.1678 \hat{&theta;}_mme^2}{n}</i></p>

<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&theta;}_mme}^2 = \frac{\hat{&theta;}_mle^2}{n} \frac{(&beta;_2 - 1)}{4} = \frac{1.1 \hat{&theta;}_mme^2}{n}</i></p>

<p>where the quantities 
</p>
<p style="text-align: center;"><i>&radic;{&beta;_1}, \; &beta;_2</i></p>

<p>denote the skew and kurtosis of the distribution, and <i>&epsilon;</i> 
denotes <a href="../../EnvStats/help/EulersConstant.html">Euler's constant</a>. 
</p>
<p>The estimated asymptotic variances of the mmue's of <i>&eta;</i> and <i>&theta;</i> 
are the same, except replace the mme of <i>&theta;</i> in the above equations with 
the mmue of <i>&theta;</i>.
</p>
<p><em>Probability-Weighted Moments</em> (<code>method="pwme"</code>) <br />
As stated above, Hosking et al. (1985) note that this method of estimation using 
the U-statistic type probability-weighted moments is equivalent to 
Downton's (1966) linear estimates with linear coefficients.  Downton (1966) 
provides exact values of the variances of the estimates of location and scale 
parameters for the smallest extreme value distribution.  For the largest extreme 
value distribution, the formula for the estimate of scale is the same, but the 
formula for the estimate of location must be modified.  Thus, Downton's (1966) 
equation (3.4) is modified to:
</p>
<p style="text-align: center;"><i>\hat{&eta;}_pwme = \frac{(n-1)log(2) + (n+1)&epsilon;}{n(n-1)log(2)} v - \frac{2 &epsilon;}{n(n-1)log(2)} w</i></p>

<p>where <i>&epsilon;</i> denotes <a href="../../EnvStats/help/EulersConstant.html">Euler's constant</a>, and 
<i>v</i> and <i>w</i> are defined in Downton (1966, p.8).  Using 
Downton's (1966) equations (3.9)-(3.12), the exact variance of the pwme of 
<i>&eta;</i> can be derived.  Note that when <code>method="pwme"</code> and 
<code>pwme.method="plotting.position"</code>, these are only the asymptotically correct 
variances.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information.  
See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>There are three families of extreme value distributions.  The one 
described here is the <a href="../../EnvStats/help/EVD.html">Type I, also called the Gumbel extreme value 
distribution or simply Gumbel distribution</a>.  The name 
&ldquo;extreme value&rdquo; comes from the fact that this distribution is 
the limiting distribution (as <i>n</i> approaches infinity) of the 
greatest value among <i>n</i> independent random variables each 
having the same continuous distribution.
</p>
<p>The Gumbel extreme value distribution is related to the 
<a href="../../stats/help/Exponential.html">exponential distribution</a> as follows. 
Let <i>Y</i> be an <a href="../../stats/help/Exponential.html">exponential random variable</a> 
with parameter <code>rate=</code><i>&lambda;</i>.  Then <i>X = &eta; - log(Y)</i> 
has an extreme value distribution with parameters 
<code>location=</code><i>&eta;</i> and <code>scale=</code><i>1/&lambda;</i>.
</p>
<p>The distribution described above and assumed by <code>eevd</code> is the 
<em>largest</em> extreme value distribution.  The smallest extreme value 
distribution is the limiting distribution (as <i>n</i> approaches infinity) 
of the smallest value among 
<i>n</i> independent random variables each having the same continuous distribution. 
If <i>X</i> has a largest extreme value distribution with parameters 
<code>location=</code><i>&eta;</i> and <code>scale=</code><i>&theta;</i>, then 
<i>Y = -X</i> has a smallest extreme value distribution with parameters 
<code>location=</code><i>-&eta;</i> and <code>scale=</code><i>&theta;</i>.  The smallest 
extreme value distribution is related to the <a href="../../stats/help/Weibull.html">Weibull distribution</a> 
as follows.  Let <i>Y</i> be a <a href="../../stats/help/Weibull.html">Weibull random variable</a> with 
parameters 
<code>shape=</code><i>&beta;</i> and <code>scale=</code><i>&alpha;</i>.  Then <i>X = log(Y)</i> 
has a smallest extreme value distribution with parameters <code>location=</code><i>log(&alpha;)</i> 
and <code>scale=</code><i>1/&beta;</i>.
</p>
<p>The extreme value distribution has been used extensively to model the distribution 
of streamflow, flooding, rainfall, temperature, wind speed, and other 
meteorological variables, as well as material strength and life data.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Castillo, E. (1988).  <em>Extreme Value Theory in Engineering</em>.  
Academic Press, New York, pp.184&ndash;198.
</p>
<p>Downton, F. (1966).  Linear Estimates of Parameters in the Extreme Value 
Distribution.  <em>Technometrics</em> <b>8</b>(1), 3&ndash;17.
</p>
<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Greenwood, J.A., J.M. Landwehr, N.C. Matalas, and J.R. Wallis. (1979).  
Probability Weighted Moments: Definition and Relation to Parameters of Several 
Distributions Expressible in Inverse Form.  <em>Water Resources Research</em> 
<b>15</b>(5), 1049&ndash;1054.
</p>
<p>Hosking, J.R.M., J.R. Wallis, and E.F. Wood. (1985).  Estimation of the 
Generalized Extreme-Value Distribution by the Method of 
Probability-Weighted Moments.  <em>Technometrics</em> <b>27</b>(3), 251&ndash;261.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York.
</p>
<p>Landwehr, J.M., N.C. Matalas, and J.R. Wallis. (1979).  Probability Weighted 
Moments Compared With Some Traditional Techniques in Estimating Gumbel 
Parameters and Quantiles.  <em>Water Resources Research</em> <b>15</b>(5), 
1055&ndash;1064.
</p>
<p>Tiago de Oliveira, J. (1963).  Decision Results for the Parameters of the 
Extreme Value (Gumbel) Distribution Based on the Mean and Standard Deviation.  
<em>Trabajos de Estadistica</em> <b>14</b>, 61&ndash;81.
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/EVD.html">Extreme Value Distribution</a>, <a href="../../EnvStats/help/EulersConstant.html">Euler's Constant</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from an extreme value distribution with 
  # parameters location=2 and scale=1, then estimate the parameters 
  # and construct a 90% confidence interval for the location parameter. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- revd(20, location = 2) 
  eevd(dat, ci = TRUE, conf.level = 0.9) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Extreme Value
  #
  #Estimated Parameter(s):          location = 1.9684093
  #                                 scale    = 0.7481955
  #
  #Estimation Method:               mle
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         location
  #
  #Confidence Interval Method:      Normal Approximation
  #                                 (t Distribution)
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                90%
  #
  #Confidence Interval:             LCL = 1.663809
  #                                 UCL = 2.273009

  #----------

  #Compare the values of the different types of estimators: 

  eevd(dat, method = "mle")$parameters 
  # location     scale 
  #1.9684093 0.7481955

  eevd(dat, method = "mme")$parameters 
  # location     scale 
  #1.9575980 0.8339256 

  eevd(dat, method = "mmue")$parameters 
  # location     scale 
  #1.9450932 0.8555896 

  eevd(dat, method = "pwme")$parameters 
  # location     scale 
  #1.9434922 0.8583633

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
