<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: The Zero-Modified Lognormal (Delta) Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for ZeroModifiedLognormal {EnvStats}"><tr><td>ZeroModifiedLognormal {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
The Zero-Modified Lognormal (Delta) Distribution
</h2>

<h3>Description</h3>

<p>Density, distribution function, quantile function, and random generation 
for the zero-modified lognormal distribution with parameters <code>meanlog</code>, 
<code>sdlog</code>, and <code>p.zero</code>.
</p>
<p>The zero-modified lognormal (delta) distribution is the mixture of a 
lognormal distribution with a positive probability mass at 0.
</p>


<h3>Usage</h3>

<pre>
  dzmlnorm(x, meanlog = 0, sdlog = 1, p.zero = 0.5)
  pzmlnorm(q, meanlog = 0, sdlog = 1, p.zero = 0.5)
  qzmlnorm(p, meanlog = 0, sdlog = 1, p.zero = 0.5)
  rzmlnorm(n, meanlog = 0, sdlog = 1, p.zero = 0.5)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>q</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>vector of probabilities between 0 and 1.
</p>
</td></tr>
<tr valign="top"><td><code>n</code></td>
<td>

<p>sample size.  If <code>length(n)</code> is larger than 1, then <code>length(n)</code> 
random values are returned.
</p>
</td></tr>
<tr valign="top"><td><code>meanlog</code></td>
<td>

<p>vector of means of the normal (Gaussian) part of the distribution on the 
log scale.  The default is <code>meanlog=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>sdlog</code></td>
<td>

<p>vector of (positive) standard deviations of the normal (Gaussian) 
part of the distribution on the log scale.  The default is <code>sdlog=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>p.zero</code></td>
<td>

<p>vector of probabilities between 0 and 1 indicating the probability the random 
variable equals 0.  For <code>rzmlnorm</code> this must be a single, non-missing number.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The zero-modified lognormal (delta) distribution is the mixture of a 
lognormal distribution with a positive probability mass at 0.  This distribution 
was introduced without a name by Aitchison (1955), and the name 
<i>&Delta;</i>-distribution was coined by Aitchison and Brown (1957, p.95).  
It is a special case of a &ldquo;zero-modified&rdquo; distribution 
(see Johnson et al., 1992, p. 312).
</p>
<p>Let <i>f(x; &mu;, &sigma;)</i> denote the density of a 
<a href="../../stats/help/Lognormal.html">lognormal random variable</a> <i>X</i> with parameters 
<code>meanlog=</code><i>&mu;</i> and <code>sdlog=</code><i>&sigma;</i>.  The density function of a 
zero-modified lognormal (delta) random variable <i>Y</i> with parameters 
<code>meanlog=</code><i>&mu;</i>, <code>sdlog=</code><i>&sigma;</i>, and <code>p.zero=</code><i>p</i>, 
denoted <i>h(y; &mu;, &sigma;, p)</i>, is given by:
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>h(y; &mu;, &sigma;, p) =</i>  </td><td style="text-align: left;">  <i>p</i>  </td><td style="text-align: left;"> for <i>y = 0</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
                                  </td><td style="text-align: left;">  <i>(1 - p) f(y; &mu;, &sigma;)</i> </td><td style="text-align: left;"> for <i>y &gt; 0</i>
  </td>
</tr>

</table>

<p>Note that <i>&mu;</i> is <em>not</em> the mean of the zero-modified lognormal 
distribution on the log scale; it is the mean of the lognormal part of the 
distribution on the log scale.  Similarly, <i>&sigma;</i> is 
<em>not</em> the standard deviation of the zero-modified lognormal distribution
on the log scale; it is the standard deviation of the lognormal part of the 
distribution on the log scale.
</p>
<p>Let <i>&gamma;</i> and <i>&delta;</i> denote the mean and standard deviation of the 
overall zero-modified lognormal distribution on the log scale.  Aitchison (1955) 
shows that:
</p>
<p style="text-align: center;"><i>E[log(Y)] = &gamma; = (1 - p) &mu;</i></p>

<p style="text-align: center;"><i>Var[log(Y)] = &delta;^2 = (1 - p) &sigma;^2 + p (1-p) &mu;^2</i></p>

<p>Note that when <code>p.zero=</code><i>p</i><code>=0</code>, the zero-modified lognormal 
distribution simplifies to the lognormal distribution.
</p>


<h3>Value</h3>

<p><code>dzmlnorm</code> gives the density, <code>pzmlnorm</code> gives the distribution function, 
<code>qzmlnorm</code> gives the quantile function, and <code>rzmlnorm</code> generates random 
deviates. 
</p>


<h3>Note</h3>

<p>The zero-modified lognormal (delta) distribution is sometimes used to 
model chemical concentrations for which some observations are reported as 
&ldquo;Below Detection Limit&rdquo; (the nondetects are assumed equal to 0).  
See, for example, Gilliom and Helsel (1986), Owen and DeRouen (1980), and 
Gibbons et al. (2009, Chapter 12).  USEPA (2009, Chapter 15) recommends this 
strategy only in specific situations, and Helsel (2012, Chapter 1) strongly 
discourages this approach to dealing with non-detects.
</p>
<p>A variation of the zero-modified lognormal (delta) distribution is the 
<a href="../../EnvStats/help/ZeroModifiedNormal.html">zero-modified normal distribution</a>, in which a 
normal distribution is mixed with a positive probability mass at 0. 
</p>
<p>One way to try to assess whether a zero-modified lognormal (delta), 
zero-modified normal, censored normal, or censored lognormal is the best 
model for the data is to construct both censored and detects-only probability 
plots (see <code><a href="../../EnvStats/help/qqPlotCensored.html">qqPlotCensored</a></code>).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Aitchison, J. (1955).  On the Distribution of a Positive Random Variable Having 
a Discrete Probability Mass at the Origin.  <em>Journal of the American 
Statistical Association</em> <b>50</b>, 901-908.
</p>
<p>Aitchison, J., and J.A.C. Brown (1957).  <em>The Lognormal Distribution 
(with special reference to its uses in economics)</em>.  Cambridge University Press, 
London. pp.94-99.
</p>
<p>Crow, E.L., and K. Shimizu. (1988).  <em>Lognormal Distributions: 
Theory and Applications</em>.  Marcel Dekker, New York, pp.47-51.
</p>
<p>Gibbons, RD., D.K. Bhaumik, and S. Aryal. (2009).  <em>Statistical Methods 
for Groundwater Monitoring</em>.  Second Edition.  John Wiley and Sons, Hoboken, NJ.
</p>
<p>Gilliom, R.J., and D.R. Helsel. (1986).  Estimation of Distributional Parameters 
for Censored Trace Level Water Quality Data: 1. Estimation Techniques.  
<em>Water Resources Research</em> <b>22</b>, 135-146.
</p>
<p>Helsel, D.R. (2012).  <em>Statistics for Censored Environmental Data Using 
Minitab and R</em>.  Second Edition.  John Wiley and Sons, Hoboken, NJ, Chapter 1.
</p>
<p>Johnson, N. L., S. Kotz, and A.W. Kemp. (1992).  <em>Univariate Discrete Distributions</em>. 
Second Edition. John Wiley and Sons, New York, p.312.
</p>
<p>Owen, W., and T. DeRouen. (1980).  Estimation of the Mean for Lognormal Data 
Containing Zeros and Left-Censored Values, with Applications to the Measurement 
of Worker Exposure to Air Contaminants.  <em>Biometrics</em> <b>36</b>, 707-719.
</p>
<p>USEPA (1992c).  <em>Statistical Analysis of Ground-Water Monitoring Data at 
RCRA Facilities: Addendum to Interim Final Guidance</em>.  Office of Solid Waste, 
Permits and State Programs Division, US Environmental Protection Agency, 
Washington, D.C.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/ZeroModifiedLognormalAlt.html">Zero-Modified Lognormal (Alternative Parameterization)</a>, 
<a href="../../stats/help/Lognormal.html">Lognormal</a>, <a href="../../EnvStats/help/LognormalAlt.html">LognormalAlt</a>, 
<a href="../../EnvStats/help/ZeroModifiedNormal.html">Zero-Modified Normal</a>, 
<code><a href="../../EnvStats/help/ezmlnorm.html">ezmlnorm</a></code>, <a href="../../EnvStats/help/Probability+20Distributions+20and+20Random+20Numbers.html">Probability Distributions and Random Numbers</a>.
</p>


<h3>Examples</h3>

<pre>
  # Density of the zero-modified lognormal (delta) distribution with 
  # parameters meanlog=0, sdlog=1, and p.zero=0.5, evaluated at 
  # 0, 0.5, 1, 1.5, and 2:

  dzmlnorm(seq(0, 2, by = 0.5)) 
  #[1] 0.50000000 0.31374804 0.19947114 0.12248683 
  #[5] 0.07843701

  #----------

  # The cdf of the zero-modified lognormal (delta) distribution with 
  # parameters meanlog=1, sdlog=2, and p.zero=0.1, evaluated at 4:

  pzmlnorm(4, 1, 2, .1) 
  #[1] 0.6189203

  #----------

  # The median of the zero-modified lognormal (delta) distribution with 
  # parameters meanlog=2, sdlog=3, and p.zero=0.1:

  qzmlnorm(0.5, 2, 3, 0.1) 
  #[1] 4.859177

  #----------

  # Random sample of 3 observations from the zero-modified lognormal 
  # (delta) distribution with parameters meanlog=1, sdlog=2, and p.zero=0.4. 
  # (Note: The call to set.seed simply allows you to reproduce this example.)

  set.seed(20) 
  rzmlnorm(3, 1, 2, 0.4)
  #[1] 0.000000 0.000000 3.146641
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
