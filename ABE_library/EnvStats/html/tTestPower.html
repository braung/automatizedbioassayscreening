<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Power of a One- or Two-Sample t-Test</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tTestPower {EnvStats}"><tr><td>tTestPower {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Power of a One- or Two-Sample t-Test
</h2>

<h3>Description</h3>

<p>Compute the power of a one- or two-sample t-test, given the sample size, scaled 
difference, and significance level.
</p>


<h3>Usage</h3>

<pre>
  tTestPower(n.or.n1, n2 = n.or.n1, delta.over.sigma = 0, alpha = 0.05, 
    sample.type = ifelse(!missing(n2), "two.sample", "one.sample"), 
    alternative = "two.sided", approx = FALSE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n.or.n1</code></td>
<td>

<p>numeric vector of sample sizes.  When <code>sample.type="one.sample"</code>, 
<code>n.or.n1</code> denotes <i>n</i>, the number of observations in the single sample.  When <br />
<code>sample.type="two.sample"</code>, <code>n.or.n1</code> denotes <i>n_1</i>, the number 
of observations from group 1.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are <b><em>not</em></b> allowed.
</p>
</td></tr>
<tr valign="top"><td><code>n2</code></td>
<td>

<p>numeric vector of sample sizes for group 2.  The default value is the value of 
<code>n.or.n1</code>. This argument is ignored when <code>sample.type="one.sample"</code>. 
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are <b><em>not</em></b> allowed.
</p>
</td></tr>
<tr valign="top"><td><code>delta.over.sigma</code></td>
<td>

<p>numeric vector specifying the ratio of the true difference <i>&delta;</i>  
(<i>&delta; = &mu; - &mu;_0</i> for the one-sample case and 
<i>&delta; = &mu;_1 - &mu;_2</i> for the two-sample case) 
to the population standard deviation (<i>&sigma;</i>).  This is also called the 
&ldquo;scaled difference&rdquo;.  
</p>
<p>The default value is <code>delta.over.sigma=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alpha</code></td>
<td>

<p>numeric vector of numbers between 0 and 1 indicating the Type I error level 
associated with the hypothesis test.  The default value is <code>alpha=0.05</code>.
</p>
</td></tr>
<tr valign="top"><td><code>sample.type</code></td>
<td>

<p>character string indicating whether to compute power based on a one-sample or 
two-sample hypothesis test.  When <code>sample.type="one.sample"</code>, the computed 
power is based on a hypothesis test for a single mean.  When <br />
<code>sample.type="two.sample"</code>, the computed power is based on a hypothesis test 
for the difference between two means.  The default value is 
<code>sample.type="one.sample"</code> unless the argument <code>n2</code> is supplied.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values 
are: 
</p>

<ul>
<li> <p><code>"two.sided"</code> (the default).  <i>H_a: &mu; \ne &mu;_0</i> for the one-sample case and 
<i>H_a: &mu;_1 \ne &mu;_2</i> for the two-sample case. 
</p>
</li>
<li> <p><code>"greater"</code>.  <i>H_a: &mu; &gt; &mu;_0</i> for the one-sample case and 
<i>H_a: &mu;_1 &gt; &mu;_2</i> for the two-sample case. 
</p>
</li>
<li> <p><code>"less"</code>.  <i>H_a: &mu; &lt; &mu;_0</i> for the one-sample case and 
<i>H_a: &mu;_1 &lt; &mu;_2</i> for the two-sample case.
</p>
</li></ul>

</td></tr>
<tr valign="top"><td><code>approx</code></td>
<td>

<p>logical scalar indicating whether to compute the power based on an approximation to 
the non-central t-distribution.  The default value is <code>FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If the arguments <code>n.or.n1</code>, <code>n2</code>, <code>delta.over.sigma</code>, and 
<code>alpha</code> are not all the same length, they are replicated to be the same 
length as the length of the longest argument.
</p>
<p><b>One-Sample Case</b> (<code>sample.type="one.sample"</code>) <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote a vector of <i>n</i> 
observations from a <a href="../../stats/help/Normal.html">normal distribution</a> with mean <i>&mu;</i> 
and standard deviation <i>&sigma;</i>, and consider the null hypothesis:
</p>
<p style="text-align: center;"><i>H_0: &mu; = &mu;_0 \;\;\;\;\;\; (1)</i></p>

<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>):
</p>
<p style="text-align: center;"><i>H_a: &mu; &gt; &mu;_0 \;\;\;\;\;\; (2)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: &mu; &lt; &mu;_0 \;\;\;\;\;\; (3)</i></p>

<p>and the two-sided alternative (<code>alternative="two.sided"</code>)
</p>
<p style="text-align: center;"><i>H_a: &mu; \ne &mu;_0 \;\;\;\;\;\; (4)</i></p>

<p>The test of the null hypothesis (1) versus any of the three alternatives (2)-(4) is 
based on the <a href="../../stats/help/t.test.html">Student t-statistic</a>:
</p>
<p style="text-align: center;"><i>t = \frac{\bar{x} - &mu;_0}{s/&radic;{n}} \;\;\;\;\;\; (5)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (6)</i></p>

<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (7)</i></p>

<p>Under the null hypothesis (1), the t-statistic in (5) follows a 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>n-1</i> degrees of freedom 
(Zar, 2010, Chapter 7; Johnson et al., 1995, pp.362-363).
</p>
<p>The formula for the power of the test depends on which alternative is being tested.  
The two subsections below describe exact and approximate formulas for the power of 
the one-sample t-test.  Note that none of the equations for the power of the t-test 
requires knowledge of the values <i>&delta;</i> (Equation (12) below) or <i>&sigma;</i> 
(the population standard deviation), only the ratio <i>&delta;/&sigma;</i>.  The 
argument <code>delta.over.sigma</code> is this ratio, and it is referred to as the 
&ldquo;scaled difference&rdquo;.
<br />
</p>
<p><b><em>Exact Power Calculations</em></b> (<code>approx=FALSE</code>) <br />
This subsection describes the exact formulas for the power of the one-sample 
t-test.
<br />
</p>
<p><em>Upper one-sided alternative</em> (<code>alternative="greater"</code>) <br />
The standard Student's t-test rejects the null hypothesis (1) in favor of the 
upper alternative hypothesis (2) at level-<i>&alpha;</i> if
</p>
<p style="text-align: center;"><i>t &ge; t_{&nu;}(1 - &alpha;) \;\;\;\;\;\; (8)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>&nu; = n - 1 \;\;\;\;\;\; (9)</i></p>

<p>and <i>t_{&nu;}(p)</i> denotes the <i>p</i>'th quantile of Student's t-distribution 
with <i>&nu;</i> degrees of freedom (Zar, 2010; Berthouex and Brown, 2002).  
The power of this test, denoted by <i>1-&beta;</i>, where <i>&beta;</i> denotes the 
probability of a Type II error, is given by:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t_{&nu;, &Delta;} &ge; t_{&nu;}(1 - &alpha;)] = 1 - G[t_{&nu;}(1 - &alpha;), &nu;, &Delta;] \;\;\;\;\;\; (10)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>&Delta; = &radic;{n} (\frac{&delta;}{&sigma;}) \;\;\;\;\;\; (11)</i></p>

<p style="text-align: center;"><i>&delta; = &mu; - &mu;_0 \;\;\;\;\;\ (12)</i></p>

<p>and <i>t_{&nu;, &Delta;}</i> denotes a 
<a href="../../stats/help/TDist.html">non-central Student's t-random variable</a> with 
<i>&nu;</i> degrees of freedom and non-centrality parameter <i>&Delta;</i>, and 
<i>G(x, &nu;, &Delta;)</i> denotes the cumulative distribution function of this 
random variable evaluated at <i>x</i> (Johnson et al., 1995, pp.508-510).
<br />
</p>
<p><em>Lower one-sided alternative</em> (<code>alternative="less"</code>) <br />
The standard Student's t-test rejects the null hypothesis (1) in favor of the 
lower alternative hypothesis (3) at level-<i>&alpha;</i> if
</p>
<p style="text-align: center;"><i>t &le; t_{&nu;}(&alpha;) \;\;\;\;\;\; (13)</i></p>

<p>and the power of this test is given by:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t_{&nu;, &Delta;} &le; t_{&nu;}(&alpha;)] = G[t_{&nu;}(&alpha;), &nu;, &Delta;] \;\;\;\;\;\; (14)</i></p>

<p><br />
</p>
<p><em>Two-sided alternative</em> (<code>alternative="two.sided"</code>) <br />
The standard Student's t-test rejects the null hypothesis (1) in favor of the 
two-sided alternative hypothesis (4) at level-<i>&alpha;</i> if
</p>
<p style="text-align: center;"><i>|t| &ge; t_{&nu;}(1 - &alpha;/2) \;\;\;\;\;\; (15)</i></p>

<p>and the power of this test is given by:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t_{&nu;, &Delta;} &le; t_{&nu;}(&alpha;/2)] + Pr[t_{&nu;, &Delta;} &ge; t_{&nu;}(1 - &alpha;/2)]</i></p>

<p style="text-align: center;"><i>= G[t_{&nu;}(&alpha;/2), &nu;, &Delta;] + 1 - G[t_{&nu;}(1 - &alpha;/2), &nu;, &Delta;] \;\;\;\;\;\; (16)</i></p>

<p>The power of the t-test given in Equation (16) can also be expressed in terms of the 
cumulative distribution function of the <a href="../../stats/help/FDist.html">non-central F-distribution</a> 
as follows. Let <i>F_{&nu;_1, &nu;_2, &Delta;}</i> denote a 
<a href="../../stats/help/FDist.html">non-central F random variable</a> with <i>&nu;_1</i> and 
<i>&nu;_2</i> degrees of freedom and non-centrality parameter <i>&Delta;</i>, and let 
<i>H(x, &nu;_1, &nu;_2, &Delta;)</i> denote the cumulative distribution function of this 
random variable evaluated at <i>x</i>. Also, let <i>F_{&nu;_1, &nu;_2}(p)</i> denote 
the <i>p</i>'th quantile of the central F-distribution with <i>&nu;_1</i> and 
<i>&nu;_2</i> degrees of freedom.  It can be shown that
</p>
<p style="text-align: center;"><i>(t_{&nu;, &Delta;})^2 \cong F_{1, &nu;, &Delta;^2} \;\;\;\;\;\; (17)</i></p>

<p>where <i>\cong</i> denotes &ldquo;equal in distribution&rdquo;.  Thus, it follows that
</p>
<p style="text-align: center;"><i>[t_{&nu;}(1 - &alpha;/2)]^2 = F_{1, &nu;}(1 - &alpha;) \;\;\;\;\;\; (18)</i></p>

<p>so the formula for the power of the t-test given in Equation (16) can also be 
written as:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr\{(t_{&nu;, &Delta;})^2  &ge; [t_{&nu;}(1 - &alpha;/2)]^2\}</i></p>

<p style="text-align: center;"><i>= Pr[F_{1, &nu;, &Delta;^2} &ge; F_{1, &nu;}(1 - &alpha;)] = 1 - H[F_{1, &nu;}(1-&alpha;), 1, &nu;, &Delta;^2] \;\;\;\;\;\; (19)</i></p>

<p><br />
</p>
<p><b><em>Approximate Power Calculations</em></b> (<code>approx=TRUE</code>) <br />
Zar (2010, pp.115&ndash;118) presents an approximation to the power for the t-test 
given in Equations (10), (14), and (16) above.  His approximation to the power 
can be derived by using the approximation
</p>
<p style="text-align: center;"><i>&radic;{n} (\frac{&delta;}{s}) \approx &radic;{n} (\frac{&delta;}{&sigma;}) = &Delta; \;\;\;\;\;\; (20)</i></p>

<p>where <i>\approx</i> denotes &ldquo;approximately equal to&rdquo;.  Zar's approximation 
can be summarized in terms of the cumulative distribution function of the 
non-central t-distribution as follows:
</p>
<p style="text-align: center;"><i>G(x, &nu;, &Delta;) \approx G(x - &Delta;, &nu;, 0) = G(x - &Delta;, &nu;) \;\;\;\;\;\; (21)</i></p>

<p>where <i>G(x, &nu;)</i> denotes the cumulative distribution function of the 
central Student's t-distribution with <i>&nu;</i> degrees of freedom evaluated at 
<i>x</i>.
</p>
<p>The following three subsections explicitly derive the approximation to the power of 
the t-test for each of the three alternative hypotheses.
<br />
</p>
<p><em>Upper one-sided alternative</em> (<code>alternative="greater"</code>) <br />
The power for the upper one-sided alternative (2) given in Equation (10) can be 
approximated as:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t &ge; t_{&nu;}(1 - &alpha;)]</i></p>

<p style="text-align: center;"><i>= Pr[\frac{\bar{x} - &mu;}{s/&radic;{n}} &ge; t_{&nu;}(1 - &alpha;) - &radic;{n}\frac{&delta;}{s}]</i></p>

<p style="text-align: center;"><i>\approx Pr[t_{&nu;} &ge; t_{&nu;}(1 - &alpha;) - &Delta;]</i></p>

<p style="text-align: center;"><i>= 1 - Pr[t_{&nu;} &le; t_{&nu;}(1 - &alpha;) - &Delta;]</i></p>

<p style="text-align: center;"><i> = 1 - G[t_{&nu;}(1-&alpha;) - &Delta;, &nu;] \;\;\;\;\;\; (22)</i></p>

<p>where <i>t_{&nu;}</i> denotes a central Student's t-random variable with <i>&nu;</i> 
degrees of freedom.
<br />
</p>
<p><em>Lower one-sided alternative</em> (<code>alternative="less"</code>) <br />
The power for the lower one-sided alternative (3) given in Equation (14) can be 
approximated as:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t &le; t_{&nu;}(&alpha;)]</i></p>

<p style="text-align: center;"><i>= Pr[\frac{\bar{x} - &mu;}{s/&radic;{n}} &le; t_{&nu;}(&alpha;) - &radic;{n}\frac{&delta;}{s}]</i></p>

<p style="text-align: center;"><i>\approx Pr[t_{&nu;} &le; t_{&nu;}(&alpha;) - &Delta;]</i></p>

<p style="text-align: center;"><i> = G[t_{&nu;}(&alpha;) - &Delta;, &nu;] \;\;\;\;\;\; (23)</i></p>

<p><br />
</p>
<p><em>Two-sided alternative</em> (<code>alternative="two.sided"</code>) <br />
The power for the two-sided alternative (4) given in Equation (16) can be 
approximated as:
</p>
<p style="text-align: center;"><i>1 - &beta; = Pr[t &le; t_{&nu;}(&alpha;/2)] + Pr[t &ge; t_{&nu;}(1 - &alpha;/2)]</i></p>

<p style="text-align: center;"><i>= Pr[\frac{\bar{x} - &mu;}{s/&radic;{n}} &le; t_{&nu;}(&alpha;/2) - &radic;{n}\frac{&delta;}{s}] + Pr[\frac{\bar{x} - &mu;}{s/&radic;{n}} &ge; t_{&nu;}(1 - &alpha;) - &radic;{n}\frac{&delta;}{s}]</i></p>

<p style="text-align: center;"><i>\approx Pr[t_{&nu;} &le; t_{&nu;}(&alpha;/2) - &Delta;] + Pr[t_{&nu;} &ge; t_{&nu;}(1 - &alpha;/2) - &Delta;]</i></p>

<p style="text-align: center;"><i>= G[t_{&nu;}(&alpha;/2) - &Delta;, &nu;] + 1 - G[t_{&nu;}(1-&alpha;/2) - &Delta;, &nu;] \;\;\;\;\;\; (24)</i></p>

<p><br />
</p>
<p><b>Two-Sample Case</b> (<code>sample.type="two.sample"</code>) <br />
Let <i>\underline{x}_1 = x_{11}, x_{12}, &hellip;, x_{1n_1}</i> denote a vector of 
<i>n_1</i> observations from a <a href="../../stats/help/Normal.html">normal distribution</a> with mean 
<i>&mu;_1</i> and standard deviation <i>&sigma;</i>, and let 
<i>\underline{x}_2 = x_{21}, x_{22}, &hellip;, x_{2n_2}</i> denote a vector of 
<i>n_2</i> observations from a normal distribution with mean <i>&mu;_2</i> and 
standard deviation <i>&sigma;</i>, and consider the null hypothesis:
</p>
<p style="text-align: center;"><i>H_0: &mu;_1 = &mu;_2 \;\;\;\;\;\; (25)</i></p>

<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>):
</p>
<p style="text-align: center;"><i>H_a: &mu;_1 &gt; &mu;_2 \;\;\;\;\;\; (26)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: &mu;_1 &lt; &mu;_2 \;\;\;\;\;\; (27)</i></p>

<p>and the two-sided alternative (<code>alternative="two.sided"</code>)
</p>
<p style="text-align: center;"><i>H_a: &mu;_1 \ne &mu;_2 \;\;\;\;\;\; (28)</i></p>

<p>The test of the null hypothesis (25) versus any of the three alternatives (26)-(28) 
is based on the <a href="../../stats/help/t.test.html">Student t-statistic</a>:
</p>
<p style="text-align: center;"><i>t = \frac{\bar{x}_1 - \bar{x}_2}{s_p&radic;{\frac{1}{n_1} + \frac{1}{n_2}}} \;\;\;\;\;\; (29)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>\bar{x}_1 = \frac{1}{n_1} &sum;_{i=1}^{n_1} x_{1i} \;\;\;\;\;\; (30)</i></p>

<p style="text-align: center;"><i>\bar{x}_2 = \frac{1}{n_2} &sum;_{i=1}^{n_2} x_{2i} \;\;\;\;\;\; (31)</i></p>

<p style="text-align: center;"><i>s_p^2 = \frac{(n_1 - 1) s_1^2 + (n_2 - 1) s_2^2}{n_1 + n_2 - 2} \;\;\;\;\;\; (32)</i></p>

<p style="text-align: center;"><i>s_1^2 = \frac{1}{n_1 - 1} &sum;_{i=1}^{n_1} (x_{1i} - \bar{x}_1)^2 \;\;\;\;\;\; (33)</i></p>

<p style="text-align: center;"><i>s_2^2 = \frac{1}{n_2 - 1} &sum;_{i=1}^{n_2} (x_{2i} - \bar{x}_2)^2 \;\;\;\;\;\; (34)</i></p>

<p>Under the null hypothesis (25), the t-statistic in (29) follows a 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>n_1 + n_2 - 2</i> degrees of 
freedom (Zar, 2010, Chapter 8; Johnson et al., 1995, pp.508&ndash;510, 
Helsel and Hirsch, 1992, pp.124&ndash;128).
</p>
<p>The formulas for the power of the two-sample t-test are precisely the same as those 
for the one-sample case, with the following modifications:
</p>
<p style="text-align: center;"><i>&nu; = n_1 + n_2 - 2 \;\;\;\;\;\; (35)</i></p>

<p style="text-align: center;"><i>&Delta; = &radic;{\frac{n_1 n_2}{n_1 + n_2}} (\frac{&delta;}{&sigma;}) \;\;\;\;\;\; (36)</i></p>

<p style="text-align: center;"><i>&delta; = &mu;_1 - &mu;_2 \;\;\;\;\;\; (37)</i></p>

<p>Note that none of the equations for the power of the t-test requires knowledge of 
the values <i>&delta;</i> or <i>&sigma;</i> (the population standard deviation for 
both populations), only the ratio <i>&delta;/&sigma;</i>.  The argument 
<code>delta.over.sigma</code> is this ratio, and it is referred to as the 
&ldquo;scaled difference&rdquo;.
</p>


<h3>Value</h3>

<p>a numeric vector powers.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Normal.html">normal distribution</a> and 
<a href="../../stats/help/Lognormal.html">lognormal distribution</a> are probably the two most 
frequently used distributions to model environmental data.  Often, you need to 
determine whether a population mean is significantly different from a specified 
standard (e.g., an MCL or ACL, USEPA, 1989b, Section 6), or whether two different 
means are significantly different from each other (e.g., USEPA 2009, Chapter 16).  
In this case, assuming normally distributed data, you can perform the 
Student's t-test.
</p>
<p>In the course of designing a sampling program, an environmental scientist may wish 
to determine the relationship between sample size, significance level, power, and 
scaled difference if one of the objectives of the sampling program is to determine 
whether a mean differs from a specified level or two means differ from each other.  
The functions <code>tTestPower</code>, <code><a href="../../EnvStats/help/tTestN.html">tTestN</a></code>, 
<code><a href="../../EnvStats/help/tTestScaledMdd.html">tTestScaledMdd</a></code>, and <code><a href="../../EnvStats/help/plotTTestDesign.html">plotTTestDesign</a></code> can be used to 
investigate these relationships for the case of normally-distributed observations.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). 
<em>Statistics for Environmental Engineers</em>.  Second Edition.   
Lewis Publishers, Boca Raton, FL.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). 
<em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, Chapter 7.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995).  <em>Continuous Univariate 
Distributions, Volume 2</em>.  Second Edition.  John Wiley and Sons, New York, 
Chapters 28, 31
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, FL.
</p>
<p>USEPA. (1989b). <em>Statistical Analysis of Ground-Water Monitoring Data at RCRA Facilities, Interim Final Guidance</em>. 
EPA/530-SW-89-026. Office of Solid Waste, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/tTestN.html">tTestN</a></code>, <code><a href="../../EnvStats/help/tTestScaledMdd.html">tTestScaledMdd</a></code>, <code><a href="../../EnvStats/help/tTestAlpha.html">tTestAlpha</a></code>, 
<code><a href="../../EnvStats/help/plotTTestDesign.html">plotTTestDesign</a></code>, <a href="../../stats/help/Normal.html">Normal</a>, 
<code><a href="../../stats/html/t.test.html">t.test</a></code>, <a href="../../EnvStats/help/Hypothesis+20Tests.html">Hypothesis Tests</a>. 
</p>


<h3>Examples</h3>

<pre>
  # Look at how the power of the one-sample t-test increases with 
  # increasing sample size:

  seq(5, 30, by = 5) 
  #[1] 5 10 15 20 25 30 

  power &lt;- tTestPower(n.or.n1 = seq(5, 30, by = 5), delta.over.sigma = 0.5) 

  round(power, 2) 
  #[1] 0.14 0.29 0.44 0.56 0.67 0.75

  #----------

  # Repeat the last example, but use the approximation.
  # Note how the approximation underestimates the power 
  # for the smaller sample sizes.
  #----------------------------------------------------

  power &lt;- tTestPower(n.or.n1 = seq(5, 30, by = 5), delta.over.sigma = 0.5, 
    approx = TRUE) 

  round(power, 2) 
  #[1] 0.10 0.26 0.42 0.56 0.67 0.75

  #----------

  # Look at how the power of the two-sample t-test increases with increasing 
  # scaled difference:

  seq(0.5, 2, by = 0.5) 
  #[1] 0.5 1.0 1.5 2.0 

  power &lt;- tTestPower(10, sample.type = "two.sample", 
    delta.over.sigma = seq(0.5, 2, by = 0.5)) 

  round(power, 2) 
  #[1] 0.19 0.56 0.89 0.99

  #----------

  # Look at how the power of the two-sample t-test increases with increasing values 
  # of Type I error:

  power &lt;- tTestPower(20, sample.type = "two.sample", delta.over.sigma = 0.5, 
    alpha = c(0.001, 0.01, 0.05, 0.1)) 

  round(power, 2) 
  #[1] 0.03 0.14 0.34 0.46

  #==========

  # Modifying the example on pages 21-4 to 21-5 of USEPA (2009), determine how 
  # adding another four months of observations to increase the sample size from 
  # 4 to 8 for any one particular compliance well will affect the power of a 
  # one-sample t-test that compares the mean for the well with the MCL of 
  # 7 ppb.  Use alpha = 0.01, assume an upper one-sided alternative 
  # (i.e., compliance well mean larger than 7 ppb), and assume a scaled 
  # difference of 2.  (The data are stored in EPA.09.Ex.21.1.aldicarb.df.) 
  # Note that the power changes from 49% to 98% by increasing the sample size 
  # from 4 to 8.

  tTestPower(n.or.n1 = c(4, 8), delta.over.sigma = 2, alpha = 0.01, 
    sample.type = "one.sample", alternative = "greater")
  #[1] 0.4865800 0.9835401

  #==========

  # Clean up
  #---------
  rm(power)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
