<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Weibull Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eweibull {EnvStats}"><tr><td>eweibull {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Weibull Distribution
</h2>

<h3>Description</h3>

<p>Estimate the shape and scale parameters of a 
<a href="../../stats/help/Weibull.html">Weibull distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  eweibull(x, method = "mle")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.  Missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Possible values are 
<code>"mle"</code> (maximum likelihood; the default), <code>"mme"</code> (methods of moments), 
and <code>"mmue"</code> (method of moments based on the unbiased estimator of variance).  
See the DETAILS section for more information on these estimation methods.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from an <a href="../../stats/help/Weibull.html">Weibull distribution</a> with 
parameters <code>shape=</code><i>&alpha;</i> and <code>scale=</code><i>&beta;</i>.
</p>
<p><b>Estimation</b> <br />
</p>
<p><em>Maximum Likelihood Estimation</em> (<code>method="mle"</code>) <br />
The maximum likelihood estimators (mle's) of <i>&alpha;</i> and <i>&beta;</i> are 
the solutions of the simultaneous equations (Forbes et al., 2011):
</p>
<p style="text-align: center;"><i>\hat{&alpha;}_{mle} = \frac{n}{\{(1/\hat{&beta;}_{mle})^{\hat{&alpha;}_{mle}} &sum;_{i=1}^n [x_i^{\hat{&alpha;}_{mle}} log(x_i)]\} - &sum;_{i=1}^n log(x_i) }  \;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>\hat{&beta;}_{mle} = [\frac{1}{n} &sum;_{i=1}^n x_i^{\hat{&alpha;}_{mle}}]^{1/\hat{&alpha;}_{mle}} \;\;\;\; (2)</i></p>

<p><br />
</p>
<p><em>Method of Moments Estimation</em> (<code>method="mme"</code>) <br />
The method of moments estimator (mme) of <i>&alpha;</i> is computed by solving the 
equation:
</p>
<p style="text-align: center;"><i>\frac{s}{\bar{x}} = \{\frac{&Gamma;[(\hat{&alpha;}_{mme} + 2)/\hat{&alpha;}_{mme}]}{\{&Gamma;[(\hat{&alpha;}_{mme} + 1)/\hat{&alpha;}_{mme}] \}^2} - 1 \}^{1/2} \;\;\;\; (3)</i></p>

<p>and the method of moments estimator (mme) of <i>&beta;</i> is then computed as:
</p>
<p style="text-align: center;"><i>\hat{&beta;}_{mme} = \frac{\bar{x}}{&Gamma;[(\hat{&alpha;}_{mme} + 1)/\hat{&alpha;}_{mme}]} \;\;\;\; (4)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\; (5)</i></p>

<p style="text-align: center;"><i>s^2_m = \frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (6)</i></p>

<p>and <i>&Gamma;()</i> denotes the <a href="../../base/help/Special.html">gamma function</a>.
<br />
</p>
<p><em>Method of Moments Estimation Based on the Unbiased Estimator of Variance</em> (<code>method="mmue"</code>) <br />
The method of moments estimators based on the unbiased estimator of variance are 
exactly the same as the method of moments estimators given in equations (3-6) above, 
except that the method of moments estimator of variance in equation (6) is replaced 
with the unbiased estimator of variance:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (7)</i></p>



<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other 
information.  See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Weibull.html">Weibull distribution</a> is named after the Swedish physicist 
Waloddi Weibull, who used this distribution to model breaking strengths of 
materials.  The Weibull distribution has been extensively applied in the fields 
of reliability and quality control.
</p>
<p>The <a href="../../stats/help/Exponential.html">exponential distribution</a> is a special case of the 
Weibull distribution: a Weibull random variable with parameters <code>shape=</code><i>1</i> 
and <code>scale=</code><i>&beta;</i> is equivalent to an exponential random variable with 
parameter <code>rate=</code><i>1/&beta;</i>.
</p>
<p>The Weibull distribution is related to the 
<a href="../../EnvStats/help/EVD.html">Type I extreme value (Gumbel) distribution</a> as follows: 
if <i>X</i> is a random variable from a Weibull distribution with parameters 
<code>shape=</code><i>&alpha;</i> and <code>scale=</code><i>&beta;</i>, then 
</p>
<p style="text-align: center;"><i>Y = -log(X) \;\;\;\; (10)</i></p>
 
<p>is a random variable from an extreme value distribution with parameters 
<code>location=</code><i>-log(&beta;)</i> and <code>scale=</code><i>1/&alpha;</i>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Weibull.html">Weibull</a>, <a href="../../stats/help/Exponential.html">Exponential</a>, <a href="../../EnvStats/help/EVD.html">EVD</a>, 
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a Weibull distribution with parameters 
  # shape=2 and scale=3, then estimate the parameters via maximum likelihood. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rweibull(20, shape = 2, scale = 3) 
  eweibull(dat) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Weibull
  #
  #Estimated Parameter(s):          shape = 2.673098
  #                                 scale = 3.047762
  #
  #Estimation Method:               mle
  #
  #Data:                            dat
  #
  #Sample Size:                     20

  #----------

  # Use the same data as in previous example, and compute the method of 
  # moments estimators based on the unbiased estimator of variance:

  eweibull(dat, method = "mmue") 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Weibull
  #
  #Estimated Parameter(s):          shape = 2.528377
  #                                 scale = 3.052507
  #
  #Estimation Method:               mmue
  #
  #Data:                            dat
  #
  #Sample Size:                     20

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
