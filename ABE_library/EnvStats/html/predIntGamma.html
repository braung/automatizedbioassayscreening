<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Prediction Interval for Gamma Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for predIntGamma {EnvStats}"><tr><td>predIntGamma {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Prediction Interval for Gamma Distribution
</h2>

<h3>Description</h3>

<p>Construct a prediction interval for the next <i>k</i> observations or 
next set of <i>k</i> transformed means for a gamma distribution.
</p>


<h3>Usage</h3>

<pre>
  predIntGamma(x, n.transmean = 1, k = 1, method = "Bonferroni", 
    pi.type = "two-sided", conf.level = 0.95, est.method = "mle", 
    normal.approx.transform = "kulkarni.powar")

  predIntGammaAlt(x, n.transmean = 1, k = 1, method = "Bonferroni", 
    pi.type = "two-sided", conf.level = 0.95, est.method = "mle",
    normal.approx.transform = "kulkarni.powar")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of non-negative observations. Missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>n.transmean</code></td>
<td>

<p>positive integer specifying the sample size associated with the <code>k</code> 
future transformed means (see the DETAILS section for an explanation of 
what the transformation is).  The default value is <code>n.transmean=1</code> 
(i.e., predicting future observations).  Note that all future 
transformed means must be based on the same sample size. 
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>positive integer specifying the number of future observations or means 
the prediction interval should contain with confidence level <code>conf.level</code>.  
The default value is <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method to use if the number of future observations 
or averages (<code>k</code>) is greater than 1.  The possible values are <code>"Bonferroni"</code> 
(approximate method based on Bonferonni inequality; the default), and <br />
<code>"exact"</code> (exact method due to Dunnett, 1955).  See the DETAILS section for more information.  
This argument is ignored if <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>pi.type</code></td>
<td>

<p>character string indicating what kind of prediction interval to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level associated with the prediction 
interval.  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>est.method</code></td>
<td>

<p>character string specifying the method of estimation for the shape and scale 
distribution parameters.  The possible values are 
<code>"mle"</code> (maximum likelihood; the default), 
<code>"bcmle"</code> (bias-corrected mle), <code>"mme"</code> (method of moments), and 
<code>"mmue"</code> (method of moments based on the unbiased estimator of variance). 
See the DETAILS section of the help file for <code><a href="../../EnvStats/help/egamma.html">egamma</a></code> for more information.
</p>
</td></tr>
<tr valign="top"><td><code>normal.approx.transform</code></td>
<td>

<p>character string indicating which power transformation to use.  
Possible values are <code>"kulkarni.powar"</code> (the default), <code>"cube.root"</code>, and <br />
<code>"fourth.root"</code>.  See the DETAILS section for more informaiton.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>The function <code>predIntGamma</code> returns a prediction interval as well as 
estimates of the shape and scale parameters.  
The function <code>predIntGammaAlt</code> returns a prediction interval as well as 
estimates of the mean and coefficient of variation.
</p>
<p>Following Krishnamoorthy et al. (2008), the prediction interval is 
computed by:
</p>

<ol>
<li><p> using a power transformation on the original data to induce approximate 
normality, 
</p>
</li>
<li><p> calling <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code> with the transformed data to 
compute the prediction interval, and then 
</p>
</li>
<li><p> back-transforming the interval to create a prediction interval 
on the original scale.  
</p>
</li></ol>

<p>The argument <code>normal.approx.transform</code> determines which transformation is used. 
The value <code>normal.approx.transform="cube.root"</code> uses 
the cube root transformation suggested by Wilson and Hilferty (1931) and used by 
Krishnamoorthy et al. (2008) and Singh et al. (2010b), and the value 
<code>normal.approx.transform="fourth.root"</code> uses the fourth root transformation suggested 
by Hawkins and Wixley (1986) and used by Singh et al. (2010b).  
The default value <code>normal.approx.transform="kulkarni.powar"</code> 
uses the &quot;Optimum Power Normal Approximation Method&quot; of Kulkarni and Powar (2010).  
The &quot;optimum&quot; power <i>p</i> is determined by:
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
  <i>p = -0.0705 - 0.178 \, shape + 0.475 \, &radic;{shape}</i> </td><td style="text-align: left;"> if <i>shape &le; 1.5</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
  <i>p = 0.246</i> </td><td style="text-align: left;"> if <i>shape &gt; 1.5</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
  </td>
</tr>

</table>

<p>where <i>shape</i> denotes the estimate of the shape parameter.  Although 
Kulkarni and Powar (2010) use the maximum likelihood estimate of shape to 
determine the power <i>p</i>, for the functions <br />
<code>predIntGamma</code> and <code>predIntGammaAlt</code> the power <i>p</i> is based on whatever estimate of 
shape is used (e.g., <code>est.method="mle"</code>, <code>est.method="bcmle"</code>, etc.).
</p>
<p>When the argument <code>n.transmean</code> is larger than 1 (i.e., you are 
constructing a prediction interval for future means, not just single 
observations), in order to properly compare a future mean with the 
prediction limits, you must follow these steps:
</p>

<ol>
<li><p> Take the observations that will be used to compute the mean and 
transform them by raising them to the power given by the value in the component 
<code>interval$normal.transform.power</code> (see the section VALUE below).
</p>
</li>
<li><p> Compute the mean of the transformed observations.
</p>
</li>
<li><p> Take the mean computed in step 2 above and raise it to the inverse of the power 
originally used to transform the observations.
</p>
</li></ol>



<h3>Value</h3>

<p>A list of class <code>"estimate"</code> containing the estimated parameters, 
the prediction interval, and other information.  See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> 
for details.  
</p>
<p>In addition to the usual components contained in an object of class 
<code>"estimate"</code>, the returned value also includes two additional 
components within the <code>"interval"</code> component:
</p>
<table summary="R valueblock">
<tr valign="top"><td><code>n.transmean</code></td>
<td>
<p>the value of <code>n.transmean</code> supplied in the 
call to <code>predIntGamma</code> or <code>predIntGammaAlt</code>.</p>
</td></tr>
<tr valign="top"><td><code>normal.transform.power</code></td>
<td>
<p>the value of the power used to 
transform the original data to approximate normality.</p>
</td></tr>  
</table>


<h3>Warning</h3>

<p>It is possible for the lower prediction limit based on the transformed data to be less than 0.  
In this case, the lower prediction limit on the original scale is set to 0 and a warning is 
issued stating that the normal approximation is not accurate in this case.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/GammaDist.html">gamma distribution</a> takes values on the positive real line. 
Special cases of the gamma are the <a href="../../stats/html/Exponential.html">exponential</a> distribution and 
the <a href="../../stats/html/Chisquare.html">chi-square</a> distributions. Applications of the gamma include 
life testing, statistical ecology, queuing theory, inventory control, and precipitation 
processes. A gamma distribution starts to resemble a normal distribution as the 
shape parameter a tends to infinity.
</p>
<p>Some EPA guidance documents (e.g., Singh et al., 2002; Singh et al., 2010a,b) strongly 
recommend against using a lognormal model for environmental data and recommend trying a 
gamma distribuiton instead.
</p>
<p>Prediction intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Nelson, 1973), and are often discussed 
in the context of linear regression (Draper and Smith, 1998; Zar, 2010).  Prediction 
intervals are useful for analyzing data from groundwater detection monitoring programs 
at hazardous and solid waste facilities.  References that discuss prediction intervals 
in the context of environmental monitoring include:  Berthouex and Brown (2002, Chapter 21), 
Gibbons et al. (2009), Millard and Neerchal (2001, Chapter 6), Singh et al. (2010b), 
and USEPA (2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton.
</p>
<p>Draper, N., and H. Smith. (1998). <em>Applied Regression Analysis</em>. Third Edition. 
John Wiley and Sons, New York.
</p>
<p>Evans, M., N. Hastings, and B. Peacock. (1993). <em>Statistical Distributions</em>. 
Second Edition. John Wiley and Sons, New York, Chapter 18.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Hawkins, D. M., and R.A.J. Wixley. (1986). A Note on the Transformation of 
Chi-Squared Variables to Normality. <em>The American Statistician</em>, <b>40</b>, 
296&ndash;298.
</p>
<p>Johnson, N.L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. Second Edition. 
John Wiley and Sons, New York, Chapter 17.
</p>
<p>Krishnamoorthy K., T. Mathew, and S. Mukherjee. (2008). Normal-Based Methods for a 
Gamma Distribution: Prediction and Tolerance Intervals and Stress-Strength Reliability. 
<em>Technometrics</em>, <b>50</b>(1), 69&ndash;78.
</p>
<p>Krishnamoorthy K., and T. Mathew. (2009). 
<em>Statistical Tolerance Regions: Theory, Applications, and Computation</em>. 
John Wiley and Sons, Hoboken.
</p>
<p>Kulkarni, H.V., and S.K. Powar. (2010). A New Method for Interval Estimation of the Mean 
of the Gamma Distribution. <em>Lifetime Data Analysis</em>, <b>16</b>, 431&ndash;447.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton.
</p>
<p>Singh, A., A.K. Singh, and R.J. Iaci. (2002). 
<em>Estimation of the Exposure Point Concentration Term Using a Gamma Distribution</em>.  
EPA/600/R-02/084. October 2002. Technology Support Center for Monitoring and 
Site Characterization, Office of Research and Development, Office of Solid Waste and 
Emergency Response, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Singh, A., R. Maichle, and N. Armbya. (2010a). 
<em>ProUCL Version 4.1.00 User Guide (Draft)</em>. EPA/600/R-07/041, May 2010. 
Office of Research and Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Singh, A., N. Armbya, and A. Singh. (2010b). 
<em>ProUCL Version 4.1.00 Technical Guide (Draft)</em>. EPA/600/R-07/041, May 2010.  
Office of Research and Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Wilson, E.B., and M.M. Hilferty. (1931). The Distribution of Chi-Squares. 
<em>Proceedings of the National Academy of Sciences</em>, <b>17</b>, 684&ndash;688.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C. 
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, New Jersey.
</p>


<h3>See Also</h3>

<p><code><a href="../../stats/html/GammaDist.html">GammaDist</a></code>, <code><a href="../../EnvStats/help/GammaAlt.html">GammaAlt</a></code>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, 
<code><a href="../../EnvStats/help/egamma.html">egamma</a></code>, <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, 
<code><a href="../../EnvStats/help/tolIntGamma.html">tolIntGamma</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a gamma distribution with parameters 
  # shape=3 and scale=2, then create a prediciton interval for the 
  # next observation. 
  # (Note: the call to set.seed simply allows you to reproduce this 
  # example.)

  set.seed(250) 
  dat &lt;- rgamma(20, shape = 3, scale = 2) 
  predIntGamma(dat)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Gamma
  #
  #Estimated Parameter(s):          shape = 2.203862
  #                                 scale = 2.174928
  #
  #Estimation Method:               mle
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Prediction Interval Method:      exact using
  #                                 Kulkarni &amp; Powar (2010)
  #                                 transformation to Normality
  #                                 based on mle of 'shape'
  #
  #Normal Transform Power:          0.246
  #
  #Prediction Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Number of Future Observations:   1
  #
  #Prediction Interval:             LPL =  0.5371569
  #                                 UPL = 15.5313783

  #--------------------------------------------------------------------

  # Using the same data as in the previous example, create an upper 
  # one-sided prediction interval for the next three averages of 
  # order 2 (i.e., each mean is based on 2 future observations), and 
  # use the bias-corrected estimate of shape.

  pred.list &lt;- predIntGamma(dat, n.transmean = 2, k = 3, 
    pi.type = "upper", est.method = "bcmle")

  pred.list

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Gamma
  #
  #Estimated Parameter(s):          shape = 1.906616
  #                                 scale = 2.514005
  #
  #Estimation Method:               bcmle
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Prediction Interval Method:      Bonferroni using
  #                                 Kulkarni &amp; Powar (2010)
  #                                 transformation to Normality
  #                                 based on bcmle of 'shape'
  #
  #Normal Transform Power:          0.246
  #
  #Prediction Interval Type:        upper
  #
  #Confidence Level:                95%
  #
  #Number of Future
  #Transformed Means:               3
  #
  #Sample Size for
  #Transformed Means:               2
  #
  #Prediction Interval:             LPL =  0.00000
  #                                 UPL = 12.17404

  #--------------------------------------------------------------------

  # Continuing with the above example, assume the distribution shifts 
  # in the future to a gamma distribution with shape = 5 and scale = 2.
  # Create 6 future observations from this distribution, and create 3 
  # means by pairing the observations sequentially.  Note we must first 
  # transform these observations using the power 0.246, then compute the 
  # means based on the transformed data, and then transform the means 
  # back to the original scale and compare them to the upper prediction 
  # limit of 12.17

  set.seed(427)
  new.dat &lt;- rgamma(6, shape = 5, scale = 2)

  p &lt;- pred.list$interval$normal.transform.power
  p
  #[1] 0.246
 
  new.dat.trans &lt;- new.dat^p
  means.trans &lt;- c(mean(new.dat.trans[1:2]), mean(new.dat.trans[3:4]), 
    mean(new.dat.trans[5:6]))
  means &lt;- means.trans^(1/p)
  means
  #[1] 11.74214 17.05299 11.65272

  any(means &gt; pred.list$interval$limits["UPL"])
  #[1] TRUE

  #----------

  # Clean up
  rm(dat, pred.list, new.dat, p, new.dat.trans, means.trans, means)

  #--------------------------------------------------------------------

  # Reproduce part of the example on page 73 of 
  # Krishnamoorthy et al. (2008), which uses alkalinity concentrations 
  # reported in Gibbons (1994) and Gibbons et al. (2009) to construct a 
  # one-sided upper 90% prediction limit.

  predIntGamma(Gibbons.et.al.09.Alkilinity.vec, pi.type = "upper", 
    conf.level = 0.9, normal.approx.transform = "cube.root")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Gamma
  #
  #Estimated Parameter(s):          shape = 9.375013
  #                                 scale = 6.202461
  #
  #Estimation Method:               mle
  #
  #Data:                            Gibbons.et.al.09.Alkilinity.vec
  #
  #Sample Size:                     27
  #
  #Prediction Interval Method:      exact using
  #                                 Wilson &amp; Hilferty (1931) cube-root
  #                                 transformation to Normality
  #
  #Normal Transform Power:          0.3333333
  #
  #Prediction Interval Type:        upper
  #
  #Confidence Level:                90%
  #
  #Number of Future Observations:   1
  #
  #Prediction Interval:             LPL =  0.0000
  #                                 UPL = 85.3495
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
