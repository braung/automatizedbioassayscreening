<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Quantiles of a Lognormal Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eqlnorm {EnvStats}"><tr><td>eqlnorm {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Quantiles of a Lognormal Distribution
</h2>

<h3>Description</h3>

<p>Estimate quantiles of a <a href="../../stats/help/Lognormal.html">lognormal distribution</a>, 
and optionally construct a confidence interval for a quantile.
</p>


<h3>Usage</h3>

<pre>
  eqlnorm(x, p = 0.5, method = "qmle", ci = FALSE, 
    ci.method = "exact", ci.type = "two-sided", conf.level = 0.95, 
    digits = 0)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>a numeric vector of positive observations, or an object resulting from a call to an 
estimating function that assumes a lognormal distribution 
(i.e., <code><a href="../../EnvStats/help/elnorm.html">elnorm</a></code>, <code><a href="../../EnvStats/help/elnormCensored.html">elnormCensored</a></code>). 
You <em>cannot</em> use objects resulting 
from a call to estimating functions that use the alternative parameterization 
such as <code>elnormAlt</code>.  If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities for which quantiles will be estimated.  
All values of <code>p</code> must be between 0 and 1.  When <code>ci=TRUE</code>, <code>p</code> 
must be a scalar. The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string indicating what method to use to estimate the quantile(s).  
The possible values are <code>"qmle"</code> (quasi maximum likelihood; the default) and <br />
<code>"mvue"</code> (minimum variance unbiased).  The method <code>"mvue"</code> is available only 
when <code>p=0.5</code> (i.e., when you are estimating the median).  See the DETAILS section 
for more information.
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the quantile.  
The default value is <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval 
for the quantile.  The possible values are <code>"exact"</code> (exact method; the default) 
and <code>"normal.approx"</code> (normal approximation).  See the DETAILS section for more 
information.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval for the quantile to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>digits</code></td>
<td>

<p>an integer indicating the number of decimal places to round to when printing out 
the value of <code>100*p</code>. The default value is <code>digits=0</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Quantiles and their associated confidence intervals are constructed by calling 
the function <code><a href="../../EnvStats/help/eqnorm.html">eqnorm</a></code> using the log-transformed data and then 
exponentiating the quantiles and confidence limits.
</p>
<p>In the special case when <code>p=0.5</code> and <code>method="mvue"</code>, the estimated 
median is computed using the method given in Gilbert (1987, p.172) and 
Bradu and Mundlak (1970).
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>eqlnorm</code> returns a list of class 
<code>"estimate"</code> containing the estimated quantile(s) and other information.  
See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>eqlnorm</code> 
returns a list whose class is the same as <code>x</code>.  The list contains the same 
components as <code>x</code>, as well as components called <code>quantiles</code> and 
<code>quantile.method</code>.  In addition, if <code>ci=TRUE</code>, the returned list 
contains a component called <code>interval</code> containing the confidence interval 
information. If <code>x</code> already has a component called <code>interval</code>, this 
component is replaced with the confidence interval information.
</p>


<h3>Note</h3>

<p>Percentiles are sometimes used in environmental standards and regulations.  For example, 
Berthouex and Brown (2002, p.71) note that England has water quality limits based on 
the 90th and 95th percentiles of monitoring data not exceeding specified levels.  They also 
note that the U.S. EPA has specifications for air quality monitoring, aquatic standards on 
toxic chemicals, and maximum daily limits for industrial effluents that are all based on 
percentiles.  Given the importance of these quantities, it is essential to characterize 
the amount of uncertainty associated with the estimates of these quantities.  
This is done with confidence intervals.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton.
</p>
<p>Bradu, D., and Y. Mundlak. (1970). Estimation in Lognormal Linear Models. 
<em>Journal of the American Statistical Association</em> <b>65</b>, 198-211.
</p>
<p>Conover, W.J. (1980). <em>Practical Nonparametric Statistics</em>. Second Edition. 
John Wiley and Sons, New York.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution Monitoring</em>. 
Van Nostrand Reinhold, New York.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). <em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, pp.88-90.
</p>
<p>Johnson, N.L., and B.L. Welch. (1940). Applications of the Non-Central t-Distribution. 
<em>Biometrika</em> <b>31</b>, 362-389.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, Florida.
</p>
<p>Owen, D.B. (1962). <em>Handbook of Statistical Tables</em>. Addison-Wesley, Reading, MA.
</p>
<p>Stedinger, J. (1983). Confidence Intervals for Design Events. 
<em>Journal of Hydraulic Engineering</em> <b>109</b>(1), 13-27.
</p>
<p>Stedinger, J.R., R.M. Vogel, and E. Foufoula-Georgiou. (1993). 
Frequency Analysis of Extreme Events. In: Maidment, D.R., ed. <em>Handbook of Hydrology</em>. 
McGraw-Hill, New York, Chapter 18, pp.29-30.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C. 
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/eqnorm.html">eqnorm</a></code>, <code><a href="../../stats/html/Lognormal.html">Lognormal</a></code>, <code><a href="../../EnvStats/help/elnorm.html">elnorm</a></code>, 
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a lognormal distribution with 
  # parameters meanlog=3 and sdlog=0.5, then estimate the 90th 
  # percentile and create a one-sided upper 95% confidence interval 
  # for that percentile. 
  # (Note: the call to set.seed simply allows you to reproduce this 
  # example.)

  set.seed(47) 
  dat &lt;- rlnorm(20, meanlog = 3, sdlog = 0.5) 
  eqlnorm(dat, p = 0.9, ci = TRUE, ci.type = "upper")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Lognormal
  #
  #Estimated Parameter(s):          meanlog = 2.9482139
  #                                 sdlog   = 0.4553215
  #
  #Estimation Method:               mvue
  #
  #Estimated Quantile(s):           90'th %ile = 34.18312
  #
  #Quantile Estimation Method:      qmle
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         90'th %ile
  #
  #Confidence Interval Method:      Exact
  #
  #Confidence Interval Type:        upper
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL =  0.00000
  #                                 UCL = 45.84008

  #----------
  # Compare these results with the true 90'th percentile:

  qlnorm(p = 0.9, meanlog = 3, sdlog = 0.5)
  #[1] 38.1214

  #----------

  # Clean up
  rm(dat)
  
  #--------------------------------------------------------------------

  # Example 17-3 of USEPA (2009, p. 17-17) shows how to construct a 
  # beta-content upper tolerance limit with 95% coverage and 95% 
  # confidence using chrysene data and assuming a lognormal 
  # distribution.

  # A beta-content upper tolerance limit with 95% coverage and 95% 
  # confidence is equivalent to the 95% upper confidence limit for the 
  # 95th percentile.

  attach(EPA.09.Ex.17.3.chrysene.df)
  Chrysene &lt;- Chrysene.ppb[Well.type == "Background"]
  eqlnorm(Chrysene, p = 0.95, ci = TRUE, ci.type = "upper")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Lognormal
  #
  #Estimated Parameter(s):          meanlog = 2.5085773
  #                                 sdlog   = 0.6279479
  #
  #Estimation Method:               mvue
  #
  #Estimated Quantile(s):           95'th %ile = 34.51727
  #
  #Quantile Estimation Method:      qmle
  #
  #Data:                            Chrysene
  #
  #Sample Size:                     8
  #
  #Confidence Interval for:         95'th %ile
  #
  #Confidence Interval Method:      Exact
  #
  #Confidence Interval Type:        upper
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL =  0.0000
  #                                 UCL = 90.9247

  #----------
  # Clean up

  rm(Chrysene)
  detach("EPA.09.Ex.17.3.chrysene.df")
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
