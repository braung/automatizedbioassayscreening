<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Quantiles of a Generalized Extreme Value...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eqgevd {EnvStats}"><tr><td>eqgevd {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Quantiles of a Generalized Extreme Value Distribution
</h2>

<h3>Description</h3>

<p>Estimate quantiles of a <a href="../../EnvStats/help/GEVD.html">generalized extreme value distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  eqgevd(x, p = 0.5, method = "mle", pwme.method = "unbiased", 
    tsoe.method = "med", plot.pos.cons = c(a = 0.35, b = 0), digits = 0)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>a numeric vector of observations, or an object resulting from a call to an 
estimating function that assumes a generalized extreme value distribution 
(e.g., <code><a href="../../EnvStats/help/egevd.html">egevd</a></code>). If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities for which quantiles will be estimated.  
All values of <code>p</code> must be between 0 and 1.  The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method to use to estimate the location, scale, and 
threshold parameters.  Possible values are 
<code>"mle"</code> (maximum likelihood; the default), 
<code>"pwme"</code> (probability-weighted moments), and 
<code>"tsoe"</code> (two-stage order-statistics estimator of Castillo and Hadi (1994)).  
See the DETAILS section of the help file for <code><a href="../../EnvStats/help/egevd.html">egevd</a></code> for more 
information on these estimation methods.
</p>
</td></tr>
<tr valign="top"><td><code>pwme.method</code></td>
<td>

<p>character string specifying what method to use to compute the 
probability-weighted moments when <code>method="pwme"</code>.  The possible values are 
<code>"ubiased"</code> (method based on the U-statistic; the default), or 
<code>"plotting.position"</code> (method based on the plotting position formula).  
See the DETAILS section of the help file for <code><a href="../../EnvStats/help/egevd.html">egevd</a></code>  for more 
information.  This argument is ignored if <code>method</code> is not equal to 
<code>"pwme"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>tsoe.method</code></td>
<td>

<p>character string specifying the robust function to apply in the second stage of 
the two-stage order-statistics estimator when <code>method="tsoe"</code>.  Possible 
values are <code>"med"</code> (median; the default), and <code>"lms"</code> 
(least median of squares).  See the DETAILS section of the help file for 
<code><a href="../../EnvStats/help/egevd.html">egevd</a></code> for more information on these estimation methods.  
This argument is ignored if <code>method</code> is not equal to <code>"tsoe"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.cons</code></td>
<td>

<p>numeric vector of length 2 specifying the constants used in the formula for the 
plotting positions when <code>method="pwme"</code> and <br />
<code>pwme.method="plotting.position"</code>.  The default value is <br />
<code>plot.pos.cons=c(a=0.35, b=0)</code>.  If this vector has a names attribute with 
the value <code>c("a","b")</code> or <code>c("b","a")</code>, then the elements will be 
matched by name in the formula for computing the plotting positions.  Otherwise, 
the first element is mapped to the name <code>"a"</code> and the second element to the 
name <code>"b"</code>.  See the DETAILS section of the help file for <code><a href="../../EnvStats/help/egevd.html">egevd</a></code> 
for more information.  This argument is used only if <code>method="tsoe"</code>, or if 
both <code>method="pwme"</code> and <br />
<code>pwme.method="plotting.position"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>digits</code></td>
<td>

<p>an integer indicating the number of decimal places to round to when printing out 
the value of <code>100*p</code>. The default value is <code>digits=0</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>eqgevd</code> returns estimated quantiles as well as 
estimates of the location, scale and threshold parameters.  
</p>
<p>Quantiles are estimated by 1) estimating the location, scale, and threshold 
parameters by calling <code><a href="../../EnvStats/help/egevd.html">egevd</a></code>, and then 2) calling the function 
<code><a href="../../EnvStats/help/GEVD.html">qgevd</a></code> and using the estimated values for 
location, scale, and threshold.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>eqevd</code> returns a 
list of class <code>"estimate"</code> containing the estimated quantile(s) and other 
information. See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>eqevd</code> 
returns a list whose class is the same as <code>x</code>.  The list 
contains the same components as <code>x</code>, as well as components called 
<code>quantiles</code> and <code>quantile.method</code>.
</p>


<h3>Note</h3>

<p>Two-parameter <a href="../../EnvStats/help/EVD.html">extreme value distributions</a> (EVD) have been 
applied extensively since the 1930's to several fields of study, including 
the distributions of hydrological and meteorological variables, human lifetimes, 
and strength of materials.  The three-parameter 
<a href="../../EnvStats/help/GEVD.html">generalized extreme value distribution</a> (GEVD) was introduced by 
Jenkinson (1955) to model annual maximum and minimum values of meteorological 
events.  Since then, it has been used extensively in the hydological and 
meteorological fields.
</p>
<p>The three families of EVDs are all special kinds of GEVDs.  When the shape 
parameter <i>&kappa;=0</i>, the GEVD reduces to the Type I extreme value (Gumbel) 
distribution.  (The function <code><a href="../../EnvStats/help/zTestGevdShape.html">zTestGevdShape</a></code> allows you to test 
the null hypothesis <i>H_0: &kappa;=0</i>.)  When <i>&kappa; &gt; 0</i>, the GEVD is 
the same as the Type II extreme value distribution, and when <i>&kappa; &lt; 0</i> 
it is the same as the Type III extreme value distribution.
</p>
<p>Hosking et al. (1985) compare the asymptotic and small-sample statistical 
properties of the PWME with the MLE and Jenkinson's (1969) method of sextiles.  
Castillo and Hadi (1994) compare the small-sample statistical properties of the 
MLE, PWME, and TSOE.  Hosking and Wallis (1995) compare the small-sample properties 
of unbaised <i>L</i>-moment estimators vs. plotting-position <i>L</i>-moment 
estimators.  (PWMEs can be written as linear combinations of <i>L</i>-moments and 
thus have equivalent statistical properties.)  Hosking and Wallis (1995) conclude 
that unbiased estimators should be used for almost all applications.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Castillo, E., and A. Hadi. (1994).  Parameter and Quantile Estimation for the 
Generalized Extreme-Value Distribution.  <em>Environmetrics</em> <b>5</b>, 417&ndash;432.
</p>
<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Greenwood, J.A., J.M. Landwehr, N.C. Matalas, and J.R. Wallis. (1979).  
Probability Weighted Moments: Definition and Relation to Parameters of Several 
Distributions Expressible in Inverse Form.  <em>Water Resources Research</em> 
<b>15</b>(5), 1049&ndash;1054.
</p>
<p>Hosking, J.R.M. (1984).  Testing Whether the Shape Parameter is Zero in the 
Generalized Extreme-Value Distribution.  <em>Biometrika</em> <b>71</b>(2), 367&ndash;374.
</p>
<p>Hosking, J.R.M. (1985).  Algorithm AS 215: Maximum-Likelihood Estimation of the 
Parameters of the Generalized Extreme-Value Distribution.  
<em>Applied Statistics</em> <b>34</b>(3), 301&ndash;310.
</p>
<p>Hosking, J.R.M., J.R. Wallis, and E.F. Wood. (1985).  Estimation of the 
Generalized Extreme-Value Distribution by the Method of 
Probability-Weighted Moments.  <em>Technometrics</em> <b>27</b>(3), 251&ndash;261.
</p>
<p>Jenkinson, A.F. (1969).  Statistics of Extremes. <em>Technical Note 98</em>, 
World Meteorological Office, Geneva.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York.
</p>
<p>Landwehr, J.M., N.C. Matalas, and J.R. Wallis. (1979).  Probability Weighted 
Moments Compared With Some Traditional Techniques in Estimating Gumbel 
Parameters and Quantiles.  <em>Water Resources Research</em> <b>15</b>(5), 
1055&ndash;1064.
</p>
<p>Macleod, A.J. (1989).  Remark AS R76: A Remark on Algorithm AS 215: 
Maximum Likelihood Estimation of the Parameters of the Generalized 
Extreme-Value Distribution.  <em>Applied Statistics</em> <b>38</b>(1), 198&ndash;199.
</p>
<p>Prescott, P., and A.T. Walden. (1980).  Maximum Likelihood Estimation of the 
Parameters of the Generalized Extreme-Value Distribution.  
<em>Biometrika</em> <b>67</b>(3), 723&ndash;724.
</p>
<p>Prescott, P., and A.T. Walden. (1983).  Maximum Likelihood Estimation of the 
Three-Parameter Generalized Extreme-Value Distribution from Censored Samples.  
<em>Journal of Statistical Computing and Simulation</em> <b>16</b>, 241&ndash;250.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/egevd.html">egevd</a></code>, <a href="../../EnvStats/help/GEVD.html">Generalized Extreme Value Distribution</a>, 
<a href="../../EnvStats/help/EVD.html">Extreme Value Distribution</a>, <code><a href="../../EnvStats/help/eevd.html">eevd</a></code>, <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a generalized extreme value distribution 
  # with parameters location=2, scale=1, and shape=0.2, then compute the 
  # MLEs of location, shape,and threshold, and estimate the 90th percentile. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(498) 
  dat &lt;- rgevd(20, location = 2, scale = 1, shape = 0.2) 
  eqgevd(dat, p = 0.9)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Generalized Extreme Value
  #
  #Estimated Parameter(s):          location = 1.6144631
  #                                 scale    = 0.9867007
  #                                 shape    = 0.2632493
  #
  #Estimation Method:               mle
  #
  #Estimated Quantile(s):           90'th %ile = 3.289912
  #
  #Quantile Estimation Method:      Quantile(s) Based on
  #                                 mle Estimators
  #
  #Data:                            dat
  #
  #Sample Size:                     20

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
