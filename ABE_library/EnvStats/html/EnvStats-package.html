<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Package for Environmental Statistics, Including US EPA...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for EnvStats-package {EnvStats}"><tr><td>EnvStats-package {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Package for Environmental Statistics, Including US EPA Guidance
</h2>

<h3>Description</h3>

<p>A comprehensive R package for environmental statistics and
the successor to the S-PLUS module <em>EnvironmentalStats for S-PLUS</em>
(first released in April, 1997).  <span class="pkg">EnvStats</span> provides a set of powerful
functions for graphical and statistical analyses of environmental data, with
a focus on analyzing chemical concentrations and physical parameters, usually in
the context of mandated environmental monitoring.  It includes major environmental
statistical methods found in the literature and regulatory guidance documents,
and extensive help that explains what these methods do, how to use them,
and where to find them in the literature.  It also includes numerous built-in data
sets from regulatory guidance documents and environmental statistics literature, and
scripts reproducing analyses presented in the User's manual:
<em>EnvStats:  An R Package for Environmental Statistics</em>
(Millard, 2013, <a href="https://link.springer.com/book/10.1007/978-1-4614-8456-1">https://link.springer.com/book/10.1007/978-1-4614-8456-1</a>).
</p>
<p>For a complete list of functions and datasets, you can do any of the following:
</p>

<ul>
<li><p> See the help file <a href="../../EnvStats/help/FcnsByCat.html">Functions By Category</a> for a listing of
functions by category.
</p>
</li>
<li><p> If you are in the on-line help, scroll to the bottom of this help page and
click on the <b>Index</b> link.
</p>
</li>
<li><p> Type <code>library(help="EnvStats")</code> at the command prompt.
</p>
</li></ul>

<p><b>Note:</b>  The names of all <span class="pkg">EnvStats</span> functions start with a lowercase letter, and
the names of all <span class="pkg">EnvStats</span> datasets and data objects start an uppercase letter.
You can type <code>newsEnvStats()</code> at the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> command prompt for the latest news for
the <span class="pkg">EnvStats</span> package.
</p>


<h3>Details</h3>


<table summary="Rd table">
<tr>
 <td style="text-align: left;">
Package: </td><td style="text-align: left;"> EnvStats</td>
</tr>
<tr>
 <td style="text-align: left;">
Type: </td><td style="text-align: left;"> Package</td>
</tr>
<tr>
 <td style="text-align: left;">
Version: </td><td style="text-align: left;"> 2.3.0</td>
</tr>
<tr>
 <td style="text-align: left;">
Date: </td><td style="text-align: left;"> 2017-10-09</td>
</tr>
<tr>
 <td style="text-align: left;">
License: </td><td style="text-align: left;"> GPL (&gt;=3)</td>
</tr>
<tr>
 <td style="text-align: left;">
LazyLoad: </td><td style="text-align: left;"> yes</td>
</tr>
<tr>
 <td style="text-align: left;">
</td>
</tr>

</table>

<p>A companion file <b>EnvStats-manual.pdf</b> containing a listing of all the current help
files is located on the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> CRAN web site at
<a href="https://cran.r-project.org/package=EnvStats/EnvStats.pdf">https://cran.r-project.org/package=EnvStats/EnvStats.pdf</a>
and also in the
<b>doc</b> subdirectory of the directory where the <span class="pkg">EnvStats</span> package
was installed.  For example, if you installed R under Windows, this file might be located
in the directory <b>C:\Program Files\R-*.**.*\library\EnvStats\doc</b>, where
<b>*.**.*</b> denotes the version of R you are using (e.g., 3.3.4) or in the directory
<b>C:\Users\<em>Name</em>\Documents\R\win-library\*.**.*\EnvStats\doc</b>, where
<b><em>Name</em></b> denotes your user name on the Windows operating system.
</p>
<p>EnvStats comes with companion scripts, located in the <b>scripts</b> subdirectory of the
directory where the package was installed.  One set of scripts lets you reproduce the examples in
the User's Manual.  There are also scripts that let you reproduce examples from
US EPA guidance documents.
</p>
<p>See the <b>References</b> section below for documentation for the predecessor to <span class="pkg">EnvStats</span>,
<em>EnvironmentalStats for S-PLUS</em> for Windows.
</p>
<p>Features of EnvStats include:
</p>

<ul>
<li>
<p>New functions for computing <a href="../../EnvStats/help/FcnsByCatSumStats.html">summary statistics</a>, as well as
creating <a href="../../EnvStats/help/FcnsByCatSumStats.html">summary plots</a> to compare the distributions
of groups side-by-side, including functions specifically designed to work with
plots created with <code><a href="../../ggplot2/help/ggplot.html">ggplot</a></code>
(see <a href="../../EnvStats/help/FcnsByCatPlotUsingggplot2.html">Plotting Using ggplot2</a>).
</p>
</li>
<li>
<p>New <a href="../../EnvStats/help/FcnsByCatProbDists.html">probability distributions</a> have been added to the
ones already available in <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span>, including the extreme value distribution and the
zero-modified lognormal (delta) distribution.  You can compute quantities associated
with these probability distributions (probability density functions, cumulative
distribution functions, and quantiles), and generate random numbers from these
distributions.
</p>
</li>
<li>
<p><a href="../../EnvStats/help/FcnsByCatPlotProbDists.html">Plot probability distributions</a> so you can see how they
change with the value of the distribution parameter(s).
</p>
</li>
<li>
<p><a href="../../EnvStats/help/FcnsByCatEstDistParams.html">Estimate distribution parameters</a> and
<a href="../../EnvStats/help/FcnsByCatEstDistQuants.html">distribution quantiles</a>, and compute confidence intervals
for commonly used probability distributions, including special methods for the
lognormal and gamma distributions.
</p>
</li>
<li>
<p>Perform and plot the results of <a href="../../EnvStats/help/FcnsByCatGOFTests.html">goodness-of-fit tests</a>:
</p>

<ul>
<li><p> Observed and Fitted Distributions
</p>
</li>
<li><p> Quantile-Quantile Plots
</p>
</li>
<li><p> Results of Shaprio-Wilk test, Kolmogorov-Smirnov test, etc.
</p>
</li></ul>

<p>Includes a new generalized goodness-of-fit test for any continuous distribution.
Also includes a new function to choose among several candidate distributions.
</p>
</li>
<li>
<p>Functions for assessing optimal <a href="../../EnvStats/help/FcnsByCatDataTrans.html">Box-Cox data transformations</a>.
</p>
</li>
<li>
<p>Compute parametric and non-parametric
<a href="../../EnvStats/help/FcnsByCatPredInts.html">prediction intervals, simultaneous prediction intervals</a>,
and <a href="../../EnvStats/help/FcnsByCatTolInts.html">tolerance intervals</a>.
</p>
</li>
<li>
<p>New functions for <a href="../../EnvStats/help/FcnsByCatHypothTests.html">hypothesis tests</a>, including:
</p>

<ul>
<li><p> Nonparametric estimation and tests for seasonal trend
</p>
</li>
<li><p> Fisher's one-sample randomization (permutation) test for location
</p>
</li>
<li><p> Quantile test to detect a shift in the tail of one population relative to another
</p>
</li>
<li><p> Two-sample linear rank tests
</p>
</li>
<li><p> Test for serial correlation based on von Neumann rank test
</p>
</li></ul>

</li>
<li>
<p>Perform <a href="../../EnvStats/help/FcnsByCatCalibration.html">calibration</a> based on a machine signal to determine
decision and detection limits and report estimated concentrations along with confidence
intervals.
</p>
</li>
<li>
<p>Easily perform <a href="../../EnvStats/help/FcnsByCatPower.html">power and sample size</a> computations and create companion
plots for sampling designs based on confidence intervals, hypothesis tests, prediction intervals,
and tolerance intervals.
</p>
</li>
<li>
<p>Handle singly and multiply <a href="../../EnvStats/help/FcnsByCatCensoredData.html">censored (less-than-detection-limit) data</a>:
</p>

<ul>
<li><p> Empirical CDF and Quantile-Quantile Plots
</p>
</li>
<li><p> Parameter/Quantile Estimation and Confidence Intervals
</p>
</li>
<li><p> Prediction and Tolerance Intervals
</p>
</li>
<li><p> Goodness-of-Fit Tests
</p>
</li>
<li><p> Optimal Box-Cox Transformations
</p>
</li>
<li><p> Two-Sample Rank Tests
</p>
</li></ul>

</li>
<li>
<p>Functions for performing
<a href="../../EnvStats/help/FcnsByCatMCandRisk.html">Monte Carlo simulation and probabilistic risk assessement</a>.
</p>
</li>
<li>
<p>Reproduce specific examples in EPA guidance documents by using
built-in data sets from these documents and running companion scripts.
</p>
</li></ul>



<h3>Author(s)</h3>

<p>Steven P. Millard
</p>
<p>Maintainer: Steven P. Millard &lt;EnvStats@ProbStatInfo.com&gt;
</p>


<h3>References</h3>

<p>Millard, S.P. (2013). <em>EnvStats: An R Package for Environmental Statistics</em>.
Springer, New York.  <a href="https://link.springer.com/book/10.1007/978-1-4614-8456-1">https://link.springer.com/book/10.1007/978-1-4614-8456-1</a>.
</p>
<p>Millard, S.P. (2002). <em>EnvironmentalStats for S-PLUS: User's Manual for Version 2.0</em>.
Second Edition. Springer-Verlag, New York.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>.
CRC Press, Boca Raton, FL.
</p>


<h3>Examples</h3>

<pre>
  # Look at plots and summary statistics for the TcCB data given in
  # USEPA (1994b), (the data are stored in EPA.94b.tccb.df).
  # Arbitrarily set the one censored observation to the censoring level.
  # Group by the variable Area.

  EPA.94b.tccb.df
  #    TcCB.orig   TcCB Censored      Area
  #1        0.22   0.22    FALSE Reference
  #2        0.23   0.23    FALSE Reference
  #...
  #46       1.20   1.20    FALSE Reference
  #47       1.33   1.33    FALSE Reference
  #48      &lt;0.09   0.09     TRUE   Cleanup
  #49       0.09   0.09    FALSE   Cleanup
  #...
  #123     51.97  51.97    FALSE   Cleanup
  #124    168.64 168.64    FALSE   Cleanup


  # First plot the data
  #--------------------
  dev.new()
  stripChart(TcCB ~ Area, data = EPA.94b.tccb.df,
    xlab = "Area", ylab = "TcCB (ppb)")
  mtext("TcCB Concentrations by Area", line = 3, cex = 1.25, font = 2)

  dev.new()
  stripChart(log10(TcCB) ~ Area, data = EPA.94b.tccb.df,
    p.value = TRUE,
    xlab = "Area", ylab = expression(paste(log[10], " [ TcCB (ppb) ]")))
  mtext(expression(paste(log[10], "(TcCB) Concentrations by Area")),
    line = 3, cex = 1.25, font = 2)

  #--------------------------------------------------------------------

  # Now compute summary statistics
  #-------------------------------

  sum(EPA.94b.tccb.df$Censored)
  #[1] 1

  with(EPA.94b.tccb.df, TcCB[Censored])
  #0.09

  # Summary statistics will treat the one censored value
  # as assuming the detection limit.

  summaryFull(TcCB ~ Area, data = EPA.94b.tccb.df)
  #                             Cleanup  Reference
  #N                             77       47
  #Mean                           3.915    0.5985
  #Median                         0.43     0.54
  #10% Trimmed Mean               0.6846   0.5728
  #Geometric Mean                 0.5784   0.5382
  #Skew                           7.717    0.9019
  #Kurtosis                      62.67     0.132
  #Min                            0.09     0.22
  #Max                          168.6      1.33
  #Range                        168.5      1.11
  #1st Quartile                   0.23     0.39
  #3rd Quartile                   1.1      0.75
  #Standard Deviation            20.02     0.2836
  #Geometric Standard Deviation   3.898    1.597
  #Interquartile Range            0.87     0.36
  #Median Absolute Deviation      0.3558   0.2669
  #Coefficient of Variation       5.112    0.4739

  summaryStats(TcCB ~ Area, data = EPA.94b.tccb.df, digits = 1)
  #           N Mean   SD Median Min   Max
  #Cleanup   77  3.9 20.0    0.4 0.1 168.6
  #Reference 47  0.6  0.3    0.5 0.2   1.3

  #----------------------------------------------------------------

  # Compute Shapiro-Wilk Goodness-of-Fit statistic for the
  # Reference Area TcCB data assuming a lognormal distribution
  #-----------------------------------------------------------

  sw.list &lt;- gofTest(TcCB ~ 1, data = EPA.94b.tccb.df,
    subset = Area == "Reference", dist = "lnorm")
  sw.list

  # Results of Goodness-of-Fit Test
  # -------------------------------
  #
  # Test Method:                     Shapiro-Wilk GOF
  #
  # Hypothesized Distribution:       Lognormal
  #
  # Estimated Parameter(s):          meanlog = -0.6195712
  #                                  sdlog   =  0.4679530
  #
  # Estimation Method:               mvue
  #
  # Data:                            TcCB
  #
  # Subset With:                     Area == "Reference"
  #
  # Data Source:                     EPA.94b.tccb.df
  #
  # Sample Size:                     47
  #
  # Test Statistic:                  W = 0.978638
  #
  # Test Statistic Parameter:        n = 47
  #
  # P-value:                         0.5371935
  #
  # Alternative Hypothesis:          True cdf does not equal the
  #                                  Lognormal Distribution.

  #----------

  # Plot results of GOF test
  dev.new()
  plot(sw.list)

  #----------------------------------------------------------------

  # Based on the Reference Area data, estimate 90th percentile
  # and compute a 95% confidence limit for the 90th percentile
  # assuming a lognormal distribution.
  #------------------------------------------------------------

  with(EPA.94b.tccb.df,
    eqlnorm(TcCB[Area == "Reference"], p = 0.9, ci = TRUE))

  # Results of Distribution Parameter Estimation
  # --------------------------------------------
  #
  # Assumed Distribution:            Lognormal
  #
  # Estimated Parameter(s):          meanlog = -0.6195712
  #                                  sdlog   =  0.4679530
  #
  # Estimation Method:               mvue
  #
  # Estimated Quantile(s):           90'th %ile = 0.9803307
  #
  # Quantile Estimation Method:      qmle
  #
  # Data:                            TcCB[Area == "Reference"]
  #
  # Sample Size:                     47
  #
  # Confidence Interval for:         90'th %ile
  #
  # Confidence Interval Method:      Exact
  #
  # Confidence Interval Type:        two-sided
  #
  # Confidence Level:                95%
  #
  # Confidence Interval:             LCL = 0.8358791
                                     UCL = 1.2154977
  #----------

  # Cleanup
  rm(TcCB.ref, sw.list)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
