<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Probability That at Least One Future Observation Falls...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for predIntNormTestPower {EnvStats}"><tr><td>predIntNormTestPower {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Probability That at Least One Future Observation Falls Outside a Prediction Interval for a Normal Distribution
</h2>

<h3>Description</h3>

<p>Compute the probability that at least one out of <i>k</i> future observations 
(or means) falls outside a prediction interval for <i>k</i> future observations 
(or means) for a normal distribution.
</p>


<h3>Usage</h3>

<pre>
  predIntNormTestPower(n, df = n - 1, n.mean = 1, k = 1, delta.over.sigma = 0, 
    pi.type = "upper", conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>vector of positive integers greater than 2 indicating the sample size upon which 
the prediction interval is based.
</p>
</td></tr>
<tr valign="top"><td><code>df</code></td>
<td>

<p>vector of positive integers indicating the degrees of freedom associated with 
the sample size.  The default value is <code>df=n-1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.mean</code></td>
<td>

<p>positive integer specifying the sample size associated with the future averages.  
The default value is <code>n.mean=1</code> (i.e., individual observations).  Note that all 
future averages must be based on the same sample size.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>vector of positive integers specifying the number of future observations that the 
prediction interval should contain with confidence level <code>conf.level</code>.  The 
default value is <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>delta.over.sigma</code></td>
<td>

<p>vector of numbers indicating the ratio <i>&Delta;/&sigma;</i>.  The quantity 
<i>&Delta;</i> (delta) denotes the difference between the mean of the population 
that was sampled to construct the prediction interval, and the mean of the 
population that will be sampled to produce the future observations.  The quantity 
<i>&sigma;</i> (sigma) denotes the population standard deviation for both populations.  
See the DETAILS section below for more information.  The default value is 
<code>delta.over.sigma=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>pi.type</code></td>
<td>

<p>character string indicating what kind of prediction interval to compute.  
The possible values are <code>pi.type="upper"</code> (the default), and 
<code>pi.type="lower"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric vector of values between 0 and 1 indicating the confidence level of the 
prediction interval.  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p><em>What is a Prediction Interval?</em> <br />
A prediction interval for some population is an interval on the real line 
constructed so that it will contain <i>k</i> future observations or averages 
from that population with some specified probability <i>(1-&alpha;)100\%</i>, 
where <i>0 &lt; &alpha; &lt; 1</i> and <i>k</i> is some pre-specified positive integer.  
The quantity <i>(1-&alpha;)100\%</i> is call the confidence coefficient or 
confidence level associated with the prediction interval.  The function 
<code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code> computes a standard prediction interval based on a 
sample from a normal distribution.  The function <code>predIntNormTestPower</code> 
computes the probability that at least one out of <i>k</i> future observations or 
averages will <b>not</b> be contained in the prediction interval, 
where the population mean for the future observations is allowed to differ from 
the population mean for the observations used to construct the prediction interval.
<br />
</p>
<p><em>The Form of a Prediction Interval</em> <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote a vector of <i>n</i> 
observations from a <a href="../../stats/help/Normal.html">normal distribution</a> with parameters 
<code>mean=</code><i>&mu;</i> and <code>sd=</code><i>&sigma;</i>.  Also, let <i>m</i> denote the 
sample size associated with the <i>k</i> future averages (i.e., <code>n.mean=</code><i>m</i>).  
When <i>m=1</i>, each average is really just a single observation, so in the rest of 
this help file the term &ldquo;averages&rdquo; will replace the phrase 
&ldquo;observations or averages&rdquo;.
</p>
<p>For a normal distribution, the form of a two-sided <i>(1-&alpha;)100\%</i> prediction 
interval is: 
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \bar{x} + Ks] \;\;\;\;\;\; (1)</i></p>
 
<p>where <i>\bar{x}</i> denotes the sample mean:
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (2)</i></p>

<p><i>s</i> denotes the sample standard deviation:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (3)</i></p>

<p>and <i>K</i> denotes a constant that depends on the sample size <i>n</i>, the 
confidence level, the number of future averages <i>k</i>, and the 
sample size associated with the future averages, <i>m</i>.  Do not confuse the 
constant <i>K</i> (uppercase K) with the number of future averages <i>k</i> 
(lowercase k).  The symbol <i>K</i> is used here to be consistent with the 
notation used for tolerance intervals (see <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>).  
</p>
<p>Similarly, the form of a one-sided lower prediction interval is: 
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, &infin;] \;\;\;\;\;\; (4)</i></p>
 
<p>and the form of a one-sided upper prediction interval is:
</p>
<p style="text-align: center;"><i>[-&infin;, \bar{x} + Ks] \;\;\;\;\;\; (5)</i></p>
 
<p>but <i>K</i> differs for one-sided versus two-sided prediction intervals.  
The derivation of the constant <i>K</i> is explained in the help file for 
<code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code>.
<br />
</p>
<p><em>Computing Power</em> <br />
The &quot;power&quot; of the prediction interval is defined as the probability that at 
least one out of the <i>k</i> future observations or averages 
will <b>not</b> be contained in the prediction interval, where the population mean 
for the future observations is allowed to differ from the population mean for the 
observations used to construct the prediction interval.  The probability <i>p</i> 
that all <i>k</i> future observations will be contained in a one-sided upper 
prediction interval (<code>pi.type="upper"</code>) is given in Equation (6) of the help 
file for 
<code><a href="../../EnvStats/help/predIntNormSimultaneousK.html">predIntNormSimultaneousK</a></code>, where <i>k=m</i> and <i>r=1</i>:
</p>
<p style="text-align: center;"><i>p = \int_0^1 T(&radic;{n}K; n-1, &radic;{n}[&Phi;^{-1}(v) + \frac{&Delta;}{&sigma;}]) [\frac{v^{k-1}}{B(k, 1)}] dv \;\;\;\;\;\; (6)</i></p>

<p>where <i>T(x; &nu;, &delta;)</i> denotes the cdf of the 
<a href="../../stats/help/TDist.html">non-central Student's t-distribution</a> with parameters 
<code>df=</code><i>&nu;</i> and <code>ncp=</code><i>&delta;</i> evaluated at <i>x</i>; 
<i>&Phi;(x)</i> denotes the cdf of the standard <a href="../../stats/help/Normal.html">normal distribution</a> 
evaluated at <i>x</i>; and <i>B(&nu;, &omega;)</i> denotes the value of the 
<a href="../../base/help/Special.html">beta function</a> with parameters <code>a=</code><i>&nu;</i> and 
<code>b=</code><i>&omega;</i>.
</p>
<p>The quantity <i>&Delta;</i> (upper case delta) denotes the difference between the 
mean of the population that was sampled to construct the prediction interval, and 
the mean of the population that will be sampled to produce the future observations.  
The quantity <i>&sigma;</i> (sigma) denotes the population standard deviation of both 
of these populations.  Usually you assume <i>&Delta;=0</i> unless you are interested 
in computing the power of the rule to detect a change in means between the 
populations, as we are here.
</p>
<p>If we are interested in using averages instead of single observations, with 
<i>w &ge; 1</i> (i.e., <code>n.mean</code><i>&ge; 1</i>), the first 
term in the integral in Equation (6) that involves the cdf of the 
<a href="../../stats/help/TDist.html">non-central Student's t-distribution</a> becomes:
</p>
<p style="text-align: center;"><i>T(&radic;{n}K; n-1, \frac{&radic;{n}}{&radic;{w}}[&Phi;^{-1}(v) + \frac{&radic;{w}&Delta;}{&sigma;}]) \;\;\;\;\;\; (7)</i></p>

<p>For a given confidence level <i>(1-&alpha;)100\%</i>, the power of the rule to detect 
a change in means is simply given by:
</p>
<p style="text-align: center;"><i>Power = 1 - p \;\;\;\;\;\; (8)</i></p>

<p>where <i>p</i> is defined in Equation (6) above using the value of <i>K</i> that 
corresponds to <i>&Delta;/&sigma; = 0</i>.  Thus, when the argument 
<code>delta.over.sigma=0</code>, the value of <i>p</i> is <i>1-&alpha;</i> and the power is 
simply <i>&alpha; 100\%</i>.  As <code>delta.over.sigma</code> increases above 0, the power 
increases.
</p>
<p>When <code>pi.type="lower"</code>, the same value of <code>K</code> is used as when 
<code>pi.type="upper"</code>, but Equation (4) is used to construct the prediction 
interval.  Thus, the power increases as <code>delta.over.sigma</code> decreases below 0.
</p>


<h3>Value</h3>

<p>vector of values between 0 and 1 equal to the probability that at least one of 
<i>k</i> future observations or averages will fall outside the prediction interval.
</p>


<h3>Note</h3>

<p>See the help files for <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code> and 
<code><a href="../../EnvStats/help/predIntNormSimultaneous.html">predIntNormSimultaneous</a></code>.
</p>
<p>In the course of designing a sampling program, an environmental scientist may wish 
to determine the relationship between sample size, significance level, power, and 
scaled difference if one of the objectives of the sampling program is to determine 
whether two distributions differ from each other.  The functions 
<code>predIntNormTestPower</code> and <code><a href="../../EnvStats/help/plotPredIntNormTestPowerCurve.html">plotPredIntNormTestPowerCurve</a></code> can be 
used to investigate these relationships for the case of normally-distributed 
observations.  In the case of a simple shift between the two means, the test based 
on a prediction interval is not as powerful as the two-sample t-test.  However, the 
test based on a prediction interval is more efficient at detecting a shift in the 
tail.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>See the help files for <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code> and 
<code><a href="../../EnvStats/help/predIntNormSimultaneous.html">predIntNormSimultaneous</a></code>.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, <code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code>, 
<code><a href="../../EnvStats/help/plotPredIntNormTestPowerCurve.html">plotPredIntNormTestPowerCurve</a></code>, <code><a href="../../EnvStats/help/predIntNormSimultaneous.html">predIntNormSimultaneous</a></code>, 
<code><a href="../../EnvStats/help/predIntNormSimultaneousK.html">predIntNormSimultaneousK</a></code>, 
<code><a href="../../EnvStats/help/predIntNormSimultaneousTestPower.html">predIntNormSimultaneousTestPower</a></code>, <a href="../../EnvStats/help/Prediction+20Intervals.html">Prediction Intervals</a>, 
<a href="../../stats/html/Normal.html">Normal</a>.
</p>


<h3>Examples</h3>

<pre>
  # Show how the power increases as delta.over.sigma increases.  
  # Assume a 95% upper prediction interval.

  predIntNormTestPower(n = 4, delta.over.sigma = 0:2) 
  #[1] 0.0500000 0.1743014 0.3990892

  #----------

  # Look at how the power increases with sample size for a one-sided upper 
  # prediction interval with k=3, delta.over.sigma=2, and a confidence level 
  # of 95%.

  predIntNormTestPower(n = c(4, 8), k = 3, delta.over.sigma = 2) 
  #[1] 0.3578250 0.5752113

  #----------

  # Show how the power for an upper 95% prediction limit increases as the 
  # number of future observations k increases.  Here, we'll use n=20 and 
  # delta.over.sigma=1.

  predIntNormTestPower(n = 20, k = 1:3, delta.over.sigma = 1) 
  #[1] 0.2408527 0.2751074 0.2936486
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
