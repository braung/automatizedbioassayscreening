<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Methods for General Linear Hypotheses</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for glht-methods {multcomp}"><tr><td>glht-methods {multcomp}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2> Methods for General Linear Hypotheses </h2>

<h3>Description</h3>

<p>Simultaneous tests and confidence intervals for general linear 
hypotheses.
</p>


<h3>Usage</h3>

<pre>
## S3 method for class 'glht'
summary(object, test = adjusted(), ...)
## S3 method for class 'glht'
confint(object, parm, level = 0.95, calpha = adjusted_calpha(), 
        ...)
## S3 method for class 'glht'
coef(object, rhs = FALSE, ...)
## S3 method for class 'glht'
vcov(object, ...)
## S3 method for class 'confint.glht'
plot(x, xlim, xlab, ylim, ...)
## S3 method for class 'glht'
plot(x, ...)
univariate()
adjusted(type = c("single-step", "Shaffer", "Westfall", "free", 
         p.adjust.methods), ...)
Ftest()
Chisqtest()
adjusted_calpha(...)
univariate_calpha(...)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>object</code></td>
<td>
<p> an object of class <code><a href="../../multcomp/help/glht.html">glht</a></code>.</p>
</td></tr>
<tr valign="top"><td><code>test</code></td>
<td>
<p> a function for computing p values.</p>
</td></tr>
<tr valign="top"><td><code>parm</code></td>
<td>
<p> additional parameters, currently ignored.</p>
</td></tr>
<tr valign="top"><td><code>level</code></td>
<td>
<p> the confidence level required.</p>
</td></tr>
<tr valign="top"><td><code>calpha</code></td>
<td>
<p> either a function computing the critical value or the critical value itself.</p>
</td></tr>
<tr valign="top"><td><code>rhs</code></td>
<td>
<p>logical, indicating whether the linear function
<i>K \hat{&theta;}</i> or the right hand side 
<i>m</i> (<code>rhs = TRUE</code>) of the linear hypothesis
should be returned.</p>
</td></tr>
<tr valign="top"><td><code>type</code></td>
<td>
<p> the multiplicity adjustment (<code>adjusted</code>) 
to be applied. See below and <code><a href="../../stats/html/p.adjust.html">p.adjust</a></code>.</p>
</td></tr>
<tr valign="top"><td><code>x</code></td>
<td>
<p>an object of class <code><a href="../../multcomp/help/glht.html">glht</a></code> or <code>confint.glht</code>.</p>
</td></tr>
<tr valign="top"><td><code>xlim</code></td>
<td>
<p>the <code>x</code> limits <code>(x1, x2)</code> of the plot.</p>
</td></tr>
<tr valign="top"><td><code>ylim</code></td>
<td>
<p>the y limits of the plot.</p>
</td></tr>
<tr valign="top"><td><code>xlab</code></td>
<td>
<p>a label for the <code>x</code> axis.</p>
</td></tr>
<tr valign="top"><td><code>...</code></td>
<td>
<p> additional arguments, such as <code>maxpts</code>, 
<code>abseps</code> or <code>releps</code> to
<code><a href="../../mvtnorm/help/pmvnorm.html">pmvnorm</a></code> in <code>adjusted</code> or 
<code><a href="../../mvtnorm/help/qmvnorm.html">qmvnorm</a></code> in <code>confint</code>. Note
that additional arguments specified to <code>summary</code>,
<code>confint</code>, <code>coef</code> and <code>vcov</code> methods
are currently ignored.</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The methods for general linear hypotheses as described by objects returned
by <code><a href="../../multcomp/help/glht.html">glht</a></code> can be used to actually test the global
null hypothesis, each of the partial hypotheses and for
simultaneous confidence intervals for the linear function <i>K &theta;</i>.
</p>
<p>The <code><a href="../../stats/html/coef.html">coef</a></code> and <code><a href="../../stats/html/vcov.html">vcov</a></code> methods compute the linear
function <i>K \hat{&theta;}</i> and its covariance, respectively.
</p>
<p>The <code>test</code> argument to <code>summary</code> takes a function specifying
the type of test to be applied. Classical Chisq (Wald test) or F statistics
for testing the global hypothesis <i>H_0</i> are implemented in functions
<code>Chisqtest</code> and <code>Ftest</code>. Several approaches to multiplicity adjusted p 
values for each of the linear hypotheses are implemented 
in function <code>adjusted</code>. The <code>type</code>
argument to <code>adjusted</code> specifies the method to be applied:
<code>"single-step"</code> implements adjusted p values based on the joint
normal or t distribution of the linear function, and
<code>"Shaffer"</code> and <code>"Westfall"</code> implement logically constraint 
multiplicity adjustments (Shaffer, 1986; Westfall, 1997). 
<code>"free"</code> implements multiple testing procedures under free 
combinations (Westfall et al, 1999).
In addition, all adjustment methods
implemented in <code><a href="../../stats/html/p.adjust.html">p.adjust</a></code> are available as well.
</p>
<p>Simultaneous confidence intervals for linear functions can be computed
using method <code><a href="../../stats/html/confint.html">confint</a></code>. Univariate confidence intervals
can be computed by specifying <code>calpha = univariate_calpha()</code>
to <code>confint</code>. The critical value can directly be specified as a scalar 
to <code>calpha</code> as well. Note that <code>plot(a)</code> for some object <code>a</code> of class
<code>glht</code> is equivalent to <code>plot(confint(a))</code>.
</p>
<p>All simultaneous inference procedures implemented here control
the family-wise error rate (FWER). Multivariate
normal and t distributions, the latter one only for models of 
class <code><a href="../../stats/html/lm.html">lm</a></code>, are evaluated using the procedures
implemented in package <code>mvtnorm</code>. Note that the default
procedure is stochastic. Reproducible p-values and confidence
intervals require appropriate settings of seeds.
</p>
<p>A more detailed description of the underlying methodology is
available from Hothorn et al. (2008) and Bretz et al. (2010).
</p>


<h3>Value</h3>

<p><code>summary</code> computes (adjusted) p values for general linear hypotheses,
<code><a href="../../stats/html/confint.html">confint</a></code> computes (adjusted) confidence intervals. 
<code><a href="../../stats/html/coef.html">coef</a></code> returns estimates of the linear function <i>K &theta;</i>
and <code><a href="../../stats/html/vcov.html">vcov</a></code> its covariance. 
</p>


<h3>References</h3>

<p>Frank Bretz, Torsten Hothorn and Peter Westfall (2010),
<em>Multiple Comparisons Using R</em>, CRC Press, Boca Raton.
</p>
<p>Juliet P. Shaffer (1986), 
Modified sequentially rejective multiple test procedures. 
<em>Journal of the American Statistical Association</em>,
<b>81</b>, 826&ndash;831.
</p>
<p>Peter H. Westfall (1997), 
Multiple testing of general contrasts using logical constraints
and correlations. <em>Journal of the American Statistical Association</em>,
<b>92</b>, 299&ndash;306.
</p>
<p>P. H. Westfall, R. D. Tobias, D. Rom, R. D. Wolfinger, Y. Hochberg (1999).
<em>Multiple Comparisons and Multiple Tests Using the SAS System</em>.
Cary, NC: SAS Institute Inc.
</p>
<p>Torsten Hothorn, Frank Bretz and Peter Westfall (2008),
Simultaneous Inference in General Parametric Models.
<em>Biometrical Journal</em>, <b>50</b>(3), 346&ndash;363;
See <code>vignette("generalsiminf", package = "multcomp")</code>.
</p>


<h3>Examples</h3>

<pre>

  ### set up a two-way ANOVA 
  amod &lt;- aov(breaks ~ wool + tension, data = warpbreaks)

  ### set up all-pair comparisons for factor `tension'
  wht &lt;- glht(amod, linfct = mcp(tension = "Tukey"))

  ### 95% simultaneous confidence intervals
  plot(print(confint(wht)))

  ### the same (for balanced designs only)
  TukeyHSD(amod, "tension")

  ### corresponding adjusted p values
  summary(wht)

  ### all means for levels of `tension'
  amod &lt;- aov(breaks ~ tension, data = warpbreaks)
  glht(amod, linfct = matrix(c(1, 0, 0, 
                               1, 1, 0, 
                               1, 0, 1), byrow = TRUE, ncol = 3))

  ### confidence bands for a simple linear model, `cars' data
  plot(cars, xlab = "Speed (mph)", ylab = "Stopping distance (ft)",
       las = 1)

  ### fit linear model and add regression line to plot
  lmod &lt;- lm(dist ~ speed, data = cars)
  abline(lmod)

  ### a grid of speeds
  speeds &lt;- seq(from = min(cars$speed), to = max(cars$speed), 
                length = 10)

  ### linear hypotheses: 10 selected points on the regression line != 0
  K &lt;- cbind(1, speeds)                                                        

  ### set up linear hypotheses
  cht &lt;- glht(lmod, linfct = K)

  ### confidence intervals, i.e., confidence bands, and add them plot
  cci &lt;- confint(cht)
  lines(speeds, cci$confint[,"lwr"], col = "blue")
  lines(speeds, cci$confint[,"upr"], col = "blue")


  ### simultaneous p values for parameters in a Cox model
  if (require("survival") &amp;&amp; require("MASS")) {
      data("leuk", package = "MASS")
      leuk.cox &lt;- coxph(Surv(time) ~ ag + log(wbc), data = leuk)

      ### set up linear hypotheses
      lht &lt;- glht(leuk.cox, linfct = diag(length(coef(leuk.cox))))

      ### adjusted p values
      print(summary(lht))
  }

</pre>

<hr /><div style="text-align: center;">[Package <em>multcomp</em> version 1.4-25 <a href="00Index.html">Index</a>]</div>
</div></body></html>
