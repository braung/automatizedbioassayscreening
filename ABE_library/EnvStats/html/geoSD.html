<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Geometric Standard Deviation.</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for geoSD {EnvStats}"><tr><td>geoSD {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Geometric Standard Deviation.
</h2>

<h3>Description</h3>

<p>Compute the sample geometric standard deviation.
</p>


<h3>Usage</h3>

<pre>
  geoSD(x, na.rm = FALSE, sqrt.unbiased = TRUE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>na.rm</code></td>
<td>

<p>logical scalar indicating whether to remove missing values from <code>x</code>. <br />
If <code>na.rm=FALSE</code> (the default) and <code>x</code> contains missing values, 
then a missing value (<code>NA</code>) is returned.  If <code>na.rm=TRUE</code>, 
missing values are removed from <code>x</code> prior to computing the coefficient 
of variation.
</p>
</td></tr>
<tr valign="top"><td><code>sqrt.unbiased</code></td>
<td>

<p>logical scalar specifying what method to use to compute the sample standard 
deviation of the log-transformed observations.  If <code>sqrt.unbiased=TRUE</code> 
(the default), the square root of the unbiased estimator of variance is used, 
otherwise the method of moments estimator of standard deviation is used.  
See the DETAILS section for more information.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any non-positive values (values less than or equal to 0), 
<code>geoMean</code> returns <code>NA</code> and issues a warning.
</p>
<p>Let <i>\underline{x}</i> denote a vector of <i>n</i> observations from some 
distribution.  The sample geometric standard deviation is a measure of variability.  
It is defined as:
</p>
<p style="text-align: center;"><i>s_G = exp(s_y) \;\;\;\;\;\; (1)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>s_y = [\frac{1}{n-1} &sum;_{i=1}^n (y_i - \bar{y})^2]^{1/2} \;\;\;\;\;\; (2)</i></p>

<p style="text-align: center;"><i>y_i = log(x_i), \;\; i = 1, 2, &hellip;, n \;\;\;\;\;\; (3)</i></p>

<p>That is, the sample geometric standard deviation is the antilog of the sample 
standard deviation of the log-transformed observations.
</p>
<p>The sample standard deviation of the log-transformed observations shown in 
Equation (2) is the square root of the unbiased estimator of variance.  
(Note that this estimator of standard deviation is not an unbiased estimator.)  
Sometimes, the square root of the method of moments estimator of variance is used 
instead:
</p>
<p style="text-align: center;"><i>s_y = [\frac{1}{n} &sum;_{i=1}^n (y_i - \bar{y})^2]^{1/2} \;\;\;\;\;\; (4)</i></p>

<p>This is the estimator used in Equation (1) when <code>sqrt.unbiased=FALSE</code>.
</p>


<h3>Value</h3>

<p>A numeric scalar &ndash; the sample geometric standard deviation.
</p>


<h3>Note</h3>

<p>The geometric standard deviation is only defined for positive observations.  
It is usually computed only for observations that are assumed to have come 
from a <a href="../../EnvStats/help/LognormalAlt.html">lognormal distribution</a>. 
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). 
<em>Statistics for Environmental Engineers, Second Edition</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution 
Monitoring</em>. Van Nostrand Reinhold, NY.
</p>
<p>Leidel, N.A., K.A. Busch, and J.R. Lynch. (1977).  <em>Occupational 
Exposure Sampling Strategy Manual</em>.  U.S. Department of Health, Education, 
and Welfare, Public Health Service, Center for Disease Control, 
National Institute for Occupational Safety and Health, Cincinnati, 
Ohio 45226, January, 1977, pp.102&ndash;103.
</p>
<p>Ott, W.R. (1995). <em>Environmental Statistics and Data Analysis</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>Taylor, J.K. (1990). <em>Statistical Techniques for Data Analysis</em>.  
Lewis Publishers, Boca Raton, FL.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/geoMean.html">geoMean</a></code>, <a href="../../stats/html/Lognormal.html">Lognormal</a>, <code><a href="../../EnvStats/help/elnorm.html">elnorm</a></code>, 
<code><a href="../../EnvStats/help/summaryFull.html">summaryFull</a></code>, <code><a href="../../EnvStats/help/Summary+20Statistics.html">Summary Statistics</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 2000 observations from a lognormal distribution with parameters 
  # mean=10 and cv=1, which implies the standard deviation (on the original 
  # scale) is 10.  Compute the mean, geometric mean, standard deviation, 
  # and geometric standard deviation. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rlnormAlt(2000, mean = 10, cv = 1) 

  mean(dat) 
  #[1] 10.23417
 
  geoMean(dat) 
  #[1] 7.160154
 
  sd(dat) 
  #[1] 9.786493
 
  geoSD(dat) 
  #[1] 2.334358

  #----------
  # Clean up
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
