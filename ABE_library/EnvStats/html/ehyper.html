<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameter of a Hypergeometric Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for ehyper {EnvStats}"><tr><td>ehyper {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameter of a Hypergeometric Distribution
</h2>

<h3>Description</h3>

<p>Estimate <i>m</i>, the number of white balls in the urn, or 
<i>m+n</i>, the total number of balls in the urn, for a 
<a href="../../stats/help/Hypergeometric.html">hypergeometric distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  ehyper(x, m = NULL, total = NULL, k, method = "mle")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>non-negative integer indicating the number of white balls out of a sample of 
size <code>k</code> drawn without replacement from the urn.  Missing (<code>NA</code>), 
undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are not 
allowed.
</p>
</td></tr>
<tr valign="top"><td><code>m</code></td>
<td>

<p>non-negative integer indicating the number of white balls in the urn.  
You must supply <code>m</code> or <code>total</code>, but not both.  
Missing values (<code>NA</code>s) are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>total</code></td>
<td>

<p>positive integer indicating the total number of balls in the urn (i.e., 
<code>m+n</code>).  You must supply <code>m</code> or <code>total</code>, but not both.
Missing values (<code>NA</code>s) are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>positive integer indicating the number of balls drawn without replacement from the 
urn.  Missing values (<code>NA</code>s) are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Possible values are 
<code>"mle"</code> (maximum likelihood; the default) and <code>"mvue"</code> 
(minimum variance unbiased).  The mvue method is only available when you 
are estimating <i>m</i> (i.e., when you supply the argument <code>total</code>).
See the DETAILS section for more information on these estimation methods. 
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are not allowed.
</p>
<p>Let <i>x</i> be an observation from a 
<a href="../../stats/help/Hypergeometric.html">hypergeometric distribution</a> with 
parameters <code>m=</code><i>M</i>, <code>n=</code><i>N</i>, and <code>k=</code><i>K</i>.  
In <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> nomenclature, <i>x</i> represents the number of white balls drawn out of a 
sample of <i>K</i> balls drawn <em>without</em> replacement from an urn containing 
<i>M</i> white balls and <i>N</i> black balls.  The total number of balls in the 
urn is thus <i>M+N</i>.  Denote the total number of balls by <i>T = M+N</i>.
</p>
<p><b>Estimation</b> <br />
</p>
<p><em>Estimating M, Given T and K are known</em> <br />
When <i>T</i> and <i>K</i> are known, the maximum likelihood estimator (mle) of 
<i>M</i> is given by (Forbes et al., 2011):
</p>
<p style="text-align: center;"><i>\hat{M}_{mle} = floor[(T + 1) x / K] \;\;\;\; (1)</i></p>

<p>where <i>floor()</i> represents the <code><a href="../../base/help/Round.html">floor</a></code> function.  
That is, <i>floor(y)</i> is the largest integer less than or equal to <i>y</i>. 
</p>
<p>If the quantity <i>floor[(T + 1) x / K]</i> is an integer, then the mle of 
<i>M</i> is also given by (Johnson et al., 1992, p.263):
</p>
<p style="text-align: center;"><i>\hat{M}_{mle} = [(T + 1) x / K] - 1 \;\;\;\; (2)</i></p>

<p>which is what the function <code>ehyper</code> uses for this case.
</p>
<p>The minimum variance unbiased estimator (mvue) of <i>M</i> is given by 
(Forbes et al., 2011):
</p>
<p style="text-align: center;"><i>\hat{M}_{mvue} = (T x / K) \;\;\;\; (3)</i></p>

<p><br />
</p>
<p><em>Estimating T, given M and K are known</em> <br />
When <i>M</i> and <i>K</i> are known, the maximum likelihood estimator (mle) of 
<i>T</i> is given by (Forbes et al., 2011):
</p>
<p style="text-align: center;"><i>\hat{T}_{mle} = floor(K M / x) \;\;\;\; (4)</i></p>



<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information.  
See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Hypergeometric.html">hypergeometric distribution</a> can be described by 
an urn model with <i>M</i> white balls and <i>N</i> black balls.  If <i>K</i> balls 
are drawn <em>with</em> replacement, then the number of white balls in the sample 
of size <i>K</i> follows a <a href="../../stats/help/Binomial.html">binomial distribution</a> with 
parameters <code>size=</code><i>K</i> and <code>prob=</code><i>M/(M+N)</i>.  If <i>K</i> balls are 
drawn <em>without</em> replacement, then the number of white balls in the sample of 
size <i>K</i> follows a <a href="../../stats/help/Hypergeometric.html">hypergeometric distribution</a> 
with parameters <code>m=</code><i>M</i>, <code>n=</code><i>N</i>, and <code>k=</code><i>K</i>.
</p>
<p>The name &ldquo;hypergeometric&rdquo; comes from the fact that the probabilities 
associated with this distribution can be written as successive terms in the 
expansion of a function of a Gaussian hypergeometric series.
</p>
<p>The hypergeometric distribution is applied in a variety of fields, including 
quality control and estimation of animal population size.  It is also the 
distribution used to compute probabilities for 
<a href="../../stats/help/fisher.test.html">Fishers's exact test</a> for a 2x2 contingency table.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and A. Kemp. (1992).  
<em>Univariate Discrete Distributions</em>.  Second Edition. John Wiley and Sons, 
New York, Chapter 6.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Hypergeometric.html">Hypergeometric</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate an observation from a hypergeometric distribution with 
  # parameters m=10, n=30, and k=5, then estimate the parameter m. 
  # Note: the call to set.seed simply allows you to reproduce this example. 
  # Also, the only parameter actually estimated is m; once m is estimated, 
  # n is computed by subtracting the estimated value of m (8 in this example) 
  # from the given of value of m+n (40 in this example).  The parameters 
  # n and k are shown in the output in order to provide information on 
  # all of the parameters associated with the hypergeometric distribution.

  set.seed(250) 
  dat &lt;- rhyper(nn = 1, m = 10, n = 30, k = 5) 
  dat 
  #[1] 1   

  ehyper(dat, total = 40, k = 5) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Hypergeometric
  #
  #Estimated Parameter(s):          m =  8
  #                                 n = 32
  #                                 k =  5
  #
  #Estimation Method:               mle for 'm'
  #
  #Data:                            dat
  #
  #Sample Size:                     1

  #----------

  # Use the same data as in the previous example, but estimate m+n instead. 
  # Note: The only parameter estimated is m+n. Once this is estimated, 
  # n is computed by subtracting the given value of m (10 in this case) 
  # from the estimated value of m+n (50 in this example).

  ehyper(dat, m = 10, k = 5)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Hypergeometric
  #
  #Estimated Parameter(s):          m = 10
  #                                 n = 40
  #                                 k =  5
  #
  #Estimation Method:               mle for 'm+n'
  #
  #Data:                            dat
  #
  #Sample Size:                     1


  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
