<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Two-Sample or Paired-Sample Randomization (Permutation) Test...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for twoSamplePermutationTestLocation {EnvStats}"><tr><td>twoSamplePermutationTestLocation {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Two-Sample or Paired-Sample Randomization (Permutation) Test for Location
</h2>

<h3>Description</h3>

<p>Perform a two-sample or paired-sample randomization (permutation) test for 
location based on either means or medians.
</p>


<h3>Usage</h3>

<pre>
  twoSamplePermutationTestLocation(x, y, fcn = "mean", alternative = "two.sided", 
    mu1.minus.mu2 = 0, paired = FALSE, exact = FALSE, n.permutations = 5000, 
    seed = NULL, tol = sqrt(.Machine$double.eps))
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations from population 1.    
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>y</code></td>
<td>

<p>numeric vector of observations from population 2.    
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
<p>In the case when <code>paired=TRUE</code>, the argument <code>y</code> must have the same number 
of elements as the argument <code>x</code>.
</p>
</td></tr>
<tr valign="top"><td><code>fcn</code></td>
<td>

<p>character string indicating which location parameter to compare between the two 
groups.  The possible values are <code>fcn="mean"</code> (the default) and 
<code>fcn="median"</code>.  This argument is ignored when <code>paired=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values 
are <code>"two.sided"</code> (the default), <code>"less"</code>, and <code>"greater"</code>. 
</p>
</td></tr>
<tr valign="top"><td><code>mu1.minus.mu2</code></td>
<td>

<p>numeric scalar indicating the hypothesized value of the difference between the 
means or medians.  The default value is <code>mu1.minus.mu2=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>paired</code></td>
<td>

<p>logical scalar indicating whether to perform a paired or two-sample permutation 
test.  The possible values are <code>paired=FALSE</code> (the default; indicates a 
two-sample permutation test) and <code>paired=TRUE</code> (indicates take differences of 
pairs and perform a one-sample permutation test).
</p>
</td></tr>
<tr valign="top"><td><code>exact</code></td>
<td>

<p>logical scalar indicating whether to perform the exact permutation test (i.e., 
enumerate all possible permutations) or simply sample from the permutation 
distribution.  The default value is <code>exact=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.permutations</code></td>
<td>

<p>integer indicating how many times to sample from the permutation distribution when 
<code>exact=FALSE</code>.  The default value is <code>n.permutations=5000</code>.  
This argument is ignored when <code>exact=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>seed</code></td>
<td>

<p>positive integer to pass to the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../base/html/Random.html">set.seed</a></code>.  The 
default is <code>seed=NULL</code>, in which case the current value of 
<code><a href="../../base/html/Random.html">.Random.seed</a></code> is used.  
Using the <code>seed</code> argument lets you reproduce the exact same result if all 
other arguments stay the same.
</p>
</td></tr>
<tr valign="top"><td><code>tol</code></td>
<td>

<p>numeric scalar indicating the tolerance to use for computing the p-value for the 
two-sample permutation test.  The default value is <br />
<code>tol=sqrt(.Machine$double.eps)</code>.  See the DETAILS section below for more 
information.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p><b>Randomization Tests</b> <br />
In 1935, R.A. Fisher introduced the idea of a <b><em>randomization test</em></b> 
(Manly, 2007, p. 107; Efron and Tibshirani, 1993, Chapter 15), which is based on 
trying to answer the question:  &ldquo;Did the observed pattern happen by chance, 
or does the pattern indicate the null hypothesis is not true?&rdquo;  A randomization 
test works by simply enumerating all of the possible outcomes under the null 
hypothesis, then seeing where the observed outcome fits in.  A randomization test 
is also called a <b><em>permutation test</em></b>, because it involves permuting the 
observations during the enumeration procedure (Manly, 2007, p. 3).
</p>
<p>In the past, randomization tests have not been used as extensively as they are now 
because of the &ldquo;large&rdquo; computing resources needed to enumerate all of the 
possible outcomes, especially for large sample sizes.  The advent of more powerful 
personal computers and software has allowed randomization tests to become much 
easier to perform.  Depending on the sample size, however, it may still be too 
time consuming to enumerate all possible outcomes.  In this case, the randomization 
test can still be performed by sampling from the randomization distribution, and 
comparing the observed outcome to this sampled permutation distribution.
<br />
</p>
<p><em>Two-Sample Randomization Test for Location</em> (<code>paired=FALSE</code>) <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_{n1}</i> be a vector of <i>n1</i> 
independent and identically distributed (i.i.d.) observations
from some distribution with location parameter (e.g., mean or median) <i>&theta;_1</i>, 
and let <i>\underline{y} = y_1, y_2, &hellip;, y_{n2}</i> be a vector of <i>n2</i> 
i.i.d. observations from the same distribution with possibly different location 
parameter <i>&theta;_2</i>.
</p>
<p>Consider the test of the null hypothesis that the difference in the location 
parameters is equal to some specified value: 
</p>
<p style="text-align: center;"><i>H_0: &delta; = &delta;_0 \;\;\;\;\;\; (1)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>&delta; = &theta;_1 - &theta;_2 \;\;\;\;\;\; (2)</i></p>

<p>and <i>&delta;_0</i> denotes the hypothesized difference in the meansures of 
location (usually <i>&delta;_0 = 0</i>).
</p>
<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>)
</p>
<p style="text-align: center;"><i>H_a: &delta; &gt; &delta;_0 \;\;\;\;\;\; (3)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: &delta; &lt; &delta;_0 \;\;\;\;\;\; (4)</i></p>

<p>and the two-sided alternative
</p>
<p style="text-align: center;"><i>H_a: &delta; \ne &delta;_0 \;\;\;\;\;\; (5)</i></p>

<p>To perform the test of the null hypothesis (1) versus any of the three 
alternatives (3)-(5), you can use the two-sample permutation test.  The two 
sample permutation test is based on trying to answer the question, 
&ldquo;Did the observed difference in means or medians happen by chance, or 
does the observed difference indicate that the null hypothesis is not true?&rdquo;  
Under the null hypothesis, the underlying distributions for each group are the 
same, therefore it should make no difference which group an observation gets 
assigned to.  The two-sample permutation test works by simply enumerating all 
possible permutations of group assignments, and for each permutation computing 
the difference between the measures of location for each group 
(Manly, 2007, p. 113; Efron and Tibshirani, 1993, p. 202).  The measure of 
location for a group could be the mean, median, or any other measure you want to 
use.  For example, if the observations from Group 1 are 3 and 5, and the 
observations from Group 2 are 4, 6, and 7, then there are 10 different ways of 
splitting these five observations into one group of size 2 and another group of 
size 3.  The table below lists all of the possible group assignments, along with 
the differences in the group means.
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
  <b>Group 1</b> </td><td style="text-align: left;"> <b>Group 2</b> </td><td style="text-align: left;"> <b>Mean 1 - Mean 2</b> </td>
</tr>
<tr>
 <td style="text-align: left;">
  3, 4           </td><td style="text-align: left;"> 5, 6, 7        </td><td style="text-align: left;"> -2.5  </td>
</tr>
<tr>
 <td style="text-align: left;">
  3, 5           </td><td style="text-align: left;"> 4, 6, 7        </td><td style="text-align: left;"> -1.67 </td>
</tr>
<tr>
 <td style="text-align: left;">
  3, 6           </td><td style="text-align: left;"> 4, 5, 7        </td><td style="text-align: left;"> -0.83 </td>
</tr>
<tr>
 <td style="text-align: left;">
  3, 7           </td><td style="text-align: left;"> 4, 5, 6        </td><td style="text-align: left;">  0    </td>
</tr>
<tr>
 <td style="text-align: left;">
  4, 5           </td><td style="text-align: left;"> 3, 6, 7        </td><td style="text-align: left;"> -0.83 </td>
</tr>
<tr>
 <td style="text-align: left;">
  4, 6           </td><td style="text-align: left;"> 3, 5, 7        </td><td style="text-align: left;">  0    </td>
</tr>
<tr>
 <td style="text-align: left;">
  4, 7           </td><td style="text-align: left;"> 3, 5, 6        </td><td style="text-align: left;">  0.83 </td>
</tr>
<tr>
 <td style="text-align: left;">
  5, 6           </td><td style="text-align: left;"> 3, 4, 7        </td><td style="text-align: left;">  0.83 </td>
</tr>
<tr>
 <td style="text-align: left;">
  5, 7           </td><td style="text-align: left;"> 3, 4, 6        </td><td style="text-align: left;">  1.67 </td>
</tr>
<tr>
 <td style="text-align: left;">
  6, 7           </td><td style="text-align: left;"> 3, 4, 5        </td><td style="text-align: left;">  2.5
  </td>
</tr>

</table>
 
<p>In this example, the <em>observed</em> group assignments and difference in means 
are shown in the second row of the table.
</p>
<p>For a one-sided upper alternative (Equation (3)), the p-value is computed as the 
proportion of times that the differences of the means (or medians) in the 
permutation distribution are greater than or equal to the observed difference in 
means (or medians).  For a one-sided lower alternative hypothesis (Equation (4)), 
the p-value is computed as the proportion of times that the differences in the means 
(or medians) in the permutation distribution are less than or equal to the observed 
difference in the means (or medians).  For a two-sided alternative hypothesis 
(Equation (5)), the p-value is computed as the proportion of times the absolute 
values of the differences in the means (or medians) in the permutation distribution 
are greater than or equal to the absolute value of the observed difference in the 
means (or medians).
</p>
<p>For this simple example, the one-sided upper, one-sided lower, and two-sided 
p-values are 0.9, 0.2 and 0.4, respectively.
</p>
<p><b>Note:</b> Because of the nature of machine arithmetic and how the permutation 
distribution is computed, a one-sided upper p-value is computed as the proportion 
of times that the differences of the means (or medians) in the permutation 
distribution are greater than or equal to 
[the observed difference in means (or medians) - a small tolerance value], where the 
tolerance value is determined by the argument <code>tol</code>.  Similarly, a one-sided 
lower p-value is computed as the proportion of times that the differences in the 
means (or medians) in the permutation distribution are less than or equal to 
[the observed difference in the means (or medians) + a small tolerance value].  
Finally, a two-sided p-value is computed as the proportion of times the absolute 
values of the differences in the means (or medians) in the permutation distribution 
are greater than or equal to 
[the absolute value of the observed difference in the means (or medians) - a small tolerance value].
</p>
<p>In this simple example, we assumed the hypothesized differences in the means under 
the null hypothesis was <i>&delta;_0 = 0</i>.  If we had hypothesized a different 
value for <i>&delta;_0</i>, then we would have had to subtract this value from each of 
the observations in Group 1 before permuting the group assignments to compute the 
permutation distribution of the differences of the means.  As in the case of the 
<a href="../../EnvStats/help/oneSamplePermutationTest.html">one-sample permutation test</a>, if the sample sizes 
for the groups become too large to compute all possible permutations of the group 
assignments, the permutation test can still be performed by sampling from the 
permutation distribution and comparing the observed difference in locations to the 
sampled permutation distribution of the difference in locations.
</p>
<p>Unlike the two-sample <a href="../../stats/help/t.test.html">Student's t-test</a>, we do not have to worry 
about the normality assumption when we use a permutation test.  The permutation test 
still assumes, however, that under the null hypothesis, the distributions of the 
observations from each group are exactly the same, and under the alternative 
hypothesis there is simply a shift in location (that is, the whole distribution of 
group 1 is shifted by some constant relative to the distribution of group 2).  
Mathematically, this can be written as follows:
</p>
<p style="text-align: center;"><i>F_1(t) = F_2(t - &delta;), \;\; -&infin; &lt; t &lt; &infin; \;\;\;\;\; (6)</i></p>

<p>where <i>F_1</i> and <i>F_2</i> denote the cumulative distribution functions for 
group 1 and group 2, respectively.  If <i>&delta; &gt; 0</i>, this implies that the 
observations in group 1 tend to be larger than the observations in group 2, and 
if <i>&delta; &lt; 0</i>, this implies that the observations in group 1 tend to be 
smaller than the observations in group 2.  Thus, the shape and spread (variance) 
of the two distributions should be the same whether the null hypothesis is true or 
not.  Therefore, the Type I error rate for a permutation test can be affected by 
differences in variances between the two groups.
<br />
</p>
<p><em>Confidence Intervals for the Difference in Means or Medians</em> <br />
Based on the relationship between hypothesis tests and confidence intervals, it is 
possible to construct a two-sided or one-sided <i>(1-&alpha;)100\%</i> confidence 
interval for the difference in means or medians based on the two-sample permutation 
test by finding the values of <i>&delta;_0</i> that correspond to obtaining a 
p-value of <i>&alpha;</i> (Manly, 2007, pp. 18&ndash;20, 114).  A confidence interval 
based on the bootstrap however, will yield a similar type of confidence interval 
(Efron and Tibshirani, 1993, p. 214); see the help file for 
<code><a href="../../boot/help/boot.html">boot</a></code> in the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> package <span class="pkg">boot</span>.
<br />
</p>
<p><b>Paired-Sample Randomization Test for Location</b> (<code>paired=TRUE</code>) <br />
When the argument <code>paired=TRUE</code>, the arguments <code>x</code> and <code>y</code> are 
assumed to have the same length, and the <i>n1 = n2 = n</i> differences
<i>y_i = x_i - y_i</i>, <i>i = 1, 2, &hellip;, n</i> are assumed to be independent 
observations from some symmetric distribution with mean <i>&mu;</i>.  The 
<a href="../../EnvStats/help/oneSamplePermutationTest.html">one-sample permutation test</a> can then be applied 
to the differences.
</p>


<h3>Value</h3>

<p>A list of class <code>"permutationTest"</code> containing the results of the hypothesis 
test.  See the help file for <code><a href="../../EnvStats/help/permutationTest.object.html">permutationTest.object</a></code> for details.
</p>


<h3>Note</h3>

<p>A frequent question in environmental statistics is &ldquo;Is the concentration of 
chemical X in Area A greater than the concentration of chemical X in Area B?&rdquo;.  
For example, in groundwater detection monitoring at hazardous and solid waste sites, 
the concentration of a chemical in the groundwater at a downgradient well must be 
compared to &ldquo;background&rdquo;.  If the concentration is &ldquo;above&rdquo; the 
background then the site enters assessment monitoring.  As another example, soil 
cleanup at a Superfund site may involve comparing the concentration of a chemical 
in the soil at a &ldquo;cleaned up&rdquo; site with the concentration at a 
&ldquo;background&rdquo; site.  If the concentration at the &ldquo;cleaned up&rdquo; site is 
&ldquo;greater&rdquo; than the background concentration, then further investigation and 
remedial action may be required.  Determining what it means for the chemical 
concentration to be &ldquo;greater&rdquo; than background is a policy decision: you may 
want to compare averages, medians, 95'th percentiles, etc.
</p>
<p>Hypothesis tests you can use to compare &ldquo;location&rdquo; between two groups include:  
<a href="../../stats/help/t.test.html">Student's t-test</a>, Fisher's randomization test 
(described in this help file), the <a href="../../stats/help/wilcox.test.html">Wilcoxon rank sum test</a>, 
other <a href="../../EnvStats/help/twoSampleLinearRankTest.html">two-sample linear rank tests</a>, 
the <a href="../../EnvStats/help/quantileTest.html">quantile test</a>, and a test based on a bootstrap confidence 
interval.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Efron, B., and R.J. Tibshirani. (1993).  <em>An Introduction to the Bootstrap</em>.  
Chapman and Hall, New York, Chapter 15.
</p>
<p>Manly, B.F.J. (2007).  <em>Randomization, Bootstrap and Monte Carlo Methods in 
Biology</em>.  Third Edition. Chapman &amp; Hall, New York, Chapter 6.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001).  
<em>Environmental Statistics with S-PLUS</em>.  CRC Press, Boca Raton, FL, 
pp.426&ndash;431.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/permutationTest.object.html">permutationTest.object</a></code>, <code><a href="../../EnvStats/help/plot.permutationTest.html">plot.permutationTest</a></code>, 
<code><a href="../../EnvStats/help/oneSamplePermutationTest.html">oneSamplePermutationTest</a></code>, <br />
<code><a href="../../EnvStats/help/twoSamplePermutationTestProportion.html">twoSamplePermutationTestProportion</a></code>, 
<a href="../../EnvStats/help/Hypothesis+20Tests.html">Hypothesis Tests</a>, <code><a href="../../boot/help/boot.html">boot</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 10 observations from a lognormal distribution with parameters 
  # mean=5 and cv=2, and and 20 observations from a lognormal distribution with 
  # parameters mean=10 and cv=2.  Test the null hypothesis that the means of the 
  # two distributions are the same against the alternative that the mean for 
  # group 1 is less than the mean for group 2. 
  # (Note: the call to set.seed allows you to reproduce the same data 
  # (dat1 and dat2), and setting the argument seed=732 in the call to 
  # twoSamplePermutationTestLocation() lets you reproduce this example by 
  # getting the same sample from the permutation distribution).

  set.seed(256) 
  dat1 &lt;- rlnormAlt(10, mean = 5, cv = 2) 
  dat2 &lt;- rlnormAlt(20, mean = 10, cv = 2) 

  test.list &lt;- twoSamplePermutationTestLocation(dat1, dat2, 
    alternative = "less", seed = 732) 

  # Print the results of the test 
  #------------------------------
  test.list 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 mu.x-mu.y = 0
  #
  #Alternative Hypothesis:          True mu.x-mu.y is less than 0
  #
  #Test Name:                       Two-Sample Permutation Test
  #                                 Based on Differences in Means
  #                                 (Based on Sampling
  #                                 Permutation Distribution
  #                                 5000 Times)
  #
  #Estimated Parameter(s):          mean of x =  2.253439
  #                                 mean of y = 11.825430
  #
  #Data:                            x = dat1
  #                                 y = dat2
  #
  #Sample Sizes:                    nx = 10
  #                                 ny = 20
  #
  #Test Statistic:                  mean.x - mean.y = -9.571991
  #
  #P-value:                         0.001


  # Plot the results of the test 
  #-----------------------------
  dev.new()
  plot(test.list)

  #==========

  # The guidance document "Statistical Methods for Evaluating the Attainment of 
  # Cleanup Standards, Volume 3: Reference-Based Standards for Soils and Solid 
  # Media" (USEPA, 1994b, pp. 6.22-6.25) contains observations of 
  # 1,2,3,4-Tetrachlorobenzene (TcCB) in ppb at a Reference Area and a Cleanup Area.  
  # These data are stored in the data frame EPA.94b.tccb.df.  Use the 
  # two-sample permutation test to test for a difference in means between the 
  # two areas vs. the alternative that the mean in the Cleanup Area is greater.  
  # Do the same thing for the medians.
  #
  # The permutation test based on comparing means shows a significant differnce, 
  # while the one based on comparing medians does not.


  # First test for a difference in the means.
  #------------------------------------------

  mean.list &lt;- with(EPA.94b.tccb.df, 
    twoSamplePermutationTestLocation(
      TcCB[Area=="Cleanup"], TcCB[Area=="Reference"], 
      alternative = "greater", seed = 47)) 

  mean.list

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 mu.x-mu.y = 0
  #
  #Alternative Hypothesis:          True mu.x-mu.y is greater than 0
  #
  #Test Name:                       Two-Sample Permutation Test
  #                                 Based on Differences in Means
  #                                 (Based on Sampling
  #                                 Permutation Distribution
  #                                 5000 Times)
  #
  #Estimated Parameter(s):          mean of x = 3.9151948
  #                                 mean of y = 0.5985106
  #
  #Data:                            x = TcCB[Area == "Cleanup"]  
  #                                 y = TcCB[Area == "Reference"]
  #
  #Sample Sizes:                    nx = 77
  #                                 ny = 47
  #
  #Test Statistic:                  mean.x - mean.y = 3.316684
  #
  #P-value:                         0.0206

  dev.new()
  plot(mean.list)


  #----------

  # Now test for a difference in the medians.
  #------------------------------------------

  median.list &lt;- with(EPA.94b.tccb.df, 
    twoSamplePermutationTestLocation(
      TcCB[Area=="Cleanup"], TcCB[Area=="Reference"], 
      fcn = "median", alternative = "greater", seed = 47)) 

  median.list

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 mu.x-mu.y = 0
  #
  #Alternative Hypothesis:          True mu.x-mu.y is greater than 0
  #
  #Test Name:                       Two-Sample Permutation Test
  #                                 Based on Differences in Medians
  #                                 (Based on Sampling
  #                                 Permutation Distribution
  #                                 5000 Times)
  #
  #Estimated Parameter(s):          median of x = 0.43
  #                                 median of y = 0.54
  #
  #Data:                            x = TcCB[Area == "Cleanup"]  
  #                                 y = TcCB[Area == "Reference"]
  #
  #Sample Sizes:                    nx = 77
  #                                 ny = 47
  #
  #Test Statistic:                  median.x - median.y = -0.11
  #
  #P-value:                         0.936

  dev.new()
  plot(median.list)

  #==========

  # Clean up
  #---------
  rm(test.list, mean.list, median.list)
  graphics.off()
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
