<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Test Whether the Shape Parameter of a Generalized Extreme...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for zTestGevdShape {EnvStats}"><tr><td>zTestGevdShape {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Test Whether the Shape Parameter of a Generalized Extreme Value Distribution is Equal to 0
</h2>

<h3>Description</h3>

<p>Estimate the shape parameter of a <a href="../../EnvStats/help/GEVD.html">generalized extreme value distribution</a> 
and test the null hypothesis that the true value is equal to 0.
</p>


<h3>Usage</h3>

<pre>
  zTestGevdShape(x, pwme.method = "unbiased", 
    plot.pos.cons = c(a = 0.35, b = 0), alternative = "two.sided")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>pwme.method</code></td>
<td>

<p>character string specifying the method of estimating the probability-weighted 
moments.  Possible values are <code>"unbiased"</code> (method based on the U-statistic; 
the default), and <code>"plotting.position"</code> (plotting position).  See the help file 
for <code><a href="../../EnvStats/help/egevd.html">egevd</a></code> for more information on these estimation methods. 
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.cons</code></td>
<td>

<p>numeric vector of length 2 specifying the constants used in the formula for the 
plotting positions.  The default value is <code>plot.pos.cons=c(a=0.35, b=0)</code>.  
If this vector has a names attribute with the value <code>c("a","b")</code> or 
<code>c("b","a")</code>, then the elements will be matched by name in the formula for 
computing the plotting positions.  Otherwise, the first element is mapped to the 
name <code>"a"</code> and the second element to the name <code>"b"</code>.  See the help file 
for <code><a href="../../EnvStats/help/egevd.html">egevd</a></code> for more information.  This argument is ignored if 
<code>pwme.method</code> is not equal to <br />
<code>"plotting.position"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values 
are <code>"two.sided"</code> (shape not equal to 0; the default), <code>"less"</code> 
(shape less than 0), and <code>"greater"</code> (shape greater than 0). 
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> be a vector of <i>n</i> observations 
from a <a href="../../EnvStats/help/GEVD.html">generalized extreme value distribution</a> with parameters 
<code>location=</code><i>&eta;</i>, <code>scale=</code><i>&theta;</i>, and <code>shape=</code><i>&kappa;</i>.  
Furthermore, let <i>\hat{&kappa;}_{pwme}</i> denote the probability-weighted moments 
estimator (PWME) of the shape parameter <i>&kappa;</i> (see the help file for 
<code><a href="../../EnvStats/help/egevd.html">egevd</a></code>).  Then the statistic
</p>
<p style="text-align: center;"><i>z = \frac{\hat{&kappa;}_{pwme}}{&radic;{0.5633/n}} \;\;\;\;\;\; (1)</i></p>

<p>is asymptotically distributed as a N(0,1) random variable under the null hypothesis 
<i>H_0: &kappa; = 0</i> (Hosking et al., 1985).  The function <code>zTestGevdShape</code> 
performs the usual one-sample z-test using the statistic computed in Equation (1).  
The PWME of <i>&kappa;</i> may be computed using either U-statistic type 
probability-weighted moments estimators or plotting-position type estimators 
(see <code><a href="../../EnvStats/help/egevd.html">egevd</a></code>).  Although Hosking et al. (1985) base their statistic on 
plotting-position type estimators, Hosking and Wallis (1995) recommend using the 
U-statistic type estimators for almost all applications.
</p>
<p>This test is only asymptotically correct.  Hosking et al. (1985), however, found 
that the <i>&alpha;</i>-level is adequately maintained for samples as small as 
<i>n = 25</i>.
</p>


<h3>Value</h3>

<p>A list of class <code>"htest"</code> containing the results of the hypothesis test.  
See the help file for <code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code> for details.
</p>


<h3>Note</h3>

<p><a href="../../EnvStats/help/EVD.html">Two-parameter extreme value distributions (EVD)</a> have been applied 
extensively since the 1930's to several fields of study, including the 
distributions of hydrological and meteorological variables, human lifetimes, and 
strength of materials.  The 
<a href="../../EnvStats/help/GEVD.html">three-parameter generalized extreme value distribution (GEVD)</a> was 
introduced by Jenkinson (1955) to model annual maximum and minimum values of 
meteorological events.  Since then, it has been used extensively in the hydological 
and meteorological fields.
</p>
<p>The three families of EVDs are all special kinds of GEVDs.  When the shape parameter 
<i>&kappa; = 0</i>, the GEVD reduces to the Type I extreme value (Gumbel) distribution.  
When <i>&kappa; &gt; 0</i>, the GEVD is the same as the Type II extreme value 
distribution, and when <i>&kappa; &lt; 0</i> it is the same as the Type III extreme value 
distribution.
</p>
<p>Hosking et al. (1985) introduced the test used by the function <code>zTestGevdShape</code> 
to test the null hypothesis <i>H_0: &kappa; = 0</i>.  They found this test has power 
comparable to the modified likelihood-ratio test, which was found by Hosking (1984) 
to be the best overall test the thirteen tests he considered.
</p>
<p>Fill and Stedinger (1995) denote this test the &ldquo;kappa test&rdquo; and compare it 
with the <em>L-C</em>s test suggested by Chowdhury et al. (1991) and the probability 
plot correlation coefficient goodness-of-fit test for the Gumbel distribution given 
by Vogel (1986) (see the sub-section for <code>test="ppcc"</code> under the Details section 
of the help file for <code><a href="../../EnvStats/help/gofTest.html">gofTest</a></code>).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Chowdhury, J.U., J.R. Stedinger, and L. H. Lu. (1991).  Goodness-of-Fit Tests for 
Regional Generalized Extreme Value Flood Distributions.  
<em>Water Resources Research</em> <b>27</b>(7), 1765&ndash;1776.
</p>
<p>Fill, H.D., and J.R. Stedinger. (1995).  L Moment and Probability Plot Correlation 
Coefficient Goodness-of-Fit Tests for the Gumbel Distribution and Impact of 
Autocorrelation.  <em>Water Resources Research</em> <b>31</b>(1), 225&ndash;229.
</p>
<p>Hosking, J.R.M. (1984).  Testing Whether the Shape Parameter is Zero in the 
Generalized Extreme-Value Distribution.  <em>Biometrika</em> <b>71</b>(2), 367&ndash;374.
</p>
<p>Hosking, J.R.M., and J.R. Wallis (1995).  A Comparison of Unbiased and 
Plotting-Position Estimators of L Moments.  
<em>Water Resources Research</em> <b>31</b>(8), 2019&ndash;2025.
</p>
<p>Hosking, J.R.M., J.R. Wallis, and E.F. Wood. (1985).  Estimation of the 
Generalized Extreme-Value Distribution by the Method of Probability-Weighted 
Moments.  <em>Technometrics</em> <b>27</b>(3), 251&ndash;261.
</p>
<p>Jenkinson, A.F. (1955).  The Frequency Distribution of the Annual Maximum 
(or Minimum) of Meteorological Events.  <em>Quarterly Journal of the Royal 
Meteorological Society</em> <b>81</b>, 158&ndash;171.
</p>
<p>Vogel, R.M. (1986).  The Probability Plot Correlation Coefficient Test for the 
Normal, Lognormal, and Gumbel Distributional Hypotheses.  
<em>Water Resources Research</em> <b>22</b>(4), 587&ndash;590.  
(Correction, <em>Water Resources Research</em> <b>23</b>(10), 2013, 1987.)
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/GEVD.html">GEVD</a>, <code><a href="../../EnvStats/help/egevd.html">egevd</a></code>, <a href="../../EnvStats/help/EVD.html">EVD</a>, <code><a href="../../EnvStats/help/eevd.html">eevd</a></code>, 
<a href="../../EnvStats/help/Goodness-of-Fit+20Tests.html">Goodness-of-Fit Tests</a>, <code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 25 observations from a generalized extreme value distribution with 
  # parameters location=2, scale=1, and shape=1, and test the null hypothesis 
  # that the shape parameter is equal to 0. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 

  dat &lt;- rgevd(25, location = 2, scale = 1, shape = 1) 

  zTestGevdShape(dat) 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 shape = 0
  #
  #Alternative Hypothesis:          True shape is not equal to 0
  #
  #Test Name:                       Z-test of shape=0 for GEVD
  #
  #Estimated Parameter(s):          shape = 0.6623014
  #
  #Estimation Method:               Unbiased pwme
  #
  #Data:                            dat
  #
  #Sample Size:                     25
  #
  #Test Statistic:                  z = 4.412206
  #
  #P-value:                         1.023225e-05

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
